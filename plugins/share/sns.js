//2016.12.07
//SNS 관련 API 모음
var SNS = (function(SNS, $){
	
	var option = {
		facebook : {
			use : false
		},
		kakaotalk: {
			use : false
		},
		insta: {
			use : false
		}
	};
	
	var facebook = function(){
		
		init();
		
		window.fbAsyncInit = function() {
			FB.init({
				appId      : SNS.options.facebook.appId,
				version    : SNS.options.facebook.version,
				xfbml      : true,
				status     : true,
				cookie     : true
			});
			
			FB.Event.subscribe('auth.logout', logoutResult); 
			FB.getLoginStatus();
		};
		
		function init(){
			
			(function(d, s, id){
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) {return;}
				js = d.createElement(s); js.id = id;
				js.src = "//connect.facebook.net/en_KR/sdk.js";
				fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
			
			$('.btn-facebook').click(loginAction);
			
		}
		
		function loginAction(){
			var loginResultAction = $(this).data('login');
			var callback = $(this).data('callback');
			
			FB.login(function(logresponse){
			   var fbname;  
			   var accessToken = logresponse.authResponse.accessToken; 
			   if (logresponse.status === 'connected') {
				   FB.api('/me', function(response) {
					   
						if(loginResultAction){
							$.post( loginResultAction, { name: response.name, id: response.id })
							.done(function( data ) {
								if(callback !== undefined && typeof eval(callback) == 'function' ){
									eval(callback)(data);
								}
							});
						}
				     });
			   }
				  
			}, {scope: 'public_profile,email'});
		}
		
	}
	
	var kakaotalk = function(){
		
	}
	
	
	
	SNS.init = function(options){
		SNS.options = $.extend({}, option, options);
		
		if( SNS.options.facebook.use === true){
			facebook();
		}
	}
	
	return SNS;
	
})(window.SNS = window.SNS || {}, jQuery);