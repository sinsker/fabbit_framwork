<?php

use core\module\View;
use core\module\Manager;

class layouts{
	
	public function header(){
		if($this->isAjax()) return;
		View::load($this->layoutsFactory('header'));
	}
	
	public function footer(){
		if($this->isAjax()) return;
		View::load($this->layoutsFactory('footer'));
	}
	
	//Ajax 처리시 레이아웃 제외
	private function isAjax(){
		if( count(Manager::call()->get('param')->post) > 0 ) return true;
		return false;
	}
	
	private function layoutsFactory($pos){
		$module = Manager::call()->get('uri')->resource(0);
		
		switch($module){
			case 'auth':
			case 'admin':
				$module = 'admin';
				break;
			case 'footer':
				break;
			case 'lnb':
				break;
		}
		
		$layout = 'default';
		
		return 'layouts/'.$module.'/'.$pos;
	}
}