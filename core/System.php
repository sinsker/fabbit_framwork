<?php
namespace core;

use core\module\Singleton;
use core\module\shape\BuilderInterface;
use core\module\CacheManager;
use core\module\Manager;

class System extends Singleton {
	
	private $option = array();
	
	function __construct(){
		global $app;
		
		if (isset($SESSION_CACHE_LIMITER)) @session_cache_limiter($SESSION_CACHE_LIMITER);
		else @session_cache_limiter("no-cache, must-revalidate");
		ini_set("session.cache_expire", 180);
		session_start();
		
		$app['debug'] == true ? error_reporting(E_ALL ^ E_NOTICE) : error_reporting(0);
		header('P3P: CP="ALL CURa ADMa DEVa TAIa OUR BUS IND PHY ONL UNI PUR FIN COM NAV INT DEM CNT STA POL HEA PRE LOC OTC"');
		header('Content-Type: text/html; charset="'.$app['charset'].'"');
		
		if (function_exists("date_default_timezone_set"))
			date_default_timezone_set($app['timezone']);
		
		set_magic_quotes_gpc();
			
		if ($_GET['ROOT'] || $_POST['ROOT'] || $_COOKIE['ROOT']) {
			unset($_GET['ROOT']);
			unset($_POST['ROOT']);
			unset($_COOKIE['ROOT']);
		}
		//==========================================================================================================================
		// extract($_GET); 명령으로 인해 page.php?_POST[var1]=data1&_POST[var2]=data2 와 같은 코드가 _POST 변수로 사용되는 것을 막음
		//--------------------------------------------------------------------------------------------------------------------------
		$ext_arr = array ('PHP_SELF', '_ENV', '_GET', '_POST', '_FILES', '_SERVER', '_COOKIE', '_SESSION', '_REQUEST',
				'HTTP_ENV_VARS', 'HTTP_GET_VARS', 'HTTP_POST_VARS', 'HTTP_POST_FILES', 'HTTP_SERVER_VARS',
				'HTTP_COOKIE_VARS', 'HTTP_SESSION_VARS', 'GLOBALS');
		$ext_cnt = count($ext_arr);
		for ($i=0; $i<$ext_cnt; $i++) {
			// GET, POST 로 선언된 전역변수가 있다면 unset() 시킴
			if (isset($_GET[$ext_arr[$i]])) unset($_GET[$ext_arr[$i]]);
			if (isset($_POST[$ext_arr[$i]])) unset($_POST[$ext_arr[$i]]);
		}
		//==========================================================================================================================
			
		# 캐쉬 불러오기 #
		if(CacheManager::call()->is_CacheCreate() != true){
			CacheManager::call()->load();
		}
	}
	
	function __destruct(){
		global $app;
		# 캐쉬 저장하기 #
		CacheManager::call()->config($app['cache_use'], $app['cache_interval']);
		if(CacheManager::call()->is_CacheCreate() != true && CacheManager::call()->is_loaded != true ){
			CacheManager::call()->save();
		}
	}
	
	public function build(BuilderInterface $builder){
		$builder->init();
		$builder->service();
	}
	
	public function module($option){
		Manager::call()->init($option);
	}
	
}

