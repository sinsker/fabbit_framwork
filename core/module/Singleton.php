<?
namespace core\module;

class Singleton{
	
	public static function getInstance(){
		static $instance = null;
		if( null === $instance ){
			$instance = new static();
		}
		return $instance;
	}
	
	public static function call(){
		return static::getInstance();
	}
	
	
	protected function __construct(){}
	private function __wakeup(){}
	private function __clone(){}
}

