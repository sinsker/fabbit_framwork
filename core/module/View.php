<?php 
namespace core\module;

use core\module\output\JsonOutput;
use core\module\output\SerializationOutput;

class View extends Singleton {
	
	private static $data = array();
	private static $returnType;
	
	
	public static function data($data){
		array_push($this->data, $data);
	}
	
	public static function load($page, $data = ''){
		if(!empty($data)){ 
			if(is_subclass_of($data,'core\module\Dto')){
				View::$data = $data->objectToArray();
			}else{
				View::$data = $data;
			}
			extract(View::$data);
		}
		
		if(is_file(BASE.'public/view/'.$page.SUFFIX)){
			require BASE.'public/view/'.$page.SUFFIX;
		}
	}
	
	public static function redirect($href){
		
		$url = 'http://'.$_SERVER['HTTP_HOST'];
		echo '<script>location.href = "'.$url.$href.'"; </script>';
		exit;
	}
	
	public static function returnType($type, $data = ''){
		
		if(empty($data)) return false;
		
		switch ($type){
			case 'json':
				$obj = new JsonOutput();
				break;
			case 'serialization':
				$obj = new SerializationOutput();
				break;
		}
		
		echo $obj::set($data);
		exit;
	}
	
	public static function jsonData($data = ''){
		if(empty($data)) $data = &View::$data;
		self::returnType('json',$data);
	}
	
	public static function serializationData($data = ''){
		if(empty($data)) $data = &View::$data;
		self::returnType('serialization',$data);
	}
	
}