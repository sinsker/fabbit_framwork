<?php 
namespace core\module;

class Manager extends Singleton {
	
	private $modules = array();
	
	public function init($modules = array()){
		$this->addItem($modules);
	}
	
	public function &get($moduleName){
		$moduleName = strtolower($moduleName);
		if(isset($this->modules[$moduleName])){
			return $this->modules[$moduleName];
		}else{
			echo $name.' 모듈이 없습니다. [Manager]<br>';
		}
	}
	
	public function addItem($modules = array()){
		
		if(empty($modules)) return false;
		
		if(isset($modules) && is_array($modules) ){
			foreach ($modules as $module){
				$this->setModule($module);
			}
			return true;
		}else{
			$this->setModule($module);
		}
	}
	
	private function setModule($module){
		if(!is_subclass_of($module, 'core\module\shape\Module')) return false;
		$moduleName = get_class($module);
		if(strpos($moduleName,'\\') !== false) {
			$moduleName = explode('\\',$moduleName);
			$moduleName = $moduleName[count($moduleName)-1];
		}
		
		if(empty($this->modules[$moduleName])){
			$moduleName = strtolower($moduleName);
			$module->M = &$this;
			$this->modules[$moduleName] = $module;
		}
		
	}
	
	public function removeItem($name){
		if(!empty($this->module[$name])){
			unset($this->module[$name]);
		}
	}
}