<?php
namespace core\module\databasecomposition;

use core\module\Singleton;
use core\module\shape\DatabaseInterface;

Class Database extends Singleton {
	
	private $database;
	private $dbconfig;
	
	
	public function __destruct(){
		if(!empty($this->database)){ 
			$this->disconnect();
		}
	}
	 
	//DB 객체 주입
	public function databaseDependency(DatabaseInterface $database){
		$this->database = $database;
	}

	public function setConfig($config){
		
		$this->dbconfig = $config;
		return $this->dbconfig;
	}
	
	//DB 연결 해제
	public function disconnect(){
		$this->database->disconnect();
	}
	
	//DB 쿼리 동작
	public function query($sql, $file = __FILE__,$line = __LINE__ ){
		$this->result = $this->database->query($sql,$file,$line);
		return $this->result;
	}
	
	//DB SELECT
	public function fetch($result){
		return $this->database->fetch($result);
	}
	
	public function getInsertId(){
		return $this->database->insert_id();
	}
	
	
}