<?php 
namespace core\module;

use core\module\Base;
use core\module\databasecomposition\Database;

class Model extends Base {
	
	protected $database;

	public function __construct(){
		$this->database = Database::getInstance();
		$this->manager  = Manager::getInstance();
		
	}

	public function databaseComposition($databaseInterface){
		$dbconfig = $this->manager->get('config')->get('dbconfig','default');
		
		$dbComposition = NAMESPACE_DB.$databaseInterface;
		$this->database->databaseDependency(new $dbComposition($this->database->setConfig($dbconfig)));
	}
	

}