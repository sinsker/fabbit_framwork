<?php 
namespace core\module;

use core\module\shape\Module;

class Hooker implements Module {
	
	private $isUse = true;
	private $hookArray = array();
	
	public function __construct(){}
	
	private function init(){
		$hookArray = $this->M->get('config')->get('hook');
		
		if(empty($hookArray) && !is_array($hookArray)){
			$this->isUse = false;
			return false;
		}
		$this->hookArray = $hookArray;
	}
	
	
	public function hook($point){
		
		$this->init();
		
		if($this->isUse === false) return false;
		
		$this->loadHook($this->hookArray[$point]);
	}
	
	private function loadHook($hooks){
	
		if(isset($hooks[0]) && is_array($hooks[0]) ){
			
			foreach($hooks as $hook){
				$this->playHook($hook);
			}
		}else{
			$hook = $hooks; 
			
			$this->playHook($hook);
		}
	}
	
	private function playHook($hook){
		
		if( empty($hook['path']) || !file_exists(BASE.$hook['path'])){
			return false;
		}
		
		$path		= BASE.$hook['path'];
		$class 	 	= $hook['class'];
		$function	= $hook['function'];
		$params		= $hook['params'];
		
		if(empty($class) && empty($function)) return false;
		
		if(!empty($class)){
			
			if(!class_exists($class)) include $path;
			
			$hookClass = new $class();
			
			if(!method_exists($hookClass,$function)) return false;
			
			$hookClass->$function($params);
			
		}else{
			
			if(!function_exists($function)) include $path;
			
			$function($params);
		}
		
	}
}