<?php 
namespace core\module;

use core\module\shape\Module;

class Uri implements Module {
	
	private $part = array();
	private $uri;
	
	function parseRequestUri(){
		
		if(isset($_SERVER['PATH_INFO'])){
			
			$this->uri = preg_replace("(\'|\"|%20)","", $_SERVER['PATH_INFO']);
			
			$this->part = substr($this->uri, 0,1) == '/' ? substr($this->uri, 1, strlen($this->uri)) : $this->uri;
			$this->part = explode('/',substr($this->part, strlen($this->part)-1,1) == '/' ? substr($this->part, 0,strlen($this->part)-1) : $this->part);
		}
		
		if(isset($_SERVER['QUERY_STRING'])){
			
			$this->uri .= preg_replace("(\'|\"|%20)","", $_SERVER['QUERY_STRING']);
		}
	}
	
	function ref(){
		return $this->uri;
	}
	
	
	function totalPart(){
		return count($this->part);
	}
	
	function resource($no = ''){
		return empty($this->part[$no]) ? false : $this->part[$no];
	}
	
	function resources(){
		return $this->part; 
	}
	
}