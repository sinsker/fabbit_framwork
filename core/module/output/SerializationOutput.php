<?php
namespace core\module\output;
use  core\module\output\Output;

class SerializationOutput implements Output{
	
	public static function set(&$data){
		return serialize($data);
	}
	
}