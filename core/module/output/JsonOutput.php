<?php
namespace core\module\output;
use  core\module\output\Output;

class JsonOutput implements Output{
	
	public static function set(&$data){
		return json_encode($data);
	}
	
	
}