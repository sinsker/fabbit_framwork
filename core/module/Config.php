<?php 
namespace core\module;

use core\module\shape\Module;

class Config implements Module {
	
	public $configs = array();
	
	public function get($file, $val = ''){
		
		$configFile =  BASE.'config/'.$file.SUFFIX;
		if(empty($this->configs[$file])){
			if(is_file($configFile)){
				if( in_array( $file ,array('_var','_expression'))){
					return ;
				}
				require $configFile;
				if( isset($$file) ){
					$this->configs[$file] = $$file;
					
					if( $val != '' ) {
						return $this->configs[$file][$val];
					}else{
						return $this->configs[$file];
					}
				}
			}
		}else{
			if( $val != '' ){
				return $this->configs[$file][$val];
			}else{
				return $this->configs[$file];
			}
			
		}
		return ;
		
	}
	
	public function getConfigs(){
		return $this->configs;
	}
	
	
	
	
}