<?php 
namespace core\module;

use core\module\shape\Module;

class Param implements Module {
	
	private $class;
	private $function;
	private $paramater = array();
	
	public  $get;
	public  $post;
	public  $request;
	
	public function requestInitialization(){
		
		$U = &Manager::call()->get('uri');
		$C = &Manager::call()->get('config');
		
		if(!empty($U->ref())){
				
			$class		= $U->resource(0);
			$function	= $U->resource(1);
			
			if($C->get('route','request_param_trigger') == TRUE &&  $_REQUEST[$C->get('route','controller')]){
		
				$class		= $_REQUEST[$C->get('route','controller')] ? $_REQUEST[$C->get('route','controller')] : $class;
				$function	= $_REQUEST[$C->get('route','function')] ? $_REQUEST[$C->get('route','function')] : $function;
				
				if($_REQUEST[$C->get('route','function')] ){
					$function = $_REQUEST[$C->get('route','function')];
				}
			}
		}else{
			$class	  =  $C->get('route','default_controller');
			$function =  $C->get('route','default_function');
		}
		
		$class = NAMESPACE_PACKAGE.'controller\\'.(strpos($class, DIRECTORY_SEPARATOR) ?  str_replace(DIRECTORY_SEPARATOR, '\\',$class) : $class);
		
		$function = $function ? $function : 'index';
		
		
		$this->setClass($class);
		$this->setFunction($function);
		
		if(isset($_GET)) 	$this->get 		= $_GET;
		if(isset($_POST)) 	$this->post 	= $_POST;
		if(isset($_REQUEST))$this->request	= $_REQUEST;
		
		$this->unsetRequest();
	}
	
	public function unsetRequest(){
		unset($_GET);
		unset($_POST);
		unset($_REQUEST);
	}
	
	
	public function setClass($class){
		 $this->class = $class;
	}
	
	public function getClass(){
		return $this->class;
	}
	
	public function setFunction($function){
		$this->function = $function;
	}
	
	public function getFunction(){
		return $this->function;
	}
}