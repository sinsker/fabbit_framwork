<?php
namespace core\module;

#Data Trance Object
abstract class Dto{
	
	public abstract function setDto();
	
	//해당 객체의 변수를 배열로
	public function objectToArray(){
		$tmpThis = static::getObject();
		if( !is_object($tmpThis) && !is_array($tmpThis)){
			return $tmpThis;
		}
	
		if(is_object($tmpThis)){
			$tmpThis = get_object_vars($tmpThis);
	
		}
		return $tmpThis;
	}
	
	public function getObject(){
		return $static;
	}
	
}