<?
namespace core\module;

use core\module\Singleton;
use core\module\shape\Cache;

class CacheManager extends Singleton implements Cache{

	public $dir;
	public $interval;
	public $use;
	public $is_loaded;
	public $confirmCache		= '_URLCACHE';
	public $confirmCacheValue	= 'NOCACHE';
	
 /*	public static function getInstance(){
		static $instance = null;
		if( null === $instance ){
			$instance = new static();
		}
		return $instance;
	} */
	
	protected function __construct(){
		$this->dir = BASE.'tmp/_cache';
	}
	
	
	public function config($use, $time = '', $dir = ''){
		$this->use = $use;
		if($time){ $this->interval = $time * 60;  }#분단위
		if($dir) { $this->dir = $dir; }
	}
	

	function setCacheDir($CacheDir){
		$this->CacheDir	= $CacheDir;
	}

	function setInterval($Interval){
		$this->Interval	= $Interval;
	}
	
	function _serialize($object){
		return serialize($object);
	}

	function _unserialize($content){
		return unserialize($content);
	}
	
	function _makeFileName($url){
		return $this->dir."/".md5($url);
	}

	function Error($errmsg){
		printf($errmsg);
	}
	
	#Override
	function set($url, $obj){
		$fname	= $this->_makeFileName($url);
		$fp = fopen($fname,'w');
		if($fp){
			fwrite($fp,$this->_serialize($obj));
			fclose($fp);
			return true;
		} else {
			$this->Error("Unable to open file for writing : $fname");
			return false;
		}
	}
	
	#Override
	function get($url){
		$fname	= $this->_makeFileName($url);
		$fp = fopen($fname,'r');
		if($fp){
			$data = fread($fp,filesize($fname));
			fclose($fp);
			return $this->_unserialize($data);
		} else {
			$this->Error("Cache doesn't contain : $url");
			return false;
		}
	}

	function CheckCache($url, $Interval){
		$fname = $this->_makeFileName($url);
		if(file_exists($fname)){
			if((time()- filemtime($fname)) >= $Interval){
				return 0;
			} else {
				return 1;
			}
		} else {
			return -1;
		}
	}

	
	#세션 파일 생성 루트 확인	
	function is_CacheCreate(){
		global $_GET;
		
		if($_GET[$this->confirmCache] == $this->confirmCacheValue){
			return true;
		}else{
			return false;
		}
		
	}
	
	#불러오기
	function load(){ 
		global $HTTP_GET_VARS,$HTTP_POST_VARS;
		
		$url = sprintf("http://%s%s",$_SERVER['HTTP_HOST'],$_SERVER['REQUEST_URI']);
		$fname = $this->_makeFileName($url);
		if(file_exists($fname)){
			$obj = $this->get($url);
			if((time()- filemtime($fname)) < $obj['interval']){
				$this->is_loaded = true;
				echo $obj['content'];
				exit;
			}
		}
	}
	
	#저장
	function save(){ 
		global $HTTP_GET_VARS,$HTTP_POST_VARS;
		
		if($this->use == true){
			$url = sprintf("http://%s%s",$_SERVER['HTTP_HOST'],$_SERVER['REQUEST_URI']);
			$curl = sprintf("http://%s%s?$this->confirmCache=$this->confirmCacheValue&%s",$_SERVER['HTTP_HOST'],$_SERVER['PHP_SELF'],$_SERVER['QUERY_STRING']);
			$ch = curl_init(); 
			curl_setopt ($ch, CURLOPT_URL,$curl); 				//접속할 URL 주소 
			curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 	// 인증서 체크같은데 true 시 안되는 경우가 많다. 
			curl_setopt ($ch, CURLOPT_HEADER, 0); 				// 헤더 출력 여부 
			curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1); 		// 결과값을 받을것인지 
			$content = curl_exec ($ch); 
			curl_close ($ch); 
			
			$obj['content'] = $content;
			$obj['interval'] = $this->interval;
			
			$this->set($url,$obj);
		}
	}
}
