<?php
namespace core\module\shape;

interface BuilderInterface{
	
	public function init();		#초기화
	public function service();	#서비스 시작
}