<?php
namespace core\module\shape;

interface Cache{
	function get($key);
	function set($key, $value);
}