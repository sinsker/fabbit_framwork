<?php
namespace core\module\shape;

interface DatabaseInterface{
	
	public function connect($config);
	public function disconnect();
	public function query($sql, $file, $line);
	public function fetch($result);
	public function insert_id();
	
}

