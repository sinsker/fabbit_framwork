<?
namespace core\module;

class Widget extends Singleton{

	private static $module = array();
	private static $js = array();
	private static $css = array();
	
	public static function load($wtype, $wvar = array()){
		
		if(empty(Widget::$module[$wtype])){
			$wskin 	= BASE.'widget/'.$wtype.'/'.'main.php';
			$wcss 	= BASE.'widget/'.$wtype.'/'.'main.css';
			$wjs 	= BASE.'widget/'.$wtype.'/'.'main.js';
			
			$wcss_rec= './widget/'.$wtype.'/main.css';
			$wjs_rec = './widget/'.$wtype.'/main.js';
			
			#고유 아이디 생성
			$wvar['id'] = $wvar['id'] ? $wvar['id'] : 'widget-'.uniqid($wtype);
			
			if(is_file($wcss)){ array_push(Widget::$css, "<link href='$wcss_rec' rel='stylesheet'>\n");  }
			if(is_file($wjs)){	array_push(Widget::$js, "<script src='$wjs_rec'></script>\n"); }
			if(is_file($wskin)){
				Widget::$module[$wtype] = $wskin;
				Widget::$module['wvar'][$wtype] = array();
				Widget::$module['wvar'][$wtype] = $wvar;
			}
		}
		return Widget::getInstance();
	}
	public function get($wtype){
		$wvar = Widget::$module['wvar'][$wtype];
		$wdir = './widget/'.$wtype;
		require_once Widget::$module[$wtype];
	}
	
	public  function view($type){
		if(!empty(Widget::$$type)){
			Widget::$$type = array_unique(Widget::$$type, SORT_STRING );
			$sub = implode("", Widget::$$type);
			return $sub;
		}
	}
	
}

?>