<?php
namespace core\module\builder;

use core\module\shape\BuilderInterface;
use core\module\Param;

#Basic 빌더
class BasicBuilder implements BuilderInterface {
	
	public  function init(){
		
		if(!empty($_GET)){
			foreach($_GET as $key => $val){
				Param::$get[$key] = $val;
				Param::$request[$key] = $val;
			}
		}
		if(!empty($_POST)){
			foreach($_POST as $key => $val){
				Param::$post[$key] = $val;
				Param::$request[$key] = $val;
			}
		}
		unset($_POST);
		unset($_GET);
		unset($_REQUEST);
	}
	
	public function service(){
		/* global $autoload; */
		
		if(Param::$request[SERVICE_KEY] && isset(Param::$request[SERVICE_KEY]) ){
			$service = Param::$request[SERVICE_KEY];
		}else{
			$service = DEFAULT_SERVICE;
		}
		
		if(Param::$request[ACTION_KEY] && isset(Param::$request[ACTION_KEY]) ){
			$action = Param::$request[ACTION_KEY];
		}else{
			$action = 'index';
		}
		
		$serviceName = strpos($service, DIRECTORY_SEPARATOR) ?  str_replace(DIRECTORY_SEPARATOR, '\\',$service) : $service;
		$class =  "package\\controller\\".$serviceName;
		$controller = new $class();
		
		/*foreach($autoload as $key => $val ){
			
			if( empty($autoload[$key])) continue; 
			
			if($key == 'lib'){
				foreach ($autoload[$key] as $nkey => $nval){
					$controller->lib($nval);
				}
			}
			
			if($key == 'config'){
				foreach ($autoload[$key] as $nkey => $nval){
					$controller->config($nval);
				}
			}
		} */
		
		if(method_exists ( $controller, $action) ){
			$controller->$action();
		}
	}
	
	
}

