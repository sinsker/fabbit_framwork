<?php
namespace core\module\builder;

use core\module\shape\BuilderInterface;
use core\module\Manager;

#RestfulBuilder 빌더
class RestfulBuilder implements BuilderInterface {
	
	public function init(){
		
		$this->M = &Manager::call();
		
		$this->M->get('hooker')->hook('init_system');
		
		
		$this->M->get('uri')->parseRequestUri();
		$this->M->get('Param')->requestInitialization();
	}
	
	
	public function service(){
				
		$class		= $this->M->get('param')->getClass();
		$function	= $this->M->get('param')->getFunction();
		
		$this->M->get('hooker')->hook('post_controller');

		if(class_exists($class)){
			$class = $class;
		}else if(class_exists($class.'Controller')){
			$class = $class.'Controller';
		}else{
			goto_url('/bonus/error404');
		}
		
		$controller = new $class();
		$controller->manager = &$this->M;
		
		if(!method_exists ( $controller, $function)){
			goto_url('/bonus/error404');
		}
		
		$this->M->get('hooker')->hook('set_controller');
		
		if($this->M->get('uri')->totalPart() > 2){
			$argument = array_slice($this->M->get('uri')->resources(), 2);
		}
		
		call_user_func_array(array(&$controller, $function), $argument ? $argument : array()  );
		
		$this->M->get('hooker')->hook('set_function');
	}
	
	public function __destruct(){
		
		$this->M->get('hooker')->hook('last_system');
		
	}
}

