<?php 
namespace core\module;

class Base {
	
	//모듈에 등록 되지 않은 액션 요청시 에러 처리
	public function __call($method, $args){
		if (!method_exists($static, $method)) {
			echo("사용되지 않는 모듈입니다. Not Found Error :  $method ");
		}
	}
	
	public function model($modelName){
		if(empty($this->modelArray[$modelName])){
			$class = "package\\model\\".$modelName.'Model';
			$this->modelArray[$modelName] = new $class;
		}
		return $this->modelArray[$modelName];
	}
	
	
}