<?

/*************************************************************************
**
**  일반 함수 모음
**
*************************************************************************/

function set_magic_quotes_gpc(){
	
	if( !get_magic_quotes_gpc() ) {
		if( is_array($_GET) ) {
			while( list($k, $v) = each($_GET) ) {
				if( is_array($_GET[$k]) ) {
					while( list($k2, $v2) = each($_GET[$k]) ) {
						$_GET[$k][$k2] = addslashes($v2);
					}
					@reset($_GET[$k]);
				} else {
					$_GET[$k] = addslashes($v);
				}
			}
			@reset($_GET);
		}
			
		if( is_array($_POST) ) {
			while( list($k, $v) = each($_POST) ) {
				if( is_array($_POST[$k]) ) {
					while( list($k2, $v2) = each($_POST[$k]) ) {
						$_POST[$k][$k2] = addslashes($v2);
					}
					@reset($_POST[$k]);
				} else {
					$_POST[$k] = addslashes($v);
				}
			}
			@reset($_POST);
		}
			
		if( is_array($_COOKIE) ) {
			while( list($k, $v) = each($_COOKIE) ) {
				if( is_array($_COOKIE[$k]) ) {
					while( list($k2, $v2) = each($_COOKIE[$k]) ) {
						$_COOKIE[$k][$k2] = addslashes($v2);
					}
					@reset($_COOKIE[$k]);
				} else {
					$_COOKIE[$k] = addslashes($v);
				}
			}
			@reset($_COOKIE);
		}
	}
}

// 메타태그를 이용한 URL 이동
// header("location:URL") 을 대체
function goto_url($url)
{
    echo "<script type='text/javascript'> location.replace('$url'); </script>";
    exit;
}

// 쿠키변수 생성
function set_cookie($cookie_name, $value, $expire)
{
    setcookie(md5($cookie_name), base64_encode($value), TIME + $expire, '/', COOKIE_DOMAIN);
}


// 쿠키변수값 얻음
function get_cookie($cookie_name)
{
    return base64_decode($_COOKIE[md5($cookie_name)]);
}


//암호화
function encrypt($key, $string, $password) {
	$password = hash('sha256', $password, true);
    
    // 용량 절감과 보안 향상을 위해 평문을 압축한다.
    
    $plaintext = gzcompress($plaintext);
    
    // 초기화 벡터를 생성한다.
    
    $iv_source = defined('MCRYPT_DEV_URANDOM') ? MCRYPT_DEV_URANDOM : MCRYPT_RAND;
    $iv = mcrypt_create_iv(32, $iv_source);
    
    // 암호화한다.
    
    $ciphertext = mcrypt_encrypt('rijndael-256', $password, $plaintext, 'cbc', $iv);
    
    // 위변조 방지를 위한 HMAC 코드를 생성한다. (encrypt-then-MAC)
    
    $hmac = hash_hmac('sha256', $ciphertext, $password, true);
    
    // 암호문, 초기화 벡터, HMAC 코드를 합하여 반환한다.
    
    return base64_encode($ciphertext . $iv . $hmac);
}


//복호화
function decrypt($ciphertext, $password) {
	 $ciphertext = @base64_decode($ciphertext, true);
    if ($ciphertext === false) return false;
    $len = strlen($ciphertext);
    if ($len < 64) return false;
    $iv = substr($ciphertext, $len - 64, 32);
    $hmac = substr($ciphertext, $len - 32, 32);
    $ciphertext = substr($ciphertext, 0, $len - 64);
    
    // 암호화 함수와 같이 비밀번호를 해싱한다.
    
    $password = hash('sha256', $password, true);
    
    // HMAC 코드를 사용하여 위변조 여부를 체크한다.
    
    $hmac_check = hash_hmac('sha256', $ciphertext, $password, true);
    if ($hmac !== $hmac_check) return false;
    
    // 복호화한다.
    
    $plaintext = @mcrypt_decrypt('rijndael-256', $password, $ciphertext, 'cbc', $iv);
    if ($plaintext === false) return false;
    
    // 압축을 해제하여 평문을 얻는다.
    
    $plaintext = @gzuncompress($plaintext);
    if ($plaintext === false) return false;
    
    // 이상이 없는 경우 평문을 반환한다.
	return $result;
}


function base64encode($string) {
	$data = str_replace(array('+','/','='),array('-','_',''),base64_encode($string));
	return $data;
}

function base64decode($string) {
	$data = str_replace(array('-','_'),array('+','/'),$string);
	$mod4 = strlen($data) % 4;
	if ($mod4) {
		$data .= substr('====', $mod4);
	}
	return base64_decode($data);
}



// 경고메세지를 경고창으로
function alert($msg='', $url='')
{
    if (!$msg) $msg = '올바른 방법으로 이용해 주십시오.';

	echo "<script type='text/javascript'>alert('$msg');";
    if (!$url) echo "history.go(-1);";
    echo "</script>";
    if ($url)  goto_url($url);
    exit;
}


// 경고메세지 출력후 창을 닫음
function alert_close($msg)
{
    echo "<script type='text/javascript'> alert('$msg'); window.close(); </script>";
    exit;
}


// url에 http:// 를 붙인다
function set_http($url)
{
    if (!trim($url)) return;

    if (!preg_match("/^(http|https|ftp|telnet|news|mms)\:\/\//i", $url))
        $url = "http://" . $url;

    return $url;
}

// url에 https:// 를 붙인다
function set_https($url)
{
	if (!trim($url)) return;

	if (!preg_match("/^(http|https|ftp|telnet|news|mms)\:\/\//i", $url))
		$url = "https://" . $url;

		return $url;
}


// 파일의 용량을 구한다.
function get_filesize($size)
{
    //$size = @filesize(addslashes($file));
    if ($size >= 1048576) {
        $size = number_format($size/1048576, 1) . "M";
    } else if ($size >= 1024) {
        $size = number_format($size/1024, 1) . "K";
    } else {
        $size = number_format($size, 0) . "byte";
    }
    return $size;
}

// 폴더의 용량 ($dir는 / 없이 넘기세요)
function get_dirsize($dir)
{
    $size = 0;
    $d = dir($dir);
    while ($entry = $d->read()) {
        if ($entry != "." && $entry != "..") {
            $size += filesize("$dir/$entry");
        }
    }
    $d->close();
    return $size;
}

//시간값 가져오기
function get_time() {
	list($usec, $sec) = explode(" ", microtime());
	return ((float)$usec + (float)$sec);
}

//폴더 읽기
function dir_read($path){
	$entry = $dirs = '';
	$dirs = dir($path);
	while(false !== ($entry = $dirs->read())){ // 읽기
		if(($entry != '.') && ($entry != '..')) {
			if(is_dir($path.'/'.$entry)) { // 폴더이면
				$entrys[$entry] =  array (dir_read($path.'/'.$entry) );
			}else { // 파일이면
				$entrys[] = $entry;
			}
		}
	}
	return $entrys;
}

//접근자 상태
function agent_info($u_agent = '')
{
	if(empty($u_agent)) $u_agent = $_SERVER['HTTP_USER_AGENT'];
	
	$ary_m = array("iPhone","iPod","IPad","Android","Blackberry","SymbianOS|SCH-M\d+","Opera Mini","Windows CE","Nokia","Sony","Samsung","LGTelecom","SKT","Phone");
	$bname = 'Unknown';
	$platform = 'Unknown';
	$model = '';
	$version= "";
	$isMobile = 'N';
	
	if (preg_match('/Mobile/i',$u_agent)){
		$isMobile = 'Y';
	}
	
	if($isMobile == 'N'){ //모바일이 아닐 경우
		if (preg_match('/linux/i', $u_agent)) {
			$platform = 'Linux';
		}
		elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
			$platform = 'Mac';
		}
		elseif (preg_match('/windows|win32/i', $u_agent)) {
			$platform = 'Windows';
		}
		$model = 'PC '.$platform;
		
	}else{ //모바일일 경우
		
		for($i=0; $i<count($ary_m); $i++){
			if(preg_match("/$ary_m[$i]/i", strtolower($u_agent))) {
				$m_platform = $ary_m[$i];
				break;
			}
		}
		$model = $m_platform;
	}
	
	if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
	{
		$bname = '인터넷 익스플로러';
		$ub = "MSIE";
	}
	elseif(preg_match('/Firefox/i',$u_agent))
	{
		$bname = '파이어 폭스';
		$ub = "Firefox";
	}
	elseif(preg_match('/Chrome/i',$u_agent))
	{
		$bname = '크롬';
		$ub = "Chrome";
	}
	elseif(preg_match('/Safari/i',$u_agent))
	{
		$bname = '사파리';
		$ub = "Safari";
	}
	elseif(preg_match('/Opera/i',$u_agent))
	{
		$bname = '오페라';
		$ub = "Opera";
	}
	elseif(preg_match('/Netscape/i',$u_agent))
	{
		$bname = '넷스케이프';
		$ub = "Netscape";
	}else if(preg_match('/facebookexternalhit/i',$u_agent))
	{
		$bname = '스크랩';
		$ub = "facebookext";
		$isBot = 'Y';
	}else if(preg_match('/bandscraper/i',$u_agent))
	{
		$banme = '밴드 스크랩';
		$ub ='naverband';
		$isBot = 'Y';
	}

	$known = array('Version', $ub, 'other');
	$pattern = '#(?<browser>' . join('|', $known) .')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';

	$i = count($matches['browser']);
	if ($i != 1) {
		if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
			$version= $matches['version'][0];
		}
		else {
			$version= $matches['version'][1];
		}
	}
	else {
		$version= $matches['version'][0];
	}

	if ($version==null || $version=="") {$version="?";}
	
	if(preg_match('/bot/i',$u_agent)) {
		$isBot = 'Y';
	}

	return array(
		'userAgent'	=> $u_agent,
		'name'		=> $bname,
		'shortName'	=> $ub,
		'version'	=> $version,
		'platform'	=> $platform,
		'pattern'	=> $pattern,
		'isMobile'	=> $isMobile,
		'model'		=> $model,
		'isBot'		=> $isBot
	);
}


//파일 업로드
function fileUpload($_file, $_typeArray, $_path = ''){
	if(!$_file) return false;
	if(!$_path) $_path = '/upload/';
	
	$f_info = pathinfo( $_file['name']);
	$f_ext = strtolower($f_info["extension"]);
	
	$_type = @implode('|' ,$_typeArray);
	
	if(!preg_match("/($_type)/",$f_ext) ){
		alert( "해당 파일은 업로드 불가능합니다.");
		exit();
	}
		
	$f_a_name = md5(str_replace($f_ext, "", $_file['name']).date('YmdHis')).".{$f_ext}";
	$upload_file = $_SERVER['DOCUMENT_ROOT'].$_path.$f_a_name;
	if(is_uploaded_file($_file['tmp_name'])) {
		if(!move_uploaded_file($_file['tmp_name'], $upload_file)) {
			alert("업로드된 파일을 옮기는 중 오류가 발생하였습니다.");
			exit();
		}
	}
	#클라이언트 path
	return $_path.$f_a_name;
}


#파일이름 가져오기
function get_file_name($full_file_name) {
	$array = explode('/', $full_file_name);
	if (sizeof($array) <= 1)  return $full_file_name;
	return $array[sizeof($array) -1];
}

#상세 보기[디버깅 용도]
function pr($text){
	echo '<pre>';
	print_r($text);
	echo '</pre>';
}

function fileDownload($file){
	// 파일 Path를 지정합니다.
	// id값등을 이용해 Database에서 찾아 오거나 GET이나 POST등으로 가져와 주세요.
	$file_size = filesize($file);
	$filename = get_file_name($file);
	if (is_file($file)) // 파일이 존재하면
	{
		// 파일 전송용 HTTP 헤더를 설정합니다.
		if(strstr($HTTP_USER_AGENT, "MSIE 5.5"))
		{
			header("Content-Type: doesn/matter");
			Header("Content-Length: ".$file_size);
			header("Content-Disposition: filename=".$filename);
			header("Content-Transfer-Encoding: binary");
			header("Pragma: no-cache");
			header("Expires: 0");
		}
		else
		{
			Header("Content-type: file/unknown");
			Header("Content-Disposition: attachment; filename=".$filename);
			Header("Content-Transfer-Encoding: binary");
			Header("Content-Length: ".$file_size);
			Header("Content-Description: PHP3 Generated Data");
			header("Pragma: no-cache");
			header("Expires: 0");
		}
		//파일을 열어서, 전송합니다.
		$fp = fopen($file, "rb");
		if (!fpassthru($fp))
			fclose($fp);
	}
}

function execlDownload($context, $filename = ''){
	if(!$filename) $filename = date('mdHis');
	
	header('Content-Disposition: attachment; filename='.$filename.'.xls');
	header('Content-type: application/vnd.ms-excel; charset='.CHAR_SET );
	header('Content-Description: PHP5 Generated Data');
	header('Cache-Control: no-cache');
	header('Pragma: no-cache');
	echo '<html>';
	echo '	<head>';
	echo '		<meta http-equiv="Content-Type" content="application/vnd.ms-excel; charset="'.CHAR_SET.'">';
	echo '		<style>';
	echo '			td {border: 1px solid #000000;}';
	echo '		</style>';
	echo '	</head>';
	echo $context;
}

function createWhereText($values, $operator = 'and'){
	$whereText = array();
	foreach ($values as $key => $value){
		$type = gettype($value);
		if($type = 'string'){$value = "'$value'"; }
		$text[] = $key.'='.$value;
	}
	$whereText = ($operator == 'and' ? ' where ':'').@join($operator.' ',$text);
	
	return $whereText;
}

// 스킨 치환 및 반환
function fetchSkin($datas, $skin) {
	if(is_array($skin)) $skin = implode('', $skin);
	 
	preg_match_all('/{{(.*?)}}/', $skin, $pattern);
	if(!empty($pattern[1])) {
		$infos = array();
		foreach(array_unique($pattern[1]) as $field){
			$infos[] = $datas[$field];
		}

		$skin = str_replace(array_unique($pattern[0]), $infos, $skin);
	}
	return $skin;
}

//반복문 치환
function fetchContent($datas, $skin){
	if(preg_match_all('/\{\{START\}\}(.|\s)*\{\{END\}\}/',$skin, $pattern)){
		if(!empty($pattern[1])) {
			$repetition = '';
			$loop = count($datas);
			for($i = 0; $i < $loop; $i++){
				$repetition .= fetchSkin($datas[$i],$pattern[0]);
			}
			return preg_replace('/\{\{START\}\}(.|\s)*\{\{END\}\}/',$repetition, $skin);
		}
	}
	return ;
}

//셀렉트 박스 만드는 함수
function selectBox($_array, $_selected_value , $name, $type=0, $defult=1, $subOption=''){
	echo "<select class=' form-control' name='$name' id='$name' {$subOption}>";
	if($defult){
		echo "<option value=''> - 선택 - </option>";
	}
	foreach($_array as $key => $value){
		$selected ='';
		if( $key == $_selected_value){
			$selected = "selected";
		}
		echo "<option value='$key' $selected >";
		if($type == 1){
			echo '['.$key.'] '.$value;
		}else{
			echo $value;
		}
		echo "</option>";
	}
	echo "</select>";
}

//플러그인 추가
function pluginImport($plugin,$path,$kind){
	if ($kind == 'js') echo '<script src="'.APP_BASE.'public/plugins/'.$plugin.'/'.$path.'.js"></script>';
	else echo '<link href="'.APP_BASE.'public/plugins/'.$plugin.'/'.$path.'.css" rel="stylesheet">';
}

//인크루드
function inc($page){
	include BASE.$page.SUFFIX;
}


?>