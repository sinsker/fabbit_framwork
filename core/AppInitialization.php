<?php   if (!defined('FABBIT')) exit('잘못된 접근입니다. URL을 확인해 주십시오.');

$app['PATH'] = str_replace('/', '', $app['PATH']) == '' ? '' : $app['PATH'].'/';

#최상위 경로
define('ROOT',		$_SERVER['DOCUMENT_ROOT'].'/');

#프레임워크 최상위 경로
define('BASE',		ROOT . $app['PATH']);

#프레임워크 프론트 디렉토리
define('APP_BASE',	'/'.$app['PATH']);

#공통 확장자
define('SUFFIX',	'.php');

#전역 함수
require BASE.'core/Common'.SUFFIX; 

#클래스 자동 로드
require BASE.'core/AutoLoader'.SUFFIX;

#공통 변수
require BASE.'config/_var'.SUFFIX;

#라우터 변수
require BASE.'config/_expression'.SUFFIX;

#시스템 생성
core\System::call()->module([new core\module\Config(),
							 new core\module\Logger(),
							 new core\module\Uri(),
							 new core\module\Param(),
							 new core\module\Hooker()
]);

core\System::call()->build(new core\module\builder\RestfulBuilder());