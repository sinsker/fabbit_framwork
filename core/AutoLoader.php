<?php

spl_autoload_register(function($className){
	$className = ltrim($className, '\\');
	$fileName = '';
	$namespace = '';
	$classLoad = false;
	
	if($lastNsPos = strpos($className, '\\')) {
		$namespace = substr($className, 0, $lastNsPos);
		$className = substr($className, $lastNsPos + 1);
		$fileName = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
	}
	$fileName .= str_replace('\\', DIRECTORY_SEPARATOR, $className);
	
	if(is_file($fileName.SUFFIX)){
		$classLoad = $fileName.SUFFIX;
	}else if(is_file(strtolower($fileName).SUFFIX)){
		$classLoad = strtolower($fileName).SUFFIX;
	}
	
	if($classLoad !== false) require $classLoad;
	
});
