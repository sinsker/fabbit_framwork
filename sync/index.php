<? 
include_once $_SERVER['DOCUMENT_ROOT'].'/inc/common.php';

#액션단 처리
if(!empty($_GET['action'])){
	include_once $_SERVER['DOCUMENT_ROOT'].'/'.ACTION_PATH.'/'.$_GET['action'].'.action.php';
	exit;
}

#스킨 선택 < 관리자 / 기본  >
define('CONTENT', ($_GET['admin']== 'y' || defined('IS_ADMIN') )? ADMIN_PATH : SKIN_PATH );
define('CUR_DIR', CONTENT.'/'.$pg['f'] );								//현재 경로
define('CUR_PATH', $_SERVER['DOCUMENT_ROOT'].CONTENT.'/'.$pg['f'] );	//현재 PATH경로


include_once $_SERVER['DOCUMENT_ROOT'].ROOT.'/head.php'; #상단
@include_once $_SERVER['DOCUMENT_ROOT'].CONTENT.'/'.$pg['f'].'/init.php'; #페이지 초기화
$client = $_SERVER['DOCUMENT_ROOT'].CONTENT.'/'.$pg['f'].'/'.$pg['v'].'.php'; #클라이언트단
if(file_exists($client)){
	include_once $client;
}else{
	if(defined('IS_ADMIN')){
		include_once $_SERVER['DOCUMENT_ROOT'].CONTENT.'/error404.php';
	}else{
		goto_url('/');
	}
}
include_once $_SERVER['DOCUMENT_ROOT'].ROOT.'/tail.php'; #하단


?>

