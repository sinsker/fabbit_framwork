<?if (!defined('SIN')) exit;?>

<!DOCTYPE html> 
<html lang="ko">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=<?=CHAR_SET?>" />
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
		<meta name="author" content="" />
		<meta name="keywords" content="" />
		<meta name="robots" content="all" />
		<link rel="shortcut icon" href="/favicon.ico">
		<title><?=defined('IS_ADMIN') ? 'SYNC | 관리자 ': 'SYNC | 성락성결교회 청년부'?></title>
		<?
			include_once ( defined('IS_ADMIN') ? $_SERVER['DOCUMENT_ROOT'].'/'.ROOT.'/admin.head.sub.php' : $_SERVER['DOCUMENT_ROOT'].'/'.ROOT.'/head.sub.php'   );
		?>
	</head>
	<body class="page-body login-page login-form-fall landing" >
		<? if( defined('IS_ADMIN') ){ ?>
		<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
			<?if( $_GET['nav'] != 'n' && !empty($_SESSION['auth_id']) ){
				@include_once $_SERVER['DOCUMENT_ROOT'].'/'.CONTENT.'/'.GNB; 
			}?>
				
			<?if($_SESSION['auth_id']){ ?> 
			<div class="col-md-12 col-xs-12 clearfix" style="background: #ffffff;  padding-top: 5px; " >
				<ul class="user-info pull-left pull-none-xsm">
					<li class="profile-info dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<img src="<?=IMG_PATH?>/admin/thumb-1@2x.png" alt="" class="img-circle" width="44" />
							<?=$_SESSION[$userAuth->session_name]?>
						</a>
						<ul class="dropdown-menu">
							<li class="caret"></li>
							<li>
								<a href="extra-timeline.html">
									<i class="entypo-user"></i>
									개인 정보 수정
								</a>
							</li>
							<li>
								<a href="mailbox.html">
									<i class="entypo-mail"></i>
									메시지
								</a>
							</li>
							<li>
								<a href="extra-calendar.html">
									<i class="entypo-calendar"></i>
									일정 관리
								</a>
							</li>
							<li>
								<a href="?action=auth&cmd=logout&type=get">
									<i class="entypo-clipboard"></i>
									로그 아웃
								</a>
							</li>
						</ul>
					</li>
					<li >
						<div class="user-info pull-right pull-none-xsm" style="position: absolute; right: 0; margin-right: 15px;">
							<ol class="breadcrumb bc-3 " style="margin: 0px; padding-top: 15px;">
								<li>
									<a href="/admin/"><i class="entypo-home"></i>메인</a>
								</li>
								<? if(!empty($gnb[ $pg['f'] ][ $pg['v'] ] )){?>
									<? $gnbMenu = $gnb[ $pg['f'] ][ $pg['v'] ]; ?>
									<? if($gnbMenu['parent']){?>
									<li>
										<a href="<?=$gnbMenu['url']?>"><?=$gnbMenu['parent']?></a>
									</li>
								<?}?>
								<li class="active">
									<a href="#;"><?=$gnbMenu['name']?></a>
								</li>
								<? }?>
							</ol>
						</div>
					</li>
				</ul>
			</div>
			<hr style="background: #ffffff; margin: 0px;" >
		<?}}?>