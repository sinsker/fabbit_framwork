//해당 아이디 창 토글
function toggleView(id){
	$("#"+id).toggle();
}

//위젯 모든 플레이 정지
function widget_movie_allpause(id){
	
	var jwplayer_movie_array = new Array();
	var youtube_movie_array = new Array();
	
	$(".widget-movie.jwplayer.play").each(function(){
		jwplayer_movie_array.push($(this).attr('data-movie-key'));
	});
	
	$(".widget-movie.youtube.play").each(function(){
		youtube_movie_array.push($(this).attr('data-movie-key'));
	});
	
	for(var i=0 ; i < jwplayer_movie_array.length ; i++){
		if(jwplayer_movie_array[i] != id || id == ''){
			jwplayer(jwplayer_movie_array[i]).pause(true);
		}
	}
	
	for(var i=0 ; i < youtube_movie_array.length ; i++){
		if(youtube_movie_array[i] != id || id == ''){
			widget_movie_youtube_stop(youtube_movie_array[i]);
		}
		
	}
}

//위젯 유투브 플레이어 정지 
function widget_movie_youtube_stop(id){
	
	var yobj = $("[data-movie-key="+id+"]");
	var temp_img	= yobj.find('[name=img]').val();
	var temp_file	= yobj.find('[name=file]').val();
	var temp_width	= yobj.find('[name=width]').val();
	var temp_height = yobj.find('[name=height]').val();
	var default_img = $('<a href="#;"><img src="'+temp_img+'" width="'+temp_width+'" height="'+temp_height+'" ></a>');
	var youtube_vidio = $('<iframe id="'+id+'" width="'+temp_width+'" height="'+temp_height+'" src="'+temp_file+'?rel=0&enablejsapi=1&autoplay=1" frameborder="0" allowfullscreen></iframe>');
	
	yobj.find('iframe').remove();	//아이프레임 삭제
	yobj.removeClass('play'); 		
 	yobj.append(default_img);		//기본 이미지 다시 붙임
 	
 	//클릭 이벤트 추가
 	yobj.find('a').click(function(){ 
 		widget_movie_allpause(id); 
 		$(this).remove();
 		yobj.addClass('play');
 		yobj.append(youtube_vidio);
 	});
}

function timer(_start, _end){
	var date, finish, num, tmptime, finishtime;
	
	// 시간설정
	date = new Date();
	var start = getDate(_start,'timestamp');
	var end = getDate(_end,'timestamp');

	if (start < 0 || end < 0) {
		return;
	}
	
	num = 1;
	
	while (date < start || date >= end) {
		start += 604800000;
		end += 604800000;
		num++;
	}
		
	
	tmptime = parseInt((end - date) / 1000);
	play(tmptime);
}

// 타이머스타트
function play(time){
		
	var d, h, m, s, d_mod, h_mod;
	d = Math.floor(time / 86400);
	d_mod = time % 86400;
	h = Math.floor(d_mod / 3600); 
	h_mod = d_mod % 3600;
	m = Math.floor(h_mod / 60);
	s = h_mod % 60;
	d = (d < 10) ? '0' + d : d;
	h = (h < 10) ? '0' + h : h;
	m = (m < 10) ? '0' + m : m;
	s = (s < 10) ? '0' + s : s;
	
	$("[data-format=timer]").text(h+':'+ m +':'+s);
	time--;
	
	if (time < 0) {
		$("body").html('<div class="title">게임이 종료 되었습니다.<br> <code>숲속의 무대</code>로 모여주세요.<br><a href="http://map.naver.com/?dlevel=14&pinType=site&pinId=18811006&x=127.0799904&y=37.5487858&enc=b64" class="btn btn-primary" target="_blank">찾아오는 길 보기</div></div>');
		return;
	} else {
		setTimeout(function() { play(time); }, 1000);
	}
}
function getDate(ymdhis, type) {
	var obj = {
		y : ymdhis.toString().substr(0, 4),
		m : ymdhis.toString().substr(5, 2),
		d : ymdhis.toString().substr(8, 2),
		h : ymdhis.toString().substr(11, 2),
		i : ymdhis.toString().substr(14, 2),
		s : ymdhis.toString().substr(17, 2)
	};

	switch (type) {
		case 'timestamp': 
			return new Date(obj.y, Number(obj.m) - 1, obj.d, obj.h, obj.m, obj.s).getTime();
		case 'Y-m-d':
		default: 
			return obj.y + '-' + obj.m + '-' + obj.d;
	}
}


function timer_counter(_start,_end){
	var start_date = timer_getDate(_start);
	var end_date = timer_getDate(_end);

	var tmptime = parseInt((end_date - start_date) / 1000);
	timer_play(tmptime);
}
function timer_getDate(ymdhis) {
	var obj = {
		y : ymdhis.toString().substr(0, 4),
		m : ymdhis.toString().substr(5, 2),
		d : ymdhis.toString().substr(8, 2),
		h : ymdhis.toString().substr(11, 2),
		i : ymdhis.toString().substr(14, 2),
		s : ymdhis.toString().substr(17, 2)
	};
	return new Date(obj.y, Number(obj.m) - 1, obj.d, obj.h, obj.i, obj.s).getTime();
}


function timer_play(time){
	
	var days = time / 60 / 60 / 24;
	var daysRound = Math.floor(days);
	
	var hours = time / 60 / 60 - (24 * daysRound);
	var hoursRound = Math.floor(hours);
	
	var minutes = time /60 - (24 * 60 * daysRound) - (60 * hoursRound);
	var minutesRound = Math.floor(minutes);
	
	var seconds = time - (24 * 60 * 60 * daysRound) - (60 * 60 * hoursRound) - (60 * minutesRound);
	var secondsRound = Math.round(seconds);
	if(daysRound< 10)	 daysRound 		= "0" + daysRound;
	if(hoursRound< 10)	 hoursRound 	= "0" + hoursRound;
	if(minutesRound< 10) minutesRound	= "0" + minutesRound;
	if(secondsRound< 10) secondsRound	= "0" + secondsRound;

	$("[data-format=timer]").text(hoursRound+':'+ minutesRound +':'+secondsRound);
	
	time--;
	if (time < 0) {
		$(".lastgo").html('<div class="title">게임이 종료 되었습니다.<br> <code>숲속의 무대</code>로 모여주세요.<br><a href="http://map.naver.com/?dlevel=14&pinType=site&pinId=18811006&x=127.0799904&y=37.5487858&enc=b64" class="btn btn-primary" target="_blank">찾아오는 길 보기</div></div>');
		return;
	} else {
		setTimeout(function() { timer_play(time); }, 1000);
	}
	
	
}


//애니메이션 재생
function playAnim(id, ani) {
	  $('#'+id).removeClass().addClass(ani + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
	    $(this).removeClass();
	  });
};

//페이스북 공유
function share_facebook(url){
	var sns_popup = window.open('http://www.facebook.com/sharer.php?u='+encodeURIComponent(url)+'&t='+encodeURIComponent(url),'facebook_share_dialog','toolbar=0,status=0,width=550,height=436');
	if (sns_popup.focus) {
		sns.popup.focus();
	}
}

function share_kakao(title, subtitle, url, image) { //http://sync2030.org/sync/resources/images/note0.jpg
    Kakao.Link.sendTalkLink({
        label: title,
        webLink: {
            text: subtitle,
            url: url
        },
        image: { // 80 * 80
            src: image,
             width: '100',
            height: '100'
        },
        webButton  :{
             url : url,
        }
    });
}

//URL 공유
function share_url(url){
    if (!window.clipboardData) { 
          prompt("Ctrl+C를 눌러 클립보드로 복사하세요.", url);
     }else{
          if(confirm("링크 주소를 클립보드에 복사하시겠습니까?")){
               window.clipboardData.setData("Text", url);
               alert("링크가 복사되었습니다.");
          }
     }
}

function evt_data_insert(evt_id,data){
	$.post("/?action=event_data_insert", data+"&evt_id="+evt_id  , function(data){
		if(data.msg) alert(data.msg);
		if(data.result) location.reload(true);
	}, 'json');
}