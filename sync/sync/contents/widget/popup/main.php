<?php
/****************************************************************************************************
 * popup 위젯
 * 사용법 : <? Unit::load('popup', array('title' => '제목', 'content' => '내용 ', 'start' => '2015-03-14 12:00:00', 'end'=> '2027-03-14 12:00:00'));?>
 ****************************************************************************************************/
// 옵션 정의
$wvar['title']		= $wvar['title'] ? $wvar['title'] : '';
$wvar['content']	= $wvar['content'] ? $wvar['content'] : '';
$wvar['sdate']		= $wvar['sdate'] ? $wvar['sdate'] : TIME_FULL;
$wvar['edate']		= $wvar['edate'] ? $wvar['edate'] :  date(TIME_FULL, strtotime("+1 day"));
?>

<?
if( TIME >= strtotime($wvar['sdate']) && TIME <= strtotime($wvar['edate'] )){ ?>
<div class="remodal " data-remodal-id="popup-<?=$wvar['id']?>">
  <header><?=$wvar['title']?></header>
  <div class="remodal-content">
  		<?=$wvar['content']?>
  </div>
  <!-- <a class="remodal-cancel" href="#">Cancel</a>
  <a class="remodal-confirm" href="#">OK</a> -->
  <div class="remodal-cancel" style="width: 100%; top: 30px; padding: 3px; position: relative;"> 오늘 하루동안 보지 않기 </div>
</div>

<script>
(function($){
	if($.cookie('popup-<?=$wvar['id']?>') != 'Y'){
		var inst = $("[data-remodal-id=popup-<?=$wvar['id']?>]").remodal();
		inst.open();
	}
	 $("[data-remodal-id=popup-<?=$wvar['id']?>]").find(".remodal-cancel").on("click",function(){
		$.cookie('popup-<?=$wvar['id']?>', 'Y', { expires: 1});
	 });
})(jQuery);
</script>
<?}?>