<?php
/****************************************************************************************************
 * Music 위젯
 * 사용법 : <? Unit::load('music', array('file' => 'mp3파일', 'autoplay' => '', 'location' => '', 'volume'=> '3'));?>
 ****************************************************************************************************/
// 옵션 정의
$wvar['file']		= $wvar['file'] ? $wvar['file'] : '';
$wvar['autoplay']	= $wvar['autoplay'] ? true : false;
$wvar['volume']		= $wvar['volume'] ? $wvar['volume'] : 1;
$wvar['img']		= $wvar['img'] ? $wvar['img'] : IMG_PATH.'/media.png';
?>
<div class="wg-music-box <?=$wvar['autoplay'] ? 'play' : ''?>" data-widget="music" id="<?=$wvar['id']?>">
	<img src="<?=$wvar['img']?>" alt="재생버튼" width="45px" height="45px;">
	<a href="#;" class="wg-music"><?=$wvar['autoplay'] ? 'ON' : 'OFF'?></a>
	<audio hidden <?=$wvar['autoplay'] ? 'autoplay = "autoplay"' : '' ?>  id="video-<?=$wvar['id']?>">
		<source src="<?=RES_PATH?>/mp3/<?=$wvar['file']?>" type="audio/mp3">
	</audio> 
</div>

<script>
	(function(doc, undefined) {
			var wvideo = document.getElementById("video-<?=$wvar['id']?>");
			wvideo.volume = <?=$wvar['volume']?>;
			
			$("#<?=$wvar['id']?>").click(function(){
				if($(this).hasClass("play") === true ){
					$(this).removeClass("play");
					$(this).find("a").text("OFF");
					wvideo.pause();
				}else{
					$(this).addClass("play");
					$(this).find("a").text("ON");
					
					wvideo.play();
				}
			});
	})(document);	
</script>