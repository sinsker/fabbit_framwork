<?php
/****************************************************************************************************
 * JwPlayer 위젯
 * 사용법 : <?php getWidget('player/jwplayer', array('file' => 'http://mvod.hackers.co.kr/champstudymobile/wmv/sample_movie/6494_s.mp4', 'width' => 640, 'height' => 360, 'autostart' => 1));?>
 ****************************************************************************************************/
// 옵션 정의
$option = array(
	'skin' => $wdgvar['skin'] ? $wdgvar['skin'] : '/plugins/jwplayer/6.0.2790/five/src/five.xml',
	'file' => $wdgvar['file'] ? $wdgvar['file'] : '',
	'playlist' => $wdgvar['playlist'] ? $wdgvar['playlist'] : array(),
	'width' => ($wdgvar['width'] || $wdgvar['width'] === 0) ? $wdgvar['width'] : '100%',
	'height' => $wdgvar['height'] || $wdgvar['height'] === 0 ? $wdgvar['height'] : '270',
	'aspectratio' => $wdgvar['aspectratio'] ? $wdgvar['aspectratio'] : '16:9',
	'repeat' => $wdgvar['repeat'] ? 'true' : 'false',
	'autostart' => $wdgvar['autostart'] ? 'true' : 'false',
	'controls' => $wdgvar['controls'] === (0 || false) ? 'false' : 'true',
	'volume' => $wdgvar['volume'] ? $wdgvar['volume'] : 100,
	'img'	=> $wdgvar['img'] ? $wdgvar['img'] : 'http://image.champstudy.com/img/event/2015/14110516_renew/tab_01.gif'
);

if ($wdgvar['id']) {
	$jwplayer_id = $wdgvar['widget_id'].$wdgvar['id'];
	$jwplayer_fixid = 'true';
} else {
	$jwplayer_id = uniqid($wdgvar['widget_id']);
	$jwplayer_fixid = 'false';
}
?>

<div class="widget-movie jwplayer" data-movie-key="<?php echo $jwplayer_id?>" style="display: inline;">
	<div id="<?php echo $jwplayer_id?>" >
		<a href="#;">
			<img src="<?php echo $option['img']?>" style="width: <?php echo $option['width']?>px; height: <?php echo $option['height']?>px" >
		</a>
	</div>
</div>


<script type='text/javascript'>
(function(doc, undefined) {
	
	var id = '<?php echo $jwplayer_id?>';
	var fixid = <?php echo $jwplayer_fixid?>;
	var muteid = id + '_mute';
	var jw = jwplayer(id);
	
	var option = {
		skin: '<?php echo $option['skin']?>',
		file: '<?php echo $option['file']?>',
		width: '<?php echo $option['width']?>',
		height: '<?php echo $option['height']?>',
		aspectratio: '<?php echo $option['aspectratio']?>',
		repeat: <?php echo $option['repeat']?>,
		autostart: true,
		controls: <?php echo $option['controls']?>,
		events: {
			onReady: function() {
				var _this = this;
				var pause = function(e) {
					var keyCode = e.keyCode ? e.keyCode : e.which;
					if (keyCode == 27) {
						_this.pause();
					}
				};
				
				if (fixid == true && typeof getCookie === 'function' && getCookie(muteid) == '1') {
					_this.setVolume(0);
				} else {
					_this.setVolume(<?php echo $option['volume'];?>);
				}
							
				if (doc.addEventListener) {
					doc.addEventListener('keyup', function(e) {
						pause(e);
					}, false);
				} else {
					doc.attachEvent('onkeyup', function(e) {
						pause(e);
					});
				}
			},
			onMute: function(e) {
				if (fixid == true) {
					if (typeof setCookie === 'function') {
						if (e.mute == true) {
							setCookie(muteid, '1', 1);
						} else {
							setCookie(muteid, '', 1);
						}
					}
				}
			},
			onPlay : function(){
				 widget_movie_allpause('<?php echo $jwplayer_id?>'); 
			}
		}
	};
	
	var playlist = [];
	<?php if (!empty($option['playlist'])) { ?>
	<?php foreach ($option['playlist'] as $file) { ?>
	playlist.push({file: '<?php echo $file?>'});
	<?php } ?>
	<?php } ?>
	
	if (playlist.length > 0) {
		option.playlist = playlist;
		delete option.file;	
	}

	if(<?=$option['autostart']?> == true){
		$("[data-movie-key=<?=$jwplayer_id?>]").addClass('play');
		jw.setup(option);
	}
	
	$("#<?=$jwplayer_id?> > a").on('click',function(){
		$("[data-movie-key=<?=$jwplayer_id?>]").addClass('play');
		jw.setup(option);
	});
	
})(document);


</script>