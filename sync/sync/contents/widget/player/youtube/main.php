<?php
/****************************************************************************************************
 * youtube 위젯
 * 사용법 : <?php getWidget('event/player/youtube', array('file' => 'https://www.youtube.com/embed/b3RovrQWPTI', 'width' => 640, 'height' => 360, 'autostart' => 1));?>
 ****************************************************************************************************/
// 옵션 정의
$option = array(
	'file' => $wdgvar['file'] ? $wdgvar['file'] : '',
	/* 'playlist' => $wdgvar['playlist'] ? $wdgvar['playlist'] : array(), */
	'width' => ($wdgvar['width'] || $wdgvar['width'] === 0) ? $wdgvar['width'] : '100%',
	'height' => $wdgvar['height'] || $wdgvar['height'] === 0 ? $wdgvar['height'] : '270',
	/* 'repeat' => $wdgvar['repeat'] ? 'true' : 'false', */
	'autostart' => $wdgvar['autostart'] ? true : false,
	/* 'volume' => $wdgvar['volume'] ? $wdgvar['volume'] : 10, */
	'img'	=> $wdgvar['img'] ? $wdgvar['img'] : 'http://image.champstudy.com/img/event/2015/14110516_renew/tab_01.gif'
);


if ($wdgvar['id']) {
	$youtube_id = $wdgvar['widget_id'].$wdgvar['id'];
	$youtube_fixid = 'true';
} else {
	$youtube_id = uniqid($wdgvar['widget_id']);
	$youtube_fixid = 'false';
}
?>

<div class="widget-movie youtube <?if($option['autostart'] ==true){ echo 'play'; }?>" data-movie-key="<?php echo $youtube_id?>" >
	<input type="hidden" name="file" value="<?php echo $option['file']?>">
	<input type="hidden" name="img" value="<?php echo $option['img']?>">
	<input type="hidden" name="width" value="<?php echo $option['width']?>">
	<input type="hidden" name="height" value="<?php echo $option['height']?>">
	<?if($option['autostart'] ==true){ //자동 재생일 경우 ?> 
		<iframe id="<?php echo $youtube_id?>" width="<?php echo $option['width']?>" height="<?php echo $option['height']?>" src="<?php echo $option['file']?>?rel=0&enablejsapi=1&autoplay=<?if($option['autostart']) echo 1;?>" frameborder="0" allowfullscreen></iframe>
	<?}else{ ?>
		<a href="#;" >
			<img src="<?php echo $option['img']?>" style="width: <?php echo $option['width']?>px; height: <?php echo $option['height']?>px" >
		</a>
		<script>
			$("[data-movie-key=<?php echo $youtube_id?>]").find('a').click(function(){
				var yobj = $("[data-movie-key=<?php echo $youtube_id?>]");
				var youtube_vidio = $('<iframe id="<?php echo $youtube_id?>" width="<?php echo $option['width']?>" height="<?php echo $option['height']?>" src="<?php echo $option['file']?>?rel=0&enablejsapi=1&autoplay=1" frameborder="0" allowfullscreen></iframe>');

				widget_movie_allpause('<?=$youtube_id?>'); //모든 영상 스탑
				$(this).remove();
				yobj.addClass('play');
		 		yobj.append(youtube_vidio);
			});
		</script>
	<? } ?>
</div>
