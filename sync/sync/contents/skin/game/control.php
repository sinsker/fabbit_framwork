<?
$sql = "select *  from fam_control";
$res = $db->query($sql);
$control = $res->fetch_array();


$start = $control['start'];
$end_date = explode(' ',$control['end_date']);
$end_date = explode(':',$end_date[1]);

$end_h = $end_date[0];
$end_m = $end_date[1];

?>

<div class="container">
	<div id="logo" class="" >
	FAM GAME
	<a href="http://sync2030.org/?f=game&v=control"  class="btn btn-danger" >컨트롤</a>
	<a href="http://sync2030.org/?f=game&v=save"  class="btn btn-default" >미션</a>
	<a href="http://sync2030.org/?f=game&v=info"  class="btn btn-default" >관리자</a>
	</div>
	
	<div style="font-size:55px;color: black;position: relative; margin-top: 30px;">
		<span data-component="timer" data-format = "timer"></span>
	</div>
	<div class="content">
		<div >
			<hr>
			종료시간 세팅  
			<select id="end_date_h">
				<?php  for($i = 0; $i < 24; $i++){?>
				<option value="<?=$i?>" <? if($i == $end_h){ echo 'selected'; }?> ><?=$i?>시</option>
				<? } ?>
			</select>
			<select  id="end_date_m">
				<?php  for($i = 0; $i < 60; $i++){?>
				<option value="<?=$i?>" <? if($i == $end_m){ echo 'selected'; }?> ><?=$i?>분</option>
				<? } ?>
			</select>
			<a href="#;" id="set_time" class="btn btn-info">확인</a>
			<hr>
			<a href="#;" id="game_start" class="btn btn-<?=$start == 1 ? 'info': 'danger'?>" value="<?=$start?>" ><?=$start == 1 ? '게임중': '정지중'?></a>
			<hr>
		 </div>
		<div class="title">Controler</div>
	</div>
	<!-- <a href="#;" class="btn btn-default btn-lg btn-block" style="border:1px solid #918888;">전체 미션 뿌리기</a> -->
	<br>
	<table class="table table-bordered">
		<tr class="" >
			<th>이름</th><th>역할</th><th>상황</th><th>코드</th><th>코드입력시간</th>
		</tr>
	</table>
	<br>
	<a href="#;" class="btn btn-danger btn-lg btn-block  reset" style="border:1px solid #918888;">전체 초기화</a>
</div>



<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal-head" aria-describedby="modal1Desc">
  <button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
  <div>
    <div id="modal-head"><h3>미션확인</h3></div>
    <p id="modal1Desc">
     	<blockquote style="color: #000000; !important;  ">
			<h4 style="text-align: left; font-size:15px;">▶ <g id="mission1"></g></h3>
			<h4 style="text-align: left; font-size:15px;">▶ <g id="mission2"></g></h3>
			<h4 style="text-align: left; font-size:15px;">▶ <g id="mission3"></g></h3>
		</blockquote>
    </p>
  </div>
  <br>
 <!--  <button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
  <button data-remodal-action="confirm" class="remodal-confirm">OK</button> -->
</div>
