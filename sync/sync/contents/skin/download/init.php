<?

$path = $_SERVER['DOCUMENT_ROOT'].'/upload/download/';
if(!is_file($path.urldecode($file))){
	goto_url('http://sync2030.org');
}

$file_info = pathinfo($path.urldecode($file));
$extension = $file_info['extension']; //파일확장자
if(in_array($extension,array('jpg','jpeg','gif','png'))){
	$img = 'img.png' ;
}else if(in_array($extension,array('xls','cvs','xlsx'))){
	$img = 'excel.png';
}else if(in_array($extension,array('zip'))){
	$img = 'zip.png';
}else if(in_array($extension,array('pdf'))){
	$img = 'pdf.png';
}else if(in_array($extension,array('ppt'))){
	$img = 'ppt.png';
}else{
	$img = 'txt.png';
}

function file_size($size) { 
      $sizes = array(" Bytes", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB"); 
      if ($size == 0) { 
           return('n/a'); 
      } else { 
           return (round($size/pow(1024, ($i = floor(log($size, 1024)))), 2) . $sizes[$i]);
      } 
 } 
 
$basename = $file_info['basename']; //파일용량
$filesize = file_size(filesize($path.urldecode($file))); //파일용량


if($_GET['down'] == 'Y'){
	$fileManager = FileManager::getInstance();
	$fileManager->fileDownload($_SERVER['DOCUMENT_ROOT'].'/upload/download/'.$file, $file);
	exit;
}
?>