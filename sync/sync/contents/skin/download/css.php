<link rel="stylesheet" type="text/css" href="<?=CSS_PATH?>/bootstrap.css" /> 
<link rel="stylesheet" href="<?=CSS_PATH?>/admin/neon-core.css">
<link rel="stylesheet" href="<?=CSS_PATH?>/admin/neon-theme.css">
<style>
.page-body .page-container .sidebar-menu {
    position: relative;
    width: 100%;
    min-height: 0px !important;
}
.page-body .page-container .sidebar-menu .logo-env {
    padding: 15px 20px;
}

body{
	overflow: hidden;
}
.page-container {
    padding-left: 0px;
    width: 100%;
}

.down-container{
	background: #ffffff;
	min-height: 556px;
    display: table;
    width: 100%;
}

.down-box{
    display: table-cell;
    vertical-align: middle;
    text-align: center;
}

.down-box > div {
	width: 230px;
	min-height: 230px; 
	display: inline-block;
    box-shadow: 2px 2px 1px 2px #5E5C5C;
}
.icon{
	padding: 5px;
}
</style>