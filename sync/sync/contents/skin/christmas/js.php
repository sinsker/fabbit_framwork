<script type="text/javascript" src="<?=JS_PATH?>/bootstrap.min.js"></script>
<!-- Scripts -->
<script src="<?=JS_PATH?>/jquery.min.js"></script>
<script src="<?=JS_PATH?>/jquery.scrolly.min.js"></script>
<script src="<?=JS_PATH?>/jquery.poptrox.min.js"></script>
<script src="<?=JS_PATH?>/skel.min.js"></script>
<script src="<?=JS_PATH?>/util.js"></script>
<!--[if lte IE 8]><script src="<?=JS_PATH?>/ie/respond.min.js"></script><![endif]-->
<script src="<?=JS_PATH?>/christmas.js"></script>
<script src="<?=JS_PATH?>/snow.js"></script>
<script>
$(document).snow({ SnowImage: "<?=IMG_PATH?>/snow.gif" });
$("form").submit(function(){
	alert("행사가 종료 되었습니다.");
	return false;	
});
</script>
