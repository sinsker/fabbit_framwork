<script>
 (function($){
	 $(function(){
		_window = $(window);
		
		_content = $(".note_content"); //메인 
		_windowHeight =_window.height();
		_navHeight = $(".sidebar-menu").length > 0 ? $(".sidebar-menu").height() : 0;  //상단 푸터 높이값

		_window.resize(resizeHight( _content, _windowHeight - _navHeight) );
		_window.load(resizeHight( 	_content, _windowHeight - _navHeight) );
		
	});
	function resizeHight(content, height){
		if($(".note_content").length > 0){ 
			content.css('min-height', height+'px');
		}
	}; 
})(jQuery);
</script>