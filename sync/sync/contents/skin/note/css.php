<link rel="stylesheet" type="text/css" href="<?=CSS_PATH?>/bootstrap.css" /> 
<link rel="stylesheet" href="<?=CSS_PATH?>/admin/neon-core.css">
<link rel="stylesheet" href="<?=CSS_PATH?>/admin/neon-theme.css">
<style>
.page-body .page-container .sidebar-menu {
    position: relative;
    width: 100%;
    min-height: 0px !important;
}
.page-body .page-container .sidebar-menu .logo-env {
    padding: 15px 20px;
}

.page-container {
    padding-left: 0px;
    width: 100%;
}
.write-pad{
    width: 100%;
    float: none;
    background: #fffced;
    position: relative;
}
.write-pad .note_content {
    font-size: 13px;
    line-height: 18px;
    color: black;
    overflow: auto;
    word-wrap: break-word;
    resize: horizontal;
    background: transparent;
    border: none;
    background: url(/sync/resources/images/admin/notes-lines.png) left top;
    background-attachment: local;
    font: 14px/30px "Noto Sans", sans-serif, serif;
    padding: 5px;
    width: 100%;
    padding-left: 40px;
 }
    
.write-pad:after {
    display: block;
    content: '';
    position: absolute;
    left: 30px;
    top: 0;
    bottom: 0;
    background: #f9d4d1;
    width: 1px;
}

body{
	overflow: hidden;
}
</style>