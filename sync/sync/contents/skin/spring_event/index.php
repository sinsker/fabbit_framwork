		<!-- Header -->
			<section id="header">
				<header>
					<h1 style="color: #fff;">1 2 / 1 9 </h1>
					<p>S Y N C 성 탄 행 사</p>
				</header>
				<footer>
					<a href="#banner" class="button style2 scrolly-middle">따뜻하게 겨울 보내는 법</a>
				</footer>
			</section>

		<!-- Banner -->
			<section id="banner">
				<header>
					<h1>SYNC 나눔천사를 모집합니다.</h1>
				</header>
				<div class="header_info">
					<div>많은 독거 노인 분들이 추위를 <span>혼자서</span> </div>
					<div>어렵게 이겨내고 있습니다.</div>
					<div>하루만 시간을 내어 함께 따뜻함을 전달해주세요.</div>
					<div>나의 참여로 인해 </div>
					<div><span style='color:#EBFF53; font-size: 14px;'>최소 한가구 이상이 따뜻한 겨울을 보낼수 있습니다.</span></div>
				</div>
				<div class="header_line">				
					<div style="text-align: left;">일시 : 12 월 19 일 오전 10시 30분</div><br/>
					<div style="text-align: left;">장소 : 4층 401 호</div> <br />
					<div style="text-align: left;">참가비 : 5000원 + [ 당일날 제출 ]</div><br/>
					<div style="text-align: left;">봉사장소 : 자양동,송정동,성수동</div><br/>
				</div>
				
				<footer>
					<a href="#step1" class="button style2 scrolly-middle">당일 프로그램 확인하기</a>
				</footer>
			</section>

		<!-- Feature 1 -->
			<article id="step1" class="container box style1 left">
				<a href="#" class="image fit"><img src="<?=IMG_PATH?>/event/christmas/step1.jpg" alt="" /></a>
				<div class="inner">
					<header>
						<h2 ><span class="step">STEP 1</span><br />
						선물 더하기 <span class="A">+</span></h2>
					</header>
					<p><div>바다를 보려면 바다를 가야 볼수 있듯이 </div>베푸는 마음도 마찬가지랍니다.<div>조원들끼리 이야기도 나누고 점심도 먹고 즐겁게 선물 포장 해봐요♥</div></p>
				</div>
				<a href="#step2" class=" scrolly-middle next_btn">다 음</a>
			</article>

		<!-- Feature 2 -->
			<article  id="step2" class="container box style1 left">
				<a href="#" class="image fit"><img src="<?=IMG_PATH?>/event/christmas/step2.jpg" alt="" /></a>
				<div class="inner">
					<header>
						<h2 ><span class="step">STEP 2</span><br />
						마음 곱하기 <span class="A">x</span></h2>
					</header>
					<p>우리가 선물만 드리는것만이 아니라 마음도 같이 전달 드려야겠죠? 자신이 맡은 담당 어르신께 마음이 담긴 편지 한통도 넣어요 ^ㅁ^</p>
				</div>
				<a href="#step3" class=" scrolly-middle next_btn">다 음</a>
			</article>
			
		<!-- Feature 3 -->
			<article class="container box style1 left" id="step3">
				<a href="#" class="image fit"><img src="<?=IMG_PATH?>/event/christmas/step3.jpg" alt="" /></a>
				<div class="inner">
					<header>
						<h2 ><span class="step">STEP 3</span><br />
						따뜻함 전하기 <span class="A">=</span></h2>
					</header>
					<p>준비 완료!! 이제 조원들과 함께 어르신들께 준비한 선물과 마음을 전달하러 출발!!</p>
				</div>
				<a href="#photo" class=" scrolly next_btn">다 음</a>
			</article>
			
		<!-- Portfolio -->
			<article class="container box style2" id="photo">
				<header>
					<h2> 독거노인 자료 보고 </h2>
					<p>대한민국 독거노인 가구의 현황<br />
				    </p>
				</header>
				<div class="inner gallery">
					<div class="row 0%">
						<div class="3u 12u(mobile)"><a href="<?=IMG_PATH?>/event/christmas/data1.jpg" class="image fit"><img src="<?=IMG_PATH?>/event/christmas/data1.jpg" alt="" title="독거노인 증가추이" /></a></div>
						<div class="3u 12u(mobile)"><a href="<?=IMG_PATH?>/event/christmas/data2.jpg" class="image fit"><img src="<?=IMG_PATH?>/event/christmas/data2.jpg" alt="" title="65세 이상 노인 빈곤률" /></a></div>
						<div class="3u 12u(mobile)"><a href="<?=IMG_PATH?>/event/christmas/data3.jpg" class="image fit"><img src="<?=IMG_PATH?>/event/christmas/data3.jpg" alt="" title="여러분의 작은 움직임이 필요합니다." /></a></div>
						<div class="3u 12u(mobile)"><a href="<?=IMG_PATH?>/event/christmas/data4.jpg" class="image fit"><img src="<?=IMG_PATH?>/event/christmas/data4.jpg" alt="" title="우린 주님안에서 모두 같은 자녀입니다." /></a></div>
						<!-- <div class="3u 12u(mobile)"><a href="<?=IMG_PATH?>/fulls/03.jpg" class="image fit"><img src="<?=IMG_PATH?>/thumbs/03.jpg" alt="" title="Raven" /></a></div>
						<div class="3u 12u(mobile)"><a href="<?=IMG_PATH?>/fulls/04.jpg" class="image fit"><img src="<?=IMG_PATH?>/thumbs/04.jpg" alt="" title="I'll have a cup of Disneyland, please" /></a></div>
						 -->
					</div>
					<!--<div class="row 0%">
						 <div class="3u 12u(mobile)"><a href="<?=IMG_PATH?>/event/christmas/data3.jpg" class="image fit"><img src="<?=IMG_PATH?>/event/christmas/data3.jpg" alt="" title="여러분의 작은 움직임이 필요합니다." /></a></div>
						<div class="3u 12u(mobile)"><a href="<?=IMG_PATH?>/event/christmas/data4.jpg" class="image fit"><img src="<?=IMG_PATH?>/event/christmas/data4.jpg" alt="" title="Different." /></a></div>
						<div class="3u 12u(mobile)"><a href="<?=IMG_PATH?>/fulls/07.jpg" class="image fit"><img src="<?=IMG_PATH?>/thumbs/07.jpg" alt="" title="History was made here" /></a></div>
						<div class="3u 12u(mobile)"><a href="<?=IMG_PATH?>/fulls/08.jpg" class="image fit"><img src="<?=IMG_PATH?>/thumbs/08.jpg" alt="" title="People come and go and walk away" /></a></div>
					</div> -->
				</div>
			</article>

		<!-- Contact -->
		<article class="container box style3">
			<header>
				<h2>참가 신청서</h2>
				<p>2015 SYNC 성탄 행사 신청서</p>
			</header>
			<form method="post" action="<?=EVENT_PATH?>/christmas.process.php" target="process" >
				<div class="row 50%">
					<div class="6u 12u$(mobile)"><input type="text" class="text" name="name" placeholder="이름" /></div>
					<div class="6u$ 12u$(mobile)"><input type="text" class="text" name="gajang" placeholder="가정 섬김이 이름" /></div>
					<div class="6u$ 12u$(mobile)"><input type="text" class="text" name="tel" placeholder="전화번호 ( - 없이 입력 )" /></div>
					<!-- <div class="12u$">
						<textarea name="message" placeholder="Message"></textarea>
					</div> -->
					<div class="12u$">
						<ul class="actions">
							<li><input type="submit" value="참여하기" /></li>
						</ul>
					</div>
				</div>
			</form>
		</article>

		<!-- Generic -->
		<!--
			<article class="container box style3">
				<header>
					<h2>Generic Box</h2>
					<p>Just a generic box. Nothing to see here.</p>
				</header>
				<section>
					<header>
						<h3>Paragraph</h3>
						<p>This is a subtitle</p>
					</header>
					<p>Phasellus nisl nisl, varius id <sup>porttitor sed pellentesque</sup> ac orci. Pellentesque
					habitant <strong>strong</strong> tristique <b>bold</b> et netus <i>italic</i> malesuada <em>emphasized</em> ac turpis egestas. Morbi
					leo suscipit ut. Praesent <sub>id turpis vitae</sub> turpis pretium ultricies. Vestibulum sit
					amet risus elit.</p>
				</section>
				<section>
					<header>
						<h3>Blockquote</h3>
					</header>
					<blockquote>Fringilla nisl. Donec accumsan interdum nisi, quis tincidunt felis sagittis eget.
					tempus euismod. Vestibulum ante ipsum primis in faucibus.</blockquote>
				</section>
				<section>
					<header>
						<h3>Divider</h3>
					</header>
					<p>Donec consectetur <a href="#">vestibulum dolor et pulvinar</a>. Etiam vel felis enim, at viverra
					ligula. Ut porttitor sagittis lorem, quis eleifend nisi ornare vel. Praesent nec orci
					facilisis leo magna. Cras sit amet urna eros, id egestas urna. Quisque aliquam
					tempus euismod. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices
					posuere cubilia.</p>
					<hr />
					<p>Donec consectetur vestibulum dolor et pulvinar. Etiam vel felis enim, at viverra
					ligula. Ut porttitor sagittis lorem, quis eleifend nisi ornare vel. Praesent nec orci
					facilisis leo magna. Cras sit amet urna eros, id egestas urna. Quisque aliquam
					tempus euismod. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices
					posuere cubilia.</p>
				</section>
				<section>
					<header>
						<h3>Unordered List</h3>
					</header>
					<ul class="default">
						<li>Donec consectetur vestibulum dolor et pulvinar. Etiam vel felis enim, at viverra ligula. Ut porttitor sagittis lorem, quis eleifend nisi ornare vel.</li>
						<li>Donec consectetur vestibulum dolor et pulvinar. Etiam vel felis enim, at viverra ligula. Ut porttitor sagittis lorem, quis eleifend nisi ornare vel.</li>
						<li>Donec consectetur vestibulum dolor et pulvinar. Etiam vel felis enim, at viverra ligula. Ut porttitor sagittis lorem, quis eleifend nisi ornare vel.</li>
						<li>Donec consectetur vestibulum dolor et pulvinar. Etiam vel felis enim, at viverra ligula. Ut porttitor sagittis lorem, quis eleifend nisi ornare vel.</li>
					</ul>
				</section>
				<section>
					<header>
						<h3>Ordered List</h3>
					</header>
					<ol class="default">
						<li>Donec consectetur vestibulum dolor et pulvinar. Etiam vel felis enim, at viverra ligula. Ut porttitor sagittis lorem, quis eleifend nisi ornare vel.</li>
						<li>Donec consectetur vestibulum dolor et pulvinar. Etiam vel felis enim, at viverra ligula. Ut porttitor sagittis lorem, quis eleifend nisi ornare vel.</li>
						<li>Donec consectetur vestibulum dolor et pulvinar. Etiam vel felis enim, at viverra ligula. Ut porttitor sagittis lorem, quis eleifend nisi ornare vel.</li>
						<li>Donec consectetur vestibulum dolor et pulvinar. Etiam vel felis enim, at viverra ligula. Ut porttitor sagittis lorem, quis eleifend nisi ornare vel.</li>
					</ol>
				</section>
				<section>
					<header>
						<h3>Table</h3>
					</header>
					<div class="table-wrapper">
						<table class="default">
							<thead>
								<tr>
									<th>ID</th>
									<th>Name</th>
									<th>Description</th>
									<th>Price</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>45815</td>
									<td>Something</td>
									<td>Ut porttitor sagittis lorem quis nisi ornare.</td>
									<td>29.99</td>
								</tr>
								<tr>
									<td>24524</td>
									<td>Nothing</td>
									<td>Ut porttitor sagittis lorem quis nisi ornare.</td>
									<td>19.99</td>
								</tr>
								<tr>
									<td>45815</td>
									<td>Something</td>
									<td>Ut porttitor sagittis lorem quis nisi ornare.</td>
									<td>29.99</td>
								</tr>
								<tr>
									<td>24524</td>
									<td>Nothing</td>
									<td>Ut porttitor sagittis lorem quis nisi ornare.</td>
									<td>19.99</td>
								</tr>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="3"></td>
									<td>100.00</td>
								</tr>
							</tfoot>
						</table>
					</div>
				</section>
				<section>
					<header>
						<h3>Form</h3>
					</header>
					<form method="post" action="#">
						<div class="row">
							<div class="6u">
								<input class="text" type="text" name="name" id="name" value="" placeholder="John Doe" />
							</div>
							<div class="6u">
								<input class="text" type="text" name="email" id="email" value="" placeholder="johndoe@domain.tld" />
							</div>
						</div>
						<div class="row">
							<div class="12u">
								<select name="department" id="department">
									<option value="">Choose a department</option>
									<option value="1">Manufacturing</option>
									<option value="2">Administration</option>
									<option value="3">Support</option>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="12u">
								<input class="text" type="text" name="subject" id="subject" value="" placeholder="Enter your subject" />
							</div>
						</div>
						<div class="row">
							<div class="12u">
								<textarea name="message" id="message" placeholder="Enter your message"></textarea>
							</div>
						</div>
						<div class="row">
							<div class="12u">
								<ul class="actions">
									<li><input type="submit" value="Submit" /></li>
									<li><input type="reset" class="style3" value="Clear Form" /></li>
								</ul>
							</div>
						</div>
					</form>
				</section>
			</article>
		-->

		<section id="footer">
			<ul class="icons">
				<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
				<li><a href="#" class="icon fa-facebook"><span class="label">C</span></a></li>
				<li><a href="#" class="icon fa-google-plus"><span class="label">SYNC</span></a></li>
				<!-- 
				<li><a href="#" class="icon fa-pinterest"><span class="label">Pinterest</span></a></li>
				<li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
				<li><a href="#" class="icon fa-linkedin"><span class="label">LinkedIn</span></a></li>
				 -->
			</ul>
			<div class="copyright">
				<ul class="menu">
					<li>&copy;2015 SYNC All rights reserved.</li></li>
				</ul>
			</div>
		</section>
		 <audio hidden name="media"  class="dis1_1" autoplay="autoplay"><source src="<?=RES_PATH?>/j.mp3" type="audio/mp3" ></audio>
		<iframe name ="process" id = "process" width="0" height="0" ></iframe>