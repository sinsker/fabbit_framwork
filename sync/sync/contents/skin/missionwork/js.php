<script type="text/javascript" src="<?=JS_PATH?>/bootstrap.min.js"></script>
<script type="text/javascript" src="<?=JS_PATH?>/jquery.remodal.js"></script>

<script src="<?=CUR_DIR?>/resource/assets/js/jquery.min.js"></script>
<script src="<?=CUR_DIR?>/resource/assets/js/jquery.scrolly.min.js"></script>
<script src="<?=CUR_DIR?>/resource/assets/js/jquery.dropotron.min.js"></script>
<script src="<?=CUR_DIR?>/resource/assets/js/jquery.scrollex.min.js"></script>
<script src="<?=CUR_DIR?>/resource/assets/js/skel.min.js"></script>
<script src="<?=CUR_DIR?>/resource/assets/js/util.js"></script>
<!--[if lte IE 8]><script src="<?=CUR_DIR?>/resource/assets/js/ie/respond.min.js"></script><![endif]-->
<script src="<?=CUR_DIR?>/resource/assets/js/main.js"></script>

<script>
$(".submit-btn").click(function(){
	var name = $("input#name").val();
	var giso = $("#giso").val();
	var tel1 = $("input#tel1").val();
	var tel2 = $("input#tel2").val();
	var tel3 = $("input#tel3").val();
	var fam	 = $("input#fam").val();

	if(name == ''){
		alert('이름을 입력해 주세요.');
		$("input#name").focus();
		return false;
	}

	if(giso == ''){
		alert('기수를 선택해 주세요.');
		$("input#giso").focus();
		return false;
	}

	if(tel1 == '' || tel2 == ''  || tel3 == '' ){
		alert('전화번호를 정확히 기입해 주세요.');
		$("input#tel1").focus();
		return false;
	}

	if(fam == ''){
		alert('가정이름을 입력해 주세요.');
		$("input#fam").focus();
		return false;
	}
	evt_data_insert(evt_id, $("#proc-submit").serialize() );
});

$("#tel1").keyup(function(event){
	if($(this).val().length == 3 && (event.which >= 48 && event.which <=57) ){
		$("#tel2").focus();
	}
});

$("#tel2").keyup(function(event){
	if($(this).val().length == 4 && (event.which >= 48 && event.which <=57)){
		$("#tel3").focus();
	}
});


</script>