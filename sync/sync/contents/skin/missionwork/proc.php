<?
Class syncEventDataAccess extends EventDataAccess {
	
		function eventInsertData($data){
			
			$tel = $data['tel1'].'-'.$data['tel2'].'-'.$data['tel3'];
			
			$sql = "insert into $this->dataTable set 
					pkey = '$this->evt_id',
					name = '{$data['name']}',
					tel = '{$tel}',
					wr1 = '".$data['giso']."',
					wr2 = '".$data['fam']."',
					wdate = '".TIME_FULL."',
					wip = '".ACCESS_IP."',
					wagent = '".ACCESS_AGENT."'";
			
			return $this->DBObject->query($sql);	
		}
		
		function isDataDuplication($data){
			$resultCheck = false;
			
			$tel = $data['tel1'].'-'.$data['tel2'].'-'.$data['tel3'];
			
			$sql = "select count(*) as cnt from $this->dataTable where pkey = '$this->evt_id' and tel = '{$tel}' ";
			
			$res = $this->DBObject->query($sql);
			$row = $res->fetch_array();
			
			if($row['cnt'] > 0){
				$resultCheck = true;
			}
			
			
			return $resultCheck;
		}
		
		function eventMassgeSetting(){
			$this->msg->setSuccess("신청이 완료되었습니다.");
			$this->msg->setFail("신청이 실패하였습니다.");
			$this->msg->setDuplication("이미 신청하셨습니다.");
		}
}

?>