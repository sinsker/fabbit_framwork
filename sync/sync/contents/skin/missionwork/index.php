<div id="page-wrapper">
	<!-- Header -->
	<header id="header">
		<h1 id="logo"><a href="#" >SYNC</a></h1>
		<nav id="nav" >
			<ul>
				<li><a href="#one" class="scrolly">국내 선교 소개</a></li>
				<li>
					<a href="#two" class="scrolly">프로그램 일정</a>
					<ul>
						<li><a href="#two" class="scrolly">Nanum - 나눔</a></li>
						<li><a href="#two" class="scrolly">Worship - 예배</a></li>
						<li><a href="#two" class="scrolly">Light - 빛</a></li>
					</ul>
				</li>
				<li>
					<a href="#five" class="scrolly">국내선교 신청안내</a>
					<ul>
						<li><a href="#five" class="scrolly">국내선교 참가 신청</a></li>
						<li><a href="#five" class="scrolly">국내선교 참가비 계좌번호</a></li>
						<li><a href="#five" class="scrolly">선교 문의</a></li>
					</ul>
				</li>
			</ul>
		</nav>
	</header>
	<!-- Banner -->
	<section id="banner">
		<div class="content">
			<header class="major">
				<h2 id="userText">2016 SYNC 국내 선교 <br>"너로 말미암아"</h2>
			</header>
			<p>어찌구 저찌구 어찌구 저찌구어찌구 저찌구어찌구 저찌구어찌구 저찌구어찌구 저찌구어찌구 저찌구어찌구 저찌구어찌구 저찌구어찌구 저찌구어찌구 저찌구</p>
			<ul class="actions fit" style=" text-align: center;">
				<li><a href="#" class="button fit">선교소개</a></li>
				<li><a href="#" class="button fit">일정안내</a></li>
				<li><a href="#" class="button fit special">선교신청</a></li>
			</ul>
			<a href="#one" class="goto-next scrolly" style="bottom: -30px;">Next</a>
		</div>
	</section>
	<!-- One -->
	<section id="one" class="wrapper fade-up wp">
		<div class="content">
			<header  style="text-align: center;">
				<h2>국내 선교 소개</h2>
				<p>이번 국내 선교는 대관령입니다.</p>
				<span class="icon alt major fa-area-chart" style="font-size: 20px;  margin: 0 auto; display: inherit;"></span>
				<br>
			</header>
			<div class="container">
			<p>이번 국내 선교는 대관령입니다.이번 국내 선교는 대관령입니다.
			이번 국내 선교는 대관령입니다.이번 국내 선교는 대관령입니다.이번
			 국내 선교는 대관령입니다.이번 국내 선교는 대관령입니다.이번 국내 
			 선교는 대관령입니다.이번 국내 선교는 대관령입니다.이번 국내 선교는
			  대관령입니다.이번 국내 선교는 
			대관령입니다.이번 국내 선교는 대관령입니다.</p>
			</div>
		</div>
		<a href="#two" class="goto-next scrolly">Next</a>
	</section>

	<!-- Two -->
	<section id="two" class="wrapper  fade-up" >
		<div class="content">
			<div class="container">
				<header class="major">
					<h2>국내 선교 프로그램 일정</h2>
					<p>일정안내일정안내 일정안내일정안내 일정안내일정안내 일정안내일정안내 </p>
				</header>
				<!-- Content -->
				<section id="content">
					<div class="table-wrapper">
						<table>
							<thead>
								<tr>
									<th>시간</th>
									<th>내용</th>
									<th>장소</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>09:00~11:00</td>
									<td>아침식사</td>
									<td>강당</td>
								</tr>
								<tr>
									<td>11:00~13:00</td>
									<td>물놀이</td>
									<td>병원</td>
								</tr>
								<tr>
									<td>13:00~15:00</td>
									<td>아침식사</td>
									<td>강당</td>
								</tr>
								<tr>
									<td>15:00~19:00</td>
									<td>집회</td>
									<td>휘진이형네</td>
								</tr>
							</tbody>
						</table>
					</div>
				</section>
				<ul class="actions">
					<li style="display: block;"><a href="#" class="button fit big">일정 상세히 보기</a></li>
				</ul>
			</div>
			<a href="#five" class="goto-next scrolly">Next</a>
		</div>
	</section>

<!-- 
	<section id="four" class="wrapper style1 special fade-up">
		<div class="container">
			<header class="major">
				<h2>국내 선교 프로그램 일정</h2>
				<p>큰 영역별로 안내 해드립니다.~^^</p>
			</header>
			<div class="box alt">
				<div class="row uniform">
					<section class="4u 6u(medium) 12u$(xsmall)">
						<span class="icon alt major fa-area-chart"></span>
						<h3>선교 전략 세우기</h3>
						<p>선교 전략 세우기선교 전략 세우기선교 전략 세우기선교 전략 세우기선교 전략 세우기</p>
					</section>
					<section class="4u 6u$(medium) 12u$(xsmall)">
						<span class="icon alt major fa-comment"></span>
						<h3>함께 나누기</h3>
						<p>함께 나누기함께 나누기함께 나누기함께 나누기함께 나누기함께 나누기</p>
					</section>
					<section class="4u$ 6u(medium) 12u$(xsmall)">
						<span class="icon alt major fa-flask"></span>
						<h3>아이템 제작</h3>
						<p>아이템 제작아이템 제작아이템 제작아이템 제작아이템 제작아이템 제작</p>
					</section>
					<section class="4u 6u$(medium) 12u$(xsmall)">
						<span class="icon alt major fa-paper-plane"></span>
						<h3>곳곳에 보내기</h3>
						<p>곳곳에 보내기곳곳에 보내기곳곳에 보내기곳곳에 보내기곳곳에 보내기곳곳에 보내기곳곳에 보내기</p>
					</section>
					<section class="4u 6u(medium) 12u$(xsmall)">
						<span class="icon alt major fa-file"></span>
						<h3>예배로 더 후끈!</h3>
						<p>예배로 더 후끈!예배로 더 후끈!예배로 더 후끈!예배로 더 후끈!예배로 더 후끈!예배로 더 후끈!예배로 더 후끈!</p>
					</section>
					<section class="4u$ 6u$(medium) 12u$(xsmall)">
						<span class="icon alt major fa-lock"></span>
						<h3>서로 친해지는 시간</h3>
						<p>서로 친해지는 시간서로 친해지는 시간서로 친해지는 시간서로 친해지는 시간서로 친해지는 시간서로 친해지는 시간서로 친해지는 시간</p>
					</section>
				</div>
			</div>
			<footer class="major">
				<ul class="actions">
					<li><a href="#" class="button">일정 상세히 보기</a></li>
				</ul>
			</footer>
		</div>
	</section>
-->
<!-- Five -->
	<section id="five" class="wrapper style2 special fade-up ">
		<div class="container">
			<header>
				<h2>국내 선교 참가 신청</h2>
				<p>참가 신청을 온라인으로 제출해주세요~^^</p>
			</header>
			<form method="post" action="#" id="proc-submit">
				<div class="field">
					<label for="name">이름</label>
					<input type="text" name="name" id="name" />
				</div>
				<div class="field">
					<label for="giso">기수</label>
					<select name="giso" id="giso">
						<option value="">선택해주세요</option>
						<? for($i = 80; $i <= 99; $i++){?>
							<option value="<?=$i?>기"><?=$i?>기</option>
						<? }?>
					</select>
				</div>
				<div class="field" style=" display: inline-block;margin: 0px;width: 100%;text-align: left;">
						<label for="tel">전화번호</label>
						<input type="tel" name="tel1" id="tel1" class="tel" maxlength="3" />
						<div class="tel_sub"> - </div>
						<input type="tel" name="tel2" id="tel2" class="tel" maxlength="4"/>
						<div class="tel_sub"> - </div>
						<input type="tel" name="tel3" id="tel3" class="tel" maxlength="4"/>
				</div>
				<div class="field">
					<label for="fam">가정이름</label>
					<input type="text" name="fam" id="fam" />
				</div>
				<br>
				<ul class="actions">
					<li><a href="#;" class="button icon fa-download submit-btn">신청하기</a></li>
				</ul>
			</form>
		</div>
		<hr>
		<section>
			<h3>국내선교참가비 계좌번호</h3>
			<div class="container">
				<blockquote class="ggg"  >
					신한 010-1237832-1238794 <br>
					예금주 : 전휘진
				</blockquote>
			</div>
		</section>
	</section>
	
	<section class="wrapper  special fade-up ">
		<div class="container">
			
			<header class="major">
				<h2>문의사항</h2>
				<p>아래 내용을 확인해주세요</p>
			</header>
			<ul class="contact">
				<li class="">
					1공동체 회계 <strong>안수진</strong>에게 문의 주세요
				</li>
				<li class="">카카오톡 아이디 : zxzxzczv@naver.com</li>
				<li class="">010-000-0000</li>
				<li class=""><a href="#">information@sync.org</a></li>
			</ul>
		</div>
	</section>
<!-- Footer -->
	<footer id="footer">
		<ul class="icons">
			<li><a href="#" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
			<li><a href="#" class="icon alt fa-linkedin"><span class="label">LinkedIn</span></a></li>
			<li><a href="#" class="icon alt fa-instagram"><span class="label">Instagram</span></a></li>
			<li><a href="#" class="icon alt fa-github"><span class="label">GitHub</span></a></li>
			<li><a href="#" class="icon alt fa-envelope"><span class="label">Email</span></a></li>
		</ul>
		<ul class="copyright">
			<li> I  LOVE SYNC ♥</li><li>싱크는 당신과 함께 해서 행복합니다.</li>
		</ul>
	</footer>
</div>
