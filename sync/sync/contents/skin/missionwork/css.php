<link href='http://fonts.googleapis.com/earlyaccess/jejugothic.css' rel='stylesheet' type='text/css'>

<link rel="stylesheet" type="text/css" href="<?=CSS_PATH?>/bootstrap.css" /> 
<link rel="stylesheet" type="text/css" href="<?=CSS_PATH?>/jquery.remodal.css">

<!--[if lte IE 8]><script src="<?=CUR_DIR?>/resource/assets/js/ie/html5shiv.js"></script><![endif]-->
<link rel="stylesheet" href="<?=CUR_DIR?>/resource/assets/css/main.css" />
<!--[if lte IE 9]><link rel="stylesheet" href="<?=CUR_DIR?>/resource/assets/css/ie9.css" /><![endif]-->
<!--[if lte IE 8]><link rel="stylesheet" href="<?=CUR_DIR?>/resource/assets/css/ie8.css" /><![endif]-->
<style>

.ggg{
		text-align: center;
	}
	
@media screen and (max-width: 736px){
	#titleBar .title a{
		font-size: 45px;
	    padding: 10px;
	    display: inline-block;
	}
	
	.ggg{
	text-align: left;
	}
	
}

#logo > a{
	padding: 30px;
	display: inline-block; 
	font-size: 70px;
}
body {
	font-family: 'Jeju Gothic', serif !important;

}
input[type="tel"].tel{
	width: 30%;
    height: 2.75em;
    display: inline-block;
    
    -moz-appearance: none;
    -webkit-appearance: none;
    -ms-appearance: none;
    appearance: none;
    -moz-transition: border-color 0.2s ease-in-out;
    -webkit-transition: border-color 0.2s ease-in-out;
    -ms-transition: border-color 0.2s ease-in-out;
    transition: border-color 0.2s ease-in-out;
    background: transparent;
    border-radius: 4px;
    border: solid 1px rgba(255, 255, 255, 0.3);
    color: inherit;
    outline: 0;
    padding: 0 1em;
    text-decoration: none;
}

.tel_sub{
    width: 1%;
   display: inline-block;
}

.tel_box{

}

.major{
	    font-size: 25px;
}

label{
    color: #ffffff;
    display: block;
    font-size: 0.9em;
    font-weight: 300;
    margin: 1em 0 0em 0;
    text-align: left;
}

.wp{
	 background: whitesmoke;
    color: black !important;
}

.wp h2, .wp p{
	color: black !important;
}

.wp header.major:after {
    background: #272833;
    content: '';
    display: inline-block;
    height: 0.2em;
    max-width: 20em;
    width: 75%;
</style>