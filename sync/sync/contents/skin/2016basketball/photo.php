
<style>
<? if($device['isMobile'] == 'Y'){?>
	#banner.bg-bask {
		background-image: url("/sync/resources/images/2016basketball/rei3.jpg");
		background-attachment: fixed;
		background-repeat: no-repeat;
		background-size: 100% auto; 
		background-position: top;
		text-align: center;
	}
<?}else{?>
	#banner.bg-bask {
		background-image: url("/sync/resources/images/2016basketball/rei.jpg");
		background-attachment: fixed;
		background-repeat: no-repeat;
		background-size: cover;
		background-position: top;
		text-align: center;
	}
<?}?>
</style>
<header id="header" class="alt">
	<h1 id="logo"><a href="">S Y N C  성락성결교회 청년부</a></h1>
</header>
<section id="banner" class="bg-bask">
	<div class="inner">
		<p >2016 SYNC배</p>
		<header>
			<h2 id='b-header'>농구 토너먼트</h2>
		</header>
		<div class="banner-content">
			농구대회 현장 사진들을 아래 날짜별로 확인하실수 있습니다.
		</div>
		<footer>
			<ul class="buttons vertical">
				<li><a href="javascript:history.go(-1);" class="button ">메인으로 돌아가기</a></li>
			</ul>
		</footer>
	</div>
</section>

<article id="image-main">

	<!-- 갤러리 정보 -->
	<section id="image-info" class="wrapper style2 ">
		<div class="wrap small-width">
			<ul class="buttons vertical" style="text-align: center;">
				<li><a href="#image-list" class="button scrolly gallery" data-folder-name="0410">4월 10일 경기 사진</a></li>
				<li><a href="#image-list" class="button scrolly gallery " data-folder-name="0417">4월 17일 경기 사진</a></li>
				<li><a href="#image-list" class="button scrolly gallery " data-folder-name="0424">4월 24일 경기 사진</a></li>
			</ul>
		</div>
	</section>
	
	<!-- 갤러리 -->
	<section id="image-list" class="wrapper style1 container special">
		<hr>
		<div class="wrap small-width g-box">
			<ul id="box-container" class="g folder-0410" display="none;">
				<?
				$handler = opendir($_SERVER['DOCUMENT_ROOT'].'/'.SKIN_PATH.'/2016basketball/file/0410/th');
				while ($fi = readdir($handler)) { 
		        	if ($fi == '.' || $fi == '..' ) continue; ?>
					<li class="box">
						<a href="<?=SKIN_PATH?>/2016basketball/file/0410/<?=$fi?>" class="swipebox" title="">
							<img src="<?=SKIN_PATH?>/2016basketball/file/0410/th/<?=$fi?>" alt="image">
						</a>
					</li>
				<?}?>
			</ul>
		</div>
	</section>
	<!-- Footer -->
	<footer id="footer">

		<ul class="buttons vertical">
			<li><a href="javascript:history.go(-1);" class="button">메인으로 돌아가기</a></li>
		</ul>
			
		<ul class="icons">
			<li><a href="javascript:share_facebook('http://sync2030.org/?f=2016basketball')" class="icon circle fa-facebook"><span class="label">Facebook</span></a></li>
			<li><a href="javascript:share_url('http://sync2030.org/?f=2016basketball')" class="icon circle fa-dribbble"><span class="label">Dribbble</span></a></li>
		</ul>

		<ul class="copyright">
			<li>&copy; 2016 SYNC | 성락성결교회 </li><li></li>
		</ul>
	</footer>
</div>
