<script type="text/javascript" src="<?=JS_PATH?>/bootstrap.min.js"></script>
<!--[if lte IE 8]><script src="<?=JS_PATH?>/js/ie/html5shiv.js"></script><![endif]-->
<script type="text/javascript" src="<?=JS_PATH?>/jquery.remodal.js"></script>
 <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js" type="text/javascript"></script>
<script src="<?=JS_PATH?>/jquery.dropotron.min.js"></script>
<script src="<?=JS_PATH?>/jquery.scrolly.min.js"></script>
<script src="<?=JS_PATH?>/modal/moaModal-minified.js"></script>
<script src="<?=JS_PATH?>/2016basketball/ios-orientationchange-fix.js"></script>
<script src="<?=JS_PATH?>/2016basketball/jquery.swipebox.min.js"></script>
<script src="<?=JS_PATH?>/2016basketball/jquery.touchSwipe.js"></script>

<script>
<?if(empty($curData['end'])){?>
	timer('<?=$curData['a_date']?>','<?=$curData['e_date']?>');
<?}?>

$(function(){
	
	setTimeout(function(){  playAnim('b-header','flash'); }, 1000 );
	setTimeout(function(){  playAnim('b-header','flash'); }, 3000 );

	 $('#a-view').click(function(){
         $('#a-modal').modal('view', {
        	 position : '35px auto',
             animation : 'top',
             easing : 'easeOutBounce',
			 overlayColor : '#000',
             overlayOpacity : .9,
             speed : 500,
         });
     });
     
	 $('#b-view').click(function(){
         $('#b-modal').modal('view', {
        	 position : '35px auto',
             animation : 'top',
             easing : 'easeOutBounce',
			 overlayColor : '#000',
             overlayOpacity : .9,
             speed : 500,
         });
     });
     
	$('.scrolly').scrolly({
		speed: 1000,
		offset: -10
	});

	$('#nav > ul').dropotron({
		mode: 'fade',
		noOpenerFade: true,
	});

	/* Basic Gallery */
	$( '.swipebox' ).swipebox({
		hideBarsDelay : 5000, // delay before hiding bars on desktop
	});

});

function murl(url){
	if(isMobile == 'Y'){
		if(confirm("WIFI가 아닌 3G/4G 사용시 데이터 요금이 부과 될수 있습니다. 접속하시겠습니까?") == true ){
			location.href= url;
		}
	}else{
		location.href= url;
	}
}

</script>