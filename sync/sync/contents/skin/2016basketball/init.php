<?

$curData = getCurrentData(TIME_FULL);

function getCurrentData($date){

	$game = array(
			array( 'a_date'=>'2015-04-10 16:00', 'e_date'=>'2016-04-10 16:00', 'title'=>'A조 1,2 경기 / B조 1,2 경기' ),
			array( 'a_date'=>'2016-04-10 17:10', 'e_date'=>'2016-04-17 16:00', 'title'=>'A조 결승 / B조 결승' ),
			array( 'a_date'=>'2016-04-17 17:10', 'e_date'=>'2016-04-24 16:00', 'title'=>'최종 결승 / 3,4위전' ),
			array( 'a_date'=>'2016-04-24 17:10', 'e_date'=>'2016-04-24 17:11', 'title'=>'최종 결승 / 3,4위전' )
	);
	$curData = '';
	foreach ($game as $key => $val ){
		if($val['a_date'] <= $date && $val['e_date'] >= $date){
			return $val;
		}

		if($date >= $val['e_date']){
			array_shift($game);
		}
	}
	if(count($game) > 0){
		$curData['end'] = '경기 시합중';
	}else{
		$curData['end'] = '토너먼트가 종료 되었습니다.';
		$curData['ew'] = 'Y';
	}

	return $curData;
}


?>