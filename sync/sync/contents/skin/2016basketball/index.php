
<style>
<? if($device['isMobile'] == 'Y'){?>
	#banner.bg-bask {
		background-image: url("/sync/resources/images/2016basketball/rei3.jpg");
		background-attachment: fixed;
		background-repeat: no-repeat;
		background-size: 100% auto; 
		background-position: top;
		text-align: center;
	}
<?}else{?>
	#banner.bg-bask {
		background-image: url("/sync/resources/images/2016basketball/rei.jpg");
		background-attachment: fixed;
		background-repeat: no-repeat;
		background-size: cover;
		background-position: top;
		text-align: center;
	}
<?}?>
</style>
	<header id="header" class="alt">
		<h1 id="logo"><a href="">S Y N C  성락성결교회 청년부</a></h1>
		<!-- <nav id="nav">
			<ul>
				<li class="current ">
					<a href="#main;" class="button" >상품</a>
				</li>
				<li >
					<a href="#vs;" class="button">대진표</a>
				</li>
				<li >
					<a href="#cta;" class="button">결과표</a>
				</li>
			</ul>
		</nav> -->
	</header>
<!-- Banner -->
	<section id="banner" class="bg-bask">
		<div class="inner">
			<p >2016 SYNC배</p>
			<header>
				<h2 id='b-header'>농구 토너먼트</h2>
			</header>
			<div class="banner-content">
				<p>최종우승 : 경민가정</p>
				<p>2위 : 기주가정</p>
				<p>3위 : 세은 + 예승가정</p>
				<p>4위 : 휘진 + 지현가정</p>
				<p>이벤트 당첨자</p>
				<p>한효정 | 권희원 | 한현희 | 기여운</p>
			</div>
			<footer>
				<ul class="buttons vertical">
					<li><a href="#main" class="button fit scrolly">우승팀 확인</a></li>
					<li><a href="javascript:murl('?f=2016basketball&v=photo');" class="button fit scrolly">농구대회 갤러리</a></li>
				</ul>
			</footer>
		</div>
	</section>

<!-- Main -->
	
	<article id="main" style="    padding: 20px 0 0 0;">
		<header class=" container" style="text-align: center;">
				<div>최종 우승 팀은 <span style="color:#21A9E1;"><i class=" fa fa-dribbble fa-spin "></i>경민 가정<i class=" fa fa-dribbble fa-spin"></i></span>
				입니다.</div> 
				<div>축하드립니다!!</div>
				<hr>
				<div>
					<img src="<?=SKIN_PATH?>/2016basketball/file/0424/KakaoTalk_20160424_221525548.jpg" alt="image" style=" width: 100%; border: 2px solid #757575; border-radius: 15px;">
				</div>
		</header>
		<!-- One -->
			<section class="wrapper style2 container special-alt">
				<div class="row  ">
					<div class="8u 12u(narrower)"  style="    width: 100%; ">
						<div class="4u 12u(narrower) important(narrower)" style=" width: 100%;">
						<span class="icon fa-gift featured">
						</span>
						</div>
						<header style="text-align: center;">
							<h2>우승 상품 & 이벤트</h2>
						</header>
						<div style="text-align: center;">1등 - 가정 지원비 5만원 + USB허브 5개</div>
						<div style="text-align: center;">2등 - 가정 지원비 3만원 + USB허브 3개</div>
						<div style="text-align: center;">3등 - 가정 지원비 2만원 + USB허브 2개</div>
						<div style="text-align: center;">매 경기 이벤트 - 베아뚜스 상품권 5000원</div>
						<div style="font-size: 14px;color: black; text-align: center;" >※ 현장에서 선수를 제외한 인원중 <br>2점슛 이벤트 통해 베아뚜스 상품권을 드립니다.</div>
						<br>
						<footer>
							<ul class="buttons"  style="text-align: center;">
								<li><a href="#vs" class="button scrolly">대진표</a></li>
							</ul>
						</footer>
				
					</div>
				</div>
			</section>

		<!-- Two -->
			<section class="wrapper style1 container special" id="vs">
				<div class="row">
					<div class="4u 12u(narrower)" style="    width: 100%; ">
						<section>
							<span class="icon featured fa-flag-o"></span>
							<header>
								<h3>A조 토너먼트 대진표 <a  href="#;" class="btn btn-info" id="a-view">상세보기</a></h3> 
							</header>
							<table class="table">
								<tr>
									<td>#</td><td>팀</td><td>상태</td>
								</tr>
								<tr class = "lose">
									<td>1</td>
									<td>세은+예승</td>
									<td>3위</td>
								</tr>
								<tr>
									<td>4</td>
									<td>경민</td>
									<td>우승팀</td>
								<tr class = "lose">
									<td>2</td>
									<td>민우+다솜</td>
									<td>OUT</td>
								</tr>
								<tr class = "lose">
									<td>3</td>
									<td>소망+나영</td>
									<td>OUT</td>
								</tr>
								</tr>
							</table>
						</section>
					</div>
					<div class="4u 12u(narrower)" style="    width: 100%; ">

						<section>
							<span class="icon featured fa-flag-o"></span>
							<header>
								<h3>B조 토너먼트 대진표 <a  href="#;" class="btn btn-info" id="b-view">상세보기</a></h3> </h3>
							</header>
							<table class="table">
								<tr>
									<td>#</td><td>팀</td><td>상태</td>
								</tr>
								<tr class = "lose">
									<td>6</td>
									<td>휘진+지현</td>
									<td>4위</td>
								</tr>
								<tr class = "lose">
									<td>8</td>
									<td>기주</td>
									<td>2위</td>
								</tr>
								<tr class = "lose">
									<td>5</td>
									<td>수진</td>
									<td>OUT</td>
								</tr>
								<tr class = "lose">
									<td>7</td>
									<td>세일</td>
									<td>OUT</td>
								</tr>
							</table>
						</section>

					</div>
				</div>
			</section>
	</article>
<!-- Three -->

<!-- CTA -->
	<section id="cta">
		<h2></h2>
		<span style="    font-size: 13px;">참여하고 응원해주신 모든 분들 고생 많으셨습니다 ^^</span>
		<hr>
		<header>
			<div>
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
				  <div class="carousel-inner" role="listbox">
				    <div class="item active">
						<img src="<?=SKIN_PATH?>/2016basketball/file/0424/KakaoTalk_20160424_221524785.jpg" alt="image" style="width: 100%">
					</div>
					<div class="item">
						<img src="<?=SKIN_PATH?>/2016basketball/file/0424/KakaoTalk_20160424_221523472.jpg" alt="image" style="width: 100%">
					</div>
				 </div>
				  <!-- Controls -->
				  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
				    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
				    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a>
				  
				</div>
				
			</div>
			<!-- 
			<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			  <div class="carousel-inner" role="listbox">
			    <div class="item active">
					<div class="carousel-heaer">4월 10일 (첫째날)</div>
					<table class="table" style="margin-bottom: 0px;">
						<tr>
							<td colspan="2">A조 예선 경기</td>
						</tr>
						<tr class="active">
							<td style="color: #645862;">세은+예승</td>
							<td style="color: #645862;">승</td>
						</tr>
						<tr class="active">
							<td style="color: #645862;">민우+다솜</td>
							<td style="color: #645862;">패</td>
						</tr>
						<tr class="active">
							<td style="color: #645862;">소망+나영</td>
							<td style="color: #645862;">패</td>
						</tr>
						<tr class="active">
							<td style="color: #645862;">경민</td>
							<td style="color: #645862;">승</td>
						</tr>
						<tr>
							<td colspan="2">B조 예선 경기</td>
						</tr>
						<tr class="active">
							<td style="color: #645862;">수진</td>
							<td style="color: #645862;">패</td>
						</tr>
						<tr class="active">
							<td style="color: #645862;">휘진+지현</td>
							<td style="color: #645862;">승</td>
						</tr>
						<tr class="active">
							<td style="color: #645862;">세일</td>
							<td style="color: #645862;">패</td>
						</tr>
						<tr class="active">
							<td style="color: #645862;">기주</td>
							<td style="color: #645862;">승</td>
						</tr>
					</table>
				</div>
				<div class="item">
					<div class="carousel-heaer">4월 17일 (둘째날)</div>
					<table class="table" style="margin-bottom: 0px;">
						<tr>
							<td colspan="2">A조 결승 경기</td>
						</tr>
						<tr class="active">
							<td style="color: #645862;">세은+예승</td>
							<td style="color: #645862;">패</td>
						</tr>
						<tr class="active">
							<td style="color: #645862;">경민</td>
							<td style="color: #645862;">승</td>
						</tr>
						<tr>
							<td colspan="2">B조 결승 경기</td>
						</tr>
						<tr class="active">
							<td style="color: #645862;">휘진+지현</td>
							<td style="color: #645862;">패</td> 
						</tr>
						<tr class="active">
							<td style="color: #645862;">기주</td>
							<td style="color: #645862;">승</td>
						</tr>
						<tr >
							<td colspan="2">이벤트 경기 당첨자</td>
						</tr>
						<tr class="active"> 
							<td colspan="2" style="color: #645862;">한효정</td>
						</tr>
						<tr class="active"> 
							<td colspan="2" style="color: #645862;">권희원</td>
						</tr>
					</table>
				</div>
				<div class="item">
					<div class="carousel-heaer">4월 24일 (셋째날)</div>
					<table class="table" style="margin-bottom: 0px;">
						<tr>
							<td colspan="2">최종 결승 경기</td>
						</tr>
						<tr class="active">
							<td style="color: #645862;">기주</td>
							<td style="color: #645862;">패</td>
						</tr>
						<tr class="active">
							<td style="color: #645862;">경민</td>
							<td style="color: #645862;">승</td>
						</tr>
						<tr >
							<td colspan="2">이벤트 경기 당첨자</td>
						</tr>
						<tr class="active"> 
							<td colspan="2" style="color: #645862;">한효정</td>
						</tr>
						<tr class="active"> 
							<td colspan="2" style="color: #645862;">권희원</td>
						</tr>
						<tr class="active"> 
							<td colspan="2" style="color: #645862;">한현희</td>
						</tr>
						<tr class="active"> 
							<td colspan="2" style="color: #645862;">기여운</td>
						</tr>
					</table>
				</div>
			  </div>
			   -->
			 
		</header>
		<footer>
			<ul class="buttons">
				<li><a href="#vs" class="button  scrolly">대진표 확인</a></li>
				<li><a href="#main" class="button">상품 소개</a></li>
				<li><a href="javascript:murl('?f=2016basketball&v=photo');" class="button  scrolly">농구대회 갤러리</a></li>
			</ul>
		</footer>
	</section>
	
	 
<!-- Footer -->
		<footer id="footer">

			<ul class="icons">
				<li><a href="javascript:share_facebook('http://sync2030.org/?f=2016basketball')" class="icon circle fa-facebook"><span class="label">Facebook</span></a></li>
				<li><a href="javascript:share_url('http://sync2030.org/?f=2016basketball')" class="icon circle fa-dribbble"><span class="label">Dribbble</span></a></li>
			</ul>

			<ul class="copyright">
				<li>&copy; 2016 SYNC | 성락성결교회 </li><li></li>
			</ul>
		</footer>
</div>

<div class="modal" id="a-modal">
	<h1>A조 대진표</h1>
	<div>
		<img alt="A조 대진표" src="<?=IMG_PATH?>/2016basketball/a.png">
	</div>
</div>
<div class="modal" id=b-modal>
	<h1>B조 대진표</h1>
	<div>
		<img alt="B조 대진표" src="<?=IMG_PATH?>/2016basketball/b.png">
	</div>
</div>
<!-- 팝업 위젯  -->
<? Unit::load('popup', array('id' => 'main' , 'sdate'=> '2016-04-19','edate'=>'2016-04-24 16:00:00', 'title' => '<i class=" fa fa-dribbble fa-spin"></i>  4월 24일 빅매치!!<i class=" fa fa-dribbble fa-spin "></i>',
		'content' => '
			<p>
				▶1경기 [3-4위전]<br>
				<span>세은+예승 VS 휘진+지현 </span><br>
				▶2경기 [최종결승]<br>
				<span>경민 VS 기주</span>
			</p>
			<p>
				<sub>
					드디어 2016 SYNC배 상반기 농구대회 최고의 팀을 가리는 날이 왔습니다!!<br>
					당일 농구경기를 보러오시는 모든 분들께 음료 드리오니 많이 응원 하러 와주시기 바랍니다.
				</sub>
			</p>'));?>