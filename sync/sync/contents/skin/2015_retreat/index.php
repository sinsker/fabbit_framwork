<?php 
?>

<div id="page-wrapper">

	<!-- Header -->
		<div id="header">
			<img src="<?=IMG_PATH?>/20160114/header.jpg" alt="" style="width:100%; max-width: 560px;" class="zoom"/>
			<!-- Inner -->
				<!--<div class="inner">
					<header>
						 2016 SYNC 동계수련회
						<hr style="margin-top: 00px;"/>
						<span id="h_sync" style="font-size: 3.0em;">너로 말미암아!</span>
						<hr style="margin-top: 00px;"/>
						<div>
						<br><br><br>
							<div id='text'>창세기 12장 1절 ~ 3절</div>
							<div id='text_sub'>1. 여호와께서 아브람에게 이르시되 너는 너의 고향과 친척과 아버지의 집을 떠나 내가 네게 보여 줄 땅으로 가라<br>
								 2. 내가 너로 큰 민족을 이루고 네게 복을 주어 네 이름을 창대하게 하리니 너는 복이 될지라<br>
								 3. 너를 축복하는 자에게는 내가 복을 내리고 너를 저주하는 자에게는 내가 저주하리니 땅의 모든 족속이 너로 말미암아 복을 얻을 것이라 하신지라 </div>
						</div>
						 
					</header>
					<footer>
					</footer>
					 
				</div>
				-->
				<br>
			<!-- Nav -->
				<nav id="nav">
					<ul>
						<li>
							<a href="#index" ><center>▶ 2016 SYNC 동계수련회 ◀</center></a>
							<ul>
								<li><a href="#pray">기도 모임 안내</a></li>
								<li><a href="#worship">예배 안내</a></li>
								<li><a href="#program">프로그램 안내</a></li>
								<li><a href="javascript:show_info()">준비물 및 신청 안내</a></li>
								<li><a href="#">[문의관련] 신훈수 회장 010-2299-0658</a></li>
							</ul>
						</li>
					</ul>
				</nav>
		</div>
		<div style="text-align: center;">
			<div style="max-width: 500px; width: 100%; margin: 0 auto;">
 			<iframe width=100% height=300px" src="https://www.youtube.com/embed/hp2yoQaqYdU" frameborder="0"  style=""></iframe>
 			</div>
 		</div>
		<!-- <section id="banner">
		</section>
	<!-- Carousel -->
		<section class=" wrapper carousel" id="pray" >
			<header>
				<h2><center  class="pt color_ani" style="color:#101733;">Pray !</center></h2>
				<div style="padding: 5px;font-size: 15px;">
					<br>
					기도가 너무 어렵게 느껴지나요?<br> 마음은 있지만 함께 할 공간이 부족하나요?<br> 너무 어렵게 생각하지 마세요!<br> 기도의 자리에 함께 하는것만으로도 주님께서 기뻐하실거에요
				</div>
			</header>
			<article >
				<div class="pray_wrap">
					<div class="pray_head ">매주 화요일</div>
					<div class="pray_contents">
						<img src="<?=IMG_PATH?>/20160114/pray1.png" alt="" style="width:100%; max-width: 560px;" class="zoom"/>
						<br>
						장소 : 503 싱크사무실<br>시간 : 저녁 8 시<br>
						<div class="pray_contents_s">
							바쁜 일상 가운데 잠시 시간 내주는 당신 아름답습니다.♥ <br>[ 끝나고 임원한테 맛있는거 사달라고 조르세요 ~]
						</div>
					</div>
				</div>
			</article>
			<article >
				<div class="pray_wrap">
					<div class="pray_head ">매주 토요일</div>
					<div class="pray_contents">
						<img src="<?=IMG_PATH?>/20160114/_talkm_oWmUSVdJg8_pG1qYiVh8Qt5grRvdjXEX1_i_g9oikf9m0ho3.jpg" alt="기도모임을 고뇌하는 상우" style="width:100%; max-width: 560px;" class="zoom"/>
						<br>
						장소 : 703호 <br>시간 : 오후 1 시<br>
						<div class="pray_contents_s">
							기도모임은 계속 되어야 한다~!!<br> 토요일도 진행 되는 기도모임! <br>
						</div>
					</div>
				</div>
			</article>
			<article >
				<div class="pray_wrap">
					<div class="pray_head ">매주 일요일</div>
					<div class="pray_contents">
						<img src="<?=IMG_PATH?>/20160114/pray4.jpg" alt="" style="width:100%; max-width: 560px;" class="zoom"/>
						<br>
						장소 : 403호 <br>시간 : 오후 4 시 15분<br>
						<div class="pray_contents_s">
							가정 모임 끝난 후 모두가 모이는 풍성한 기도모임!<br> 그리고.. 매주 뽑기를 통해 <span style="color: red;">무료 수련회 당첨자</span>도 뽑습니다!
						</div>
					</div>
				</div>
			</article>
			<article >
				<div class="pray_wrap">
					<div class="pray_head ">1월 8일 금요 예배</div>
					
					<div class="pray_contents">
						<img src="<?=IMG_PATH?>/20160114/ye_2.jpg" alt="" style="width:100%; max-width: 560px;" class="zoom"/>
						<br>
						장소 : 503 고등부 예배당<br>시간 : 저녁 8 시<br>
						<div class="pray_contents_s">
							연말이다 휴일이다 정신 없죠? 수련회 가기 전 딱 한번 있는 소중한 금요 예배시간! 한 번 오기 시작하면 계속 오게 될껄?<br>
							예조이스 팀과 불타는 금요일♥<br> 수련회를 기대하는 마음으로♥
						</div>
					</div>
				</div>
			</article>
			<article >
				<div class="pray_wrap">
					<div class="pray_head ">SYNC 릴레이 기도</div>
					<br>
					<div class="pray_contents">
						<img src="<?=IMG_PATH?>/20160114/pray5.png" alt="" style="width:100%; max-width: 560px;" class="zoom"/>
						<div class="pray_contents_s">
							릴레이 기도표 입니다.<br>
							이렇게 지금 이 글을 보고 있는 당신!
							여러분의 작은 움직임으로 SYNC는 조금씩 조금씩 움직이기 시작합니다. 
						</div>
					</div>
				</div>
			</article>
			<!-- 
			<div class="reel">
				<article>
					<a href="#" class="image featured lightbox_trigger" sub_id="pray_1" class="" ><img src="<?=IMG_PATH?>/20160114/pic01.jpg" alt="" />
						<header>
							<h3 class='week_box'>매주 화요일</h3>
						</header>
						<p>장소 : 503 싱크사무실<br>시간 : 저녁 8 시</p>
						<div id ='pray_1' class='pray_content'>
							기도1
						</div>
					</a>
				</article>

				<article>
					<a href="#" class="image featured lightbox_trigger" sub_id="pray_2" class="" ><img src="<?=IMG_PATH?>/20160114/pic03.jpg" alt=""/>
					<header>
						<h3 class='week_box'>매주 토요일</h3>
					</header>
					<p>장소 : 703호 리더모임후<br>시간 : 오후 1 시</p>
					<div id ='pray_2' class='pray_content'>
						기도2
					</div>
					</a>
				</article>
				
				<article>
					<a href="#" class="image featured lightbox_trigger" sub_id="pray_3" class="" ><img src="<?=IMG_PATH?>/20160114/pic04.jpg" alt=""/>
					<header>
						<h3 class='week_box'>매주 일요일</h3>
					</header>
					<p>장소 : 403호 <br>시간 : 오후 4 시 15 분</p>
					<div id ='pray_3' class='pray_content'>
						기도3
					</div>
					</a>
				</article>
				
				<article>
					<a href="#;" class="image featured lightbox_trigger" sub_id="pray_4" class="" ><img src="<?=IMG_PATH?>/20160114/pic02.jpg" alt="" />
					<header>
						<h3 class='week_box'>2016년 1월 8일</h3>
					</header>
					<p>장소 : 503호 금요예배<br>시간 : 저녁 8 시</p>
					<div id ='pray_4' class='pray_content'>
						기도4
					</div>
					</a>
				</article>

			</div> -->
	<!-- 	</section>
			<div class="do">
            <ul id="autoplay" class="content-slider">
                <li>
                    <article>
					<a href="#" class="image featured lightbox_trigger" sub_id="pray_1" class="" ><img src="<?=IMG_PATH?>/20160114/pray1.png" alt="" /></a>
						<header>
							<h3 class='week_box'>매주 화요일</h3>
						</header>
						<p class="p_rl">장소 : 503 싱크사무실<br>시간 : 저녁 8 시</p>
						<div id ='pray_1' class='pray_content'>
							기도1
						</div>
					
				</article>
                </li>
                <li>
                    <article>
					<a href="#" class="image featured lightbox_trigger" sub_id="pray_2" ><img src="<?=IMG_PATH?>/20160114/pray3.png" alt=""/></a>
					<header>
						<h3 class='week_box'>매주 토요일</h3>
					</header>
					<p class="p_rl">장소 : 703호 리더모임후<br>시간 : 오후 1 시</p>
					<div id ='pray_2' class='pray_content'>
						기도2
					</div>
					
				</article>
                </li>
                <li>
                   <article>
					<a href="#" class="image featured lightbox_trigger" sub_id="pray_3" class="" ><img src="<?=IMG_PATH?>/20160114/pray3.png" alt=""/></a>
					<header>
						<h3 class='week_box'>매주 일요일</h3>
					</header>
					<p class="p_rl">장소 : 403호 <br>시간 : 오후 4 시 15 분</p>
					<div id ='pray_3' class='pray_content'>
						기도3
					</div>
					
				</article>
                </li>
                <li>
                   <article>
						<a href="#;" class="image featured lightbox_trigger" sub_id="pray_4" class="" ><img src="<?=IMG_PATH?>/20160114/pic02.jpg" alt="" /></a>
						<header>
							<h3 class='week_box'>2016년 1월 8일</h3>
						</header>
						<p class="p_rl">장소 : 503호 금요예배<br>시간 : 저녁 8 시<br>수련회 시작 전 단 하루 뿐인 금요예배! 예조이스와 함께 예배로 불금 보내요♥</p>
						<div id ='pray_4' class='pray_content'>
							기도4
						</div>
				</article>
                </li>
            </ul>
        </div>
         -->
</section>
	<!-- Main -->
		<div   id="worship" class="wrapper carousel"  >
			<header>
				<h2><center  class="pt" style="  color: #790F5C;">Worship !</center></h2> 
					<br> 
				<div style="text-align: left; padding: 5px;font-size: 15px; ">
					수련회를 가는 이유는 바로 여기에 있죠!<br> 자신의 모든 것을 이 자리에 나와 주님께 내려놓는 시간이 되었으면 좋겠습니다.<br>
					나의 기쁨, 슬픔, 두려움들 이 순간을 계기로 다시 회복하는 계기가 되길 원합니다. <br>
					이번 집회는 외부 찬양팀인 <span style="color: #CA0000;">'오버플로잉'</span>팀이 진행합니다.  온몸으로 예배 드리며
					주님과의 특별한 만남 기대해보아요 ♥
				</div>
			</header>
			<article id="main" class="container special">
				<a href="#" class="image featured"><img src="<?=IMG_PATH?>/20160114/overflow.png" alt="" class="zoom"/></a>
				<header>
					집회는<br> 첫째날 저녁(14일)과 둘째날 저녁(15일)<br>
					 총 2번에 걸쳐 진행됩니다.
				</header>
				<!-- 
				<header>
					<h2><a href="#">Sed massa imperdiet magnis</a></h2>
					<p>
						Sociis aenean eu aenean mollis mollis facilisis primis ornare penatibus aenean. Cursus ac enim
						pulvinar curabitur morbi convallis. Lectus malesuada sed fermentum dolore amet.
					</p>
				</header>
				<p>
					Commodo id natoque malesuada sollicitudin elit suscipit. Curae suspendisse mauris posuere accumsan massa
					posuere lacus convallis tellus interdum. Amet nullam fringilla nibh nulla convallis ut venenatis purus
					sit arcu sociis. Nunc fermentum adipiscing tempor cursus nascetur adipiscing adipiscing. Primis aliquam
					mus lacinia lobortis phasellus suscipit. Fermentum lobortis non tristique ante proin sociis accumsan
					lobortis. Auctor etiam porttitor phasellus tempus cubilia ultrices tempor sagittis. Nisl fermentum
					consequat integer interdum integer purus sapien. Nibh eleifend nulla nascetur pharetra commodo mi augue
					interdum tellus. Ornare cursus augue feugiat sodales velit lorem. Semper elementum ullamcorper lacinia
					natoque aenean scelerisque.
				</p>
				<footer>
					<a href="#" class="button">Continue Reading</a>
				</footer>
				 -->
			</article>

		</div>

	<!-- Features -->
		<div class="wrapper carousel" id="program" >
			<section id="main" class="container">
				<header>
					<h2><h2><center  class="pt" style="    color: black;">Program !</center></h2></h2>
					<div style="padding: 5px;font-size: 15px; text-align: left;">
						<br>
						2016년 스마트한 두뇌를 가진 인원들로 구성된 임원진 <br>
						감동과 즐거움에 당신은 정신차릴수 없다.<br>
						살짝 맛보기로 몇개의 프로그램 타이틀을 공개합니다.<br>
					</div>
					<br>
				</header>
				<div class="row" style=" margin: 0 auto;">
						<a href="#" class="image featured"><img src="<?=IMG_PATH?>/20160114/pro1.png" alt="" class="zoom"/></a>
						<!-- <p>
							Amet nullam fringilla nibh nulla convallis tique ante proin sociis accumsan lobortis. Auctor etiam
							porttitor phasellus tempus cubilia ultrices tempor sagittis. Nisl fermentum consequat integer interdum.
						</p> -->
						<a href="#" class="image featured"><img src="<?=IMG_PATH?>/20160114/pro2.png" alt="" class="zoom"/></a>
						<!-- <p>
							Amet nullam fringilla nibh nulla convallis tique ante proin sociis accumsan lobortis. Auctor etiam
							porttitor phasellus tempus cubilia ultrices tempor sagittis. Nisl fermentum consequat integer interdum.
						</p> -->
						<!-- <a href="#" class="image featured"><img src="<?=IMG_PATH?>/20160114/pic07.jpg" alt="" /></a>
						 <p>
							Amet nullam fringilla nibh nulla convallis tique ante proin sociis accumsan lobortis. Auctor etiam
							porttitor phasellus tempus cubilia ultrices tempor sagittis. Nisl fermentum consequat integer interdum.
						</p> -->
				</div><center style="color: #A9A4A4;"><br>
				2016년 하나님과 더욱 더 깊은 관계가 되길 윈하신다면 망설이지말고 함께 해요 ^^* </center>
			</section>

		</div>

	<!-- Footer -->
		<div id="footer">
			<div class="container">
				<div onclick="sendLink();"  id='kakao' >
					<div>
						<img src="<?=IMG_PATH?>/kako.png" width="42px"><span style="padding: 5px;position: relative; top: -13px;">카카오톡 공유하기</span>
					</div>
				</div>
				<div class="row">
					<!-- Tweets -->
						<section class="12u 12u(mobile)">
						<ul class="divided">
								<li>
									<img src="<?=IMG_PATH?>/20160114/l.png" alt="임원사진" style="width: 100%" class="zoom">
									<article class="tweet" style="text-align: left; font-size: 14px;">
										마지막으로 드리고 싶은 말은 저희 2016년 SYNC 임원단들이 정말 밤새가며 열심히 준비 하고 있습니다.
										어떻게 하면 더 즐거운 수련회, 한사람도 소외 되지 않는 수련회, 하나님을 만날수 있는 수련회를 만들까..
										그런데 결국 중요한것은 여러분이 참여하지 않는다면 이 모든것을 경험할 기회조차 없어집니다. 꼭 부디 와주셔서
										이러한 수련회를 함께 나눴으면 좋겠습니다. 감사합니다. <br><sub  class="timestamp" style="text-align: center; font-size: 15px;"> - SYNC 임원진 일동 -</sub>
										<!-- <span class="timestamp">5 minutes ago</span> -->
									</article>
								</li>
							</ul>
						</section>
					<!-- Posts -->
						 <section class="12u 12u(mobile)">
							<header>
							</header>
							<ul class="divided">
								<li>
									<article class="post stub">
										<header>
										
										</header>
									</article>
								</li>
								<li></li>
							</ul>
						</section> 
				</div>
				<div class="row">
					<div class="12u">
					
						<!-- Contact -->
							<section class="contact">
							<div style="margin-bottom: 5px;font-size: 13px;">2016년 SYNC 동계 수련회</div>
								<header>
									<span id="h_sync" style="font-size: 2.0em;">너로 말미암아!</span>
								</header>
								<p><br>1월 14일 부터 1월 16일 까지<br>신덕기도원에서</p>
							</section>
						<!-- Copyright -->
							<div class="copyright">
								<ul class="menu">
									<li>&copy; 2015-2016 SYNC. All rights reserved.</li><li>Design: <a href="/">Shin il</a>, <a href="/">미디어사역팀</a></li>
								</ul>
							</div>
					</div>
				</div>
			</div>
		</div>
</div>
<!-- <ul id="particles" class="particles"></ul> -->
<div id = "info_button"><img src="<?=IMG_PATH?>/20160114/19-128.png" alt="상세버튼" ></div>
<div id = "info" style="display: none;">
	<div id = "info_wrap">
		<div id="info_head" class="f-j">동계수련회:너로말미암아!</div><hr style="    top: 0;
    margin-bottom: 0;
    margin-top: 15px;
    background: #A9772C;
">
		<div id="info_content">
		<!-- <span>날짜 / 장소</span><br> 1월 14일 ~ 1월 16일 신덕기도원<br><br> -->
		<span>첫째날[14일(목)] 집합장소</span><br> 아침 8시 50분까지 503호<br>
		<span>둘째날[15일(금)] 집합장소</span><br> 저녁 7시 20분까지 교회 앞 집합<br>
		<span>준비물</span><br>여벌 옷,세면도구, 필기도구, 성경책<br>
		<span>참가신청비</span><br>45,000원 [당일날 (1공)안수진에게 제출]<br>
		<br> 자신이 출발하는 날 집합 시간 꼼꼼히 체크하셔서<br> 늦으시는 일 없도록 해주세요^^<br>특히 둘째날 참여자는 <br> 집회 시간때문에 늦지않길바래요~♥
		<!-- <span>참가신청안내</span><br>섬김이에게 현금 제출 혹은<br> 아래 계좌번호로 입금 후 <br>안수진/김미현 에게 전화 후 체크<br>
		<span>신청 입금 계좌번호</span><br>361401-04-122325 [ 국민 ] /<br> 예금주 : 안수진(1공동체)<br>
		<span>수련회 신청  문의</span><br>1공 회계 안수진 010-4108-530</div> -->
	</div>
</div>
<!-- 
<audio autoplay="autoplay" loop="loop" >
	<source src="<?=RES_PATH?>/20160114.mp3" type="audio/mpeg"/>
</audio>
 -->