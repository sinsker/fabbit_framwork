<link rel="stylesheet" type="text/css" href="<?=CSS_PATH?>/bootstrap.css" /> 
<link href='https://fonts.googleapis.com/css?family=Luckiest+Guy' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/earlyaccess/jejuhallasan.css' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/earlyaccess/jejugothic.css' rel='stylesheet' type='text/css'>
<!--[if lte IE 8]><script src="<?=JS_PATH?>/ie/html5shiv.js"></script><![endif]-->
<link rel="stylesheet" href="<?=CSS_PATH?>/20160114.css" />
<!--[if lte IE 8]><link rel="stylesheet" href="<?=CSS_PATH?>/ie8.css" /><![endif]-->
<!-- <link type="text/css" href="<?=CSS_PATH?>/countdown.css?" rel="stylesheet"> -->
<link rel="stylesheet" href="<?=CSS_PATH?>/20160114/lightslider.css"/>
<link type="text/css" href="<?=CSS_PATH?>/jquery.countdown.css" rel="stylesheet">



<!-- font-family: 'Jeju Gothic', serif; 
	 font-family: 'Nanum Pen Script', serif;-->
<style>

body{ font-family: 'Jeju Gothic', serif;}

#h_sync{
	font-family: 'Jeju Hallasan', serif;
	font-size: 44px;
}

.f-j{
	font-family: 'Jeju Hallasan', serif;
}
.depth-0{
	text-align: center;
}

.header_info{
	font-size: 15px;
    margin-bottom: 15px;
    color: #171717;
}

.header_line{
	border-top: 1px solid #ffffff;
    border-bottom: 1px solid #ffffff;
    padding: 5px 5px 10px 5px;
}

.A{
	color:#F90404;
}

.step{
	/* background: #ACEFFF; */
    padding: 5px;
    border-radius: 15px;
    margin: 15px;
}

.next_btn{
    margin-bottom: 15px;
    display: block;
    color: black;
    /* border: brown; */
    padding: 10px 0 15px 0;
    background: #000000;
    border:1px solid #ffffff;
    color: #ffffff;
    padding: 7px;
}

.next_btn:HOVER, .next_btn:FOCUS,.next_btn:ACTIVE{
	 color: black;
	 background: white;
	  border:1px solid black;
}

    	ul{
			list-style: none outside none;
		    padding-left: 0;
            margin: 0;
		}
     /*    .demo .item{
            margin-bottom: 60px;
        }
		.content-slider h3 {
		    margin: 0;
		    padding: 70px 0;
		} */
/* /* /* /*  */ 
.content-slider li{
    text-align: center;
}

#lightbox {
	position:fixed; /* keeps the lightbox window in the current viewport */
	top:0; 
	left:0; 
	width:100%; 
	height:100%; 
	background : rgba(0, 0, 0, 0.9);
	text-align:center;
	z-index: 99999;
}
#lightbox p {
	text-align:right; 
	color:#fff; 
	margin-right:20px; 
	font-size:12px; 
}
#lightbox img {
	box-shadow:0 0 25px #111;
	-webkit-box-shadow:0 0 25px #111;
	-moz-box-shadow:0 0 25px #111;
	max-width:940px;
}

.pray_content{
	display: none;
}

.p_rl{
	padding:0 15px 0 15px;
}

#do{
    padding: 5px;
    background: #F3FFB4;
}

.pray_wrap{
	border: 1px solid #C7C1C1;
	    background: #ffffff;
	    box-shadow: 2px 2px 2px #A99E9E;
}

#pray{
	background: linear-gradient( to top, #E2D85B, white );
}

#pray > article {
	width: 100%;
    max-width: 700px;
    border: 0px;
    display: block;
    background: rgb(220, 192, 127);
    padding: 0;
    margin-bottom: 10px;
        margin: 0 auto;
}

#worship{
	    background: linear-gradient( to top, rgba(98, 224, 245, 0.46), #E2D85B);
}

#program{
	    background: linear-gradient( to top, #2B252C, rgba(98, 224, 245, 0.46));
}

.pray_head{
    padding: 13px 0px 13px 0px;
    border: 1px solid #636060;
    color: #000000;
    background: rgba(232, 234, 147, 0.21);
    font-size: 15px;
    box-shadow: 2px 2px 2px #636363;
}

.pray_contents > img{
	padding: 5px;
}

.pray_contents_s{
	text-align: left;
	font-size: 12px;
	    padding: 0 10px 0 10px;
}

#info_button{
	cursor: pointer;
	z-index: 99999;
	position: fixed;
    bottom: 0;
    left: 0;
    margin: 10px;
}

#info_button > img{
	width: 60px;
	height: 60px;
}

#info{
	display: block;
	position: fixed;
    width: 100%;
    height: 100%;
    top: 0;
    z-index: 99999;
    background: #151519;
}

#info_head{
	text-align: center;
       margin-top: 30px;
    font-size: 26px;
    color: #EFEFEF;
}

#info_content{
	text-align: center;
    margin-top: 20px;
    color: #DCCFCD;
}

#info_content span{
	font-size: 15px;
    color: #A9772C;
    display: inline-block;
    padding-top: 3px;
}

#kakao{
padding: 10px;
    display: block;
    text-align: center;
    margin: 0 auto;
    border: 1px solid #4A4141;
    height: 63px;
    box-shadow: 2px 1px 2px 1px #000000;
    margin-bottom: 10px;
    background: rgba(0, 0, 0, 0.7);
    cursor: pointer;
        color: #E0E0D9;
    font-size: 15px;
}

</style>