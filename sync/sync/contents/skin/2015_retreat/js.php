<script type="text/javascript" src="<?=JS_PATH?>/bootstrap.min.js"></script>
<!-- Scripts -->
<script src="<?=JS_PATH?>/jquery.min.js"></script>
<script src="<?=JS_PATH?>/jquery.dropotron.min.js"></script>
<script src="<?=JS_PATH?>/jquery.scrolly.min.js"></script>
<script src="<?=JS_PATH?>/jquery.onvisible.min.js"></script>
<script src="<?=JS_PATH?>/skel.min.js"></script>
<script src="<?=JS_PATH?>/util.js"></script>
<!--[if lte IE 8]><script src="<?=JS_PATH?>/ie/respond.min.js"></script><![endif]-->
<script src="<?=JS_PATH?>/20160114.js"></script>
<script src="<?=JS_PATH?>/20160114/lightslider.js"></script>
<script type="text/javascript" src="<?=JS_PATH?>/jquery.countdown.min.js?v=1.0.0.0"></script>
<script src="https://developers.kakao.com/sdk/js/kakao.min.js"></script>
<script type="text/javascript">

	jQuery(document).ready(function($) {
		/* $("#header").height($(window).height()); */
		$('img.zoom').click(function(e) {
			
			e.preventDefault();
			
			var image_href = $(this).attr("src");
			var contents_id = $(this).attr("alt");
			
			//var pray_contents =  $("#"+contents_id).html();
			
			if ($('#lightbox').length > 0) { // #lightbox exists
				$('#content').html('<img src="' + image_href + '"style="width:100%" />'); 
				//$('#content').html(pray_contents);
				$('#lightbox').fadeIn();
			}
			
			else { //#lightbox does not exist - create and insert (runs 1st time only)
				//create HTML markup for lightbox window
				var lightbox = 
				'<div id="lightbox" style="display:none; display:table;">' +
					'<div id="content" style=" vertical-align: middle; display: table-cell;">' + //insert clicked link's href into img src
						//pray_contents +
					 	'<img src="' + image_href +'"  style="width:100%"/>' + 
					'</div>' +	
				'</div>';
					
				//insert lightbox HTML into page
				$('body').append(lightbox);
				$('#lightbox').fadeIn();
			}
			
			$('#lightbox').on('click', function() { //must use live, as the lightbox element is inserted into the DOM
				$('#lightbox').hide();
			});
		});
		
	});
	
	$(document).ready(function() {
	    var autoplaySlider = $('#autoplay').lightSlider({
	        auto:true,
	        item:1,
	        slideMargin:0,
	        loop:true,
	        onBeforeSlide: function (el) {
	            $('#current').text(el.getCurrentSlideCount());
	        } 
	    });
	    /* $('#total').text(autoplaySlider.getTotalSlideCount()); */
		
		var flag_b = true;
	   /*  setInterval(function(){

			if(flag_b == true){
	    		$("#info_button > img").fadeOut();
	    		flag_b = false;
			}else{
				$("#info_button > img").fadeIn();
	    		flag_b = true;
			}
		    
	    },1500); */
	    	
	});

	$("#info_button").click(function(){
		$("#info").fadeIn(1000);
	});

	$("#info").click(function(){
		$("#info").fadeOut();
	});

	function show_info(){
		$("#info").fadeIn(1000);
	}
    Kakao.init('f008023c5c337c908c1dd0fdb7e030c0');
     
    function sendLink() {
        Kakao.Link.sendTalkLink({
            label: '[2016 SYNC 동계수련회]',
            webLink: {
                text: '당신을 초대합니다.',
                url: 'http://sync2030.org/?pg=20160114'
            },
            image: { // 80 * 80
                src: 'http://sync2030.org/sync/resources/images/20160114/123.jpg',
                 width: '200',
                height: '200' 
            },
            webButton  :{
            	url :'http://sync2030.org/?pg=20160114',
            	text:'수련회 엿보기'
            }
        });
    }
</script>


