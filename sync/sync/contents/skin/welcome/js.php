<!--js작성 페이지 입니다.-->
<script type="text/javascript" src="<?=JS_PATH?>/bootstrap.min.js"></script>
<!-- Modernizr -->
<script src="<?=CUR_DIR?>/resource/js/modernizr.js"></script>
<!-- Js -->
<script src="https://developers.kakao.com/sdk/js/kakao.min.js"></script>
<script src="<?=CUR_DIR?>/resource/js/supersized.3.2.7.min.js"></script> <!-- Slider -->
<script src="<?=CUR_DIR?>/resource/js/waypoints.js"></script> <!-- WayPoints -->
<script src="<?=CUR_DIR?>/resource/js/waypoints-sticky.js"></script> <!-- Waypoints for Header -->
<script src="<?=CUR_DIR?>/resource/js/jquery.isotope.js"></script> <!-- Isotope Filter -->
<script src="<?=CUR_DIR?>/resource/js/jquery.fancybox.pack.js"></script> <!-- Fancybox -->
<script src="<?=CUR_DIR?>/resource/js/jquery.fancybox-media.js"></script> <!-- Fancybox for Media -->
<script src="<?=CUR_DIR?>/resource/js/plugins.js"></script> <!-- Contains: jPreloader, jQuery Easing, jQuery ScrollTo, jQuery One Page Navi -->
<script src="<?=CUR_DIR?>/resource/js/main.js"></script> <!-- Default JS -->
<script>
Kakao.init('f008023c5c337c908c1dd0fdb7e030c0');

$("#submit").click(function(){

	if($("#contact_name").val() == ''){
		alert('닉네임을 입력해 주세요.');
		return;
	}
	if($("#contact_message").val() == ''){
		alert('내용을 입력해 주세요.');
		return;
	}
	evt_data_insert(evt_id, $("#contact-form").serialize() );
});	

$("#contact_name").keyup(function(){
	emptyValCheck();
});
$("#contact_message").keyup(function(){
	emptyValCheck();
});

function emptyValCheck(){
	var submit_btn = $("#submit");
	if($("#contact_message").val() != '' && $("#contact_name").val() != ''){
		submit_btn.addClass("submit");
		submit_btn.removeClass("non-submit");
	}else{
		submit_btn.addClass("non-submit");
		submit_btn.removeClass("submit");
	}

}

$(function(){
	$(".back-content").height( $(window).height());

	$(".back-wrap").click(function(){
		$(this).fadeOut(1500);
	});
	
});

</script>