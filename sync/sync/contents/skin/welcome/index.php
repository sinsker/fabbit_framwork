
<!-- Mobile Internet Explorer ClearType Technology -->
<!--[if IEMobile]>  <meta http-equiv="cleartype" content="on">  <![endif]-->

<!-- <link rel="apple-touch-icon" href="#">
<link rel="apple-touch-icon" sizes="114x114" href="#">
<link rel="apple-touch-icon" sizes="72x72" href="#">
<link rel="apple-touch-icon" sizes="144x144" href="#">
 -->




<!-- This section is for Splash Screen -->
<div class="ole">
<section id="jSplash">
	<div id="circle"></div>
</section>
</div>
<!-- End of Splash Screen -->

<!-- Homepage Slider -->
<div id="home-slider">	
    <div class="overlay"></div>

    <div class="slider-text">
    	<div id="slidecaption"></div>
    </div>   
	
	<div class="control-nav">
        <a id="prevslide" class="load-item"><i class="font-icon-arrow-simple-left"></i></a>
        <a id="nextslide" class="load-item"><i class="font-icon-arrow-simple-right"></i></a>
        <ul id="slide-list"></ul>
        <a id="nextsection" href="#work"><i class="font-icon-arrow-simple-down"></i></a>
    </div>
</div>
<!-- End Homepage Slider -->

<!-- Header -->
<header>
    <div class="sticky-nav">
    	<a id="mobile-nav" class="menu-nav" href="#menu-nav"></a>
        
        <div id="sync" style="    float: left;">
        	<a id="goUp" href="#home-slider" title="SYNC ">
        		<img alt="" src="/sync/resources/images/admin/sync_t.png" width="100px;"  style="padding-top: 18px;    display: block;">
			</a>
        </div>
        
        <nav id="menu">
        	<ul id="menu-nav">
            	<li class="current"><a href="#home-slider">HOME</a></li>
                <li><a href="#INFO">INFO</a></li>
                 <li><a href="#STORY">STORY</a></li>
                <li><a href="#GUESTBOOK">GUESTBOOK</a></li>
				<li><a href="#SITEMAP" >SITEMAP</a></li>
            </ul>
        </nav>
        
    </div>
</header>
<!-- End Header -->

<!-- About Section -->
<div id="INFO" class="page">
	<div class="container">
	    <!-- Title Page -->
	    <div class="row">
	        <div class="span12">
	            <div class="title-page">
	                <h2 class="title">SYNC INFO</h2>
	                <div class="video-container">
	                	<iframe class="video"  src="https://www.youtube.com/embed/SsAJC9Is4Nw" frameborder="0" allowfullscreen ></iframe>
	                </div>
	                <br>
	                <h3 class="title-description" style="line-height: 30px;">
	                	<a href="#" style="font-size: 30px;">SYNC</a>는 성락 성결 교회의 젊은이 공동체로써 20~30대로 이루어져 있으며,
	                	예배는 크게 매주 금요일 저녁 8시 5층 503호 <a href="#">예조이스팀</a>이 진행하는 금요예배와
	                	매주 일요일 오후 1시 3층 예배당 <a href="#">W팀</a>이 진행하는 본 예배가 있습니다. 
	                </h3><br>
                	<hr>
	                <div class="row">
	                	<div class="span6">
			                <h3 class="spec">* 청년부 주일 예배 안내</h3>
							<blockquote>
					            <p>
						            - 시간: 매주 주일 오후 1시<br>
									- 장소: 3층 304호 (대예배실)<br>
									- 찬양인도: W팀
								</p>
			                </blockquote>
						</div>
						<div class="span6">
							<h3 class="spec">* 청년부 금요 예배 안내</h3>
							<blockquote>
					            <p>
							        - 시간: 매주 금요일 저녁 8시<br>
									- 장소: 5층 503호<br>
									- 찬양인도: 예조이스
								</p>
			                </blockquote>
						</div>
					</div>
	            </div>
	        </div>
	    </div>
	    <div class="row">
	        	<div class="span6">
	                <h3 class="spec">사역팀</h3>
	                
	                <div class="tabbable">
	                    <ul class="nav nav-tabs" id="myTab">
	                        <li class="active"><a href="#tab1" data-toggle="tab">예배팀</a></li>
	                        <li class=""><a href="#tab2" data-toggle="tab">W팀</a></li>
	                        <li class=""><a href="#tab3" data-toggle="tab">YE'JOICE</a></li>
	                    </ul>
	                    <div class="tab-content">
	                        <div class="tab-pane fade  active in" id="tab1">
	                        예배 순서 점검 및 안내 부터 싱크뉴스, 기술적인 부분까지 예배를 전체적으로 준비하는 팀입니다.
	                        </div>
	                        <div class="tab-pane fade" id="tab2">
	                          팀이름인 W는 worship의 약자로, 주일 3부예배를 섬기고 있는 찬양팀입니다.
	                        </div>
	                        <div class="tab-pane fade" id="tab3">
	                            Yes 와 rejoice 의 합성어로, 금요예배를 섬기고 있는 찬양팀입니다.    
	                        </div>
                   		 </div>
						</div>
						<br>
						<div class="tabbable">
		                    <ul class="nav nav-tabs" id="myTab">
		                        <li class="active"><a href="#tab4" data-toggle="tab">새가족팀</a></li>
		                        <li class=""><a href="#tab5" data-toggle="tab">미디어홍보팀</a></li>
		                        <li class=""><a href="#tab6" data-toggle="tab">KANAPH</a></li>
		                    </ul>
		                    <div class="tab-content">
		                        <div class="tab-pane fade   active in" id="tab4">
		                            SYNC [청년부] 에 등록 하는 청년들을 기초 훈련 및 케어 하는 팀입니다.    
		                        </div>
		                        <div class="tab-pane fade" id="tab5">
		                            영상, 포스터, 말씀 나눔 카드등 홍보 관련된 컨텐츠를 제작하는 팀입니다. 
		                        </div>
		                        <div class="tab-pane fade" id="tab6">
		                            히브리어로 '날개'란 의미를 가진 팀으로 댄스, 연극등으로 섬기는 문화사역팀입니다    
		                        </div>
	                   		 </div>
						</div>
						 <br>
	        	</div>
	           
	            <div class="span6">
	            	<h3 class="spec">훈련 및 교육 </h3>
	            	<!-- Start Toggle -->
	                <div class="tabbable">
		                    <ul class="nav nav-tabs" id="myTab">
		                        <li class="active"><a href="#tab7" data-toggle="tab">양육 훈련</a></li>
		                        <li class=""><a href="#tab8" data-toggle="tab">제자 훈련</a></li>
		                    </ul>
		                    <div class="tab-content">
		                        <div class="tab-pane fade   active in" id="tab7">
		                           양육자 [멘토] 와 양육생 [멘티] 로 이루어진 1:1 기초훈련으로써 총 3개월간의 기간동안 교제와 나눔을 통해 하나님에 대해 정확히 알아가고 더욱 더 친밀한 관계가 되는 시간을 갖게 됩니다.
		                        </div>
		                        <div class="tab-pane fade" id="tab8">
		                           양육 훈련의 한단계 위의 단계의 훈련으로 성경과 친해지고 말씀과 삶이 어우러 지는 성숙한 신앙인으로 훈련받게 됩니다. 
		                        </div>
	                   		 </div>
						</div>
	                <!-- End Toggle -->
	            </div>
	        </div>
	    <!-- End Title Page -->
	</div>
</div>
<!-- End About Section -->



<!-- Our Work Section -->
<div id="STORY" class="page">
	<div class="container">
    	<!-- Title Page -->
        <div class="row">
            <div class="span12">
                <div class="title-page">
                    <h2 class="title">SYNC STORY</h2>
                    <h4 class="title-description"><a href="#">SYNC 공동체</a>에서는 다양한 감정′소통′나눔′추억이 담긴 많은 이야기가 있습니다.</h4>
                </div>
            </div>
        </div>
        <!-- End Title Page -->
        
        <!-- Portfolio Projects -->
        <div class="row">
        	<div class="span3">
            	<!-- Filter -->
                <nav id="options" class="work-nav">
                    <ul id="filters" class="option-set" data-option-key="filter">
                    	<li class="type-work">SYNC 갤러리</li>
                        <li><a href="#filter" data-option-value="*" class="selected">모든 이야기</a></li>
                        <li><a href="#filter" data-option-value=".design">소통</a></li>
                        <li><a href="#filter" data-option-value=".photography">나눔</a></li>
                        <li><a href="#filter" data-option-value=".video2">즐거움</a></li>
                    </ul>
                </nav>
                <!-- End Filter -->
            </div>
            
            <div class="span9">
            	<div class="row">
                	<section id="projects">
                    	<ul id="thumbs">
                        
							<!-- Item Project and Filter Name -->
                        	<li class="item-thumbs span3 design">
                            	<!-- Fancybox - Gallery Enabled - Title - Full Image -->
                            	<a class="hover-wrap fancybox" data-fancybox-group="gallery" title="2016 농구대회 단체샷" href="<?=CUR_DIR?>/resource/img/work/full/image-01-full.jpg">
                                	<span class="overlay-img"></span>
                                    <span class="overlay-img-thumb font-icon-plus"></span>
                                </a>
                                <!-- Thumb Image and Description -->
                                <img src="<?=CUR_DIR?>/resource/img/work/thumbs/image-01-full.jpg" alt="2016 SYNC에서 3 ON 3 길거리 농구 대회가 진행 되었습니다!! 많은 참여와 많은 먼지(?)속에 즐거웠습니다^^">
                            </li>
                        	<!-- End Item Project -->
                            
							<!-- Item Project and Filter Name -->
                        	<li class="item-thumbs span3 design">
                            	<!-- Fancybox - Gallery Enabled - Title - Full Image -->
                            	<a class="hover-wrap fancybox" data-fancybox-group="gallery" title="2016 농구대회 우승팀" href="<?=CUR_DIR?>/resource/img/work/full/image-02-full.jpg">
                                	<span class="overlay-img"></span>
                                    <span class="overlay-img-thumb font-icon-plus"></span>
                                </a>
                                <!-- Thumb Image and Description -->
                                <img src="<?=CUR_DIR?>/resource/img/work/thumbs/image-02-full.jpg" alt="2016 농구대회 우승팀은 1공동체 경민가정이였습니다!! 짝짝짝">
                            </li>
                        	<!-- End Item Project -->
                            
							<!-- Item Project and Filter Name -->
                        	<li class="item-thumbs span3 video2">
                            	<!-- Fancybox - Gallery Enabled - Title - Full Image -->
                            	<a class="hover-wrap fancybox" data-fancybox-group="gallery" title="포토존 앞에서" href="<?=CUR_DIR?>/resource/img/work/full/image-03-full.jpg">
                                	<span class="overlay-img"></span>
                                    <span class="overlay-img-thumb font-icon-plus"></span>
                                </a>
                                <!-- Thumb Image and Description -->
                                <img src="<?=CUR_DIR?>/resource/img/work/thumbs/image-03-full.jpg" alt="우리 이쁘고 멋진 SYNC 청년들과 함께">
                            </li>
                        	<!-- End Item Project -->
                        	
                        	<!-- Item Project and Filter Name -->
                        	<li class="item-thumbs span3 photography">
                            	<!-- Fancybox - Gallery Enabled - Title - Full Image -->
                            	<a class="hover-wrap fancybox" data-fancybox-group="gallery" title="금요예배 기도시간" href="<?=CUR_DIR?>/resource/img/work/full/image-04-full.jpg">
                                	<span class="overlay-img"></span>
                                    <span class="overlay-img-thumb font-icon-plus"></span>
                                </a>
                                <!-- Thumb Image and Description -->
                                <img src="<?=CUR_DIR?>/resource/img/work/thumbs/image-04-full.jpg" alt="청년들끼리 함께 서로를 위해 중보하는 소중한 시간">
                            </li>
                        	<!-- End Item Project -->
                        	
                        	<!-- Item Project and Filter Name -->
                        	<li class="item-thumbs span3 photography">
                            	<!-- Fancybox - Gallery Enabled - Title - Full Image -->
                            	<a class="hover-wrap fancybox" data-fancybox-group="gallery" title="수련회를 위한 기도모임" href="<?=CUR_DIR?>/resource/img/work/full/image-05-full.jpg">
                                	<span class="overlay-img"></span>
                                    <span class="overlay-img-thumb font-icon-plus"></span>
                                </a>
                                <!-- Thumb Image and Description -->
                                <img src="<?=CUR_DIR?>/resource/img/work/thumbs/image-05-full.jpg" alt="작지만 아주 큰 기도모임. 수련회 준비기간동안 평일에도 기도로 뭉치다.">
                            </li>
                        	<!-- End Item Project -->
                            
                            
							<!-- Item Project and Filter Name -->
                        	<li class="item-thumbs span3 video2">
                            	<!-- Fancybox - Gallery Enabled - Title - Full Image -->
                            	<a class="hover-wrap fancybox" data-fancybox-group="gallery" title="임원 투표 ..." href="<?=CUR_DIR?>/resource/img/work/full/image-06-full.jpg">
                                	<span class="overlay-img"></span>
                                    <span class="overlay-img-thumb font-icon-plus"></span>
                                </a>
                                <!-- Thumb Image and Description -->
                                <img src="<?=CUR_DIR?>/resource/img/work/thumbs/image-06-full.jpg" alt="저 포스터가 엘레베이터 앞에 붙여 있으면 올게 왔구나.. 라고 생각하세요.">
                            </li>
                        	<!-- End Item Project -->
                            
							<!-- Item Project and Filter Name -->
                        	<li class="item-thumbs span3 photography">
                            	<!-- Fancybox - Gallery Enabled - Title - Full Image -->
                            	<a class="hover-wrap fancybox" data-fancybox-group="gallery" title="금요 예배 " href="<?=CUR_DIR?>/resource/img/work/full/image-07-full.jpg">
                                	<span class="overlay-img"></span>
                                    <span class="overlay-img-thumb font-icon-plus"></span>
                                </a>
                                <!-- Thumb Image and Description -->
                                <img src="<?=CUR_DIR?>/resource/img/work/thumbs/image-07-full.jpg" alt="귀한 금요일 저녁을 금요 예배로 섬기는 멋진 Ye'Joice 자매님들..">
                            </li>
                        	<!-- End Item Project -->
                            
                            
							<!-- Item Project and Filter Name -->
                        	<li class="item-thumbs span3 video2">
                            	<!-- Fancybox - Gallery Enabled - Title - Full Image -->
                            	<a class="hover-wrap fancybox" data-fancybox-group="gallery" title="싱크 순환 행사" href="<?=CUR_DIR?>/resource/img/work/full/image-08-full.jpg">
                                	<span class="overlay-img"></span>
                                    <span class="overlay-img-thumb font-icon-plus"></span>
                                </a>
                                <!-- Thumb Image and Description -->
                                <img src="<?=CUR_DIR?>/resource/img/work/thumbs/image-08-full.jpg" alt="조별 팀 대결 게임을 하고 있습니다. 간장 공장 콩간장~">
                            </li>
                        	<!-- End Item Project -->
                            
							<!-- Item Project and Filter Name -->
                        	<li class="item-thumbs span3 video2">
                            	<!-- Fancybox - Gallery Enabled - Title - Full Image -->
                            	<a class="hover-wrap fancybox" data-fancybox-group="gallery" title="사역자 엠티" href="<?=CUR_DIR?>/resource/img/work/full/image-09-full.jpg">
                                	<span class="overlay-img"></span>
                                    <span class="overlay-img-thumb font-icon-plus"></span>
                                </a>
                                <!-- Thumb Image and Description -->
                                <img src="<?=CUR_DIR?>/resource/img/work/thumbs/image-09-full.jpg" alt="사역자들이 모두 함께~ 고기도 굽고, 이야기도 하고 즐거운 시간이였죠오~">
                            </li>
                        	<!-- End Item Project -->
                        	<!-- Item Project and Filter Name -->
                        	<li class="item-thumbs span3 photography">
                            	<!-- Fancybox - Gallery Enabled - Title - Full Image -->
                            	<a class="hover-wrap fancybox" data-fancybox-group="gallery" title="대니 & 올리브" href="<?=CUR_DIR?>/resource/img/work/full/image-10-full.jpg">
                                	<span class="overlay-img"></span>
                                    <span class="overlay-img-thumb font-icon-plus"></span>
                                </a>
                                <!-- Thumb Image and Description -->
                                <img src="<?=CUR_DIR?>/resource/img/work/thumbs/image-10-full.jpg" alt="금요 예배를 함께 섬기시 멋진 형님(대니), 누님(올리브)">
                            </li>
                        	<!-- End Item Project -->
                        	<!-- Item Project and Filter Name -->
                        	<li class="item-thumbs span3 photography">
                            	<!-- Fancybox - Gallery Enabled - Title - Full Image -->
                            	<a class="hover-wrap fancybox" data-fancybox-group="gallery" title="금요 예배 기도시간" href="<?=CUR_DIR?>/resource/img/work/full/image-11-full.jpg">
                                	<span class="overlay-img"></span>
                                    <span class="overlay-img-thumb font-icon-plus"></span>
                                </a>
                                <!-- Thumb Image and Description -->
                                <img src="<?=CUR_DIR?>/resource/img/work/thumbs/image-11-full.jpg" alt="여러분 생길거 같죠? 안생겨요~ 불금엔 금요 예배로 컴온!!">
                            </li>
                        	<!-- End Item Project -->
                        	<!-- Item Project and Filter Name -->
                        	<li class="item-thumbs span3 design">
                            	<!-- Fancybox - Gallery Enabled - Title - Full Image -->
                            	<a class="hover-wrap fancybox" data-fancybox-group="gallery" title="식탁 교제" href="<?=CUR_DIR?>/resource/img/work/full/image-12-full.jpg">
                                	<span class="overlay-img"></span>
                                    <span class="overlay-img-thumb font-icon-plus"></span>
                                </a>
                                <!-- Thumb Image and Description -->
                                <img src="<?=CUR_DIR?>/resource/img/work/thumbs/image-12-full.jpg" alt="(흐뭇)">
                            </li>
                        	<!-- End Item Project -->
                        </ul>
                    </section>
                    
            	</div>
            </div>
        </div>
        <!-- End Portfolio Projects -->
    </div>
</div>
<!-- End Our Work Section -->



<!-- Contact Section -->
<div id="GUESTBOOK" class="page">
	<div class="container">
	    <!-- Title Page -->
	    <div class="row">
	        <div class="span12">
	            <div class="title-page" style="margin-bottom: 50px;">
	                <h2 class="title">SYNC GUESTBOOK</h2>
	                <h3 class="title-description" ><a href="#;">SYNC</a>에 바라는 점 혹은 하고싶은 말을 익명으로 자유롭게 작성해 주세요♥</h3>
	            </div>
	        </div>
	    </div>
	    <!-- End Title Page -->
	    <!-- Contact Form -->
	    <div class="row">
	    	<div class="span12">
	        	<form id="contact-form" class="contact-form" action="#">
	            	<p class="contact-name">
	            		<input id="contact_name" type="text" placeholder="닉네임" value="" name="name" />
	                </p>
	                <p class="contact-message">
	                	<textarea id="contact_message" placeholder="자유롭게 작성해주세요." name="content" rows="5" cols="20"></textarea>
	                </p>
	                <p class="contact-submit" style="text-align: center;">
	                	<a id="submit" class="non-submit" href="#;">확인</a>
	                </p>
	                <div id="response">
	                </div>
	            </form>
	        </div>
	    </div>
	    <!-- End Contact Form -->
	</div>
</div>
<!-- End Contact Section -->

<!-- Twitter Feed -->
<div id="SITEMAP" class="page-alternate">
	<div class="container">
    	<div class="row">
            <div class="span12">
            <h3 style="text-align: center;">SYNC SITE MAP</h3>
	            <ul>
		            <li style="list-style: none; padding-bottom: 10px;">- SYNC 페이스북 <a href="https://www.facebook.com/thesyncpage;">facebook.com/thesyncpage</a></li>  
		            <li style="list-style: none; padding-bottom: 10px;">- SYNC 지난 행사 &nbsp;&nbsp;<a href="http://sync2030.org/?f=2016basketball">sync2030.org</a></li>
		            <li style="list-style: none; padding-bottom: 10px;" >- 성락성결교회 &nbsp;&nbsp;<a href="http://www.sungnak.org;">www.sungnak.org</a></li>
	            </ul>
            </div>
        </div>
    </div>
</div>
<!-- End Twitter Feed -->

<!-- Socialize -->
<div id="social-area" class="page">
	<div class="container" >
    	<div class="row">
            <div class="span12">
            	<h3>페이지 공유하기</h3>
            	<br>
                <nav id="share">
                    <ul>
                        <li><a style="background: #26292E;" href="javascript:share_kakao('SYNC 를 소개합니다♥', '안녕하세요 여러분! 싱크의 A 부터 Z 까지 여러분께 소개 드립니다.' , 'http://sync2030.org/?f=welcome','http://sync2030.org/sync/contents/skin/welcome/resource/img/slider-images/image02.jpg')" title="Follow Me on KAKAO" style="width: 100%; height: auto; "><span class=""  style="font-size: 15px; padding: 5px; color: white;">kakao</span></a></li>
                        <li><a style="background: #26292E;" href="javascript:share_facebook('http://sync2030.org/?f=welcome')" title="Follow Me on FB" style="width: 100%; height: auto;"><span class=""  style="font-size: 15px; padding: 5px; color: white;">facebook</span></a></li>
                        <li><a style="background: #26292E;" href="javascript:share_url('http://sync2030.org/?f=welcome')" title="Follow Me on URL" style="width: 100%; height: auto;"><span class=""  style="font-size: 15px; padding: 5px; color: white;">URL</span></a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- End Socialize -->

<!-- Footer -->
<footer>
<br><br>
	<div class="row">
		<div class="container">
			<div class="span12">
        		<h3>Contact Details</h3>
				 <div class="contact-details">
	                <ul>
	                    <li><a href="http://www.sungnak.org/ChurchIntroduce/MapGuide.php" target="_blank">찾아오시는길 사이트</a></li>
	                    <li>TEL : 02-467-8105~0 <br>ADDRESS : 서울특별시 성동구 성수일로 10길 33 </li>
	                    <l>
	                </ul>
	        	</div>
	        </div>
		</div>
	</div>	
    <div class="row">
    	<div class="container">
			<p class="credits"> <a href="http://www.sungnak.org/" title="SYNC 문구" target="_blank">성락 성결 교회</a> SYNC 청년부가 당신과 함께 합니다 ♥</p>
		</div>
    </div>
</footer>
<!-- End Footer -->

<!-- Back To Top -->
<a id="back-to-top" href="#">
	<i class="font-icon-arrow-simple-up"></i>
</a>
<!-- End Back to Top -->


<div class="back-wrap" style="
    display: table;
    width: 100%;
    height: 100%;
    position: fixed;
    top: 0px;
    left: 0px;
    z-index: 9999; background: hsla(0, 0%, 0%, 0.88);
">
	<div class="back-content" style="  display: table-cell; vertical-align: middle; text-align: center;
">
		<img alt="메인" src="<?=CUR_DIR?>/resource/img/light.jpg" style="height: 100%; width: 100%;" >
	</div>
</div>



