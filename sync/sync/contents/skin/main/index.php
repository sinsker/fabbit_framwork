<!-- Page Wrapper -->
	<section id="banner">
			<div class="inner">
				<h2>S Y N C</h2>
				<p> <br />
				<!-- <a href="http://www.sungnak.org/NextGeneration/SYNCSub.php">Welcome to SYNC</a><br /> -->
				</p>
	</section>
	<div id="page-wrapper">

		<!-- Header
			<header id="header" class="alt">
				<h1><a href="index.html">S Y N C</a></h1>
				<nav id="nav">
					<ul>
						<li class="special">
							<a href="#menu" class="menuToggle"><span>Menu</span></a>
							<div id="menu">
								<ul>
									<li><a href="index.html">Home</a></li>
									<li><a href="generic.html">Generic</a></li>
									<li><a href="elements.html">Elements</a></li>
									<li><a href="#">Sign Up</a></li>
									<li><a href="#">Log In</a></li>
								</ul>
							</div>
						</li>
					</ul>
				</nav>
			</header>
 -->
		<!-- Banner -->
		<!-- One -->
			<section id="one" class="wrapper style2 special">
				<div class="inner">
					<header class="major">
						<h2><br/>성락 성결 교회 청년부 SYNC란?</h2>
						<p>20세 ~ 34세의 청년들로 이루어진 공동체로<br> Sungnak Youth Next & network Community의 줄임말입니다. </p>
					</header>
				</div>
			</section>
		<!-- <ul class=syncslider>
		  <li><div>1</div></li>
		  <li><div>2</div></li>
		  <li><div>3</div></li>
		  <li><div>4</div></li>
		</ul> --> 
		<!-- Two -->
		<!-- 
			<section id="two" class="wrapper alt style2">
				<section class="spotlight">
					<div class="image"><img src="<?=IMG_PATH?>/main/pic01.jpg" alt="" /></div><div class="content">
						<h2>Magna primis lobortis<br />
						sed ullamcorper</h2>
						<p>Aliquam ut ex ut augue consectetur interdum. Donec hendrerit imperdiet. Mauris eleifend fringilla nullam aenean mi ligula.</p>
					</div>
				</section>
				<section class="spotlight">
					<div class="image"><img src="<?=IMG_PATH?>/main/pic02.jpg" alt="" /></div><div class="content">
						<h2>Tortor dolore feugiat<br />
						elementum magna</h2>
						<p>Aliquam ut ex ut augue consectetur interdum. Donec hendrerit imperdiet. Mauris eleifend fringilla nullam aenean mi ligula.</p>
					</div>
				</section>
				<section class="spotlight">
					<div class="image"><img src="<?=IMG_PATH?>/main/pic03.jpg" alt="" /></div><div class="content">
						<h2>Augue eleifend aliquet<br />
						sed condimentum</h2>
						<p>Aliquam ut ex ut augue consectetur interdum. Donec hendrerit imperdiet. Mauris eleifend fringilla nullam aenean mi ligula.</p>
					</div>
				</section>
			</section>
 -->
		<!-- Three -->
			<section id="three" class="wrapper style3 special">
				<div class="inner">
					<header class="major">
						<h2>지난 행사 페이지</h2>
						<p>
						<a href="http://sync2030.org/?f=spring_event" target="_blank">- 성탄 행사 (2015년 12월)</a><br />
						<a href="http://sync2030.org/?f=2015_retreat" target="_blank">- 동계수련회 - 너로 말미암아! (2016년 1월)</a><br />
						<a href="http://sync2030.org/?f=2016basketball" target="_blank">- 상반기 농구 토너먼트 대회 (2016년 4월)</a><br />
						</p>
					</header>
					<!-- <ul class="features">
						<li class="icon fa-paper-plane-o">
							<h3>Arcu accumsan</h3>
							<p>Augue consectetur sed interdum imperdiet et ipsum. Mauris lorem tincidunt nullam amet leo Aenean ligula consequat consequat.</p>
						</li>
						<li class="icon fa-laptop">
							<h3>Ac Augue Eget</h3>
							<p>Augue consectetur sed interdum imperdiet et ipsum. Mauris lorem tincidunt nullam amet leo Aenean ligula consequat consequat.</p>
						</li>
						<li class="icon fa-code">
							<h3>Mus Scelerisque</h3>
							<p>Augue consectetur sed interdum imperdiet et ipsum. Mauris lorem tincidunt nullam amet leo Aenean ligula consequat consequat.</p>
						</li>
						<li class="icon fa-headphones">
							<h3>Mauris Imperdiet</h3>
							<p>Augue consectetur sed interdum imperdiet et ipsum. Mauris lorem tincidunt nullam amet leo Aenean ligula consequat consequat.</p>
						</li>
						<li class="icon fa-heart-o">
							<h3>Aenean Primis</h3>
							<p>Augue consectetur sed interdum imperdiet et ipsum. Mauris lorem tincidunt nullam amet leo Aenean ligula consequat consequat.</p>
						</li>
						<li class="icon fa-flag-o">
							<h3>Tortor Ut</h3>
							<p>Augue consectetur sed interdum imperdiet et ipsum. Mauris lorem tincidunt nullam amet leo Aenean ligula consequat consequat.</p>
						</li>
					</ul> -->
				</div>
			</section>

		<!-- CTA -->
			<section id="cta" class="wrapper style4 special">
				<div class="inner">
					<!-- <header>
						<h2>Arcue ut vel commodo</h2>
						<p>Aliquam ut ex ut augue consectetur interdum endrerit imperdiet amet eleifend fringilla.</p>
					</header> -->
						<ul class="actions ">
							<li><a href="http://www.sungnak.org" class="button fit special" target="_blank">성락성결교회 페이지</a></li>
							<li><a href="#banner" class="button fit">위로</a></li>
						</ul>
				</div>
			</section>

		<!-- Footer -->
			<footer id="footer">
				<!-- <ul class="icons">
					<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
					<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
					<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
					<li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
					<li><a href="#" class="icon fa-envelope-o"><span class="label">Email</span></a></li>
				</ul> -->
				<ul class="copyright">
					<li>2016 Sungnak Youth Next & network Community</li><li><!-- Design: <a href="http://html5up.net">HTML5 UP --></a></li>
				</ul>
			</footer>
	</div>
	
	<? Unit::load('popup', array('id' => 'smain' , 'sdate'=> '2016-04-19','edate'=>'2016-04-24 16:00:00', 'title' => '<i class=" fa fa-dribbble fa-spin"></i> 농구 경기 4월 24일 빅매치!!<i class=" fa fa-dribbble fa-spin "></i>',
		'content' => '
			<p>
				▶1경기 [3-4위전]<br>
				<span>세은+예승 VS 휘진+지현 </span><br>
				▶2경기 [최종결승]<br>
				<span>경민 VS 기주</span>
			</p>
			<p>
				<sub>
					드디어 2016 SYNC배 상반기 농구대회 최고의 팀을 가리는 날이 왔습니다!!<br>
					당일 농구경기를 보러오시는 모든 분들께 음료 드리오니 많이 응원 하러 와주시기 바랍니다.
				</sub>
			<br><br>
			<a href="http://sync2030.org/?f=2016basketball" target="_blank" style="color:blue">2016 SYNC 농구 토너먼트 정보 보러가기</a>
			</p>'));?>
