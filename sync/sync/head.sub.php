<?if (!defined('SIN')) exit;?>
	<?php 
	$module = Module::getInstance();
	$currentPage = $module->getModuleContent($pg['f']);
	?>
	<meta property="og:type" content="website">
	<meta property="og:title" content="<?=$currentPage['p_name']?>">
	<meta property="og:description" content="<?=$currentPage['p_subname']?>">
	<meta property="og:url" content="http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>" >
	<script type="text/javascript">
		var path      = '<?=ROOT?>';
		var url       = '<?=URL?>';
		var isMobile    = '<?=$device['isMobile']?>';
		var charset   = '<?=CHAR_SET?>';
		var serverTime = '<?=TIME_FULL?>';
	
		var evt_id = '<?=$pg['f']?>';
	</script>
	<link rel="stylesheet" type="text/css" href="<?=CSS_PATH?>/jquery-ui.min.css" />
	<link rel="stylesheet" type="text/css" href="<?=CSS_PATH?>/default.css" />
<?
	@include_once $_SERVER['DOCUMENT_ROOT'].CONTENT.'/'.$pg['f'].'/css.php';
	@include_once $_SERVER['DOCUMENT_ROOT'].CONTENT.'/'.$pg['f'].'/'.$pg['v'].'.css.php';
?>
