<?
$member = new Member($db);
$cnt = 0;

if(!$cmd) alert('잘못된 요청입니다.');

switch ($cmd){
	case 'insert_member': #회원 등록
		if(!empty($_FILES['member_execl'])){ # 엑셀 업로드
			$execlDatas = getExcelDatas($_FILES['member_execl']);
			for ($i = 2; $i <= $execlDatas->sheets[0]['numRows']; $i++) {
				
				$memData = array();
				for ($j = 1; $j <= $execlDatas->sheets[0]['numCols']; $j++) { //1:이름 2:그룹 3:전화번호 4:기수 5:생일 6: 성별
					#================================================#
					$data = $execlDatas->sheets[0]['cells'][$i][$j];
					#================================================#
					if(!$data){ continue; }
					
					#이름
					if($j == 1){
						$memData['m_name'] = $data;
					}
					#그룹
					if($j == 2){
						$memData['m_group'] = $data;
						$memData['m_group_name'] = $member_group[$data];
					}
					
					#전화번호
					if($j == 3 && strlen($data) <= 13 ){ 
						$data = str_replace('-','',$data);
						$memData['m_tel'] = $data;
					}
					
					#기수
					if($j == 4){
						if(strlen($data) == 4){
							$data =substr($data, 2, 2);
						}
						$memData['m_giso'] = $data;
					}
					#생일
					if($j == 5){ 
						if(strlen($data) >=3 ){
						 	$tmp_data = explode('/',$data);
						 	if(strlen($tmp_data[0]) == 1){$tmp_data[0] = '0'.$tmp_data[0];}
						 	if(strlen($tmp_data[1]) == 1){$tmp_data[1] = '0'.$tmp_data[1]; }
						 	$data = $tmp_data[0].$tmp_data[1];
						}else{
							$data = '';
						}
						
						$memData['m_birthday'] = $data;
					}
					
					//$line[$j] = $data;
					
				}
				if(empty($memData['m_name'])) continue; //이름이 빈칸이면 넣지 않는다.
				
				$user = $member->getMemberData("and m_tel = '{$memData['m_tel']}' and m_name= '{$memData['m_name']}'");
				if(empty($user['idx'])){	#해당 맴버가 없을 경우 추가
					
					$memData['m_insert_date'] = date('Y-m-d H:i:s');
					$values = createWhereText($memData,',');
					$member->insertMemberData($values);
					
					$cnt++;
				}else{ #해당 맴버가 있을 경우 수정
					
				}
			}
			alert('총'. $cnt.'명을 등록하였습니다.','/admin/?f=member');
		}
		break;
		
	case 'edit_member': #회원 추가/수정
		
		if($m_birth){
			$tmpBirth =  explode('-',$m_birth);
			$m_birthday = (strlen($tmpBirth[1]) == 1 ? '0'.$tmpBirth[1] : $tmpBirth[1]).(strlen($tmpBirth[2]) == 1 ? '0'.$tmpBirth[2] : $tmpBirth[2]);
			$m_giso = substr($tmpBirth[0],2,2);
		}
		
		if($idx){
			$result = $member->updateMemberData($m_name, $m_group, $m_tel, $m_giso, $m_birthday, $m_sex, $m_join_date, $m_memo, $idx);
		}else{
			$result = $member->setMemberData($m_name, $m_group, $m_tel, $m_giso, $m_birthday, $m_sex, $m_join_date, $m_memo);
			$idx = $result;
		}
		
		if($result){
			if(!empty($_FILES['m_img']['name'])){
				 $isUpload = fileUpload($_FILES['m_img'], array('png','jpg','gif','jpeg'), '/upload/profile/');
				 if($isUpload){ #업로드가 성공할경우 이미지 추가
				 	$result = $member->setMemberImage($isUpload, $idx);
				 }
			}
			
			alert('등록 되었습니다.','?f=member&v=mview&midx='.$idx);
			
		}else{ alert('등록에 실패하였습니다.'); }
		break;
	case 'delete_member':
		if(empty($ids)){ $msg = '삭제 실패 정상적인 값이 넘어오지 않았습니다.'; }
			$result = $member->deleteMembersData($ids);
			if($result){
				$msg = '정상적으로 삭제 되었습니다.';
			}
		
		echo json_encode( array( 'msg' =>  $msg,'result'=>$result ) );
		exit;
		break;
		
	case 'member_sample_down': #샘플 파일 다운
		fileDownload($_SERVER['DOCUMENT_ROOT'].'/tmp/static/sample.xls');
		break;
		
	case 'write_comment': #회원 코멘트 달기
		$type = 'membercomment';
		#$midx 
		#$memo
		$mc_write_id =  $_SESSION[$userAuth->session_id];
		$mc_write_name = $_SESSION[$userAuth->session_name];
		$result = $member->setMemberCommentData($type, $midx, $memo, $mc_write_id, $mc_write_name);
		if($result){
			$msg ='등록 되었습니다.';
		}else{
			$msg ='등록에 실패하였습니다.';
		}
		
		
		$returnArray = array('result' => $result, 'msg' => $msg );
		echo json_encode( $returnArray );
		break;
		
	case 'update_comment' :
		$result = $member->updateMemberCommentData($idx, $midx, $mc_memo);
		if($result){
			$msg ='수정 되었습니다.';
		}else{
			$msg ='수정에 실패하였습니다.';
		}
		
		$returnArray = array('result' => $result, 'msg' => $msg );
		echo json_encode( $returnArray );
		break;
	
	case 'delete_comment' :
		$result = $member->deleteMemberCommentData($idx);
		if($result){
			$msg ='삭제 되었습니다.';
		}else{
			$msg ='삭제에 실패하였습니다.';
		}
	
		$returnArray = array('result' => $result, 'msg' => $msg );
		echo json_encode( $returnArray );
		break;
		
		
	case 'insert_team': #팀 등록
			$no = $member->setTeamData('team', $t_name, $t_memo);
			if($no){
				$result = 'done';
				$msg = '등록 되었습니다.';
			}else{
				$result = 'fail';
				$msg = '등록에 실패하였습니다.';
			}
			
			$returnArray = array('result' => $result, 'msg' => $msg, 'no' => $result );
			echo json_encode( $returnArray );
		break;
	case 'team_del': #팀 삭제 
		$result = $member->deleteTeamData($idx);
		if($no){
			$result = 'done';
			$msg = '삭제 되었습니다.';
		}else{
			$result = 'fail';
			$msg = '삭제에 실패하였습니다.';
		}
			
		$returnArray = array('result' => $result, 'msg' => $msg, 'no' => $result );
		echo json_encode( $returnArray );
		exit;
		break;
	case 'detail_team': #팀 상세보기
		$team = $member->getTeamDetailDate($idx);
		if($team) $result = 'done';
		
		$returnArray = array('html' => $team, 'result' => $result);
		echo json_encode( $returnArray );
		exit;
		break;
	
	case 'insert_team_member': #팀원 등록
		if(!$midx || !$tidx) $result ='fail';
		#$mname
		#tm_position
		$result = $member->setTeamMemberData($tidx, $midx, $mname, $tm_position);
		if($result){
			$result = 'done';
			$msg = '추가하였습니다.';
		}else{
			$result = 'fail';
			$msg = '등록에 실패하였습니다.';
		}
			
		$returnArray = array('result' => $result, 'msg' => $msg, );
		echo json_encode( $returnArray );
		break;
		
	case 'update_gm_position': #실시간 팀원 직급 변경
		if(!$idx) exit;
		$result = $member->updateTeamMemberPosition($position,$idx);
		break;
			
	case 'delete_gm': #팀원 삭제 
		if(!$idx) exit;
		$result = $member->deleteTeamMember($idx);
		if($result){
			$returnArray = array('result' => 'done', 'msg' => '삭제되었습니다.', );
		}
		echo json_encode( $returnArray );
		break;
		
		
	case 'get_name': #검색한 이름에 대한 리스트 가져오기
		 $notInMembers = $member->getGroupMemberData();
		 if(!empty($notInMembers)){
			 $notInMemberWhere = '';
			 foreach($notInMembers as $gm){
			 	 $notInMemberArray[] =  $gm['gm_member_idx'];
			 }
			$notInMemberWhere =  " and idx not in(".@join(',',$notInMemberArray).")";
			$where_non_leader_id.= $notInMemberWhere;
		 }
		 
		if($leader_id_str){ $where_non_leader_id .= " and idx not in($leader_id_str)"; } #group 빼기 
		if($group_member_ids_str){ $where_non_leader_id .= " and idx not in($group_member_ids_str)"; } #group 빼기 
		list($list, $paging) = $member->getMemberList(1,"$where_non_leader_id and m_name like '%$name%' ",30);
		$html = '';
		if(!empty($list)){
			foreach($list as $data){
				if($op != 'group'){
				 	$onclick= "onclick=\"team.memberinfo({$data['idx']})\"";
				}
				$html  .= "<li id='{$data['idx']}' class='mb list-group-item' {$onclick} ><span>{$data['m_name']} [{$data['m_group_name']}]</span></li>";
			}
		}else{
			$html  .= "<li class='mb list-group-item non-item' ><span>이름이 없습니다.</span></li>";
		}
		echo json_encode( array('html' => $html) );
		exit;
		break;
		
	case 'get_member': #사용자 정보 가져오기
		if($midx){ $where = 'and idx ='.$midx;}
		else{ alert('잘못된 접근입니다.'); }
		$user = $member->modifyMemberData($member->getMemberData($where));
		
		echo json_encode( $user );
		exit;
		break;	
	
	case 'edit_group': #그룹 추가 & 수정
		if($group_member_ids_str){  #그룹 idx 가져왔는지?
			$where_gmember_id = " and idx in($group_member_ids_str)"; 
		} 
		
		if($gidx){#수정
			$member->deleteGroupMemberData($gidx);
			
			if($group_member_ids_str){
				$set = array(
					'g_type' => $g_type
				);
				$where = createWhereText($set,',');
				$member->updateGroupData($where,$gidx);
				list($groupMemberArray,$pageing) = $member->getMemberList(1,$where_gmember_id,30);
				foreach ($groupMemberArray as $gmember){
					$member->insertGroupMemberData($gidx, $gmember['idx'], $gmember['m_name']);
				}
			}
			$result ='done';
			$msg = '수정이 완료 되었습니다.';
		}else{
			$group = $member->getGroup("and g_leader_id = $leader_id_str");
			
			if(!empty($group)){
				$msg = '이미 추가되있는 가정입니다.';
			}else{
			
				if(!$group_member_ids_str){
					$msg = '가정원을 한명 이상 추가해 주세요.';
				}else{
					$leader = $member->getMemberData("and idx = $leader_id_str");
					if(!$leader){ 
						$msg = "리더의 아이디가 없습니다.";
					}else{
						$insert_id = $member->insertGroupData($g_type, $leader['m_name'], $leader_id_str);
					}
					
					if($insert_id){
						list($groupMemberArray,$pageing) = $member->getMemberList(1,$where_gmember_id,30);
						foreach ($groupMemberArray as $gmember){
							$member->insertGroupMemberData($insert_id, $gmember['idx'], $gmember['m_name']);
						}
						$result ='done';
						$msg = '추가가 완료 되었습니다.';
					}
				}
			}
		}
		
		echo json_encode( array( 'msg' =>  $msg,'result'=>$result ) );
		exit;
		break;
		
	case 'delete_group':
		$member->deleteGroupMemberData($gidx);
		$result = $member->deleteGroupData($gidx);
		if($result){
			$result = 'done';
			$msg =  '삭제가 완료 되었습니다.';
		}else{
			$msg = '삭제 실패하였습니다.';
		}
		
		echo json_encode( array( 'msg' =>  $msg,'result'=>$result ) );
		exit;
		break;
		
}



?>