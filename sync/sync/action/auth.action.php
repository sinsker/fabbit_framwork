<?
   $admin = new Administer($db);

if($_REQUEST['type'] != 'get'){
	foreach($_POST as $key => $value ){
		if(empty($value)){
			$msg = '빈칸을 모두 입력해주세요.'; 
			$result = 'fail'; 
			define('IS_PASS', TRUE);
			break; 
		}else{$$key = $value; }
	}
}


# POST 값에 빈칸이 없을경우 아래 실행
if(!defined('IS_PASS')){ 
	switch ($cmd){
		case 'join':
			$joinUser = $admin->adminGetInfo($id);
			
			if(!$joinUser){
				$result = $admin->adminJoin($id,$name,$password,$group,$admin_group[$group]);
				if($result){
					$msg = '회원가입이 완료되었습니다.';
					$result = 'done';
					$return_url = '/admin/';
					
				}else{
					$msg = '회원가입이 실패하였습니다. 다시 확인해 주세요.';
					$result = 'fail';
				}
			}else{
				$msg = '중복되는 아이디입니다. 다른 아이디를 사용해주세요.';
				$result = 'fail';
			}
			$admin->adminLog($cmd, $id, $result, $msg);
			break;
			
		case 'login':
			
			if($password == 'autologin'){ $auto = true;}
			$administrator = $admin->adminLogin($id, $password, $auto);
			
			if($administrator){
				$msg = '로그인이 성공하였습니다.';
				$result = 'done';
				$return_url = '/admin/';
				
				$_SESSION['reset'] = ''; #초기 로그인 
				$admin->setSession($administrator['a_id'],$administrator['a_name'],$administrator['a_group'],$administrator['is_use']);
				
				setcookie('a', 'y', time()+(60*60*24 * 15), '/');
				setcookie('a_id', $administrator['a_id'], time()+(60*60*24 * 15), '/');
				setcookie('a_name', $administrator['a_name'], time()+(60*60*24 * 15), '/');
			}else{
				$msg = '아이디 혹은 비밀번호를 다시 확인해 주세요.';
				$result = 'fail';
			}
			
			$admin->adminLog($cmd, $id, $result, $msg);
			break;
		
		case 'reset':
			setcookie('a', 'n', 0, 0);
			setcookie('a_id', '', 0, 0);
			setcookie('a_name', '', 0, 0);
			$_SESSION['reset'] = 'y';
			goto_url('/admin/?');
			break;
			
		case 'logout':
			$admin->adminLog($cmd, $_SESSION[$admin->session_id], 'done', '로그아웃');
			$admin->adminLogout();
			alert('로그아웃이 되셨습니다.','/admin/');
			break;
	}
}


$returnArray = array( 'return_url'=>$return_url, 'msg'=>$msg, 'result'=>$result );
echo json_encode($returnArray);

?>