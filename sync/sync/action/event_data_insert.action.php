<?
#############################################################
## 단일 페이지 관련 데이터 저장
#############################################################
	if(empty($evt_id)) exit;
	
	include_once $_SERVER['DOCUMENT_ROOT'].'/inc/class/EventDataAccess.class.php'; #페이지 모듈 관리
	include_once $_SERVER['DOCUMENT_ROOT'].'/inc/class/Dao/Massge.class.php'; #메세지 
	
	include_once $_SERVER['DOCUMENT_ROOT'].'/'.SKIN_PATH.'/'.$evt_id.'/proc.php'; #상속받은 클래스
	$eventDataAccess = new syncEventDataAccess($db, $evt_id);
	
	$data = $_POST;
	
	$eventDataAccess->eventMassgeSetting(); //메시지 세팅
	$eventMassge = $eventDataAccess->getMassage(); //메시지 객체 불러오기
	
	#중복 체크
	$check = $eventDataAccess->isDataDuplication($data);
	
	if($check){
		$msg = $eventMassge->getDuplication();
		$result = false;
	}else{
		$result = $eventDataAccess->eventInsertData($data);
		if($result){
			$msg = $eventMassge->getSuccess();
		}else{
			$msg = $eventMassge->getFail();
		}
	}
	
	echo json_encode(array('result'=> $result, 'msg'=> $msg ));	
?>