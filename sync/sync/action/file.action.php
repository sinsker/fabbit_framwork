<?
$fileManager = FileManager::getInstance();

$baseUrl = '/upload'.($type ? '/'.$type : ''); 	#기본 url
$basePath = $_SERVER['DOCUMENT_ROOT'].$baseUrl; #기본 경로
$fileManager->uploadFolerinit($basePath);

switch($mode){
	
	case 'UPLOAD':
		if (empty($_FILES)){ echo '파일이 존재 하지 않습니다.'; exit; }
		if(empty($type)){ echo '업로드하려는 타입을 정확히 입력해 주세요.'; exit; }
		if($folder){$basePath = $basePath.'/'.$folder;}
		$fileManager->setCondition($type);
		$result = $fileManager->fileUpload($_FILES['Filedata'],$basePath);
		$fileManager->setFileData( $result['extension'], $type, $basePath, $baseUrl, $result['name'], $result['realname'], $result['size'], $result['mine']);
		break;
		
	case 'MOBILE_UPLOAD':
		if (empty($_FILES)){ echo '파일이 존재 하지 않습니다.'; exit; }
		if(empty($type)){ echo '업로드하려는 타입을 정확히 입력해 주세요.'; exit; }
		$fileManager->setCondition($type);
		$_FILES['mobile-upload'] = $fileManager->reArrayFiles($_FILES['mobile-upload']);
		if($folder){
			$basePath = $basePath.'/'.$folder;
			$baseUrl = $baseUrl.'/'.$folder;
		}
		foreach($_FILES['mobile-upload'] as $key => $file ){
			unset($result);
			$result = $fileManager->fileUpload($file,$basePath);
			$fileManager->setFileData( $result['extension'], $type, $basePath, $baseUrl, $result['name'], $result['realname'], $result['size'], $result['mine']);
		}
		
		echo "<script> alert('업로드가 완료 되었습니다.'); parent.location.replace('/admin/?f=file&v=".($type == 'file'?$type : '')."'); </script>";
		break;
		
	case 'SET_DATA':
		$fileDatas = array();
		$fileManager->setArrayFileData($fileDatas);
		break;
	
	case 'ADD_FOLDER': #폴더 추가
		if(empty($folder_name)) alert('파일이름을 입력해주세요.');
		if(is_dir($basePath.'/'.$folder_name)) alert('이미 폴더가 생성되어 있습니다.');
		mkdir($basePath.'/'.$folder_name,0777);
		
		echo "<script> parent.location.replace('/admin/?f=file&v=".($type == 'file'?$type : '')."'); </script>";
		break;
		
	case 'DELETE': # 파일 삭제
		if (empty($idx)){ echo '파일이 존재 하지 않습니다.'; exit; }
		$fileData = $fileManager->getFileData(' and idx = '.$idx);
		$filePath = $fileData['f_path'].'/'.$fileData['f_name'];
		
		$fileManager->deleteFileData(' and idx = '.$idx);
		$fileManager->fileDelete($filePath);
		
		echo json_encode(array('result' => true, 'msg'=>'삭제 하셨습니다.'));
		exit;
		break;
		
	case 'DOWNLOAD':
		if (empty($idx)){ alert('파일이 존재하지 않습니다.'); }
		$fileData = $fileManager->getFileData(' and idx = '.$idx);
		$filePath = $fileData['f_path'].'/'.$fileData['f_name'];
		if(!is_file($filePath)){
			alert('파일이 존재하지 않습니다.');
		}
		$fileManager->fileDownload($fileData['f_path'].'/'.$fileData['f_name'], $fileData['f_realname']);
		exit;
		break;
		
	case 'FOLDER_RENAME': # 폴더 이름 변경
		if (empty($old_folder)){ alert('폴더정보가 정확하지 않습니다.'); }
		if (empty($folder)){ alert('폴더정보가 정확하지 않습니다.'); }
		if(is_dir($basePath.'/'.$folder)) alert('해당 폴더 이름은 이미 존재합니다.');
		
		if(is_dir($basePath.'/'.$old_folder)){ 
			rename($basePath.'/'.$old_folder, $basePath.'/'.$folder);
			echo "<script> parent.location.replace('/admin/?f=file&v=file'); </script>";
			exit;
		}else{
			alert($basePath.'/'.$old_folder.'변경하려는 폴더가 존재하지 않습니다.');
		}
		break;
		
	case 'FOLDER_DELETE': # 폴더 삭제
		if (empty($folder))alert('폴더정보가 정확하지 않습니다.'); 
		$entry = $dirs = $is_file ='';
		$dirs = dir($basePath.'/'.$folder);
		while(false !== ($entry = $dirs->read())){ // 읽기
			if(($entry != '.') && ($entry != '..')) {
				$is_file = true;
			}
		}
		if($is_file) alert('해당 폴더안에 파일이 존재합니다.\\n 파일을 모두 삭제 후 진행해주세요.');
		if(!is_dir($basePath.'/'.$folder)) alert('해당 폴더가 존재하지 않습니다.');
		
			
		rmdir($basePath.'/'.$folder);
		echo "<script> parent.location.replace('/admin/?f=file&v=file'); </script>";
		exit;
		break;
	}
	
?>