<?
$note = new Note($db);
$cnt = 0;

if(!$cmd) alert('잘못된 요청입니다.');

switch ($cmd){
	case 'insert_note':
		$type ='history';
		$title ='회의록';
		$writeId = $_SESSION[$userAuth->session_id];
		$writeName = $_SESSION[$userAuth->session_name];
		
		if($idx){
			$result = $note->updateNoteData($type, $title, htmlspecialchars($n_memo), $writeId, $writeName, $idx);
			$msg = '수정 되었습니다.';
		}else{
			$result = $note->setNoteData($type, $title, htmlspecialchars($n_memo), $writeId, $writeName);
			$idx = $result;
			$msg = '추가 하였습니다.';
		}
		
		if($result){ $result = 'done';}
		else{ $result ='fail'; }
		
		break;
		
	case 'delete_note':
		if($idx){
			$result = $note->deleteNoteData($idx);
			$msg = '삭제 되었습니다.';
		}
		break;
		
	case 'share_url':
		if($idx){
			$result = "http://sync2030.org/?f=note&code=".(base64encode($idx));
		}
		break;
}

$resultArray = array("result" => $result, "msg" => $msg, "idx" => $idx);
echo json_encode($resultArray);


?>