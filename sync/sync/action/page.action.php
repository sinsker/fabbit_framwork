<?
	$moduleManager = Module::getInstance();
	
	$createTemplatePages = array(
			'index'	=>	array('/index.php'),
			'css' 	=>  array('/css.php',	file_get_contents($_SERVER['DOCUMENT_ROOT'].'/'.TEMPLATE_PATH.'/header.tpl')),
			'js'	=>  array('/js.php',	file_get_contents($_SERVER['DOCUMENT_ROOT'].'/'.TEMPLATE_PATH.'/footer.tpl')),
			'proc'	=>  array('/proc.php',	file_get_contents($_SERVER['DOCUMENT_ROOT'].'/'.TEMPLATE_PATH.'/event_data_extends.tpl')),
			'init'	=>  array('/init.php')
	);
	
	$default_proc = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/'.TEMPLATE_PATH.'/event_data_extends.tpl');
	
	switch($mode){
		case "initcreate":
			if(empty($p_key)){ alert('키값이 정상적으로 넘어오지 않았습니다.'); }
			if(empty($p_name)){ alert('제목값이 정상적으로 넘어오지 않았습니다.'); }
			if(empty($p_subname)){ alert('부제목값이 정상적으로 넘어오지 않았습니다.'); }
			$tmpModule = $_SERVER['DOCUMENT_ROOT'].'/'.SKIN_PATH.'/'.$p_key;
			if(is_dir($tmpModule)){ 
				alert('이미 만들어진 모듈 페이지입니다.');
				exit;
			}else{
				mkdir($tmpModule,0777);
				
				foreach ($createTemplatePages as $key => $tempPage){
					$pg =  fopen($tmpModule.$tempPage[0],"w") or alert("파일을 생성할수 없습니다.");
					$temp = $tempPage[1] ? $tempPage[1] : '';
					
					fwrite($pg, $temp);
					fclose($pg);
				}
				
				$result = $moduleManager->initPage($p_key, $p_name, $p_subname, $p_content, $p_security, $p_securitycode);
				if($result){
					$createPage = $moduleManager->getModuleContent($result);
					alert('페이지가 완성 되었습니다.','?f=page&v=mdetail&key='.$p_key);
				}
			}
			break;
			
		case "private_approval":
			$module = $moduleManager->getPrivateModuleContent($p_idx,$code);
			if($module){
				$result = true;
				set_cookie($p_idx, $code, 3600);
			}else{
				$msg = "코드가 맞지않습니다.";
				$result = false;
			}
			echo json_encode(array( 'result'=> $result, 'msg'=> $msg , 'url'=>'?f=page&v=mdetail&key='.$module['p_key'] ) );
			exit;
			break;
			
		case 'build':
			if(empty($p_name))		$msg = '타이틀을 작성해주세요.';
			if(empty($p_subname))	$msg = '부제목을 작성해주세요.';
			if(empty($p_top))		$msg = '상단 네비게이션을 선택해주세요.';
			if(empty($p_bottom))	$msg = '하단 네비게이션을 선택해주세요.';
				
			/* 공통 속성 변경 */
			$setData = array(
					'p_name'	=> $p_name,
					'p_subname'	=> $p_subname,
					'p_head'	=> $p_top,
					'p_footer'	=> $p_bottom
				);
			
			$result = $moduleManager->updatePage($p_key, $setData);
			/* END 공통 속성 변경  */
			if($result) $msg = '성공적으로 수정되었습니다.';
			
			
			
			/* $modulePage = $_SERVER['DOCUMENT_ROOT'].'/'.SKIN_PATH.'/'.$p_key;
			   if($p_type == 'professional'){ //전문가용
				if(!is_dir($modulePage)){
					$msg = "정상적인 페이지가 아닙니다. 삭제 후 새로 추가해 주시기 바랍니다.";				
					$result = false;
				}else{
					$moduleManager->fileWrite($modulePage.$createTemplatePages['index'], stripslashes($professional_html));
					$moduleManager->fileWrite($modulePage.$createTemplatePages['css'], stripslashes($professional_css));
					$moduleManager->fileWrite($modulePage.$createTemplatePages['js'], stripslashes($professional_js));
					
					//직렬화를 위한 배열 저장
					$content = array(
						'js'	=> $professional_js,
						'html'	=> $professional_html,
						'css'	=> $professional_css
					);
					
					$setData = array(
						'p_content' => addslashes(serialize($content))
					);
					
					$reuslt = $moduleManager->professionalBuildPage($p_key, $setData);
					if($reuslt){
						$msg="성공적으로 적용 되었습니다.";
						$result = true;
					}else{
						$msg="데이터값이 정상적으로 보존 되지 않았습니다.";
						$result = false;
					}
				}
			}else{ //기본형
				
			} */
			echo json_encode(array( 'result'=> $result, 'msg'=> $msg ));
			exit;
			break;
		
		//기본형 카테고리 생성
		case 'basic_module_category':
			if($category){
				$categoryList = $moduleManager->category;
				$result = true;
			}
			echo json_encode(array( 'result'=> $result, 'categoryDetail'=> $categoryList[$category] ));
			exit;
			break;
		
		//기본형 모듈 컨텐츠 추가
		case 'basic_module_create':
			if(empty($p_key)) alert('정상적인 데이터가 넘어오지 않았습니다.');
			if(empty($category)) alert('카테고리를 선택해주세요.');
			if(empty($category_detail)) alert('상세 카테고리를 선택해주세요.');
			if($moduleManager->basicPageCreate($p_key, $category.':'.$category_detail)){
			}
			goto_url('?f=page&v=mdetail&key='.$p_key);
			exit;
			break;
			
		case 'basic_module_form_open':
			echo json_encode(array( 'result'=> $result, 'value'=> '' ));
			exit;
			break;
	}
	
	
?>