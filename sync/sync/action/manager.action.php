<?
 $admin = new Administer($db);

if(!$cmd) alert('잘못된 요청입니다.');

switch ($cmd){
	case 'approval':
		$result = $admin->adminSwitchUse($idx,'Y');
		if($result){
			$url = '?f=manager';
		}else{
			$msg = '사용허가가 되지 않았습니다. 다시 시도하여 주시기 바랍니다.';
		}
		break;
		
	case 'dismiss':
		$result = $admin->adminSwitchUse($idx,'N');
		if($result){
			$url = '?f=manager';
		}else{
		}
		break;
		
	case 'delete':
		$result = $admin->adminDelete($idx);
		if($result){
			$url = '?f=manager';
		}else{
			$msg = '삭제가 되지 않았습니다. 다시 시도하여 주시기 바랍니다.';
		}
		break;
		
}

$resultArray = array("result" => $result, "msg" => $msg, "url" => $url );
echo json_encode($resultArray);


?>