<?

$code_arr = array('성령이오시면','성령안에서사십니까','예수의이름으로','길이보이나요','부활과말씀의증언',
		'갈릴리로가라','안뜰에서','사랑과두려움은만나지않는다');

switch($order){
	case 'code_insert':
		
		$_code = str_replace(" ", "", $code);
		$_code = preg_replace ("/[ #\&\+\-%@=\/\\\:;,\.'\"\^`~\_|\!\?\*$#<>()\[\]\{\}]/i", "", $_code);
		$name = trim($name);
		
		 if($_SESSION['fam_name'] == $name){
			$result = true;
			echo json_encode( array('result'=>$result ));
			exit;
		} 
		
		$sql = "select * from fam_game where fam_name = '{$name}' ";
		$res = $db->query($sql);
		$team = $res->fetch_array();
		
		
		if( (in_array($_code, $code_arr) && $team['type'] == '다윗') || ($_code == '사울' &&  $team['type'] == '사울')) {
			$resultCheck = true;
		}
		#값이 있다면
		if($resultCheck){
			
			if(!$team['misson1'] || !$team['misson2'] || !$team['misson1']){
			
				if($team['type'] == '다윗'){
					$mission_array = array();
					$mission = array();
					while( true){
						
						if(count($mission) >= 3) break;
						
						$sql = "select mission,idx  from fam_mission where `use` = 0";
						$res = $db->query($sql);
						while($row = $res->fetch_array()){
							$mission_array[] = $row['mission'];
						}
						$c = $mission_array[mt_rand( 0, count($mission_array) - 1)];
						
						$sql2 = "select mission,idx  from fam_mission where `use` = 1";
						$res2 = $db->query($sql2);
						while($row2 = $res2->fetch_array()){
							$use_mission_array[] = $row2['mission'];
						}
						
						if(count($use_mission_array > 0)){
							if(in_array($c, $use_mission_array)) continue;
						}
						
						$sql = "update fam_mission set `use` = 1 where mission = '$c'";
						$db->query($sql);
						$mission[] = $c;
					}
				}else{
					
					$mission[0] = ' 다윗팀이 어린이대공원에서 미션을 진행하고 있습니다.';
					$mission[1] = ' 도둑팀 섬김이의 전신샷을 찍어주세요.';
					$mission[2] = ' 3팀이상의 사진을 찍어서 카톡방에 인증받으면 승리';
				}
				
				$sql = "update fam_game set
						misson1 = '{$mission[0]}',
						misson2 = '{$mission[1]}',
						misson3 = '{$mission[2]}',
						use_code	= '$code',
						use_code_date = '".date('H:i:s')."'
						where
						fam_name = '{$name}'";
				$db->query($sql);
			}
			
			$_SESSION['code'] = $_code;
			$_SESSION['fam_name'] = $name;
			$result = true;
		}else{
			$result = false;
			$msg = '코드가 틀렸습니다.';
		}
		
		echo json_encode( array('result'=>$result ,'msg'=>$msg ));
		break;
			
		case 'view':
		$sql = "select *  from fam_game";
		$res = $db->query($sql);
		$html = '<tr><th>이름</th><th>역할</th><th>상황</th><th>코드</th><th>어대도착시간</th><th>미션</th></tr>';
		while($row = $res->fetch_array()){ 
			$color = '';
			if($row['type'] == '다윗'){
				if($row['use_code_date'] == '' ){
					$row['state'] = '이동중';
					
				}else{
					$row['state'] = '미션 수행중';
					$color = 'class="info"';
				}
			}
			
			if($row['type'] == '사울'){
				if($row['use_code_date'] == '' ){
					$row['state'] = '대기중';
						
				}else{
					$row['state'] = '어대 이동';
					$color = 'class="warning"';
				}
			}
			
			
			$html .= "<tr $color >
				<td>{$row['fam_name']}</td>
				<td>{$row['type']}</td>
				<td>{$row['state']}</td>
				<td>{$row['use_code']}</td>
				<th>{$row['use_code_date']}</th>
				<th><a href='#modal' class='btn btn-default mission' data-misson1='{$row['misson1']}' data-misson2='{$row['misson2']}' data-misson3='{$row['misson3']}' onclick='mission_open(this)' >미션보기</a> </th>
			</tr>";
		 }
		 
		echo json_encode( array('html'=>$html ));
		break;
	case 'reset':
		$sql = "update fam_game set misson1 = '', misson2 = '', misson3 = '', use_code='', use_code_date = '' ";
		$db->query($sql);
		$sql = "update fam_mission set `use` = '0'";
		$db->query($sql);
		
		echo json_encode( array('result'=>false ,'msg'=>'초기화 되었습니다.' ));
		break;
		
	case 'set_time':
		
		
		if(strlen($h) == 1)  $h = "0".$h;
		if(strlen($m) == 1)  $m = "0".$m;
		
		$end_date = date('Y-m-d').' '.$h.':'.$m.':00';
		
		$sql = "update fam_control set 	end_date = '$end_date'";
		$result = $db->query($sql);
		$msg = '종료시간이 세팅 되었습니다.';
		
		goto_url('/?f=game&v=control');
		break;
	case 'start':
		if($start == 1){
			$start = 0;
		}else{
			$start = 1;
		}
		
		$sql = "update fam_control set 	start = '$start'";
		$result = $db->query($sql);
		
		goto_url('/?f=game&v=control');
		break;
	case 'mission_add':
		
		$sql = "insert into fam_mission set mission = '$mission' ";
		
		$result = $db->query($sql);
		goto_url('/?f=game&v=save');
		break;
		
	case 'mission_del':
		
		$sql = "delete from fam_mission where idx = $idx";
		$result = $db->query($sql);
		goto_url('/?f=game&v=save');
		break;
}
exit;
?>