<?
if (preg_match("/\b(insert|update|delete|create)\b/i", $queryText)){
	$type = 'NON_SELECT';
} else {
	$type = 'SELECT';
}

$queryText = stripslashes($queryText);
$queryText = !empty($queryLimit) && $type =='SELECT' ? $queryText.' limit 50' : $queryText;
$result = $db->query($queryText);

if($result){
	#SELECT 일경우
	if($type == 'SELECT'){ 
		$view .= '<table class="table">';
		
		$view .= '<tr>';
		$view .= '<td>#</td>';
		while ( $finfo = $result->fetch_field()) {
			$view .= '<td>';
			$view .= $finfo->name;
			$view .= '</td>';
		}
		$view .= '</tr>';
		
		$i=0;
		while($row = $result->fetch_array(MYSQLI_NUM)){
			$view .= '<tr>';
			$view .= '<td>'. ++$i.'</td>';
			foreach($row as $v){
				$view .= '<td>'.htmlspecialchars($v).'</td>';
			}
			$view .= '</tr>';
		}
		$view .= '</table>';
	#UPDATE 일경우
	}else{
		$view = '정상적으로 처리 되었습니다.';
	}
	$result = true;
}else{
	$result = false;
	$view = '쿼리문을 다시 확인해주시기 바랍니다.';
}


$returnArray = array( 'result'=>$result, 'view'=>($view), 'type'=>$type);
echo json_encode($returnArray);
?>