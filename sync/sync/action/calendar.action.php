<?
$calendar = new Calendar($db);
$cnt = 0;

if(!$cmd) alert('잘못된 요청입니다.');

switch ($cmd){
	case 'cal_excel' : // 대당 달 엑셀 다운로드
		
		if($month){
			$where = ' m_birthday like "'.$month.'%" order by m_birthday';
			$subject = $month.'월 일정 계획표';
		}else{
			$subject = "전체 일정 계획표";
		}
		
		$list = $member->getMemberAllInfoList($where);
		$context  = $subject;
		$context .= "<table>";
		$context .= "<tr>";
		$context .= "<td>그룹</td><td>기수</td><td>생일</td><td>이름</td><td>가정</td><td>소속팀</td><td>전화번호</td>";
		$context .= "</tr>";
		if(!empty($list)){
			foreach($list as $key => $val){
				$val = $member->modifyMemberData($val);
				$context .= "<tr>";
				$context .= "<td>{$val['m_group_name']}</td><td>{$val['m_giso']}</td><td>{$val['m_birthday']}</td><td>{$val['m_name']}</td><td>{$val['g_name']}</td><td>{$val['t_name']}</td><td>{$val['m_tel']}</td>";
				$context .= "</tr>";
			}
		}
		$context .="</table>";
		
		execlDownload($context,$subject);
		break;
	case 'add':
		
		if(!$title) 	alert('일정 제목을 입력해주세요.');
		if(!$content)	alert('일정 내용을 입력해주세요.');
		if(!$wdate)		alert('일정 날짜를 입력해주세요.');
		
		$set = array(
			'title' 	=> $title,
			'content'	=> $content,
			'wdate'		=> $wdate,
		);
		$result = $calendar->addSchedule($set);
		if($result) echo '<script> alert("일정이 추가되었습니다."); location.parent.href = "/?f=calendar" </script>';
		break;
		
	case 'getDate':
	
		if(!$wdate) alert('일정 제목을 입력해주세요.');
	
		$list = $calendar->getDateSchedule($wdate);
		$html = '';
		
		foreach($list as $key => $val){
			$html .= '<div class="panel panel-danger" data-collapsed="0">
					<div class="panel-heading"><a href="#;" class="btn btn-default" onclick="del_s('.$val['idx'].');" style=" float: right;margin-top: 3px;">삭제</a>
						<div class="panel-title" >'.
						$val['title']
						.' </div>
					</div>
					<div class="panel-body">'.
						nl2br($val['content'])
					.'</div>
				</div>';
		}
		
		echo json_encode( array('html' => $html) );
		exit;
		break;
	
	case 'del':
		if(!$idx) alert('일정 제목을 입력해주세요.');
	
		$result = $calendar->delS($idx);
		echo json_encode( array('result' => $result) );
		exit;
		break;
		
	default :
		break;
}
?>