<?if (!defined('SIN')) exit;?>
<?if (!defined('IS_ADMIN')) exit;?>

<script type="text/javascript">
var path      = '<?=ROOT?>';
var url       = '<?=URL?>';
var isMobile    = '<?=$device['isMobile']?>';
var adminId   = '<?=$_SESSION['auth_id']?>';
var adminLoginDate = '<?=$_SESSION['auth_date']?>';
var charset   = '<?=CHAR_SET?>';
var serverTime = '<?=TIME_FULL?>';
var queryString = '<?=QUERY_STRING?>';
</script>

<link rel="stylesheet" href="<?=CSS_PATH?>/jquery-ui.min.css" />
<link rel="stylesheet" href="<?=CSS_PATH?>/bootstrap.css" />
<link rel="stylesheet" href="<?=CSS_PATH?>/admin/font-icons/entypo/css/entypo.css">
<link rel="stylesheet" href="<?=CSS_PATH?>/admin/neon-core.css">

<link rel="stylesheet" href="<?=CSS_PATH?>/admin/neon-theme.css">
<link rel="stylesheet" href="<?=CSS_PATH?>/admin/neon-forms.css">

<link rel="stylesheet" href="<?=CSS_PATH?>/admin/jquery-jvectormap-1.2.2.css">
<link rel="stylesheet" href="<?=CSS_PATH?>/admin/rickshaw.min.css">

<link rel="stylesheet" href="<?=CSS_PATH?>/default.css" />
<?
@include_once $_SERVER['DOCUMENT_ROOT'].'/'.CONTENT.'/'.$pg['f'].'/css.php';
@include_once $_SERVER['DOCUMENT_ROOT'].'/'.CONTENT.'/'.$pg['f'].'/'.$pg['v'].'.css.php';
?>
<?if($_GET['nav'] == 'n'){ ?>
<style>
 	.page-container { padding: 0;}
 	.page-container.sidebar-collapsed {  padding: 0;}
</style>
<?}?> 
