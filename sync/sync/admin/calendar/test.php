<!-- /*  $f_left = 29; $left = 67; */
										/* $top = array('1'=> '107','2'=> '149','3'=> '191','4'=> '231','5'=> '273'); */ -->
<div class="main-content">
	<div class="row">
		<div class="calendar-env">
			<div class="calendar-body">
				<div id="calendar" class="fc fc-ltr">
					<table class="fc-header" style="width: 100%">
						<tbody>
							<tr>
								<td class="fc-header-left"><span class="fc-header-title"><h2><?=$year?>년 <?=(int)$month?>월</h2></span></td>
								<td class="fc-header-center">
								
								</td>
								<td class="fc-header-right">
								<a href="<?=$prev_href?>" class="fc-button fc-button-prev fc-state-default fc-corner-left"unselectable="on"> 엑셀</a>
									<a href="<?=$cur_href?>" class="fc-button fc-button-today fc-state-default fc-corner-left fc-corner-right fc-state-disabled" >오늘</a>
									<a href="<?=$prev_href?>" class="fc-button fc-button-prev fc-state-default fc-corner-left"unselectable="on"><span class="fc-text-arrow"> ‹</span></a>
									<a href="<?=$next_href?>" class="fc-button fc-button-next fc-state-default fc-corner-right"unselectable="on"><span class="fc-text-arrow">›</span></a>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="fc-content" style="position: relative;">
						<div class="fc-view fc-view-month fc-grid" style="position: relative" unselectable="on">
								<!-- <div class="fc-event fc-event-hori fc-event-draggable fc-event-start fc-event-end" style="position: absolute; left: 362px; width: 171px; top: 50px;">
									<div class="fc-event-inner">
										<span class="fc-event-title">김정진 생일</span>
									</div>
									<div class="ui-resizable-handle ui-resizable-e">&nbsp;&nbsp;&nbsp;</div>
								</div>
								<div class="fc-event fc-event-hori fc-event-draggable fc-event-start fc-event-end color-primary ui-draggable ui-draggable-handle"
									style="position: absolute; left: 1086px; width: 175px; top: 50px;"
									unselectable="on">
									<div class="fc-event-inner">
										<span class="fc-event-title">사역자 회의</span>
									</div>
									<div class="ui-resizable-handle ui-resizable-e">&nbsp;&nbsp;&nbsp;</div>
								</div>
							</div> -->
							<table class="table fc-border-separate table-bordered" style="width: 100%" cellspacing="0">
								<thead>
									<tr class="fc-first fc-last">
										<th class="fc-day-header fc-mon fc-widget-header fc-first">월</th>
										<th class="fc-day-header fc-tue fc-widget-header">화</th>
										<th class="fc-day-header fc-wed fc-widget-header">수</th>
										<th class="fc-day-header fc-thu fc-widget-header">목</th>
										<th class="fc-day-header fc-fri fc-widget-header">금</th>
										<th class="fc-day-header fc-sat fc-widget-header text-danger">토</th>
										<th class="fc-day-header fc-sun fc-widget-header fc-last text-info">일</th>
									</tr>
								</thead>
								<tbody><!-- 
									<tr class="fc-week fc-first">
										<td class="fc-day fc-mon fc-widget-content fc-other-month fc-past fc-first"
											data-date="2016-02-29"><div style="min-height: 82px;">
												<div class="fc-day-number">29</div>
												<div class="fc-day-content">
													<div style="position: relative; height: 69px;">&nbsp;</div>
												</div>
											</div></td>
										<td class="fc-day fc-tue fc-widget-content fc-past"
											data-date="2016-03-01"><div>
												<div class="fc-day-number">1</div>
												<div class="fc-day-content">
													<div style="position: relative; height: 69px;">&nbsp;</div>
												</div>
											</div></td>
										<td class="fc-day fc-wed fc-widget-content fc-past"
											data-date="2016-03-02"><div>
												<div class="fc-day-number">2</div>
												<div class="fc-day-content">
													<div style="position: relative; height: 69px;">&nbsp;</div>
												</div>
											</div></td>
										<td class="fc-day fc-thu fc-widget-content fc-past"
											data-date="2016-03-03"><div>
												<div class="fc-day-number">3</div>
												<div class="fc-day-content">
													<div style="position: relative; height: 69px;">&nbsp;</div>
												</div>
											</div></td>
										<td class="fc-day fc-fri fc-widget-content fc-past"
											data-date="2016-03-04"><div>
												<div class="fc-day-number">4</div>
												<div class="fc-day-content">
													<div style="position: relative; height: 69px;">&nbsp;</div>
												</div>
											</div></td>
										<td class="fc-day fc-sat fc-widget-content fc-past" data-date="2016-03-05"><div>
												<div class="fc-day-number">5</div>
												<div class="fc-day-content">
													<div style="position: relative; height: 69px;">&nbsp;</div>
												</div>
											</div></td>
										<td class="fc-day fc-sun fc-widget-content fc-past fc-last" data-date="2016-03-06"><div>
												<div class="fc-day-number">6</div>
												<div class="fc-day-content">
													<div style="position: relative; height: 69px;">&nbsp;</div>
												</div>
											</div>
										</td>
									</tr>
									 -->
									<?  $day = 1 ;
								    for($i=1; $i <= $total_week; $i++){?>
										<tr class="fc-week"> 
											<? for ($j=0; $j<7; $j++){ $class =''; ?>
												<? if(!(($i == 1 && $j < $start_week) || ($i == $total_week && $j > $last_week))){ 
													if($j == 5){ $class .= " bg-danger "; }
													else if($j == 6){ $class .= " bg-info ";}
												?>
												<td class="fc-day fc-sat fc-widget-content fc-past <?=$class?>" data-date="<?=$year?>-<?=$month?>-<?=$day?>">
													<div class="fc-day-number"><?=$day++?></div>
												<?}else{?>
												<td class="fc-day fc-sat fc-widget-content fc-past">
												<?}?>
													<div class="fc-day-content">
															<div style="position: relative; min-height: 69px;">&nbsp;</div>
													</div>
												</td>
											<?}?>
										</tr>
									<?}?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				</div>
				<!-- Sidebar -->
				<div class="calendar-sidebar">
					<!-- new task form -->
					<div class="calendar-sidebar-row">
						<form role="form" id="add_event_form">
							<div class="input-group minimal">
								<input type="text" class="form-control" placeholder="새로운 일정을 입력해주세요." />
								<div class="input-group-addon">
									<i class="entypo-pencil"></i>
								</div>
							</div>
						</form>
					</div>
				
					<!-- Events List -->
					<ul class="events-list" id="draggable_events">
						<li>
							<p>간단한 스케줄</p>
						</li>
						<li>
							<a href="#" class="color-primary" data-event-class="color-primary">사역자 회의</a>
						</li>
						<li>
							<a href="#" class="color-primary" data-event-class="color-primary">월례회</a>
						</li>
						<li>
							<a href="#" class="color-primary" data-event-class="color-primary">동기모임</a>
						</li>
					</ul>
					
				</div>
			</div>
		</div>
	</div>
</div>