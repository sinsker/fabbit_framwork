<script src="<?=JS_PATH?>/admin/remodal.js" type="text/javascript"></script>
<script>
$(function(){
	$("#cal-excel").click(function(){
		window.open('?action=calendar&cmd=cal_excel&month=<?=$month?>','iframe');
	});	

	$("#all-excel").click(function(){
		window.open('?action=calendar&cmd=cal_excel','iframe');
	});	

	$("#add-calendar").click(function(){
		$("#calendar_form").submit();
	});

	
	
		
});

function view_s(wdate,d){

	$.post("/?action=calendar", {'wdate':d, 'cmd' : 'getDate' }, function(data){
		if(data.html){
			$('.t').text(d);
			$('.d').html(data.html);
		}
	},'json');
	
	var inst = $('[data-remodal-id=sc-detail]').remodal();
	inst.open();

}

function del_s(idx){
	$.post("/?action=calendar", {'idx':idx, 'cmd':'del' }, function(data){
		if(data.result){
			alert('삭제가 완료되었습니다.');
			location.href = '?f=calendar';
		}
	},'json');
	
}

$.datepicker.regional['ko'] = {
		  closeText: '닫기',
		  prevText: '이전',
		  nextText: '다음',
		  currentText: '오늘',
		  monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
		  monthNamesShort: ['1','2','3','4','5','6','7','8','9','10','11','12'],
		  dayNames: ['일','월','화','수','목','금','토'],
		  dayNamesShort: ['일','월','화','수','목','금','토'],
		  dayNamesMin: ['일','월','화','수','목','금','토'],
		  weekHeader: 'Wk',
		  dateFormat: 'yy-mm-dd',
		  firstDay: 0,
		  isRTL: false,
       changeMonth: true, 
       changeYear: true,
		  showMonthAfterYear: true,
		  yearRange: 'c-70:c',
		  yearSuffix: ''};
		 $.datepicker.setDefaults($.datepicker.regional['ko']);


$('input[name=wdate]').datepicker({
  language : 'ko',
  dateFormat: 'yy-mm-dd'
});

</script>