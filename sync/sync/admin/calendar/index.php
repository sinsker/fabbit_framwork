<!-- /*  $f_left = 29; $left = 67; */
/* $top = array('1'=> '107','2'=> '149','3'=> '191','4'=> '231','5'=> '273'); */ -->
<div class="main-content">
	<div class="row">
		<div class="calendar-env">
			<div class="calendar-body">
				<div id="calendar" class="fc fc-ltr">
					<table class="fc-header" style="width: 100%">
						<tbody>
							<tr>
								<td class="fc-header-left"><span class="fc-header-title"><h2><?=$year?>년 <?=(int)$month?>월</h2></span></td>
								<td class="fc-header-center">
								
								</td>
								<td class="fc-header-right">
									<a href="#;" class=" btn btn-primary" id="cal-excel" ><?=(int)$month?>월 엑셀</a>
									<a href="#;" class=" btn btn-primary" id="all-excel" >전체 엑셀</a>
									<a href="<?=$cur_href?>" class="fc-button fc-button-today fc-state-default fc-corner-left fc-corner-right fc-state-disabled" >오늘</a>
									<a href="<?=$prev_href?>" class="fc-button fc-button-prev fc-state-default fc-corner-left"unselectable="on"><span class="fc-text-arrow"> ‹</span></a>
									<a href="<?=$next_href?>" class="fc-button fc-button-next fc-state-default fc-corner-right"unselectable="on"><span class="fc-text-arrow">›</span></a>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h3 class="panel-title">도구 박스</h3>
						</div>
						<div class="panel-body">
							<div class="row no_line">
								<div class="col-sm-12">
									<a href="#add-day" class="btn btn-primary ">일정 추가</a>
								</div>
							</div>
						</div>
					</div>
								
					<div class="fc-content" style="position: relative;">
						<div class="fc-view fc-view-month fc-grid" style="position: relative" unselectable="on">
							<table class="table fc-border-separate table-bordered" style="width: 100%" cellspacing="0">
								<thead>
									<tr class="fc-first fc-last">
										<th class="fc-day-header fc-mon fc-widget-header fc-first">월</th>
										<th class="fc-day-header fc-tue fc-widget-header">화</th>
										<th class="fc-day-header fc-wed fc-widget-header">수</th>
										<th class="fc-day-header fc-thu fc-widget-header">목</th>
										<th class="fc-day-header fc-fri fc-widget-header">금</th>
										<th class="fc-day-header fc-sat fc-widget-header text-danger">토</th>
										<th class="fc-day-header fc-sun fc-widget-header fc-last text-info">일</th>
									</tr>
								</thead>
								<tbody>
									<?  $day = 1 ;
								    for($i=1; $i <= $cal_total_week; $i++){?>
										<tr class="fc-week"> 
											<? for ($j=0; $j<7; $j++){ $class =''; ?>
												<? if(!(($i == 1 && $j < $cal_start_week) || ($i == $cal_total_week && $j > $cal_last_week))){
													if($j == 5){ $class .= " bg-danger "; }
													else if($j == 6){ $class .= " bg-info ";}
													
													if($year."-".$month."-".$day == date("Y-m-d")){ $class .= " calender-today "; } //오늘 날짜
												?>
													<td class="fc-day fc-sat fc-widget-content fc-past <?=$class?>" data-date="<?=$year?>-<?=$month?>-<?=strlen($day) == 1 ? '0'.$day : $day?>">
														<div class="fc-day-number"><?=$day?></div>
														<!-- 실제 컨텐츠 -->
														<div class="fc-day-content">
																<div style="position: relative; min-height: 69px;">
																	<?  $str_day = strlen($day) == 1 ? '0'.$day : $day; ?>
																	<?  if(!empty($user_list[$month][$str_day])){
																		foreach( $user_list[$month][$str_day] as $key => $val){ ?>
																			<div class="fc-event fc-event-hori" >
																				<div class="fc-event-inner">
																					<div class="event-title" data-type="brith" data-key ="<?=$val['member_idx']?>">
																						(<?=$val['m_giso']?>)<?=$val['m_name']?> 
																					</div>
																				</div>
																			</div>
																	<?}}?>
																	<?  if(!empty($s_list[$month][$str_day])){
																		foreach( $s_list[$month][$str_day] as $key => $val){ ?>
																			<div class="fc-event fc-event-hori" style="background: #e2a51b;">
																				<div class="fc-event-inner">
																					<div class="event-title " data-type="sc" href="#sc-detail" style="cursor: pointer;" onclick="view_s(<?=$val['idx']?>,'<?=$val['wdate']?>')">
																						(일정) <?=$val['title']?> 
																					</div>
																				</div>
																			</div>
																	<?}}?>
																</div>
														</div>
														<!-- // 실제 컨텐츠 -->
												<?	$day++;
													}else{ //빈값일경우 ?> 
													<td class="fc-day">
														<div class="fc-day-content"></div>
													<?}?>
												</td>
											<?}?>
										</tr>
									<?}?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="remodal" data-remodal-id="add-day" role="dialog" aria-labelledby="modal-head" aria-describedby="modal1Desc">
	<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
	<form id="calendar_form" method="post" action="?action=calendar"  target="iframe" id="folder-add-form" class="form-horizontal form-groups-bordered">
		<input type="hidden" name="cmd" value="add" >
		<div id="modal-head"><h3>일정추가</h3></div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-primary" data-collapsed="0">
					<div class="panel-heading">
						<div class="panel-title">
							아래 항목을 입력해 주세요.
						</div>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<label for="field-1" class="col-sm-3 control-label">날짜</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="wdate" name="wdate" placeholder="" value="<?=TIME_YMD?>">
							</div>
						</div>
						<div class="form-group">
							<label for="field-2" class="col-sm-3 control-label">일정 제목</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="title" name="title" placeholder="" >
							</div>
						</div>
						<div class="form-group">
							<label for="field-ta" class="col-sm-3 control-label">일정 내용</label>
							<div class="col-sm-8">
								<textarea class="form-control autogrow" id="content" name="content" placeholder="내용을 입력해주시기 바랍니다." style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 70px;"></textarea>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12">
			<button data-remodal-action="confirm" class="col-xs-4 form-control btn btn-primary" id="add-calendar">추가하기</button>
		</div>
	</form>
</div>


<div class="remodal" data-remodal-id="sc-detail">
	<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
		<input type="hidden" name="cmd" value="add" >
		<div id="modal-head"><h3>일정 확인 </h3></div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-primary" data-collapsed="0">
					<div class="panel-heading">
						<div class="panel-title t">
						</div>
					</div>
					<div class="panel-body d">
					
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12">
			<button data-remodal-action="confirm" class="col-xs-4 form-control btn btn-primary" id="add-calendar">추가하기</button>
		</div>
</div>