<?
if (!defined('IS_ADMIN')) exit;
$member = new Member($db);
$calendar = new Calendar($db);

$year           = ( $_GET['year'] )? $_GET['year'] : date( "Y" );
$month          = ( $_GET['month'] )? $_GET['month'] : date( "m" );

if(strlen($month) == 1) $month = '0'.$month;
$doms           = array(  "월", "화", "수", "목", "금", "토", "일" );
#===============================================================================#
$nextYear = $prevYear = $year;
$nextMonth = $prevMonth = $month;

$prevMonth		= $prevMonth == 1 ? 12 : $prevMonth - 1;
$nextMonth		= $nextMonth == 12 ? 1 : $nextMonth + 1;

if($nextMonth == 1){  $nextYear = $nextYear+1; }
else if($prevMonth == 12){$prevYear = $prevYear-1;}

$mktime			= mktime(0, 0, 0, $month+1, 0, $year);
$weeks			= ceil($days/ 7);
$last_day 		= date("t", $mktime);


$start_week = date("w", strtotime(date($year."-".$month)."-01"));
$total_week = ceil(($last_day + $start_week) / 7);
$last_week = date('w', strtotime(date($year."-".$month)."-".$last_day));

/* 한칸씩 뒤로 */
$cal_start_week = $start_week == 0 ? 6 : $start_week - 1;
$cal_total_week = $total_week + 1;
$cal_last_week = $last_week -1;
/* //한칸씩 뒤로  */

$prev_href= "?f=calendar&year=$prevYear&month=$prevMonth";
$next_href= "?f=calendar&year=$nextYear&month=$nextMonth";
$cur_href= "?f=calendar&year=".date("Y")."&month=".date("m");

$list = $member->getMemberAllInfoList(' m_birthday like "'.$month.'%"');
$scheduleList = $calendar->getSchedule();

$s_list = array();
foreach($scheduleList as $key => $val){
	 $wdatex = explode('-',$val['wdate']);
	 if($year == $wdatex[0]){
		 $m = $wdatex[1];
		 $d = $wdatex[2];
		 if(empty( $s_list[$m][$d])){
		 	 $s_list[$m][$d] = array();
		 }
		 array_push($s_list[$m][$d], $val);
	 }
}

$user_list = array();
foreach($list as $key => $val){
	$birth_m =  substr($val['m_birthday'],0,2);
	$birth_d =  substr($val['m_birthday'],2,2);
	if(empty($user_list[$birth_m][$birth_d])){
		$user_list[$birth_m][$birth_d] = array();
	}
	array_push($user_list[$birth_m][$birth_d], $val);
}

?>