<script>

$(".approve").click(function(){
	$.post('?action=manager', {'cmd':'approval', 'idx': $(this).parent().data('key')}, function(data){
		if(data.msg){ alert(data.msg);}
		if(data.url){  
			location.href = data.url;
		}
	},'json');
});

$(".dismiss").click(function(){
	$.post('?action=manager', {'cmd':'dismiss', 'idx': $(this).parent().data('key'), }, function(data){
		if(data.msg){ alert(data.msg);}
		if(data.url){  
			location.href = data.url;
		}
	},'json');
	
});

$(".delete").click(function(){
	if(confirm('정말로 삭제하시겠습니까?')){
		$.post('?action=manager', {'cmd':'delete', 'idx': $(this).parent().data('key') }, function(data){
			if(data.msg){ alert(data.msg);}
			if(data.url){  
				location.href = data.url;
			}
		},'json');
	}
	
});
</script>
