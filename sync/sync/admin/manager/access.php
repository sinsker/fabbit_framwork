<div class="main-content">
	<div class="header" >
		<h2 class="header-t">관리자 접근 기록</h2>
	</div>
	<div class="row">
		<div>
			<div class="col-sm-12">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<div class="panel-title text-center">총 로그 개수 <span class="badge badge-secondary"><?=$paging->rowCount?></span></div>
					</div>
						
					<table class="table table-hover">
						<thead>
							<tr class="active" align="center">
								<th style=" text-align: center;">#</th>
								<th style=" text-align: center;">아이디</th>
								<th style=" text-align: center;">결과</th>
								<th style=" text-align: center;">메모</th>
								<th style=" text-align: center;">아이피</th>
								<th style=" text-align: center;">날짜</th>
							</tr>
						</thead>
						<tbody>
						<? if(!empty($adminLogList)){
								foreach ($adminLogList as $key => $val){ ?>
								<tr style=" text-align: center;" >
									<td align="center"><?=$paging->startNo--?></td>
									<td align="center"><?=$val['al_uid']?></td>
									<td align="center"><?=$val['al_result']?></td>
									<td align="center"><?=$val['al_memo']?></td>
									<td align="center"><?=$val['al_ip']?></td>
									<td align="center"><?=$val['wdate']?></td>
								</tr>
						<? } } ?>
						</tbody>
					</table>
					<div class="text-center">
						<?=$paging->getPageBlockHtml("f=manager&v=access")?>
					</div>
				</div>
			</div>		
		</div>
	</div>
</div>