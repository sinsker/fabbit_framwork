<div class="main-content">
	<div class="header" >
		<h2 class="header-t">관리자 리스트</h2>
	</div>
	<div class="row">
		<div>
			<div class="col-sm-12">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<div class="panel-title text-center"></span></div>
					</div>
						
					<table class="table ">
						<thead>
							<tr class="active" align="center">
								<th style=" text-align: center;">#</th>
								<th style=" text-align: center;">아이디</th>
								<th style=" text-align: center;">이름</th>
								<th style=" text-align: center;">마지막접속일</th>
								<th style=" text-align: center;">가입일</th>
								<th style=" text-align: center;">관리</th>
							</tr>
						</thead>
						<tbody>
						<? if(!empty($adminList)){
								foreach ($adminList as $key => $val){ ?>
								<tr style=" text-align: center;" <?if($val['is_use'] == 'N'){?> class = "non-admin" <?}?> >
									<td align="center"><?=$paging->startNo--?></td>
									<td align="center"><?=$val['a_id']?></td>
									<td align="center"><?=$val['a_name']?></td>
									<td align="center"><?=$val['a_last_date']?></td>
									<td align="center"><?=$val['a_reg_date']?></td>
									<td align="center" data-key="<?=$val['idx']?>">
										<?if($val['is_use'] == 'N'){?>
										<a href="#;" class="btn btn-default approve">승인</a>
										<?}else{?>
										<a href="#;" class="btn btn-default dismiss">정지</a>
										<?}?> 
										<a href="#;" class="btn btn-danger delete">계정 삭제</a> 
									</td>
								</tr>
						<? } } ?>
						</tbody>
					</table>
					<div class="text-center">
						<?=$paging->getPageBlockHtml("f=manager")?>
					</div>
				</div>
			</div>		
		</div>
	</div>
</div>