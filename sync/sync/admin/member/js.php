


<!--[if lt IE 9]><script src="<?=JS_PATH?>/admin/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
	
	
<script type="text/javascript">

$("#select_ok").click(function(){
	var qe = $("#select_option option:selected").val();
	var tx = $("#select_str");
	var user_group = $("#user_group option:selected").val();
	if(tx.val() == ''){
		alert('검색란에 입력해주세요.');
		tx.focus();
		return;	
	}
	location.href= '?f=member&qe='+qe+'&tx='+tx.val()+'&user_group='+user_group;
});

//초기화 버튼
$("#replace-btn").click(function(){
	location.href= '?f=member';
});

$("#user_group").change(function(){

	var qe = $("#select_option option:selected").val();
	var tx = $("#select_str").val();
	
	location.href= '?f=member&qe='+qe+'&tx='+tx+'&user_group='+$(this).val();
	
});

$("#allcheck").change(function(){
	if($(this).prop("checked") == true){
		$(".checkbox").prop("checked",true);
	}else{
		$(".checkbox").prop("checked",false);
	}
	
});
$("#seleted-del-btn").click(function(){
	if($(".checkbox:checked").length == 0){
		alert("1개이상 선택해 주세요.");
		return false;
	}

	var seleted_idx = '';
	$(".checkbox:checked").each(function(){
		seleted_idx += (seleted_idx ? ', ': '') + $(this).val()
	});
		
	$.post('?action=member', {'cmd' : 'delete_member', 'ids' : seleted_idx  } , function(data){
		console.log(data);
		if(data.msg) alert(data.msg);
		if(data.result){
			location.href = '?'+queryString;
		}
	},'json');
	
	
});

function vmember(idx){
	location.href ='?f=member&v=mview&midx='+idx;
}


$.datepicker.regional['ko'] = {
		  closeText: '닫기',
		  prevText: '이전',
		  nextText: '다음',
		  currentText: '오늘',
		  monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
		  monthNamesShort: ['1','2','3','4','5','6','7','8','9','10','11','12'],
		  dayNames: ['일','월','화','수','목','금','토'],
		  dayNamesShort: ['일','월','화','수','목','금','토'],
		  dayNamesMin: ['일','월','화','수','목','금','토'],
		  weekHeader: 'Wk',
		  dateFormat: 'yy-mm-dd',
		  firstDay: 0,
		  isRTL: false,
         changeMonth: true, 
         changeYear: true,
		  showMonthAfterYear: true,
		  yearRange: 'c-70:c',
		  yearSuffix: ''};
		 $.datepicker.setDefaults($.datepicker.regional['ko']);

$('input[name=m_birth]').datepicker({
    language : 'ko'
});

$('input[name=m_join_date]').datepicker({
    language : 'ko',
    dateFormat: 'yy-mm-dd'
});

$("#comment_write_btn").click(function(){
	var idx = $("input[name=m_idx").val();
	var memo = $("#form_member_comment > textarea").val();
	comment.write(idx,memo);
	
});

$(".comment-edit").click(function(){
	var view_display = $(this).parent().parent().parent().parent();
	var edit_display = view_display.next();
	view_display.hide();
	edit_display.show();
});

$(".comment-update-cacel-btn").click(function(){
	var edit_display = $(this).parent().parent().parent().parent();
	var view_display = edit_display.prev();
	view_display.show();
	edit_display.hide();
	
});

$(".comment-del").click(function(){
	var comment_key =  $(this).attr("data-key");
	comment.del(comment_key);
	
});

$(".edit-submit").click(function(){
	var edit_form = $(this).parent().parent().parent();
	comment.edit(edit_form);
});



var comment = {
	comment_idx : null,
	write : function(idx,memo){

		if(memo == ''){
			alert("공란으로 작성 불가능합니다.");
			return false;
		}
		
		$.post('?action=member', {'cmd' : 'write_comment', 'midx' : idx , 'memo' : memo } , function(data){
			console.log(data);
			if(data.msg){ alert(data.msg);}
			window.location.reload(true);
			
		},'json');

	},

	edit :function(form){
		
		$.post('?action=member', form.serialize() , function(data){
			console.log(data);
			if(data.msg){ alert(data.msg);}
			window.location.reload(true);
			
		},'json');

	},

	del :function(idx){
		if (confirm('해당 코멘트를 삭제 하시겠습니까?') == true){
			$.post('?action=member', {'cmd' : 'delete_comment', 'idx' : idx }, function(data){
				console.log(data);
				if(data.msg){ alert(data.msg);}
				window.location.reload(true);
			},'json');
		}
	}
}

var team = {
		insert: function(){
			if($("#t_name").val() == ''){
				alert('사역팀 이름을 입력해주세요');
				return;
			}
			if($("#t_memo").val() == ''){
				alert('사역팀 간단한 설명을 입력해주세요');
				return;
			}

			$.post($( "#team_insert_form").attr('action'),$( "#team_insert_form").serialize() , function(data){
				console.log(data);
				alert(data.msg);
				if(data.result == 'done'){
					location.href = location.href
				}
			},'json');
		},

		detail: function(idx){
			location.href = '?f=member&v=tdetail&tid='+idx;
		},
		del : function(idx, tname){
			if(confirm(tname+"을 정말로 삭제 하시겠습니까?")){
				$.post('?action=member', {'cmd' : 'team_del','idx':idx } , function(data){
					console.log(data);
					window.location.reload(true);
				},'json');
			}
		},
		namesearch : function(obj){
			name = obj.value;
			
			$.post('?action=member', {'cmd' : 'get_name','name' : name, 'tid' : $("input#tid").val() } , function(data){
				console.log(data);
				$(".ms-list").html(data.html);
			},'json');
		},
		memberinfo : function(idx){
			$.post('?action=member', {'cmd' : 'get_member', 'midx' : idx } , function(data){
				console.log(data);
				$("#midx").val(data.idx);
				$("#mname").text(data.m_name);
				$("#mbname").val(data.m_name);
				$("#mtype").text(data.m_group_name);
				$("#mtel").text(data.m_tel);
				$(".gm-inset-box").show();
			},'json');
		},
		gminsert : function(){
			$.post( '?action=member',$( "form[name=insert_group_member_form]").serialize() , function(data){
				if(data.msg) alert(data.msg);
				if(data.result == 'done'){
					location.replace('');
				}
			},'json');	
		},

		edit : function(key){
			var position =  $("input[gm-key="+key+"]").val();
			$.post('?action=member', {'cmd' : 'update_gm_position','idx': key, 'position' : position } , function(data){
				console.log(data);
			},'json');

		},
		
		deltmember : function(key){
			if (confirm('해당 명단을 삭제 하시겠습니까?') == true){
				$.post('?action=member', {'cmd' : 'delete_gm','idx': key } , function(data){
					console.log(data);
					if(data.result == 'done'){
						location.replace('');
					}
				},'json');
			}
		}
}

var group = {
		group_leader_id_str : null,
		group_member_ids_str : null,
		setid: function(){
			var leader_member ='';
			var group_member = '';
			
			$("#leader_list>.list-group-item").each(function(){
				leader_member += $(this).attr('id')+",";
			});

			$("#current_list>.list-group-item").each(function(){
				group_member += $(this).attr('id')+",";
			});

			if(leader_member.length > 0){
			 	leader_member = leader_member.slice(0,-1);
			}

			if(group_member.length > 0){
			 	group_member = group_member.slice(0,-1);
			}
			
			this.group_leader_id_str = leader_member;
			this.group_member_ids_str = group_member;
		},
		insert: function(){
			this.setid();
			$.post('?action=member',{'cmd': 'edit_group','g_type':$("#g_type").val(), 'gidx' : $("#gidx").val(), 'leader_id_str': this.group_leader_id_str , 'group_member_ids_str' : this.group_member_ids_str  }, function(data){
				console.log(data);
				alert(data.msg);
				if(data.result == 'done'){
					location.href = '?f=member&v=group'
				}
			},'json');
		},

		detail: function(idx){
			location.href = '?f=member&v=tdetail&tid='+idx;
		},

		namesearch : function(obj){
			this.setid();
			name = obj.value;
			
			$.post('?action=member', {'op' : 'group' ,'cmd' : 'get_name','name' : name, 'leader_id_str': this.group_leader_id_str , 'group_member_ids_str' : this.group_member_ids_str } , function(data){
				console.log(data);
				$("#search_list").html(data.html);
			},'json');
		},
		memberinfo : function(idx){
			$.post('?action=member', {'cmd' : 'get_member', 'midx' : idx } , function(data){
				console.log(data);
				$("#midx").val(data.idx);
				$("#mname").text(data.m_name);
				$("#mbname").val(data.m_name);
				$("#mtype").text(data.m_group_name);
				$("#mtel").text(data.m_tel);
				$(".gm-inset-box").show();
			},'json');
		},
		gminsert : function(){
			$.post( '?action=member',$( "form[name=insert_group_member_form]").serialize() , function(data){
				if(data.msg) alert(data.msg);
				if(data.result == 'done'){
					location.href = '?f=member&v=group';
				}
			},'json');	
		},

		edit : function(key){
			location.href = '?f=member&v=gedit&gidx='+key;

		},
		
		del : function(key){
			$.post('?action=member', {'cmd' : 'delete_group', 'gidx' : key } , function(data){
				console.log(data);
				if(data.msg) alert(data.msg);
				if(data.result == 'done'){
					location.href = '?f=member&v=group';
				}
			},'json');
		},
		
		deltmember : function(key){
			if (confirm('해당 명단을 삭제 하시겠습니까?') == true){
				$.post('?action=member', {'cmd' : 'delete_gm','idx': key } , function(data){
					console.log(data);
					if(data.result == 'done'){
						location.replace('');
					}
				},'json');
			}
		}
}

$("#team-show-memu").click(function(){
	$("#team-menu").fadeIn();
});

$("li.mb").click(function(){
	var mid = $(this).attr("id");
	team.memberinfo(mid);
});

//가정리스트 + 클릭 이벤튼
$(".group-edit").click(function(event){
	var gidx = $(this).attr("data-key");
	group.edit(gidx);
	event.stopPropagation();
	
});


$(function() {
    $( "#search_list" ).sortable({
    	 connectWith: "ul",
    	 items: "li:not(.non-item)",
         revert: true
	});
    $( "#current_list" ).sortable({});
    $( "#leader_list" ).sortable({});
    $( "#current_list #leader_list").disableSelection();
});
 

</script>