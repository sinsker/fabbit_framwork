<div  class="main-content">
	<!-- 팀등록 모달 -->
	<h2>가정 등록</h2>
	<div class="row">
		<div class="col-sm-6">
			<p>추가할 명단 검색</p>
			<input type="text" class="form-control " name="name"  onkeyup="group.namesearch(this);">
			<div>
				<ul class="list-group droptrue" id="search_list">
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<input type="hidden" name="cmd" value ="insert_group">
				<input type="hidden" name="gidx" id="gidx" value ="<?=$gidx?>">
				<p>공동체</p>
				<?=inArraySelectBox($gtypeArray,$group[0]['g_type'],'g_type', 0,0)?>
				<p>섬김이 이름</p>
				<div class="row">
					<div class="col-sm-12 ">
						<ul class="list-group" id="leader_list"><?=$readerHtml?></ul>
						<label class=" control-label" >현재명단</label>
						<ul class="list-group" id="current_list"><?=$groupMemberHtml?></ul>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12 text-center row-content">
			<button type="button" class="btn btn-blue btn-icon" onclick="group.insert();">
					등록하기 
					<i class="entypo-heart"></i>
			</button>
			<? if($gidx){?>
				<button type="button" class="btn btn-red btn-icon" onclick="group.del('<?=$gidx?>');">
						삭제하기 
						<i class="entypo-cancel"></i>
				</button>
			<? } ?>
			<button type="button" class="btn btn-default btn-icon" onclick="javascript:location.href='?f=member&v=group'">
					취소하기
					<i class="entypo-cancel" ></i>
			</button>
		</div>
	</div>
</div>
<!-- <div class="gl" style="display:'none'">
	<div class="gl-wrap">
		<div class="gl-content">
			<input type="hidden" name="cmd" value ="group_leader_search">
			<input type="hidden" name="gid" id="gid" value ="<?=$gid?>">
			<label for="field-1" class=" control-label" >섬김이 이름</label>
			<div class="row">
				<div class="col-sm-12 text-center">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 text-center">
					<input class="form-control btn btn-default" type="button" value="사용하기">
				</div>
			</div>
		</div>
	</div>
</div>
 -->