<div class="main-content">

<h3> 신상 정보 </h3>
<form role="form" class="form-horizontal form-groups-bordered"  action="?action=member" method="post" enctype="multipart/form-data" >
	<input type="hidden" name="cmd" value="edit_member" > 
	<input type="hidden" name="idx" value="<?=$midx?>" > 
	<div class='form-group'>
		<label for="field-1" class="col-sm-3 control-label">이름 :</label>
		<div class="col-sm-5">
			<input class="form-control" name="m_name" placeholder="" type="text" value="<?=$user['m_name']?>"<?if($midx)echo 'readonly="readonly"';?> >
		</div>
	</div>
	<div class="form-group">
		<label for="field-ta" class="col-sm-3 control-label">비고란 :</label>
		
		<div class="col-sm-5">
			<textarea class="form-control" name='m_memo' id="m_memo"  placeholder=""><?=$user['m_memo']?></textarea>
		</div>
	</div>
	<div class="profile-env">
		
		<header class="row">
			
			<div class="col-sm-2">
				<a href="#" class="profile-picture img-rounded"">
					<img src="<?=$user['m_image_path']?>" class="img-responsive " />
				</a>
				
			</div>
			<div class="col-sm-7">
				<div class="fileinput fileinput-new" data-provides="fileinput">
					<span class="btn btn-default btn-file">
						<span class="fileinput-new">이미지 올리기</span>
						<span class="fileinput-exists">이미지 올리기</span>
						<input type="file" name="m_img">
					</span>
					<span class="fileinput-filename"></span>
					<a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
				</div>
			</div>
			
		</header>
		
		<section class="profile-info-tabs">
			<div class="row">
				<div class="col-sm-offset-2 col-sm-10">
					
					<ul class="user-details">
						<li>
							<a href="#">
								<i class="entypo-location">분류</i>
							</a>
							<span class='text-info' class="" data-input='m_group' >
								<?=inArraySelectBox($member_group,$user['m_group'],'m_group', 0,1)?>
							</span>
						</li>
						<li>
							<a href="#">
								<i class="entypo-suitcase">성별</i>
							</a>
							<div class="form-group">
								<div class="col-sm-5">
									<span data-input='m_sex'>
										<div class="radio col-sm-5">
											<label>
												<input type="radio" name="m_sex" value="1" <?if($user['m_sex'] == '1') echo 'checked=true';?>  >남자
											</label>
										</div>
										<div class="radio col-sm-5">
											<label>
												<input type="radio" name="m_sex" value="2"  <?if($user['m_sex'] == '2') echo 'checked=true';?>  >여자
											</label>
										</div>
									</span>
								</div>
							</div>
						</li>
						
						<li>
							<a href="#">
								<i class="entypo-suitcase">전화번호</i>
							</a>
							<span class='text-info' data-input='m_tel'>
								<input class="form-control" name="m_tel" placeholder="- 붙여서 입력하세요." type="text" value="<?=$user['m_tel']?>">
							</span>
						</li>
						
						<li>
							<a href="#">
								<i class="entypo-calendar">생년월일</i>
							</a>
							<span class='text-info' data-input='m_birth'>
								<input class="form-control" name="m_birth" placeholder="" type="text" value="<?=$user['m_birth']?>">
							</span>
						</li>
						
						<li>
							<a href="#">
								<i class="entypo-calendar">등록일</i>
							</a>
							<span class='text-info' data-input='m_join_date'>
								<input class="form-control" name="m_join_date" placeholder="" type="text" value="<?=$user['m_join_date']?>">
							</span>
						</li>
					</ul>
					
					<div class="row text-center">
						<input type="submit"value="등록" class='btn btn-dafault'>
						<input type="button"value="취소" class='btn btn-dafault' onclick='location.back();'>
					</div>
					
					<!-- tabs for the profile links -->
					<ul class="nav nav-tabs">
						<li ><a href="?f=member&v=mview&midx=<?=$midx?>">개인 신상</a></li>
						<li class="active"><a href="?f=member&v=medit&midx=<?=$midx?>">정보 수정</a></li>
					</ul>
				</div>
			</div>
		</section>
		</div>
	</form>
</div>
