<?
if (!defined('IS_ADMIN')) exit;

$member = new Member($db);

if($midx){ $where = 'and idx ='.$midx;}

switch ($_GET['v']){
	#가정 리스트
	case 'group':
		$group1 = $member->getAllGroupData(' and g_type = 1'); # 1공동체
		$group2 = $member->getAllGroupData(' and g_type = 2'); # 2공동체
		
		foreach($group1 as $data){
			$groupListOne[$data['gid']][] = $data;
		}
		
		foreach($group2 as $data){
			$groupListTwo[$data['gid']][] = $data;
		}
		break;

	#가정 추가, 수정하기 페이지
	case 'gedit':
		if($gidx){
			$group = $member->getAllGroupData(' and a.idx ='.$gidx);
			$readerHtml = "<li id='{$group[0]['g_leader_id']}' class='mb list-group-item' style='position: relative;'><span>{$group[0]['g_name']}</span></li>";
			foreach($group as $gmember){
				if(!$gmember['group_idx']) continue;
				$groupMemberHtml .= "<li id='{$gmember['idx']}' class='mb list-group-item' style='position: relative;'><span>{$gmember['gm_member_name']} {$gmember['m_group_name']}</span></li>";
			}
			
		}
		$gtypeArray = array( 1=>"1공동체", 2=>"2공동체"); 
		break;
		
	#팀 리스트 
	case 'team':
		list($teamList,$totel) =  $member->getTeamList($page, $where);
		break;
		
	#팀 상세정보
	case 'tdetail':
		list($teamDetail,$teamMembers) =  $member->getTeamDetailData($tid);
		$murl = "?f=member&v=mview&midx=";
		break;
		
	#회원 추가,수정하기 페이지
	case 'medit':
		if($midx){ 
			$where = 'and idx ='.$midx;
			$user = $member->getMemberData($where);
			
			$currentYear = date('Y') - 20;
			$defaultYear = substr($currentYear,0,2);
			$user['m_birth'] = $defaultYear.$user['m_giso'].'-'.substr($user['m_birthday'],0,2).'-'.substr($user['m_birthday'],2,2);
		}
		
		break;
		
	#단일 정보 보기
	case 'mview':
		if($midx){ $where = 'and idx ='.$midx;}
		else{ alert('잘못된 접근입니다.'); }
		$user = $member->modifyMemberData($member->getMemberData($where));
		$userCommentList = $member->getMemberCommentList($midx);
		
		break;
	
	#회원 리스트 보기
	default:
		if($qe && $tx){ $where .= "and $qe like '%$tx%'";	}
		if($user_group){ $where .= "and m_group = $user_group ";	}
		
		list($memberList,$pageing) = $member->getMemberList($_GET['page'],$where);
		break;
} 

if(!$user['m_image_path']){ $user['m_image_path'] = IMG_PATH.'/admin/attach-1.png'; }
?>