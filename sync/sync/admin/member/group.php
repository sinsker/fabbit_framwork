
<div  class="main-content">
	<div class="header" >
		<h2 class="header-t">가정 리스트</h2>
		<div class="header-t text-right">
				<a class="btn btn-primary btn-icon icon-left"  href="?f=member&v=gedit">
						<i class="entypo-plus"></i>추가
					</a>
			<!-- <a href="javascript:;" onclick="Show('modal-team_insert');" class="btn btn-default">팀 생성</a> -->
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="tile-stats tile-block tile-white-red">
				<div class="tile-content">
					<div class="icon"><i class="entypo-light-up"></i></div>
					<div class="num">1공동체</div>
					<h3>총 <?=count($groupListOne)?>가정</h3>
					<p>20세부터 25세까지</p>
				</div>
				<div class="tile-footer">
					<div class="panel-group" id="accordion-test">
						<?  if($groupListOne){ 
							foreach($groupListOne as $key => $group){ ?>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="" data-toggle="collapse" data-parent="#accordion-test" href="#div<?=$key?>" >
											<?=$groupListOne[$key][0]['g_name']?> 가정 
											<span class="group-edit" data-key='<?=$key?>' >
												<i class="glyphicon glyphicon-plus"></i>
											</span>
										</a>
									</h4>
								</div>
								<div id="div<?=$key?>" class="panel-collapse collapse"  style="height: 0px;">
									<div class="panel-body ">
									<table>
										<tr>
									<?  foreach($group as $key => $groupMember){
										if(!$groupMember['group_idx']) continue;?>
										
										<?=$groupMember['gm_member_name'].'&nbsp;['.$groupMember['m_group_name'].']  '/* $groupMember['m_tel'] */?><br>
									<?}?>
										</tr>
									</table>
									</div>
								</div>
							</div>
						<?} }?>
						
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="tile-stats tile-block tile-white-cyan">
				<div class="tile-content">
					<div class="icon"><i class="entypo-moon"></i></div>
					<div class="num">2공동체</div>
					<h3>총 <?=count($groupListTwo)?>가정</h3>
					<p>여호수아,기드온 - 26세 부터 34세까지</p>
				</div>
				<div class="tile-footer">
					<div class="panel-group" id="accordion-test2">
						
						<? if($groupListTwo){ 
							foreach($groupListTwo as $key => $group){?>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="collapsed" data-toggle="collapse" data-parent="#accordion-test2" href="#s<?=$key?>" aria-expanded="false">
											<?=$groupListTwo[$key][0]['g_name']?> 가정 
											<span class="group-edit" data-key='<?=$key?>'>
												<i class="glyphicon glyphicon-plus"></i>
											</span>
										</a>
									</h4>
								</div>
								<div id="s<?=$key?>" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
									<div class="panel-body text-muted">
									<?if($group){ 
										foreach($group as $key => $groupMember){?>
										<?=$groupMember['gm_member_name'].' ['.$groupMember['m_group_name'].']  '.$groupMember['m_tel']?><br>
									<?} }?>
									</div>
								</div>
							</div>
						<?} }?>
					</div>
				</div>
			</div>
		</div>
		
			<?
			if(!empty($teamList)){	
			foreach($teamList as $team){?>
			<div class="col-sm-3">
				<div class="tile-title tile-primary">
					<div class="icon team" onclick="team.detail('<?=$team['idx']?>','detail_team');" >
						<i class="glyphicon glyphicon-leaf"></i>
					</div>
					<div class="title">
						<h3><?=$team['t_name']?></h3>
						<br>
						<p><?=$team['t_memo']?></p>
					</div>
				</div>
			</div>
			<?} }else{ /* echo '현재 생성된 팀이 없습니다.'; */ }?>
		</div>

	</div>
