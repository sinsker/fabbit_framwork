<div class="main-content">
<h3> 신상 정보 - <strong><?=$user['m_name']?><input type="hidden" name="m_idx" value="<?=$midx?>" ></strong></h3>
<hr />
<div class="profile-env">
	
	<header class="row">
		
		<div class="col-sm-2">
			<a href="#" class="profile-picture img-rounded"">
				<img src="<?=$user['m_image_path']?>" class="img-responsive " />
			</a>
			
		</div>
		
		<div class="col-sm-7">
			
			<ul class="profile-info-sections">
				<li>
					<div class="profile-name">
						<strong>
							<a href="#" class="user-status is-online tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Online"></a>
							<a href="#"><?=$user['m_name']?><a> <sub><?=$user['m_giso']?></sub>
							<!-- User statuses available classes "is-online", "is-offline", "is-idle", "is-busy" -->
						</strong>
						<span data-input='m_memo'><?=$user['m_memo']?></span>
					</div>
				</li>
				
			</ul>
		</div>
		
	</header>
	
	<section class="profile-info-tabs">
		<div class="row">
			<div class="col-sm-offset-2 col-sm-10">
				
				<ul class="user-details">
					<li>
						<a href="#">
							<i class="entypo-suitcase">성별</i>
						</a>
						<span class='text-info' data-input='m_sex'><?=$user['m_sex']?></span>
					</li>
					<li>
						<a href="#">
							<i class="entypo-location">분류</i>
						</a>
						<span class='text-info' data-input='m_group_name'><?=$user['m_group_name']?></span>
					</li>
					<li>
						<a href="#">
							<i class="entypo-suitcase">전화번호</i>
						</a>
						<span class='text-info' data-input='m_tel'><?=$user['m_tel']?></span>
					</li>
					<li>
						<a href="#">
							<i class="entypo-calendar">생일</i>
						</a>
						<span class='text-info' data-input='m_birthday'><?=$user['m_birthday']?></span>
					</li>
					<li>
						<a href="#">
							<i class="entypo-calendar">등록일</i>
						</a>
						<span class='text-info' data-input='m_join_date'><?=$user['m_join_date']?></span>
					</li>
				</ul>
				
				
				<!-- tabs for the profile links -->
				<ul class="nav nav-tabs">
					<li class="active"><a href="?f=member&v=mview&midx=<?=$midx?>">개인 신상</a></li>
					<!-- <li><a href="#biography">Bio</a></li> -->
					<li><a href="?f=member&v=medit&midx=<?=$midx?>">정보 수정</a></li>
				</ul>
				
			</div>
			
		</div>
		
	</section>
	
	
	<section class="profile-feed">
		
		<!-- profile post form -->
		<form class="profile-post-form" id="form_member_comment" method="post">
			<textarea class="form-control autogrow" placeholder="500자 이내로 작성해주세요."></textarea>
			<div class="form-options">
				<div class="post-submit">
					<button type="button" class="btn btn-primary" id="comment_write_btn">작성</button>
				</div>
				
			</div>
		</form>
		
		<!-- profile stories -->
		<div class="profile-stories">
			<?if(!empty($userCommentList)){
				foreach($userCommentList as $userComment){?>
			<article class="story">
				<div class="coment-view">
					<aside class="user-thumb">
						<a href="#">
							<img src="<?=IMG_PATH?>/admin/thumb-1.png" alt="" class="img-circle" />
						</a>
					</aside>
					
					<div class="story-content">
						<!-- story header -->
						<header>
							<div class="publisher">
								<a href="#"><?=$userComment['mc_write_name']?></a>
								<em><?=$userComment['mc_insert_date']?></em>
							</div>
							
							<div class="story-type">
								<i class="entypo-feather comment-edit"></i>
								<i class="entypo-cancel comment-del" data-key="<?=$userComment['idx']?>"></i>
							</div>
						</header>
						
						<div class="cm_comment">
							<p><?=$userComment['mc_memo']?></p>
						</div>
						
						<!-- story like and comment link -->
						<hr />
					</div>
				</div>
				<div class="coment-edit" style="display: none; ">
					<form action="?action=member" method="post" >
						<input type="hidden" name="cmd" value="update_comment" >
						<input type="hidden" name="midx" value="<?=$midx?>" >
						<input type="hidden" name="idx" value="<?=$userComment['idx']?>" >
						<aside class="user-thumb">
							<a href="#">
								<img src="<?=IMG_PATH?>/admin/thumb-1.png" alt="" class="img-circle" />
							</a>
						</aside>
						
						<div class="story-content">
							<!-- story header -->
							<header>
								<div class="publisher">
									<a href="#"><?=$userComment['mc_write_name']?></a>
									<em><?=$userComment['mc_insert_date']?></em>
								</div>
							</header>
							
							<div class="cm_comment">
								<textarea class="form-control " name="mc_memo" placeholder="500자 이내로 작성해주세요."><?=$userComment['mc_memo']?></textarea>
							</div>
							
							<footer style="text-align: right;">
								<input type="button" class="btn btn-primary edit-submit" value="수정"></button>
								<button type="button" class="btn btn-default comment-update-cacel-btn">취소</button>
							</footer>
							<!-- story like and comment link -->
							<hr />
						</div>
					</form>
				</div>
			</article>
			<? } }?>
			
	</section>
</div>
