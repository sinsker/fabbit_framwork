<div  class="main-content">
	<div class="header" >
		<h2 class="header-t">사역팀 리스트</h2>
		<div class="header-t text-right">
				<a class="btn btn-primary btn-icon icon-left" href="javascript:;" data-toggle="modal" data-target="#modal-team-insert" >
						<i class="entypo-plus"></i>추가
					</a>
			<!-- <a href="javascript:;" onclick="Show('modal-team_insert');" class="btn btn-default">팀 생성</a> -->
		</div>
	</div>
	<div class="row">
		<?
		if(!empty($teamList)){	
		foreach($teamList as $team){?>
		<div class="col-sm-3">
			<span class="team-count" >인원 <?=$team['team_member_count']?>명</span> 
			<span class="team-del" onclick="team.del('<?=$team['idx']?>','<?=$team['t_name']?>');" >X</span> 
			<div class="tile-title tile-primary">
				<div class="icon team" onclick="team.detail('<?=$team['idx']?>','detail_team');" >
					<i class="glyphicon glyphicon-leaf"></i>
				</div>
				<div class="title">
					<h3><?=$team['t_name']?> </h3>
					<br>
					<p><?=$team['t_memo']?></p>
				</div>
			</div>
		</div>
		<?} }else{ echo '현재 생성된 팀이 없습니다.'; }?>
	</div> 
</div>
<!-- 팀등록 모달 -->
<div class="modal fade" id="modal-team-insert">
	<div class="modal-dialog" style="z-index: 1040;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">SYNC 팀 추가</h4>
			</div>
			<div class="modal-body" >
				<form role="form" class="form-horizontal form-groups-bordered" id="team_insert_form" action="?action=member" method="post">
					<input type="hidden" name="cmd" value ="insert_team">
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label" >팀이름</label>
						<div class="col-sm-5">
							<input class="form-control" id="t_name" name='t_name' placeholder="한글로 작성해 주세요" type="text">
						</div>
					</div>
					<div class="form-group" >
						<label for="field-1" class="col-sm-3 control-label">팀설명</label>
						<div class="col-sm-5">
							<textarea class="form-control" id="t_memo" name='t_memo' placeholder="30자이내로 작성해주세요."></textarea>
						</div>
					</div>
					<div class="form-group text-center">
						
					</div>
				</form>
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-gold btn-icon" onclick="team.insert();">
						등록하기
						<i class="entypo-heart"></i>
				</button>
				<button type="button" class="btn btn-default btn-icon" data-dismiss="modal">
						취소하기
						<i class="entypo-cancel" ></i>
				</button>
			</div>
		</div>
	</div>
</div>