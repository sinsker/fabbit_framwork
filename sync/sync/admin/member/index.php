<div class="main-content">
	<div class="header" >
		<h2 class="header-t">인원 리스트</h2>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-gradient panel-collapse" data-collapsed="0">
				<div class="panel-heading">
					<div class="panel-title" data-rel="collapse" onclick="toggleView('add-user');" style="cursor: pointer;">
						인원 추가하기
					</div>
					<div class="panel-options">
						<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
						<a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
					</div>
				</div>
				
				<div class="panel-body" id = "add-user" style="display: none;" >
					<div class="form-group">
						<form method="post" action="?action=member" enctype="multipart/form-data" name ="member_excel_upload">
							<input type="hidden" name="cmd" value="insert_member">
							
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<span class="btn btn-blue btn-file">
									<span class="fileinput-new">엑셀등록</span>
									<span class="fileinput-exists">엑셀등록</span>
									<input type="file" name="member_execl">
								</span>
								<span class="fileinput-filename"></span>
								<a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
								<a href='?action=member&cmd=member_sample_down'  class="btn btn-white control-label" target="iframe">샘플 보기</a>
								<input type="submit" value="등록" class="btn btn-default control-label"></span></br>
							</div>
						</form>
					</div>
					<div class="form-group">
						<a href='?f=member&v=medit'><span type="button" class="btn btn-blue control-label" >추가하기</span></a></br>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div>
			<div class="col-sm-12">
			
				<div class="panel panel-primary">
					<div class="panel-heading">
						<div class="panel-title text-center">SYNC 지체 명단 <span class="badge badge-secondary"><?=$member->getMembersCount()?></span></div>
						<div class="panel-options">
							<a href="#;" class="btn btn-primary" id="replace-btn" style="color:white">초기화</a>
							<a href="#;" class="btn btn-primary" id="seleted-del-btn" style="color:white">선택 삭제</a>
						</div>
					</div>
						
					<table class="table table-hover  ">
						<thead>
							<tr class="active" align="center">
								<th style=" text-align: center;">#</th>
								<th style=" text-align: center;">이름</th>
								<th style=" text-align: center;">기수</th>
								<th style=" text-align: center;">타입</th>
								<th style=" text-align: center;">생일</th>
								<th style=" text-align: center;"><input type="checkbox"  id="allcheck"></th>
							</tr>
						</thead>
						<tbody>
						<? if(!empty($memberList)){
								foreach ($memberList as $key => $val){
								 $val = $member->modifyMemberData($val);?>
								<tr style=" text-align: center;" >
									<td align="center"><?=$val['no']?></td>
									<td style='cursor: pointer;' onclick="vmember('<?=$val['idx']?>');"><?=$val['m_name']?></td>
									<td style='cursor: pointer;' onclick="vmember('<?=$val['idx']?>');"><?=$val['m_giso']?></td>
									<td style='cursor: pointer;' onclick="vmember('<?=$val['idx']?>');"><?=$val['m_group_name']?></td>
									<td style='cursor: pointer;' onclick="vmember('<?=$val['idx']?>');"><?=$val['m_birthday']?></td>
									<td align="center"><input type="checkbox" class="checkbox" name="c" value="<?=$val['idx']?>"></td>
								</tr>
						<? } }else{ ?>
							<tr style=" text-align: center;" >
								<td colspan="6">데이터가 없습니다.</td>
							</tr>
						<?}?>
						</tbody>
					</table>
					<div class="text-center">
						<?=$pageing->getPageBlockHtml("f=member&page=$page&qe=$qe&tx=$tx")?>
					</div>
				</div>
				<table class="table" border=0;>
					<tr>
						<td>
							<div class="row">
								<div class="search_inf">
									<div class="col-xs-3">
									<?selectBox($member_group,$user_group,'user_group')?>
									</div>
									<div class="col-xs-2">
										<select class="form-control" id='select_option'>
											<option value='' >- 선택 -</option>
											<option value='m_name' <? if($qe == 'm_name') echo 'selected';?> >이름</option>
											<option value='m_tel' <? if($qe == 'm_tel') echo 'selected';?>>전화번호</option>
											<option value='m_giso' <? if($qe == 'm_giso') echo 'selected';?>>기수</option>
											<option value='m_birthday' <? if($qe == 'm_birthday') echo 'selected';?>>생일</option>
										</select>
									</div>
									<div class="input-group col-xs-6">
										<input class="form-control " type="text" id='select_str' value="<?=$tx?>">
										<span class="input-group-btn">
											<a href="#;" type="button" class="btn btn-primary" type="button" id='select_ok'>검색</a>
										</span>
									</div>
								</div>
							</div>
						</td>
					</tr>
				</table>
			</div>		
		</div>
	</div>
</div>