<div class="main-content">
	<div class="row">
		<div class="col-sm-8">
			<h3><?=$teamDetail['t_name']?> 상세 보기</h3>
			<input type="hidden" name="tid" id="tid" value="<?=$teamDetail['tid']?>" >
			<table class="table table-bordered  datatable table-hover" id="table-2">
				<thead>
				</thead>
				<tbody>
					<tr class="info">
						<th colspan="3"><strong>이름</strong></th>
					</tr>
					<tr>
						<th colspan="3"><?=$teamDetail['t_name']?></th>
					</tr>
					<tr class="info">
						<th colspan="3"><strong>설명</strong></th>
					</tr>
					<tr>
						<th colspan="3"><?=$teamDetail['t_memo']?></th>
					</tr>
					<tr class="info">
						<th colspan="3"><strong>명단 리스트</strong></th>
					</tr>
					<tr class="active">
						<th>이름</th>
						<th>그룹</th>
						<th>직책</th>
					</tr>
					<!-- 팀원리스트 -->
					<? if(!empty($teamMembers)){
						foreach($teamMembers as $member){?>
					<tr>
						<th width="20%"><a href="<?=$murl.$member['midx']?>"><?=$member['m_name']?> </a></th>
						<th width="30%"><?=$member['m_group_name']?></th>
						<th>
							<div class="input-group" >
								<input id="crrent_tm_pos" gm-key="<?=$member['idx']?>" onkeyup='team.edit("<?=$member['idx']?>");'   class="form-control" type="text" value="<?=$member['tm_position']?>">
								<span class="input-group-addon" onclick='team.deltmember("<?=$member['idx']?>");' style='cursor: pointer;'>x</span>
							</div>
						</th>
					</tr>
					<? } }?>
					<tr >
						<th colspan="3" class='text-right'><button id='team-show-memu' class='btn btn-info'>팀원 추가</button></th>
					</tr>
					<!-- 팀원 리스트 end -->
				</tbody>
			</table>
		</div>
	</div>
	<div class="row" id='team-menu' style='display: none'>
		<div class="col-sm-8">
			<div class="panel panel-primary" data-collapsed="0">
				<div class="panel-heading">
					<div class="panel-title">팀에 소속 시키기</div>

					<div class="panel-options">
						<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
					</div>
				</div>
				<div class="panel-body">
						<div class="col-sm-6">
							<div class="col-sm-7" style="float: none; ">
								<div id="ms-475multiselect" class="ms-container">
									<div class="ms-selection" style='width: 150px;'>
										<ul tabindex="-1" class="ms-list" id='member-select'>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-sm-12 control-label" >
								<div class="input-group" style="width: 170px">
									<input class="form-control" id='member-name' type="text" onkeyup="team.namesearch(this);" placeholder="추가할 이름 작성"  style="width: 150px">
									<span class="input-group-addon">이름</span>
								</div>
							</div>
						</div>
						<div class="col-sm-6 control-label gm-inset-box" style="display: none;">
							<form name="insert_group_member_form">
								<input type="hidden" name="cmd"  value="insert_team_member">
								<input type="hidden" name="midx" id="midx">
								<input type="hidden" name="mname" id="mbname">
								<input type="hidden" name="tidx" value="<?=$tid?>">
								<table class="table table-bordered table-striped datatable" id="table-2">
									<thead>
									</thead>
									<tbody>
										<tr>
											<th>이름 </th><td id="mname"></td>
										</tr>
										<tr>
											<th>그룹</th><td id="mtype"></td>
										</tr>
										<tr>
											<th>전화번호</th><td id="mtel"></td>
										</tr>
										<tr>
											<th>직급</th><td><input name="tm_position" class="form-control" type="text"></td>
										</tr>
										<tr>
											<td colspan="2" align="center">
												<span class="btn btn-info" onclick="team.gminsert();">추가하기</span>
											</td>
										</tr>
										
									</tbody>
								</table>
							</form>
						</div>
				</div>
			</div>
		</div>
	</div>
</div>