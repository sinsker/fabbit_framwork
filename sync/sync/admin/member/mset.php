<div class="main-content">
	<div class="header" >
		<h2 class="header-t">인원 추가</h2>
	</div>
	<div class="row">
		<div class="col-sm-8">
			<div class="panel panel-gradient panel-collapse" data-collapsed="0">
				<div class="panel-heading">
					<div class="panel-title" data-rel="collapse">
						인원 추가하기
					</div>
					<div class="panel-options">
						<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
						<a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
					</div>
				</div>
				
				<div class="panel-body" >
					<div class="form-group">
						<form method="post" action="?action=member" enctype="multipart/form-data" name ="member_excel_upload">
							<input type="hidden" name="cmd" value="insert_member">
							
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<span class="btn btn-blue btn-file">
									<span class="fileinput-new">엑셀등록</span>
									<span class="fileinput-exists">엑셀등록</span>
									<input type="file" name="member_execl">
								</span>
								<span class="fileinput-filename"></span>
								<a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
								<a href='?action=member&cmd=member_sample_down'  class="btn btn-white control-label" target="iframe">샘플 보기</a>
								<input type="submit" value="등록" class="btn btn-default control-label"></span></br>
							</div>
						</form>
					</div>
					<div class="form-group">
						<a href='?f=member&v=medit'><span type="button" class="btn btn-blue control-label" >추가하기</span></a></br>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>