<link href='http://fonts.googleapis.com/earlyaccess/jejugothic.css' rel='stylesheet' type='text/css'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.0.0/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
<style>
/* body{ font-family: 'Jeju Gothic', serif;} */
	ul{  text-align:justify;}
	a{}
	.team { cursor: pointer; }
	
	.team-plus {   font-size: 30px;color: #424444; background: #F5F5F5; border: 2px solid #303641; border-radius: 5px; }
	.group-plus {   font-size: 30px;color: #424444; background: #F5F5F5; border: 2px solid #303641; border-radius: 5px; }
	.team-team:HOVER {background: #252A32; color: white;border: 2px solid #252A32; }
	#app{text-align:center;}
	.tile-stats {  padding: 0px; }
	.tile-stats .icon { color: #303641; top: 50px;   bottom: 0px;     height: 13px; }
	.tile-stats.tile-white-red .icon {  color: #303641; }
	.tile-stats.tile-white-cyan .icon {  color: #303641; }
	.tile-block .form-control, .tile-block .btn {    background: rgba(0, 0, 0, 0.15);color: #000000; border-color: transparent; }
	.tile-block .form-control, .tile-block .btn:HOVER { color: #ffffff; }
	.panel-group .panel > .panel-heading > .panel-title { background: #CAC8C9; font-size: 13px;}
	.group-edit { float: right; }
	.group-edit:HOVER { color: white; }
	#search_list{ max-height: 340px;overflow-y: auto; }
	#search_list li{    cursor: all-scroll;}
	#current_list{ background: #D5D6D6; min-height: 100px; padding: 3px; }
	#leader_list {  background: #D5D6D6; min-height: 45px; padding: 3px; }
	.row-content{    padding: 20px 0px; }
	.story-type > i { cursor: pointer;}
	.gl {     position: fixed; z-index: 300;width: 100%; height: 100%;top: 0;left: 0; background: rgba(0, 0, 0, 0.66);}
	.gl-wrap {     text-align: center;     padding-top: 100px;}
	.gl-content {     width: 500px;  display: inline-block;height: auto; padding: 20px; border-radius: 10px; background: white; border: 1px solid #4A4646;}
	.team-count{ position: absolute;  color: #909090; z-index: 1;padding: 5px;}
	@media (min-width: 768px){  .tile-title .title p {min-height: 70px; } }
	
	
	.col-sm-3 >.team-del{       
	    float: right;
    position: inherit;
    background: rgba(180, 175, 175, 0.61);
    display: none;
    color: black;
    width: 20px;
    height: 20px;
    z-index: 1;
    margin: 5px;
    text-align: center;
    padding: 2px;
    border-radius: 5px;
    cursor: pointer;
    position: absolute;
    right: 15px;
    }
	.col-sm-3:HOVER > .team-del{
		display: inline-block;
	}
    .col-sm-3 >.team-del:HOVER {
    	background: rgba(255, 255, 255,0.3);
	}
	
	.ms-container .ms-list > li span {
     padding:0px; 
}
</style>