
<!--[if lt IE 9]><script src="<?=JS_PATH?>/admin/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<script>
var auth = {
	type :  null,	
	init : function(type){
		this.type = type
		this.view();
	},
	view : function(){
		$('#auth_'+this.type).show();
	},

	join: function(){
		$.post('/?action=auth',$( "#form_"+this.type ).serialize() , function(data){
			console.log(data);
			alert(data.msg);
			if(data.result == 'done'){
				location.href = data.return_url;
			}
		},'json');
	}
}

auth.init('<?=$auth?>');


</script>
