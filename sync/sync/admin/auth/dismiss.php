<div class="main-content">
		<div class="page-error-404">
	
	
	<div class="error-symbol">
		<i class="entypo-attention"></i>
	</div>
	
	<div class="error-text">
		<h2><?=$_SESSION['auth_name']?>님</h2>
		<p>해당 계정은 미승인 계정입니다. 관리자에게 문의하셔서 사용 승인을 받아 주세요.</p>
	</div>
	
	<hr />
</div>