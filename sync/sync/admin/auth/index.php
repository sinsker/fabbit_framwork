
<!-- This is needed when you send requests via Ajax -->
<script type="text/javascript">
var baseurl = '';
</script>
<div class="login-container">
	<div class="login-header login-caret">
		<div class="login-content">
			<a href="index.html" class="logo">
				<img src="<?=IMG_PATH?>/admin/sync_t.png" width="120" alt="" />
			</a>
			<p class="description">SYNC 관리자 페이지 입니다.</p>
			
			<!-- progress bar indicator -->
			<div class="login-progressbar-indicator">
				<h3>43%</h3>
				<span>logging in...</span>
			</div>
		</div>
	</div>
	<div class="login-progressbar">
		<div></div>
	</div>
	
	<div class="login-form" id="auth_login" style="display: none">
		<div class="login-content">
			<div class="form-login-error">
				<h3>로그인 실패</h3>
				<p>사용자의 <strong>ID</strong> 혹은 <strong>PW</strong> 를 다시 확인 해 주세요.</p>
			</div>
			
			<form method="post" role="form" id="form_login">
				<input type="hidden" name="cmd" value="login">
				
				<? if($autoLogin){ ?>
					<input type="hidden" name="autologin" value="<?=$autoLogin?>">
					<input type="hidden" name="password" id ="password" value="autologin">
					<div class="form-group lockscreen-input">
						<div class="lockscreen-thumb">
							<img src="<?=IMG_PATH?>/admin/user-group-256.png" class="img-circle">
						</div>
						<div class="lockscreen-details"> 
							<h4><?=$_COOKIE['a_name']?><input type="hidden"  name="username" id="id" value="<?=$_COOKIE['a_id']?>" > 님 <a href="?action=auth&cmd=reset&type=get"><i class="entypo-login" style="cursor: pointer;color: #D1D695;" ></i></a></h4>
							<span data-login-text="logging in..."> 접속 기록이 남아 있습니다. </span>
						</div>
					</div>
				<? }else{ ?>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">
								<i class="entypo-user"></i>
							</div>
							<input type="text" class="form-control" name="username" id="id" placeholder="아이디" autocomplete="on" />
						</div>
					</div>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">
								<i class="entypo-key"></i>
							</div>
							<input type="password" class="form-control" name="password" id="password" placeholder="비밀번호" autocomplete="off" />
						</div>
					</div>
				<? } ?>
				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-block btn-login">
						<i class="entypo-login"></i>
						Login In
					</button>
				</div>
			</form>
			<a href="/admin/?auth=join" class="link">회원가입이 되어있지 않으신가요?</a>
			<div class="login-bottom-links">
			</div>
		</div>
	</div>
	
	<div class="login-form" id="auth_join" style="display: none">
		<div class="login-content">
			<div class="form-login-error">
				<h3>Invalid login</h3>
				<p>Enter <strong>demo</strong>/<strong>demo</strong> as login and password.</p>
			</div>
			<form method="post" role="form" id="form_join">
				<input type="hidden" name="cmd" value="join">
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-user"></i>
						</div>
						<input type="text" class="form-control" name="id" id="username" placeholder="아이디" autocomplete="off" />
						<input type="text" class="form-control" name="name" id="username" placeholder="이름" autocomplete="off" />
						<?=$groupSelectBox?>
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-key"></i>
						</div>
						<input type="password" class="form-control" name="password" id="password" placeholder="비밀번호" autocomplete="off" />
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-key"></i>
						</div>
						<input type="password" class="form-control" name="password" id="password" placeholder="비밀번호 재확인" autocomplete="off" />
					</div>
				</div>
			</form>
			<div class="form-group">
				<button type="button" class="btn btn-primary btn-block btn-login" id="join_submit" onclick="auth.join()">
					<i class="entypo-login" ></i>
					Join In
				</button>
			</div>
			<a href="/admin/" class="link">로그인 페이지로 이동</a>
			<div class="login-bottom-links">
			</div>
		</div>
	</div>
</div>

