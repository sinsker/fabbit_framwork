
<script src="<?=JS_PATH?>/admin/remodal.js" type="text/javascript"></script>
<script src="<?=JS_PATH?>/admin/file/jquery.treeview.js" type="text/javascript"></script>
<!--[if lt IE 9]><script src="<?=JS_PATH?>/admin/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
	
	
<script type="text/javascript">
$(function(){
	$("#browser").treeview({
		toggle: function() {
			console.log("%s was toggled.", $(this).find(">span").text());
		}
	});
	
	$(".btn-file-down").click(function(){
		var file_key = $(this).parent().attr('data-key');
		if(file_key == ''){
			alert('정상적인 방법으로 다운로드를 해주시기 바랍니다.');
			return false;
		}
		window.open("/?action=file&mode=DOWNLOAD&idx="+file_key,'iframe');
	});
	
	$("#cal-excel").click(function(){
		window.open('?action=calendar&cmd=cal_excel&month=<?=$month?>','iframe');
	});	
	
	$("#all-excel").click(function(){
		window.open('?action=calendar&cmd=cal_excel','iframe');
	});	
});
</script>