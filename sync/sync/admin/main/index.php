<div class="main-content">
	<div class="jumbotron">
        <h1>SYNC 관리자에 오신것을 환영합니다.</h1>
        <hr>
        <p>SYNC 에 관한 파일이나 이벤트 페이지등의 관리를 한곳에서 처리할수 있습니다. 왼쪽 메뉴에서 원하시는 메뉴를 선택해 주시기 바랍니다.</p>
        <p>아직 보완되어야 할 사항이 많이 있습니다. 회계 ,서기 기능 및 전체 차트 관리등 빠른 시일내로 업데이트 하도록 하겠습니다.</p>
        <p>
          <a class="btn btn-lg btn-primary" href="../../components/#navbar" role="button">메인 사이트로 이동 »</a>
        </p>
      </div>
	<div class="row">
		<div class="col-sm-6">
			<div class="panel panel-dark">
				<div class="panel-heading">
					<div class="panel-title text-center"><a href="?f=member">회원 명단 </a><span class="badge badge-secondary"><?=$member->getMembersCount()?></span></div>
				</div>
				<div class="panel-contants">
					<table class="table table-hover  ">
						<thead>
							<tr class="active" align="center">
								<th style=" text-align: center;">#</th>
								<th style=" text-align: center;">이름</th>
								<th style=" text-align: center;">기수</th>
								<th style=" text-align: center;">타입</th>
								<th style=" text-align: center;">생일</th>
							</tr>
						</thead>
						<tbody>
						<? if(!empty($memberList)){
								foreach ($memberList as $key => $val){
								 $val = $member->modifyMemberData($val);?>
								<tr style=" text-align: center;" >
									<td align="center"><?=$val['no']?></td>
									<td style='cursor: pointer;' onclick="vmember('<?=$val['idx']?>');"><?=$val['m_name']?></td>
									<td style='cursor: pointer;' onclick="vmember('<?=$val['idx']?>');"><?=$val['m_giso']?></td>
									<td style='cursor: pointer;' onclick="vmember('<?=$val['idx']?>');"><?=$val['m_group_name']?></td>
									<td style='cursor: pointer;' onclick="vmember('<?=$val['idx']?>');"><?=$val['m_birthday']?></td>
								</tr>
						<? } }else{ ?>
							<tr style=" text-align: center;" >
								<td colspan="6">데이터가 없습니다.</td>
							</tr>
						<?}?>
						</tbody>
					</table>
					<div class="text-center">
						<?=$pageing->getPageBlockHtml("f=member&page=$page&qe=$qe&tx=$tx")?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="panel panel-dark">
				<div class="panel-heading">
					<div class="panel-title text-center"><a href="?f=file&v=file">파일 리스트</a></div>
				</div>
				<div class="panel-contants">
					<div class="file-content">
						<ul class="filetree treeview-famfamfam treeview">
							<li><span class="folder ">파일 리스트</span>
								<ul id="browser" >
								<? if(!empty($fileList)){?>
									<?foreach($fileList as $key=> $file){  ?>
										<?if(is_array($file)){?>
										<li>
											<span class="folder" >
												<span><?=$key?></span>
												<input type="hidden" value = "<?=$key?>" name="folder" >
											</span>
											<span class="folder-edit" style="display:none;">
												<input type="text" value = "<?=$key?>" name="folder" >
												<input type="hidden" value = "<?=$key?>" name="old_folder" >
											</span>
											<ul>
												<? if(!empty($file[0])){
													foreach($file[0] as $subkey=> $subfile){  ?>
													<?if(!empty($subfile)){?>
														<li>
															<?$dbfile = $fileManager->getFileData("and f_type='file' and f_name = '$subfile' "); ?>
															<span class="file" data-key="<?=$dbfile['idx']?>">
																<?=$dbfile['f_realname']?>
																<a href="#;" class="btn-file-down"><i class="entypo-download "></i></a> <!-- 다운 -->
															</span>
														</li>
													<?}?>
												<?}}?>
											</ul>
										</li>
										<?}else{ $dbfile = $fileManager->getFileData("and f_type='file' and f_name = '$file' ");  ?>
										<li>
											<span class="file" data-key="<?=$dbfile['idx']?>">
												<?=$dbfile['f_realname']?>
												<a href="#;" class="btn-file-down"><i class="entypo-download"></i></a> <!-- 다운 -->
											</span>
										</li>
										<?}?>
								<?} }?>	
								</ul>
							</li>
						</ul>
					</div>		
				</div>
			</div>
		</div>	
		<div class="col-sm-6">
			<div class="panel panel-dark">
				<div class="panel-heading">
					<div class="panel-title text-center" ><a href="?f=page&v=module">최근 생성된 페이지 리스트 TOP 5</a></div>
				</div>
				<div class="panel-contants">
					<table class="table table-hover" >
						<thead>
							<tr class="active" align="center">
								<th style=" text-align: center;">제목</th>
								<th style=" text-align: center;">부제</th>
								<th style=" text-align: center;">URL</th>
								<th style=" text-align: center;">상태</th>
							</tr>
						</thead>
						<tbody>
						<? if($moduleList){
							foreach($moduleList as $key => $value){?>
								<tr style=" text-align: center;" >
									<td ><?=$value['p_name'];?></td>
									<td ><?=$value['p_subname']?></td>
									<td ><a class="page-link" href="<?=URL.'/?f='.$value['p_key']?>" target="_blank"><?=URL.'/?f='.$value['p_key']?> </a></td>
									<td ><?if($value['p_security'] == 'private'){?>
										<i class="entypo-lock" style="color:#FF3E41;"></i>
										<?}else{?>
										<i class="entypo-lock-open" style="color:#70EA70;"></i></td>
										<?}?>
								</tr>
						<? } }else{ ?>
							<tr style=" text-align: center;" >
								<td colspan="6">데이터가 없습니다.</td>
							</tr>
						<?}?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="panel panel-dark">
				<div class="panel-heading">
					<div class="panel-title text-center" ><a href="?f=calendar"><?=$year?>년 <?=(int)$month?>월 캘린더</a></div>
				</div>
				<div class="panel-contants">
					<div class="calendar-env">
						<div class="calendar-body" style="width: 100%">
							<div id="calendar" class="fc fc-ltr">
								<table class="fc-header" style="width: 100%">
									<tbody>
										<tr>
											<td class="fc-header-left"><span class="fc-header-title"></span></td>
											<td class="fc-header-center">
											
											</td>
											<td class="fc-header-right">
												<a href="#;" class=" btn btn-primary" id="cal-excel" ><?=(int)$month?>월 엑셀</a>
												<a href="#;" class=" btn btn-primary" id="all-excel" >전체 엑셀</a>
											</td>
										</tr>
									</tbody>
								</table>
								<div class="fc-content" style="position: relative;">
									<div class="fc-view fc-view-month fc-grid" style="position: relative" unselectable="on">
										<table class="table fc-border-separate table-bordered" style="width: 100%" cellspacing="0">
											<thead>
												<tr class="fc-first fc-last">
													<th class="fc-day-header fc-mon fc-widget-header fc-first">월</th>
													<th class="fc-day-header fc-tue fc-widget-header">화</th>
													<th class="fc-day-header fc-wed fc-widget-header">수</th>
													<th class="fc-day-header fc-thu fc-widget-header">목</th>
													<th class="fc-day-header fc-fri fc-widget-header">금</th>
													<th class="fc-day-header fc-sat fc-widget-header text-danger">토</th>
													<th class="fc-day-header fc-sun fc-widget-header fc-last text-info">일</th>
												</tr>
											</thead>
											<tbody>
												<?  $day = 1 ;
											    for($i=1; $i <= $cal_total_week; $i++){?>
													<tr class="fc-week"> 
														<? for ($j=0; $j<7; $j++){ $class =''; ?>
															<? if(!(($i == 1 && $j < $cal_start_week) || ($i == $cal_total_week && $j > $cal_last_week))){
																if($j == 5){ $class .= " bg-danger "; }
																else if($j == 6){ $class .= " bg-info ";}
																
																if($year."-".$month."-".$day == date("Y-m-d")){ $class .= " calender-today "; } //오늘 날짜
															?>
																<td class="fc-day fc-sat fc-widget-content fc-past <?=$class?>" data-date="<?=$year?>-<?=$month?>-<?=strlen($day) == 1 ? '0'.$day : $day?>">
																	<div class="fc-day-number"><?=$day?></div>
																	<!-- 실제 컨텐츠 -->
																	<div class="fc-day-content">
																			<div style="position: relative; min-height: 69px;">
																				<?  $str_day = strlen($day) == 1 ? '0'.$day : $day;
																					if(!empty($user_list[$month][$str_day])){
																					foreach( $user_list[$month][$str_day] as $key => $val){ ?>
																					<div class="fc-event fc-event-hori" >
																						<div class="fc-event-inner">
																							<div class="event-title" data-type="brith" data-key ="<?=$val['member_idx']?>">
																								(<?=$val['m_giso']?>)<?=$val['m_name']?> 
																							</div>
																						</div>
																					</div>
																				<?}}?>
																			</div>
																	</div>
																	<!-- // 실제 컨텐츠 -->
															<?	$day++;
																}else{ //빈값일경우 ?> 
																<td class="fc-day">
																	<div class="fc-day-content"></div>
																<?}?>
															</td>
														<?}?>
													</tr>
												<?}?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="panel panel-dark">
				<div class="panel-heading">
					<div class="panel-title text-center">최근 접속 기록 10개 <span class="badge badge-secondary"><?=$paging->rowCount?></span></div>
				</div>
				<div >
				<table class="table table-hover">
					<thead>
						<tr class="active" align="center">
							<th style=" text-align: center;">#</th>
							<th style=" text-align: center;">아이디</th>
							<th style=" text-align: center;">결과</th>
							<th style=" text-align: center;">메모</th>
							<th style=" text-align: center;">아이피</th>
							<th style=" text-align: center;">날짜</th>
						</tr>
					</thead>
					<tbody>
					<? if(!empty($adminLogList)){
							foreach ($adminLogList as $key => $val){ ?>
							<tr style=" text-align: center;" >
								<td align="center"><?=$paging->startNo--?></td>
								<td align="center"><?=$val['al_uid']?></td>
								<td align="center"><?=$val['al_result']?></td>
								<td align="center"><?=$val['al_memo']?></td>
								<td align="center"><?=$val['al_ip']?></td>
								<td align="center"><?=$val['wdate']?></td>
							</tr>
					<? } } ?>
					</tbody>
				</table>
				</div>
		</div>
	</div>
	
</div>