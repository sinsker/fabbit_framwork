<div class="sidebar-menu">
		<header class="logo-env">
			<!-- logo -->
			<div class="logo">
				<a href="/admin/">
					<img src="<?=IMG_PATH?>/admin/sync_t.png" width="120" alt="" />
				</a>
			</div>
			<div class="sidebar-collapse">
				<a href="#" class="sidebar-collapse-icon with-animation"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
					<i class="entypo-menu"></i>
				</a>
			</div>
			
			<!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
			<div class="sidebar-mobile-menu visible-xs">
				<a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
					<i class="entypo-menu"></i>
				</a>
			</div>
			
		</header>
				
		<ul id="main-menu" class="">
			<!-- add class "multiple-expanded" to allow multiple submenus to open -->
			<!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
			<!-- Search Bar -->
			<li id="search">
				<form method="get" action="">
					<input type="text" name="q" class="search-input"value="접속 시간 : <?=$_SESSION['auth_date']?>"/>
				</form>
				
			</li>
			<li>
				<a href="layout-api.html">
					<i class="entypo-users"></i>
					<span>SYNC 정보</span>
				</a>
				<ul>
					<!-- <li>
						<a href="?f=member&v=mset">
							<span>인원 추가</span>
						</a>
					</li> -->
					<li>
						<a href="?f=member">
							<span>인원 리스트</span>
						</a>
					</li>
					<li>
						<a href="?f=member&v=group">
							<span>가정 관리</span>
						</a>
					</li>
					<li>
						<a href="?f=member&v=team">
							<span>팀 관리</span>
						</a>
					</li>
				</ul>
			</li>
			<!-- 
			<li>
				<a href="extra-icons.html">
					<i class="entypo-book-open"></i>
					<span>회계 & 서기 보고</span>
				</a>
				<ul>
					<li>
						<a href="?f=event">
							<span>한눈에 보기</span>
						</a>
					</li>
				</ul>
				<ul>
					<li>
						<a href="extra-icons.html">
							<span>서기 보고</span>
						</a>
						<ul>
							<li>
								<a href="extra-icons.html">
									<span>월별 통계</span>
								</a>
							</li>
							<li>
								<a href="extra-icons-entypo.html">
									<span>서기 보고서 작성</span>
								</a>
							</li>
						</ul>
					</li>
				</ul>
				<ul>
					<li>
						<a href="extra-icons.html">
							<span>회계 보고</span>
						</a>
						<ul>
							<li>
								<a href="extra-icons.html">
									<span>월별 통계</span>
								</a>
							</li>
							<li>
								<a href="extra-icons-entypo.html">
									<span>서기 보고서 작성</span>
								</a>
							</li>
						</ul>
					</li>
				</ul>	
			</li>
			 -->
			<li>
				<a href="?f=notes">
					<i class="entypo-doc-text-inv"></i>
					<span>회의록</span><!-- <span class="badge badge-secondary">2</span> --></span>
				</a>
			</li>
			<li>
				<a href="?f=calendar">
					<i class="entypo-window"></i>
					<span>행사 및 기념일 안내</span><!-- <span class="badge badge-secondary">2</span> --></span>
				</a>
			</li>
			<!-- 
			<li>
				<a href="?f=chart">
					<i class="entypo-chart-bar"></i>
					<span>전체 차트</span>
				</a>
			</li>
			 -->
			<li>
				<a href="ui-panels.html">
					<i class="entypo-bookmarks"></i>
					<span>페이지 관리</span>
				</a>
				<ul>
					<li>
						<a href="?f=page&v=module">
							<span>페이지 관리</span>
						</a>
					</li>
					<li>
						<a href="?f=page">
							<span>접근 통계</span>
						</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="ui-panels.html">
					<i class="entypo-upload"></i>
					<span>파일 매니저</span>
				</a>
				<ul>
					<li>
						<a href="?f=file">
							<span>이미지 관리</span>
						</a>
					</li>
					<li>
						<a href="?f=file&v=file">
							<span>파일 관리</span>
						</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="?f=manager" >
					<i class="entypo-graduation-cap"></i>
					<span>최고 관리자</span>
				</a>
				<ul>
					<li>
						<a href="?f=manager">
							<span>회원가입 승인처리</span>
						</a>
					</li>
					<li>
						<a href="?f=manager&v=access">
							<span>접근 기록</span>
						</a>
					</li>
					<!-- <li>
						<a href="?f=file&v=file">
							<span>사용자 권한</span>
						</a>
					</li>
					<li>
						<a href="?f=file&v=file">
							<span>메뉴 권한</span>
						</a>
					</li> -->
				</ul>
			</li>
			<li>
				<a href="?f=query" >
					<i class="entypo-monitor"></i>
					<span>개발자 모드</span>
				</a>
			</li>
		</ul>
	</div>	
