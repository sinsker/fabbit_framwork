<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<script type="text/javascript">
	$(function () {
	    $('#char').highcharts({
	        chart: {  type: 'area' },
	        title: { text: '<?=$v_name.' 페이지 접속 그래프'?>'  },
	        subtitle: { text: '*지체들의 적극적인 참여도를 파악' },
	        credits: {
	            enabled: false
	        },
	        xAxis: {
	        	categories: [
				<? foreach ($event['log'] as $key => $value){ ?>
						'<?=$value['wdate']?>',
	        	<?}?>
	        	]
	        },
	        yAxis: {
	            title: {
	                text: '접속 카운트'
	            },
	            labels: {
	                formatter: function () {
	                    return this.value ;
	                }
	            }
	        },
	        tooltip: {
	            pointFormat: '{series.name}<b>{point.y:,.0f}</b><br/>' /* warheads in {point.x} */
	        },
	        plotOptions: {
	            area: {
	                marker: {
	                    enabled: true,
	                    symbol: 'circle',
	                    radius: 4,
	                    states: {
	                        hover: {
	                            enabled: true
	                        }
	                    }
	                }
	            }
	        },
	        series: [{
	            name: '접속횟수',
	            data:[
	            <? foreach ($event['log'] as $key => $value){ ?>
					<?=$value['acc_cnt']?>,
				<?}?>
				]
	        }, {
	            name: '접속자수',
	            data:[
	            <? foreach ($event['log'] as $key => $value){ ?>
					<?=$value['user_cnt']?>,
				<?}?>
				]
	        }]
	    });

	    $('#char2').highcharts({
	        chart: {
	            plotBackgroundColor: null,
	            plotBorderWidth: null,
	            plotShadow: false,
	            type: 'pie'
	        },
	        credits: {
	            enabled: false
	        },
	        title: {
	            text: '기종별 접속 분포도'
	        },
	        tooltip: {
	            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	        },
	        plotOptions: {
	            pie: {
	                allowPointSelect: true,
	                cursor: 'pointer',
	                dataLabels: {
	                    enabled: true,
	                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
	                    style: {
	                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
	                    }
	                }
	            }
	        },
	        series: [{
	            name: '분포도',
	            colorByPoint: true,
	            data: [
	            <?foreach($amodel as $key => $value){  $tmpPercent = $value/ $event['cnt']  * 100 ?>
	            	{name : '<?=$key?>',
	            	y : <?=$tmpPercent?>},
				<?}?>
	            ]
	        }]
	    });

	    $('#char3').highcharts({
	        chart: {
	            plotBackgroundColor: null,
	            plotBorderWidth: null,
	            plotShadow: false,
	            type: 'pie',
	            options3d: {
	                enabled: true,
	                alpha: 45,
	                beta: 0
	            }
	        },
			credits: {
	            enabled: false
	        },
	        title: {
	            text: 'PC/모바일 접속 분포도'
	        },
	        tooltip: {
	            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	        },
	        plotOptions: {
	            pie: {
	                allowPointSelect: true,
	                cursor: 'pointer',
	                depth: 35,
	                dataLabels: {
	                    enabled: true,
	                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
	                    style: {
	                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
	                    }
	                }
	            }
	        },
	        series: [{
	            name: '분포도',
	            colorByPoint: true,
	            data: [
	            <?foreach($atype as $key => $value){  $tmpPercent = $value/ $event['cnt']  * 100 ?>
	            	{name : '<?=$key?>',
	            	y : <?=$tmpPercent?>},
				<?}?>
	            ]
	        }]
	    });

	    
	});
	
	function viewEventPage(page,pageName){
		location.href = '?f=page&view='+page+'&v_name='+pageName;
	}
</script>