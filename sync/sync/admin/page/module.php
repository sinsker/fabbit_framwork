<div class="main-content">
	<div class="header" >
		<h2 class="header-t">페이지 관리</h2>
		<div class="header-t text-right">
			<a class="btn btn-primary btn-icon icon-left"   href="#modal">
				<i class="entypo-plus"></i>
				새로 추가
			</a>
		</div>
	</div>
	<? if($moduleList){
		foreach($moduleList as $key => $value){?>
		<div class="tile-block" data-security = "<?=$value['p_security']?>-<?=$value['idx']?>-<?=$value['p_key']?>" >
			<div class="tile-header">
				<?if($value['p_security'] == 'private'){?>
				<i class="entypo-lock" style="color:#FF3E41;"></i>
				<?}else{?>
				<i class="entypo-lock-open" style="color:#70EA70;"></i>
				<?}?>
				<a href="#;">
					<?=$value['p_name'];?>
					<span><?=$value['p_subname']?></span>
				</a>
				<a class="page-link" href="<?=URL.'/?f='.$value['p_key']?>" target="_blank"><?=URL.'/?f='.$value['p_key']?> </a>
			</div><!-- 
			<div class="tile-content tile-footer">
				<ul class="todo-list ">
						<div class="checkbox checkbox-replace color-white neon-cb-replacement">
							<label class="cb-wrapper"><input type="checkbox" ><div class="checked"></div></label>
							<label>전문가용 템플릿</label>
						</div>
					
				</ul>
			</div> -->
			<div class="tile-footer">
				<?if($value['p_security'] == 'private'){?>
				<input type="password" class="form-control" name="security" placeholder="보안코드를 입력하세요." maxlength="4">
				<?}?>
				<button type="button" class="btn btn-block edit-module-btn">수정하기</button>
				<button type="button" class="btn btn-block data-module-btn">데이터 확인</button>
			</div>
		</div>
		<?} }?>
		<div class="text-center">
			<?=$paging->getPageBlockHtml("f=page&v=module&page=$page")?>
		</div>
</div>

<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal-head" aria-describedby="modal1Desc">
  <button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
	<form method="post" action="?action=page" id='page-create-form' class="form-horizontal form-groups-bordered" >
  		<div>
		    <div id="modal-head"><h3>기본 템플릿 추가</h3></div>
		    <p class="text-danger">※ 한번 생성된 초기 데이터는 변경이 불가능 합니다.</p>
		    <p id="modal1Desc">
		     	<div class = 'page-create-modal-conteant' >
					<input type="hidden" name="mode" value="initcreate">
					<div class="form-group">
						<label for="p_key" class="col-sm-3 control-label text-danger">* 고유키 (영문)</label>
						<div class="col-sm-5">
							<input class="form-control" name="p_key" placeholder="영문으로만 작성해주세요." type="text" value="" maxlength="20">
						</div>
					</div>
					<div class="form-group">
						<label for="p_name" class="col-sm-3 control-label">제목</label>
						<div class="col-sm-5">
							<input class="form-control" name="p_name" placeholder="페이지 이름" type="text" value="">
						</div>
					</div>
					<div class="form-group">
						<label for="p_subname" class="col-sm-3 control-label">부제목</label>
						<div class="col-sm-5">
							<input class="form-control" name="p_subname" placeholder="페이지 설명" type="text" value="">
						</div>
					</div>
					<div class="form-group">
						<label for="p_key" class="col-sm-3 control-label ">보안</label>
						<div class="col-sm-5">
							<div class="radio page-switch-radio-btn">
								<label>
									<input type="radio" name="p_security" value="public" checked = "checked"><span class="" style="color:green">Public</span>
								</label>
							</div>
							<div class="radio page-switch-radio-btn">
								<label>
									<input type="radio" name="p_security" value="private" ><span class="" style="color:red">Private</span>
								</label>
							</div>
						</div>	
					</div>
					<div class="form-group" data-on="private"  style="display:none">
						<div class="row">
							<label for="p_key" class="col-sm-3 control-label ">보안코드</label>
							<div class="col-sm-5">
								<input class="form-control" name="p_securitycode" placeholder="4자리 숫자로 입력해주세요." type="password" value="" maxlength="4">
							</div>
						</div>
						<div class="row">
							<label for="p_key" class="col-sm-3 control-label ">보안코드 확인</label>
							<div class="col-sm-5">
								<input class="form-control" name="p_securitycode_re" placeholder="다시한번 입력해주세요." type="password" value="" maxlength="4">
							</div>
						</div>
					</div>
					
				</div>
			</p>
		</div>
		<br>
		<button data-remodal-action="cancel" class="remodal-cancel" style="background:#CCC1C1">취소</button>
		<input type="submit" class="remodal-confirm"  value="생성" style="background:#343534" >
	</form>
</div>

