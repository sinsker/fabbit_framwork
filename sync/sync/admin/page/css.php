<link href='http://fonts.googleapis.com/earlyaccess/jejugothic.css' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="<?=CSS_PATH?>/admin/font-icons/entypo/css/entypo.css">
<link rel="stylesheet" href="<?=CSS_PATH?>/admin/neon-core.css">
<link rel="stylesheet" href="<?=CSS_PATH?>/admin/neon-theme.css">
<link rel="stylesheet" href="<?=CSS_PATH?>/admin/neon-forms.css">
<link rel="stylesheet" href="<?=CSS_PATH?>/admin/custom.css">
<link rel="stylesheet" href="<?=CSS_PATH?>/admin/remodal.css" type="text/css" rel="stylesheet">
<link rel="stylesheet" href="<?=CSS_PATH?>/admin/remodal-default-theme.css" type="text/css" rel="stylesheet">

<!-- font-family: 'Jeju Gothic', serif; 
	 font-family: 'Nanum Pen Script', serif;-->
<style>
.page-switch-radio-area{ text-align: center; }
.page-switch-radio-btn{display: inline-block; margin-left: 15px;}
.page-switch-radio-btn span{ font-size: 14px;}
.tile-block .tile-header a.page-link{
    color: #EEDF84;
    font-size: 13px;
}

.state-wrap{
    padding: 10px;
    margin-bottom: 10px;
    color: #303641;
    -webkit-border-radius: 3px;
    -webkit-background-clip: padding-box;
    -moz-border-radius: 3px;
    -moz-background-clip: padding;
    border-radius: 3px;
    background-clip: padding-box;
    padding-top: 15px;
    border-radius: 3px;
}

.module-wrap{
	padding-bottom: 10px;
}
</style>