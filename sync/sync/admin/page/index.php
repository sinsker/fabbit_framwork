<div class="main-content">
	<div class="header" >
		<h2 class="header-t">접근 통계</h2>
	</div>
	<div id ="admin-wrap ">
		<div class="panel panel-dark" >
			<div class="panel-heading" style="text-align: center;">
				<div class="panel-title " style="display: inline-block; float: none;">전체 페이지 리스트</div>
			</div>
			<div class="panel-body">
			<table class="table table-hover" >
				<tr class = "active">
					<td>#</td>
					<td>고유키</td>
					<td>페이지 이름</td>
				</tr>
				<?
				foreach ($pageList as $key => $forder){
				?>
				<tr onclick="viewEventPage('<?=$forder['p_key']?>','<?=$forder['p_name']?>')">
					<td><?=$key+1?></td>
					<td><?=$forder['p_key']?></td>
					<td><?=$forder['p_name']?></td>
				</tr>
				<? } ?>
			</table>
			</div>
		</div>
		<? if($view){ ?>
		<div id="admin-detail-wrap">
			<div id = "admin-detail-name">
					<div class="panel panel-dark">
						<div class="panel-heading" style="text-align: center; color:white;">
							<div class="panel-title" style="display: inline-block; float: none;"><?=$v_name?> - <a href="http://sync2030.org/?f=<?=$view?>" style="color: #B6B6E1;" target="_blank">http://sync2030.org/?f=<?=$view?></a></div>
						</div>
						<div class="panel-body">
							<table class="table table-bordered">
								<tr >
									<td colspan="2" style="text-align: center;" class="active">전체 접속 횟수</td>
								</tr>
								<tr>
									<td>총 접근 수 </td><td><?=$event['cnt']?> (회)</td>
								</tr>
								<tr>
									<td>총 접속 IP 수</td><td><?=$event['user_cnt']?> (명) </td>
								</tr>
								<?if($amodel){ ?>
									<tr >
										<td colspan="2" style="text-align: center;" class="active">기종별 접속 분류</td>
									</tr>
									<?foreach($amodel as $key => $value){?>
									<tr>
										<td><?=$key?></td>
										<td><?=$value?> (회)</td>
									</tr>
								<?} }?>
								<?if($atype){?>
									<tr>
										<td colspan="2"  style="text-align: center;" class="active">PC / 모바일 분류</td>
									</tr>
									<?foreach($atype as $key => $value){?>
									<tr>
										<td><?=$key == 'M' ? '모바일' : 'PC'?></td>
										<td><?=$value?> (회)</td>
									</tr>
								<?} }?>
							</table>
						</div>
						<div  id ='char'></div>
						<div  id ='char2'></div>
						<div  id ='char3'></div>
					</div>
			</div>
		</div>
	</div>
	<? } ?>
</div>