<script src="<?=JS_PATH?>/admin/remodal.js"></script>
<script>
	var p_type = '<?=$moduleContent['p_type']?>';
	
	;(function($){
		var _radiobox =	$("input[name='p_security']");
		var _sec_private_box = $("div[data-on=private]");
		
		_radiobox.change(function(){
			var _radiobox = $(this);
			if(_radiobox.val() == 'private'){
				_sec_private_box.fadeIn();
			}else{
				_sec_private_box.fadeOut();
			}
		});
		
		$("#page-create-form").submit(function(){
			var al = /[a-zA-Z]/;
			var code = /[0-9]{4}/;
			if($("input[name=p_key]").val() == ''){
				alert('고유키를 입력해주세요.');
				return false;
			}
			if(!$("input[name=p_key]").val().match(al)){
				alert('고유키를 영문으로만 작성해 주세요.');
				return false;
			}
			if($("input[name=p_name]").val() == ''){
				alert('이름을 입력해주세요.');
				return false;
			}
			if($("input[name=p_subname]").val() == ''){
				alert('부제목을 입력해주세요.');
				return false;
			}
			if($("input[name=p_content]").val() == ''){
				alert('페이지정보를 입력해주세요.');
				return false;
			}

			if(	$("input[name='p_security']:checked").val() == 'private' ){
				if(!$("input[name=p_securitycode]").val().match(code)){
					alert('4자리 숫자를 입력해주세요.');
					return false;
				}

				if($("input[name=p_securitycode]").val() != $("input[name=p_securitycode_re]").val()){
					alert('동일한 보안코드를 입력해주세요.');
					return false;
				}
			}
			return true;
		});

		$(".edit-module-btn").click(function(){
			var module_content = $(this).parent().parent();
			var tmp_data = module_content.attr("data-security").split('-');
			
			var security_code = tmp_data[0];
			var module_key = tmp_data[1];
			
			if(security_code == 'private'){
				var code = module_content.find('input[name=security]').val();
				if(code == ''){
					alert(code+'보안코드를 입력해주세요.');
					return false;
				}
				$.post('?action=page', {'mode':'private_approval', 'p_idx': module_key,'code' : code }, function(data){
					if(data.msg){ alert(data.msg);}
					if(data.result){  
						location.href = data.url;
					}
				},'json');
			}else{
				location.href = '?f=page&v=mdetail&key='+tmp_data[2];
			}
		});

		$(".data-module-btn").click(function(){
			var module_content = $(this).parent().parent();
			var tmp_data = module_content.attr("data-security").split('-');
			var module_key = tmp_data[2];
			location.href = '?f=page&v=mdata&mkey='+module_key;
		});
	})(jQuery)
	
	
	// 상세보기 [mdetail]
	
	$(".submit").click(function(){
		moduleControl.submit();
	});
	$(".page-type").change(function(){
		var p_type = $(".page-type:checked").val();
			$(".special-content").hide();
			$("#"+p_type).show();
			moduleControl.type = p_type;
	});

	$("select[name=category]").change(function(){
		moduleControl.basicCategoryChange($(this).val());
	});
	
	var moduleControl = {
		type : '<?=$moduleContent['p_type']?>',
		submit : function(){
			var _form = $("form[name=default_module]");
			$.post('?action=page', _form.serialize(), function(data){
				if(data.msg){ alert(data.msg);}
				if(data.result){ window.location.reload(true); }
			},'json');
		},
		basicCategoryChange : function(category){
			if(this.type == 'basic'){
				$.post('?action=page', { 'mode' : 'basic_module_category','category' : category }, function(data){
					console.log(data);
					if(data.result ){
						var selectboxHtml = '';
						for(key in data.categoryDetail) {
							selectboxHtml += "<option value="+key+">"+data.categoryDetail[key]+"</option>";
						}
						$("select[name=category_detail]").html(selectboxHtml);
						
					}else{
						$("select[name=category_detail]").html('');
					}
						
				},'json');
			}
		},
	}
	$(function(){
		$("#"+p_type).show();
	});
</script>