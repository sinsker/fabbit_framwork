<div class="main-content">
	<div class="header" >
		<h2 class="header-t">모듈 데이터 확인</h2>
		<div class="header-t text-right">
			<a class="btn btn-primary btn-icon icon-left"   href="#modal">
				<i class="entypo-plus"></i>
				새로 추가
			</a>
		</div>
	</div>
	<div class="panel panel-dark">
			<div class="panel-heading" style="text-align: center;">
				<div class="panel-title " style="display: inline-block; float: none;">저장되어있는 데이터 확인</div>
			</div>
			<div class="panel-body">
				<table class="table table-hover">
					<tbody>
						<tr class="active">
							<td>#</td>
							<td>name</td>
							<td>email</td>
							<td>tel</td>
							<td>wr1</td>
							<td>wr2</td>
							<td>wr3</td>
							<td>wip</td>
							<!-- <td>wagent</td> -->
							<td>wdate</td>
						</tr>
						<? if($mdataList[0]){ ?>
						<?foreach($mdataList[0] as $key => $value){?>
						<tr class="">
							<td><?=$value['no']?></td>
							<td><?=$value['name']?></td>
							<td><?=$value['email']?></td>
							<td><?=$value['tel']?></td>
							<td><?=$value['wr1']?></td>
							<td><?=$value['wr2']?></td>
							<td><?=$value['wr3']?></td>
							<td><?=$value['wip']?></td>
							<!-- <td><?=$value['wagent']?></td> -->
							<td><?=$value['wdate']?></td>
						</tr>
						<? } ?> 
						<? } ?>
					</tbody>
				</table>
				<? if(empty($mdataList[0])) echo '데이터가 없습니다.'; ?>
			</div>
		</div>
		<div class="text-center">
			<?=$mdataList[1]->getPageBlockHtml("f=page&v=mdata&mkey=$mkey&page=$page")?>
		</div>
</div>


