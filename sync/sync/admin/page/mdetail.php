<div class="main-content">
	<div class="header" >
		<h2 class="header-t">페이지 상세 정보</h2>
		<div class="header-t text-right">
		</div>
	</div>
	<div class="row page-switch-radio-area">
	<!-- 
		페이지 특징
		<div class="radio page-switch-radio-btn">
			<label>
				<input type="radio" class="page-type" name="type" value="basic" <?if($moduleContent['p_type'] == 'basic') echo 'checked=true';?> >기본형
			</label>
		</div>
		<div class="radio page-switch-radio-btn">
			<label>
				<input type="radio" class="page-type" name="type" value="professional" <?if($moduleContent['p_type'] == 'professional') echo 'checked=true';?> >전문가용
			</label>
		</div>
	 -->
	</div>
	<div class="build-wrap">
		<div  class = "build-content" >
			<div class="row">
				<div class="col-md-12">
					<form role="form" class="form-horizontal form-groups-bordered" name="default_module">
						<input type="hidden" name="p_key"  value="<?=$moduleContent['p_key']?>" >
						<input type="hidden" name="mode" value="build" >
						<div class="panel panel-primary" data-collapsed="0">
							<div class="panel-heading">
								<div class="panel-title">
									기본 입력
								</div>
								<div class="panel-options">
									<a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a>
									<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
									<a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
									<a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
								</div>
							</div>
							<div class="panel-body">
								<div class="form-group">
									<label class="col-sm-3 control-label">제목</label>
									<div class="col-sm-5">
										<input type="text" class="form-control" id="p_name" name="p_name" value="<?=$moduleContent['p_name']?>" placeholder="제목을 입력해주세요.">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">부제목</label>
									<div class="col-sm-5">
										<input type="text" class="form-control" id="p_subname" name="p_subname" value="<?=$moduleContent['p_subname']?>"  placeholder="부제목을 입력해주세요.">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">상단 네비게이션</label>
									<div class="col-sm-5">
										<label class="radio-inline">
										  <input type="radio" name="p_top" id="p_top" value="Y" <?if($moduleContent['p_head'] == 'Y') echo 'checked=true';?> > 사용
										</label>
										<label class="radio-inline">
										  <input type="radio" name="p_top" id="p_top" value="N" <?if($moduleContent['p_head'] == 'N') echo 'checked=true';?> > 미사용
										</label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">하단 네비게이션</label>
									<div class="col-sm-5">
										<label class="radio-inline">
										  <input type="radio" name="p_bottom" id="p_bottom" value="Y" <?if($moduleContent['p_footer'] == 'Y') echo 'checked=true';?> > 사용
										</label>
										<label class="radio-inline">
										  <input type="radio" name="p_bottom" id="p_bottom" value="N" <?if($moduleContent['p_footer'] == 'N') echo 'checked=true';?> > 미사용
										</label>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- 
		<div id="basic" class = "build-content special-content" style="display:none;">
			<form role="form" class="form-horizontal form-groups-bordered">
				<input type="hidden" name="p_type" value ="basic">
				<div class="panel panel-dark" data-collapsed="0">
					<div class="panel-heading">
						<div class="panel-title">
							기본형
						</div>
						<div class="panel-options">
							<a href="#module-modal" onclick="jQuery('#module-modal').modal('show', {backdrop: 'static'});" class="bg"><i class="entypo-cog"></i></a>
						</div>
					</div>
					<div class="panel-body">
					<? if(!empty($basicContentList)){
						foreach($basicContentList as $basicContent){
							$path = explode(':',$basicContent['module_key']);
							$basicModuleInputPath =  $_SERVER['DOCUMENT_ROOT'].MODULE_PATH.'/'.$path[0].'/'.$path[0].'.input.php';
							if(is_file($basicModuleInputPath))
								$basicModuleHtml = file_get_contents($basicModuleInputPath);
							echo $basicModuleHtml;
					}}?>
					</div>
				</div>
			</form>
		</div>
		<div id="professional" class = "build-content special-content" style="display:none;">
			<form role="form" class="form-horizontal form-groups-bordered">
				<input type="hidden" name="p_type" value ="professional">
				<div class="panel panel-dark" data-collapsed="0">
					<div class="panel-heading">
						<div class="panel-title">
							전문가용
						</div>
						<div class="panel-options">
						</div>
					</div>
					<div class="panel-body">
						<div class="professional-css-content">
							<div>CSS 영역</div>
							<div><textarea name="professional_css" class="form-control" style="height: 200px;"><?=htmlspecialchars(stripslashes($content['css']))?></textarea></div>
						</div>
						<br>
						<div class="professional-html-content">
							<div>HTML 영역</div>
							<div><textarea name="professional_html" class="form-control" style="height: 500px;"><?=htmlspecialchars(stripslashes($content['html']))?></textarea></div>
						</div>
						<br>
						<div class="professional-js-content">
							<div>JS 영역</div>
							<div><textarea name="professional_js" class="form-control" style="height: 200px;"><?=htmlspecialchars(stripslashes($content['js']))?></textarea></div>
						</div>
					</div>
				</div>
			</form>
		</div>
		-->
	</div>
	
	<div style="text-align: center;">
		<button type="button" class="btn btn-green btn-icon icon-left submit" >
			확인
			<i class="entypo-check"></i>
		</button>
		<button type="button" class="btn btn-default  btn-icon icon-left" onclick="javascript:location.href='?f=page&v=module';">
			리스트
			<i class="entypo-cancel"></i>
		</button>
	</div>
	
	<div class="modal fade" id="module-modal" aria-hidden="true" style="display: none;">
		<div class="modal-dialog" style="  z-index: 10000;">
			<form method="post" action="?action=page" name ="basic-module-form">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h4 class="modal-title">모듈 추가</h4>
					</div>
					<div class="modal-body">
						<input type="hidden" name="mode" value="basic_module_create" >
						<input type="hidden" name="p_key"  value ="<?=$moduleContent['p_key']?>" >
						<div class="col-md-12">
							<div class="form-group">
								<label class=" control-label">분류</label>
								<select class="form-control" name = "category">
									<option value="">분류를 선택해주세요.</option>
									<option value="title">타이틀</option>
									<option value="slide">슬라이드</option>
									<option value="context">내용</option>
								</select>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label class=" control-label">상세분류</label>
								<select class="form-control" name = "category_detail">
									<option value="">분류를 선택해주세요.</option>
								</select>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">취소</button>
						<input type="submit" class="btn btn-info" value="확인">
					</div>
				</div>
			</form>
		</div>
	</div>
	 
</div>
