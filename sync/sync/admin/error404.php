<div class="main-content">
		<div class="page-error-404">
	
	
	<div class="error-symbol">
		<i class="entypo-attention"></i>
	</div>
	
	<div class="error-text">
		<h2>404</h2>
		<p>요청하신 페이지가 없습니다.</p>
	</div>
	
	<hr />
</div>