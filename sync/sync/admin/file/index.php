<div class="main-content"  style="display: none;">
	<div class="header" >
		<h2 class="header-t">이미지 관리</h2>
		<div class="header-t text-right">
				<a class="btn btn-primary btn-icon icon-left"  href="#modal">
						<i class="entypo-plus"></i>이미지 업로드
				</a>
				<a class="btn btn-primary  btn-icon icon-left" >
						<i class="entypo-plus"></i>폴더 생성
				</a>
			<!-- <a href="javascript:;" onclick="Show('modal-team_insert');" class="btn btn-default">팀 생성</a> -->
		</div>
	</div>
	<div class="file-content">
		<? if(!empty($fileList)){
			foreach($fileList as $key=> $file){
				$size = getimagesize($file['f_path'].'/'.$file['f_name']);
				?>
			<div id="card-<?=$key?>" class="card">
			<div class="front">
				<div class="box-wrap">
					<img class="<?=$size[0] > $size[1]? 'full_w' : 'full_h' ?>" src="<?=$file['f_url'].'/'.$file['f_name']?>" ></a>
				</div>
				<div class="file-name">
					<?/* $file['f_realname'] */?>
				</div>
			</div>
			<div class="back">
				<div class="box-wrap" data-key='<?=$file['idx']?>'>
					<a href="#;" class="btn btn-default btn-down">다운</a>&nbsp;
					<a href="#;" class="btn btn-default btn-del">삭제</a>
				</div>
				<div class="file-name">
					<?=$file['f_realname']?>
				</div>
			</div>
		</div>
		<?} }?>
	</div>
	<div class="text-center">
		<?=$paging->getPageBlockHtml("f=file&page=$page&qe=$qe&tx=$tx")?>
	</div>
</div>

<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal-head" aria-describedby="modal1Desc">
  <button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
  <div>
    <div id="modal-head"><h3>파일 업로드</h3></div>
    <p class="text-danger">※ 10MB 가 넘는 파일은 업로드가 제한됩니다.</p>
    <p id="modal1Desc">
     	<div class = 'upload-content' >
     	
			<form method="post" id='pc-upload-form' style="display:none;" enctype="multipart/form-data">
				<div id="queue"></div>
				<input id="pc-upload" name="pc-upload" type="file" multiple="true">
			</form>
			
			<form method="post" action="?action=file" id='mobile-upload-form' target="iframe" style="display:none;" enctype="multipart/form-data">
				<input type="hidden" name="type" value="img">
				<input type="hidden" name="mode" value="MOBILE_UPLOAD">
				<input id="mobile-upload-input" class="multi " name="mobile-upload[]" type="file"  multiple >
				<input type="button"  class="btn btn-primary" onclick="form_submit();" value="올리기">
			</form>
		</div>
		<div>
		</div>
    </p>
  </div>
  <br>
 <!--  <button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
  <button data-remodal-action="confirm" class="remodal-confirm">OK</button> -->
</div>
