<script src="<?=JS_PATH?>/admin/file/jquery.treeview.js" type="text/javascript"></script>
<script type="text/javascript">
	$(function (){
		$("#browser").treeview({
			toggle: function() {
				console.log("%s was toggled.", $(this).find(">span").text());
			}
		});

		//폴더 추가
		$("#folder-add").click(function() {

			if( $("input[name=folder_name]").val() == ''){
				alert('파일이름을 작성해주세요.');
				return false;
			}
			var branches = $("<li><span class='folder'>"+ $("input[name=folder_name]").val() +"</span><ul></ul></li>").appendTo("#browser");

			branches.find(".folder > span").click(function(){
				$(".folder > span").removeClass('on');
				$(this).addClass('on');
			});
			
			$("#browser").treeview({
				add: branches
			});

			$("#folder-add-form").submit();
			
		});

		//파일 삭제
		$(".btn-file-del").click(function(){
			if(confirm("해당 파일을 삭제 하시겠습니까?")){
				var file_key = $(this).parent().attr('data-key');
				$.post("/?action=file", {'idx':file_key, 'mode' : 'DELETE' }, function(data){
					if(data.result){ location.replace(''); }
				},'json');
			}
		});

		//파일 다운
		$(".btn-file-down").click(function(){
			var file_key = $(this).parent().attr('data-key');
			if(file_key == ''){
				alert('정상적인 방법으로 다운로드를 해주시기 바랍니다.');
				return false;
			}
			window.open("/?action=file&mode=DOWNLOAD&idx="+file_key,'iframe');
		});

		//폴더 선택
		$(".folder > span").click(function(){
			if(!$(this).hasClass('on')){
				$(".folder > span").removeClass('on');
				$(this).addClass('on');
			}else{
				$(this).removeClass('on');
			}
		});


		//폴더 이름 수정 인풋창
		$(".btn-folder-edit-input").click(function(){
			$(this).parent().hide();
			$(this).parent().parent().find(".folder-edit").show();
		});

		//폴더 이름 수정 취소
		$(".btn-folder-edit-cancel").click(function(){
			$(this).parent().hide();
			$(this).parent().parent().find(".folder").show();
		});

		//폴더 이름 수정
		$(".btn-folder-edit-ok").click(function(){
			var new_name = $(this).parent().find("input[name=folder]").val();
			var old_name = $(this).parent().find("input[name=old_folder]").val();
			
			window.open("/?action=file&type=file&mode=FOLDER_RENAME&folder="+new_name+"&old_folder="+old_name,'iframe');
			
		});

		//폴더 삭제
		$(".btn-folder-del").click(function(){
			if(confirm("해당 폴더를 삭제하시겠습니까?")){
				var folder = $(this).parent().find("input[name=folder]").val();
				window.open("/?action=file&type=file&mode=FOLDER_DELETE&folder="+folder,'iframe');
			}
		});
	});
</script>