<script src='//cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js'></script>
<script src="<?=JS_PATH?>/jquery.form.js" type="text/javascript"></script>
<script src="<?=JS_PATH?>/jquery.MetaData.js" type="text/javascript"></script>
<script src="<?=JS_PATH?>/jQuery.MultiFile.min.js" type="text/javascript"></script>
<script src="<?=JS_PATH?>/jquery.uploadify.min.js" type="text/javascript"></script>
<script src="<?=JS_PATH?>/admin/remodal.js" type="text/javascript"></script>
<script src="<?=JS_PATH?>/flip.js"></script>
<script>

$(".card").flip({
	trigger: "click"
});

$(window).load(function(){
	$(".main-content").fadeIn();
});

//업로드 기능 mobile 과 pc 나뉨
(function() {
	_mobileForm = $("#mobile-upload-form");
	_mobileUploadFile = $("input#mobile-upload-input");
	_mobileUploadButton = $("#mobile-upload-btn");
	_mobileForm.show();
})();


function form_submit(){
	if($(".folder > span.on").length > 0){
		var folder = $("span.on").text();
		var folder_input = $("<input type='hidden' name='folder' value='"+folder+"'>"); 
		$("#mobile-upload-form").append(folder_input);
	}

	return false;
	/* $("#mobile-upload-form").submit(); */
	/* $(".remodal-is-opened > div").html('<h3>업로드중...</h3>'); */
}

$(function (){
	$(".btn-del").click(function(){
		if(confirm("해당 파일을 삭제 하시겠습니까?")){
			var file_key = $(this).parent().attr('data-key');
			$.post("/?action=file", {'idx':file_key, 'mode' : 'DELETE' }, function(data){
				if(data.result){ location.replace(''); }
			},'json');
		}
	});

	$(".btn-down").click(function(){
		var file_key = $(this).parent().attr('data-key');
		if(file_key == ''){
			alert('정상적인 방법으로 다운로드를 해주시기 바랍니다.');
			return false;
		}
		window.open("/?action=file&mode=DOWNLOAD&idx="+file_key,'iframe');
	});
});

</script>