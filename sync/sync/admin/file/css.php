<link href="<?=CSS_PATH?>/uploadify.css" type="text/css" rel="stylesheet">
<link href="<?=CSS_PATH?>/admin/remodal.css" type="text/css" rel="stylesheet">
<link href="<?=CSS_PATH?>/admin/remodal-default-theme.css" type="text/css" rel="stylesheet">
<style>
#modal-head > h3{ margin-bottom: 30px; color: #000000; font-weight: bold;}
.card, .box-wrap {
	margin: 0 auto;
	width: 200px;
	height: 200px;
	margin: 5px;
	display: inline-block;
}

.card a.thumb {
	float: left;
	width: 100px;
	height: 60px;
	margin: 0 5px 10px 0;
}

.card a.thumb img {
	max-width: 100%;
	max-height: 100%;
}

.front, .back {
    width: 100%;
    padding: 1px;
    background-color: white;
    border-radius: 0px;
    box-shadow: 0px 1px 6px #000000;
    display: table;
}

.box-wrap{
	display: table-cell;
	vertical-align: middle;
	text-align: center;
}

.front {
	background-color: white;
}

.back {
	background-color: #303641;
}

.MultiFile-label{
}
.custom-file-input::-webkit-file-upload-button {
  visibility: hidden;
}
.custom-file-input::before {
  content: '업로드 할 파일을 선택해 주세요.';
    display: inline-block;
    padding: 5px;
    background: -webkit-linear-gradient(top, #f9f9f9, #e3e3e3);
    border: 1px solid #999;
    border-radius: 3px;
    padding: 10px 8px;
    outline: none;
    white-space: nowrap;
    -webkit-user-select: none;
    cursor: pointer;
    text-shadow: 1px 1px #fff;
    font-weight: 700;
    font-size: 10pt;
}
}
.custom-file-input:hover::before {
  border-color: black;
}
.custom-file-input:active::before {
  background: -webkit-linear-gradient(top, #e3e3e3, #f9f9f9);
}

.full_w{
	width: 100%;
	height: auto;
	padding: 8px;
}

.full_h{
	width: auto;
	height: 100%;
	padding: 8px;
}

.file-name{
    position: absolute;
    left: 0;
    bottom: 0;
    padding: 5px;
    color: #3B3936;
}


@media only screen and (max-width: 768px) {
	.file-content{
		/* text-align: center; */
	}
	
	.card, .box-wrap{
		width: 140px;
		height: 140px;
	}
	
	.full_h{
		min-height: 100px;
	}
	
}

</style>