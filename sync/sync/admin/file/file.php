<div class="main-content"  style="display: none;">
	<div class="header" >
		<h2 class="header-t">파일 관리</h2>
		<div class="header-t text-right">
				<a class="btn btn-primary btn-icon icon-left"  href="#modal">
						<i class="entypo-plus"></i>파일 업로드
				</a>
				<a class="btn btn-primary btn-icon icon-left"  href="#c-folder">
						<i class="entypo-plus"></i>폴더 추가
				</a>
			<!-- <a href="javascript:;" onclick="Show('modal-team_insert');" class="btn btn-default">팀 생성</a> -->
		</div>
	</div>
	<div class="file-content">
		<ul class="filetree treeview-famfamfam treeview">
			<li><span class="folder ">파일 리스트</span>
				<ul id="browser" >
				<? if(!empty($fileList)){?>
					<?foreach($fileList as $key=> $file){  ?>
						<?if(is_array($file)){?>
						<li>
							<span class="folder" >
								<span><?=$key?></span>
								<input type="hidden" value = "<?=$key?>" name="folder" >
								<a href="#;"class="btn-folder-edit-input"><i class="entypo-pencil" ></i></a> <!-- 수정 -->
								<a href="#;"class="btn-folder-del"><i class="entypo-cancel-circled" ></i></a> <!-- 삭제 -->
							</span>
							<span class="folder-edit" style="display:none;">
								<input type="text" value = "<?=$key?>" name="folder" >
								<input type="hidden" value = "<?=$key?>" name="old_folder" >
								<a href="#;"class="btn-folder-edit-ok"><i class="entypo-check" ></i></a> <!-- 삭제 -->
								<a href="#;"class="btn-folder-edit-cancel"><i class="entypo-cancel-circled" ></i></a> <!-- 삭제 -->
							</span>
							<ul>
								<? if(!empty($file[0])){
									foreach($file[0] as $subkey=> $subfile){  ?>
									<?if(!empty($subfile)){?>
										<li>
											<?$dbfile = $fileManager->getFileData("and f_type='file' and f_name = '$subfile' "); ?>
											<span class="file" data-key="<?=$dbfile['idx']?>">
												<?=$dbfile['f_realname']?>
												<a href="#;" class="btn-file-down"><i class="entypo-download "></i></a> <!-- 다운 -->
												<a href="#;" class="btn-file-del"><i class="entypo-cancel-circled" ></i></a> <!-- 삭제 -->
											</span>
										</li>
									<?}?>
								<?}}?>
							</ul>
						</li>
						<?}else{ $dbfile = $fileManager->getFileData("and f_type='file' and f_name = '$file' ");  ?>
						<li>
							<span class="file" data-key="<?=$dbfile['idx']?>">
								<?=$dbfile['f_realname']?>
								<a href="#;" class="btn-file-down"><i class="entypo-download"></i></a> <!-- 다운 -->
								<a href="#;" class="btn-file-del"><i class="entypo-cancel-circled" ></i></a> <!-- 삭제 -->
							</span>
						</li>
						<?}?>
				<?} }?>	
				</ul>
			</li>
		</ul>
	</div>		
</div>

<!-- 업로드 모달 -->
<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal-head" aria-describedby="modal1Desc">
  <button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
  <div>
    <div id="modal-head"><h3>파일 업로드</h3></div>
    <p class="text-danger">※ 10MB 가 넘는 파일은 업로드가 제한됩니다.</p>
    <p id="modal1Desc">
     	<div class = 'upload-content' >
     	
			<form method="post" id='pc-upload-form' style="display:none;" enctype="multipart/form-data">
				<div id="queue"></div>
				<input id="pc-upload" name="pc-upload" type="file" multiple="true">
			</form>
			
			<form method="post" action="?action=file" id='mobile-upload-form' onsubmit="form_submit();" target="iframe" style="display:none;" enctype="multipart/form-data" >
				<input type="hidden" name="type" value="file">
				<input type="hidden" name="mode" value="MOBILE_UPLOAD">
				<input id="mobile-upload-input" class="multi " name="mobile-upload[]" type="file"  multiple >
				<br><br>
				<input type="submit"  class="remodal-confirm" onclick="" value="올리기">
			</form>
			<div>
			</div>
		</div>
		<div>
		</div>
    </p>
  </div>
  <br>
</div>

<!--  폴더 추가 모달 -->
<div class="remodal" data-remodal-id="c-folder" role="dialog" aria-labelledby="modal-head" aria-describedby="modal1Desc">
	<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
	<form method="post" action="?action=file"  target="iframe" id="folder-add-form">
		<div id="modal-head"><h3>폴더 생성</h3></div>
		<input type="hidden" name="type" value="file">
		<input type="hidden" name="mode" value="ADD_FOLDER">
		<span class="col-xs-3">폴더 이름</span>
		<div class="col-xs-5">
			<input type="text" name="folder_name" class=" col-xs-5 form-control">
		</div>
	</form>
	<div class="col-xs-4">
		<button data-remodal-action="confirm" class="col-xs-4 form-control btn btn-primary" id="folder-add">추가하기</button>
	</div>
</div>