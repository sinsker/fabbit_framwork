<style>
	.note-delete{
		    position: absolute;
		    right: 30px;
		    top: 30px;
		    border: none;
		    background: none;
		    outline: none;
		    -webkit-opacity: 0;
		    -moz-opacity: 0;
		    opacity: 1;
		    filter: alpha(opacity=0);
		    -moz-transition: all 300ms ease-in-out;
		    -o-transition: all 300ms ease-in-out;
		    -webkit-transition: all 300ms ease-in-out;
		    transition: all 300ms ease-in-out;
	}
	.notes-env .notes-list .list-of-notes li {
	    position: relative;
	    padding: 10px;
	    padding-bottom: 0;
	}
	
	.notes-list > div {
		float: right;width: 26%;			
	}
	
	@media (max-width: 992px){
		.notes-list > div {
			float: right;width: 35%;			
		}
	}
	
	@media (max-width: 768px){
		.notes-list > div {
			float: none;
			width: 100%;
		}
	}
	
	.share-box{
		font-size: 13px;
	    background: #F0F0F1;
	    padding: 8px;
	    position: absolute;
	    border: 1px solid #000000;
	    z-index: 1;
	    margin-left: 82px;
	    display: block;
	}
	
	.share-menu{
		cursor: pointer;
	}
	
	.share-menu:HOVER {
		color :red;
	}
}
</style>
