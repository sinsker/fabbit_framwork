<div class="main-content">
		<div class="notes-env">
			<div class="notes-header">
				<h2>
					회의록 
					<a href="#;" class="btn btn-default btn-icon icon-left" id="share-btn">
						<i class="entypo-share"></i>
						공유
					</a>
					<span class="share-box" id="share-box">
						<span class="kakao share-menu">카카오톡</span>
						<span class="url share-menu">URL 복사</span>
					</span>
				</h2>
				
				<div class="right">
					<a class="btn btn-default icon-left" id="add-note">
						<!-- <i class=""></i> -->
						페이지 추가
					</a>
					<a class="btn btn-primary btn-icon icon-left" id="save-note">
						<i class="entypo-pencil"></i>
						저장
					</a>
				</div>
			</div>
			
			<div class="notes-list">
				<div style="">
					<ul class="list-of-notes" style="width:100%;float: none;">
						<?
						if(!empty($noteList)){
							foreach($noteList as $note ){?>
							<li class="" data-idx="<?=$note['idx']?>" >
								<a href="#">
									<div style='text-align: center;'><?=$note['n_date']?> [<?=$note['n_write_name']?>]</div><br>
									<strong><?=$note['n_memo_min']?></strong>
								</a>
								<button class="note-delete" >&times;</button>
								<div class="content"><?=$note['n_memo']?></div>
							</li>
						<?}}else{?>
							<li class="current no-notes">
								노트가 없습니다.
							</li>
						<?}?>
					</ul>
					<div style="text-align: center;">
						<?=$pageing->getPageBlockHtml("f=notes&page=$page")?>
					</div>
				</div>
				<div class="write-pad">
					<textarea class="form-control autogrow" name="n_memo" style="font-size: 13px; line-height: 18px;   color: black;">
					</textarea>
				</div>
			</div>
		</div>
</div>