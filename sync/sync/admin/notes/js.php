<script src="<?=JS_PATH?>/admin/neon-notes.js"></script>
<script src="https://developers.kakao.com/sdk/js/kakao.min.js"></script>

<!--[if lt IE 9]><script src="<?=JS_PATH?>/admin/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
	
	
<script type="text/javascript">

Kakao.init('f008023c5c337c908c1dd0fdb7e030c0');

$(function(){
	toggleView("share-box");
	<?if($_GET['nid']){?> 
		$("li[data-idx=<?=$_GET['nid']?>] > a").trigger('click');
	<?}?>
	
	$("#save-note").click(function(){
		var idx = '';
		var n_memo = $("textarea[name=n_memo]").val();
			 $("ul.list-of-notes > li").each(function(){
				if($(this).attr('class') == 'current'){
					idx = $(this).attr('data-idx');
				}
			}); 
			$.post("/?action=note", {'idx':idx,'n_memo': n_memo, 'cmd':'insert_note' } , function(data){
				alert(data.msg);
				if(data.result){
					location.href= '?f=notes';
				}
			}, 'json');
			
		
	});
	
	$(".note-delete").click(function(){
		var nparent = $(this).parent();
		if (confirm('해당 노트를 삭제 하시겠습니까')){
			$.post("/?action=note", {'idx':$(this).parent().attr('data-idx'), 'cmd':'delete_note' } , function(data){
				if(data.result){
					nparent.hide();
				}
			}, 'json');
			
		}
	});

});

function share_kakao(title, sub_title, url, image) { //http://sync2030.org/sync/resources/images/note0.jpg
    Kakao.Link.sendTalkLink({
        label: title,
        webLink: {
            text: sub_title,
            url: url
        },
        image: { // 80 * 80
            src: image,
             width: '100',
            height: '100' 
        },
        webButton  :{
        	url : url,
        }
    });
}

function share_facebook(url){
	var sns_popup = window.open('http://www.facebook.com/sharer.php?u='+encodeURIComponent(url)+'&t='+encodeURIComponent(url),'facebook_share_dialog','toolbar=0,status=0,width=550,height=436');
	if (sns_popup.focus) {
		sns.popup.focus();
	}
}

function share_url(url){
    if (!window.clipboardData) { 
          prompt("Ctrl+C를 눌러 클립보드로 복사하세요.", url);
     }else{
          if(confirm("링크 주소를 클립보드에 복사하시겠습니까?")){
               window.clipboardData.setData("Text", url);
               alert("링크가 복사되었습니다.");
          }
     }
}

$("#share-btn").click(function(){
	toggleView("share-box");
});

$(".kakao").click(function(){

	var idx = '';
	var sub_title ='';
	if(isMobile == 'Y'){
		$("ul.list-of-notes > li").each(function(){
			if($(this).attr('class') == 'current'){
				idx = $(this).attr('data-idx');
				sub_title = $(this).children().children().text();
			}
		}); 
		$.post("/?action=note", {'idx':idx, 'cmd':'share_url' } , function(data){
			share_kakao('SYNC 노트 공유',sub_title.substring(0,10)+' 작성', data.result,'http://sync2030.org/sync/resources/images/note0.jpg');
		}, 'json');
	}else{
		alert('카카오톡 공유는 모바일에서만 가능합니다.');
	}
	
});

$(".url").click(function(){
	var idx = '';

	$("ul.list-of-notes > li").each(function(){
		if($(this).attr('class') == 'current'){
			idx = $(this).attr('data-idx');
		}
	}); 
	$.post("/?action=note", {'idx':idx, 'cmd':'share_url' } , function(data){
		share_url(data.result);
	}, 'json');
});

$(".autogrow").change(function(){

	alert('s');
});


</script>