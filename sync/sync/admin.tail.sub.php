<?if (!defined('SIN')) exit;?>
<?if (!defined('IS_ADMIN')) exit;?>

<form name='shellform' id='shellform' width = 0 hight = 0 style='display: none'>
	<input tyle="hidden" name="action" value = "<?=$pg['f']?>">
</form>

<script type="text/javascript"  src="<?=JS_PATH?>/jquery-1.11.3.min.js"></script>
<script type="text/javascript"  src="<?=JS_PATH?>/jquery-ui.min.js"></script>
<script type="text/javascript"  src="<?=JS_PATH?>/bootstrap.min.js"></script>
<script type="text/javascript"  src="<?=JS_PATH?>/admin/gsap/main-gsap.js"></script>
<script type="text/javascript"  src="<?=JS_PATH?>/admin/joinable.js"></script>
<script type="text/javascript"  src="<?=JS_PATH?>/admin/resizeable.js"></script>
<script type="text/javascript"  src="<?=JS_PATH?>/admin/neon-api.js"></script>
<script type="text/javascript"  src="<?=JS_PATH?>/admin/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script type="text/javascript"  src="<?=JS_PATH?>/admin/jvectormap/jquery-jvectormap-europe-merc-en.js"></script>
<script type="text/javascript"  src="<?=JS_PATH?>/admin/jquery.sparkline.min.js"></script>
<script type="text/javascript"  src="<?=JS_PATH?>/admin/rickshaw/vendor/d3.v3.js"></script>
<script type="text/javascript"  src="<?=JS_PATH?>/admin/rickshaw/rickshaw.min.js"></script>
<script type="text/javascript"  src="<?=JS_PATH?>/admin/raphael-min.js"></script>
<script type="text/javascript"  src="<?=JS_PATH?>/admin/morris.min.js"></script>
<script type="text/javascript"  src="<?=JS_PATH?>/admin/toastr.js"></script>
<script type="text/javascript"  src="<?=JS_PATH?>/admin/jquery.validate.min.js"></script>
<script type="text/javascript"  src="<?=JS_PATH?>/admin//fileinput.js"></script>
<script type="text/javascript"  src="<?=JS_PATH?>/admin/neon-login.js"></script>
<script type="text/javascript"  src="<?=JS_PATH?>/admin/neon-custom.js"></script>
<script type="text/javascript"  src="<?=JS_PATH?>/admin/neon-demo.js"></script>
<script>
/* (function($){
	 $(function(){
		_window = $(window);
		if($(".main-content").length > 0 && _window.width() < 768 ){ //모바일 사이즈
			_content = $(".main-content"); //메인 
			_windowHeight =_window.height();
			_navHeight = $(".sidebar-menu").length > 0 ? $(".sidebar-menu").height() : 0;  //상단 푸터 높이값

			_window.resize(resizeHight( _content, _windowHeight - _navHeight) );
			_window.load(resizeHight( 	_content, _windowHeight - _navHeight) );
		}
	});
	function resizeHight(content, height){
		content.css('min-height', height+'px');
	}; 
})(jQuery); */
function toggleView(id){
	$("#"+id).toggle();
}
</script>

<?
@include_once $_SERVER['DOCUMENT_ROOT'].CONTENT.'/'.$pg['f'].'/js.php';
@include_once $_SERVER['DOCUMENT_ROOT'].CONTENT.'/'.$pg['f'].'/'.$pg['v'].'.js.php';
?>
