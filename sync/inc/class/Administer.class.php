<?
Class Administer {
	
	private $DBObject; #DB 클래스
	
	private $adminTable = 'sync_admin';
	private $adminLogTable = 'sync_admin_log';
	
	public $session_id = 'auth_id';
	public $session_name = 'auth_name';
	public $session_group = 'auth_group';
	public $session_login_date = 'auth_date';
	public $session_use = 'is_use';
	
	
	public function __construct($dbObject = ''){
		#DB 주입
		if(empty($this->DBObject)){ $this->DBObject = $dbObject;}
		
	}
	
	#관리자 페이지 로그인,회원가입 로그 남기기
	public function adminLog($al_type, $al_uid,  $al_result, $_memo){
		$cDate = TIME_FULL;
		$ip = ACCESS_IP;
		$agent = ACCESS_AGENT;
		$sql = "insert into $this->adminLogTable set al_type ='$al_type', al_uid = '$al_uid', al_result = '$al_result', al_memo ='$_memo', al_ip = '{$ip}', al_agent = '{$agent}', wdate ='$cDate'";
		
		$result = $this->DBObject->query($sql);
		return $result;
	}
	
	#관리자 페이지 행동 저장 ( 해당 페이지,수정자,안내문구 )
	public function adminActionLog($al_type, $al_uid,  $al_result, $_memo){
		$cDate = TIME_FULL;
		$ip = ACCESS_IP;
		$agent = ACCESS_AGENT;
		$sql = "insert into $this->adminLogTable set al_type ='$al_type', al_uid = '$al_uid', al_result = '$al_result', al_memo ='$_memo', al_ip = '{$ip}', al_agent = '{$agent}', wdate ='$cDate'";
		$result = $this->DBObject->query($sql);
		return $result;
	}
	
	#관리자 회원가입
	public function adminJoin($id, $name, $pw, $group, $group_name){
		$current_date = date('Y-m-d H:i:s');
		$sql = "insert into $this->adminTable set a_id ='$id',a_name ='$name', a_password ='$pw',a_group ='$group', a_group_name ='$group_name', a_reg_date = '$current_date', a_last_date = '$current_date' ";
		$result = $this->DBObject->query($sql);
		if($result){
		}else{
			
		}
		return $result;
	}
	
	#관리자 로그인
	public function adminLogin($id, $pw, $auto = false){
		
		if($auto){
			$where = "a_id ='$id'";
		}else{
			$where = "a_id ='$id' and a_password='$pw'";
		}
		
		$sql = "select * from $this->adminTable where $where";
		$result = $this->DBObject->query($sql);
		if($result){
			$auth = $result->fetch_array();
			#회원 업데이트 구문 추가
			$sql = "update $this->adminTable set a_last_date = '".date('Y-m-d H:i:s')."' where a_id = '{$id}'";
			$this->DBObject->query($sql);
		}
		return $auth;
	}
	
	#관리자 로그아웃
	public function adminLogout(){
		$_SESSION[$this->session_id] = '';
		$_SESSION[$this->session_name] = '';
		$_SESSION[$this->session_group] = '';
		$_SESSION[$this->session_login_date] = '';
	}
	
	#관리자 정보 가져오기
	public function adminGetInfo($id){
		$sql = "select * from $this->adminTable where a_id ='$id'";
		$result = $this->DBObject->query($sql);
		if($result){
			$auth = $result->fetch_array();
		}
		return $auth;
	}
	
	#관리자 사용허가/사용금지
	public function adminSwitchUse($idx, $use){
		if(!$idx){ echo '"adminGetInfo" ERROR IS NOT IDX'; exit; }
		$sql = "update $this->adminTable set is_use = '$use' where idx = $idx";
		$result = $this->DBObject->query($sql);
		return $result;
	}
	
	#관리자 삭제
	public function adminDelete($idx){
		if(!$idx){ echo '"adminGetInfo" ERROR IS NOT IDX'; exit; }
		$sql = "delete from $this->adminTable where idx = $idx";
		$result = $this->DBObject->query($sql);
		return $result;
	}
	
	#관리자 전체 리스트
	public function administerList($where = '', $page_set = 10, $block_set = 5){
		global $page;
		
		if(!$where) $where = 1; 
		if(!$page) $page = 1;
		
		$sql = "select count(*) as cnt from $this->adminTable where $where";
		$result = $this->DBObject->query($sql);
		$row = $result->fetch_array();
		$total = $row['cnt'];
		
		$paging = new Paging($page, $total,$page_set,$block_set );
		$limit_idx = $paging->limit; // limit시작위치
		$limit = " LIMIT $limit_idx, $page_set";
		
		$sql = "select * from $this->adminTable where $where $limit";
		$result = $this->DBObject->query($sql);
		
		$no = $total;
		$i = 0;
		$adminList = array();
		if($result){
			while($row = $result->fetch_array()){
				$adminList[$i]['no'] =  $no--;
				$adminList[$i] = $row;
				$i ++;
			}
		}
		
		
		return array($adminList, $paging);
	}
	
	#관리자 로그 리스트
	public function adminLogList($select = '*', $where = '', $page_set = 10, $block_set = 5){
		global $page;
		
		if(!$where) $where = 1;
		if (!$page) $page = 1; // 현재페이지(넘어온값)
		
		$sql = "select count(*) as cnt from $this->adminLogTable where $where";
		
		$result = $this->DBObject->query($sql);
		$row = $result->fetch_array();
		$total = $row['cnt'];
		
		
		$paging = new Paging($page, $total,$page_set,$block_set );
		$limit_idx = $paging->limit; // limit시작위치
		$limit = " LIMIT $limit_idx, $page_set";
		
		
		$sql =  "select $select from $this->adminLogTable where $where $limit ";
		
		$result = $this->DBObject->query($sql);
		
		$no = $total;
		$i = 0;
		$logList = array();
		if($result){
			while($row = $result->fetch_array()){
				$logList[$i]['no'] =  $no--;
				$logList[$i] = $row;
				$i ++;
			}
		}
		return array($logList, $paging);
	}
	
	
	#관리자 세션 
	public function setSession($id, $name, $group, $use){
		$_SESSION[$this->session_id] = $id;
		$_SESSION[$this->session_name] = $name;
		$_SESSION[$this->session_group] = $group;
		$_SESSION[$this->session_use] = $use;
		$_SESSION[$this->session_login_date] = date('Y-m-d H:i:s');
	}
	
	
}

?>