<?
Class UserAuth implements Singleton  { //접근 권한
	
	private $memberInfo = array();
	private $browserInfo = array();
	private $DBObject; #DB 클래스
	
	private static $instance = null;
	
	public $session_id = 'auth_id';
	public $session_name = 'auth_name';
	public $session_group = 'auth_group';
	public $session_login_date = 'auth_date';
	
	private function __construct($dbObject = ''){
		#DB 주입
		if(empty($this->DBObject)){ $this->DBObject = $dbObject;}
		$this->browserInfo = $this->setBrowser();
		
	}
	
	//인스턴스 생성
	public static function getInstance(){
		if(UserAuth::$instance === null){
			UserAuth::$instance = new UserAuth(DB_Class::getInstance());
		}
		return UserAuth::$instance;
	}
	
	
	#DB 로그남기기 [pg =페이지이름/al_page=페이지주소]
	public function userLog($pg, $al_page){
		$type = $this->browserInfo['isMobile'] == 'Y' ? 'M' : 'PC';
		
		if($this->browserInfo['isBot'] == 'Y'){ //봇일경우 튕겨낸다.
			return;
		}
		
		$sql = "insert into access_log set al_info ='{$_SERVER['HTTP_USER_AGENT']}', al_key = '{$pg}', al_page='$al_page', al_type = '$type', al_model = '{$this->browserInfo['model']}', al_wdate = now(), al_ip = '{$_SERVER['REMOTE_ADDR']}'";
		$result = $this->DBObject->query($sql);
	}
	
	#브라우저 체크
	public function setBrowser(){
		return agent_info($_SERVER['HTTP_USER_AGENT']);
	}
	
	public function getBrowser(){
		return $this->browserInfo;
	}
	
	public function getUesr(){
		return $this->memberInfo;
	}
	
}

?>