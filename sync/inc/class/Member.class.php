<?
Class Member {
	
	private $memberInfo = array();
	private $DBObject; #DB 클래스
	
	private $memberTable = 'sync_member';
	private $memberCommentTable = 'sync_member_comment';
	private $teamTable = 'sync_team';
	private $teamMemberTable = 'sync_team_member';
	private $groupTable = 'sync_group';
	private $groupMemberTable = 'sync_group_member';
	
	public function __construct($dbObject = ''){
		#DB 주입
		if(empty($this->DBObject)){ $this->DBObject = $dbObject;}
	}
	
	#회원 전체 개수
	public function getMembersCount($where = ''){
		$sql = "select count(*) as cnt from sync_member where 1 {$where}";
		$result = $this->DBObject->query($sql);
		$rows = $result->fetch_array(MYSQLI_ASSOC);
		return $rows['cnt'];
	}
	
	#회원 리스트 가져오기
	public function getMemberList($page = 1,$where = '',$page_set = 10){
		$sql = "select * from $this->memberTable where 1 {$where}";
		$result = $this->DBObject->query($sql);
		$total = $result->num_rows;#총개수
		
		// 페이지 설정
		$block_set = 5; // 한페이지 블럭수
		if (!$page) $page = 1; // 현재페이지(넘어온값)
		
		$paging = new Paging($page,$total,$page_set,$block_set );
		$limit_idx = $paging->limit; // limit시작위치
		$limit = " LIMIT $limit_idx, $page_set";
		
		$result = $this->DBObject->query($sql.$limit);
		$no = $paging->startNo;
		if($result){
			$i = 0;
			while($rows = $result->fetch_array(MYSQLI_ASSOC)){
				$list[$i] = $rows;
				$list[$i]['no'] = $no--;
				$i = $i+1;
			}
		}
		
		return array($list,$paging);
	}
	
	#회원 모든 정보 획득 리스트
	public function getMemberAllInfoList($where = 1){
		if(!$where) $where = 1;
		$sql ="	select 
				* , a.idx member_idx
				from 
				 sync_member a 
				left join sync_group_member b on a.idx = b.gm_member_idx 
				left join sync_group c on b.group_idx = c.idx 
				left join sync_team_member tm on a.idx = tm.tm_member_idx 
				left join sync_team t on tm.team_idx = t.idx where  {$where}";
		$result = $this->DBObject->query($sql);
		if($result){
			$i = 0;
			while($rows = $result->fetch_array(MYSQLI_ASSOC)){
				$list[$i] = $rows;
				$i = $i+1;
			}
		}
		return $list;
		
	}
	
	#회원 이미지 추가
	public function setMemberImage($filePath, $idx){
		$fileName=get_file_name($filePath);
		$sql = "update $this->memberTable set m_image_path = '$filePath', m_image_name ='{$fileName}'  where idx = $idx";
		$result = $this->DBObject->query($sql);
		if($result){
		}else{
			echo 'setMemberImage 실패';
			exit;
		}
		return $resut;
	}
	#회원 정보 추가하기
	public function setMemberData($name, $group, $tel, $giso ,$birthday , $sex, $join_date = '',$m_memo = ''){
		global $member_group;
		
		$groupName = $member_group[$group];
		$current_date = date('Y-m-d H:i:s');
		$sql = "insert into $this->memberTable set m_name ='$name',m_group ='$group', m_group_name ='$groupName', m_tel='$tel',m_sex ='$sex',m_giso = '$giso', m_birthday = '$birthday' ,m_join_date='$join_date', m_memo = '$m_memo', m_insert_date = '$current_date' ";
		$result = $this->DBObject->query($sql);
		if($result){
			$result = $this->DBObject->getInsertId();
		}else{
			echo 'setMemberData 실패';
			exit;
		}
		return $result;
	}
	
	#회원 정보 추가하기
	public function insertMemberData($values){
		
		$sql = "insert into $this->memberTable set $values";
		$result = $this->DBObject->query($sql);
		if($result){
			$result = $this->DBObject->getInsertId();
		}else{
			echo 'setMemberData 실패';
			exit;
		}
		return $result;
	}
	
	#회원 여러개 삭제 하기 
	public function deleteMembersData($values){
	
		$sql = "delete from $this->memberTable where idx in( $values )";
		$result = $this->DBObject->query($sql);
		if($result){
		}else{
			echo 'deleteMembersData 실패';
			exit;
		}
		return $result;
	}
	
	#회원 정보 업데이트
	public function updateMemberData($name, $group, $tel, $giso ,$birthday , $sex, $join_date, $m_memo, $idx){
		global $member_group;
	
		$groupName = $member_group[$group];
		$sql = "update $this->memberTable set m_name ='$name',m_group ='$group', m_group_name ='$groupName', m_tel='$tel',m_sex ='$sex',m_giso = '$giso', m_birthday='$birthday',m_join_date='$join_date', m_memo = '$m_memo' where idx = $idx";
		$result = $this->DBObject->query($sql);
		if($result){
		}else{
			echo 'updateMemberData 실패';
			exit;
		}
		return $result;
	}
	
	#한명의 회원 정보 가져오기
	public function getMemberData($where){
		$sql = "select * from $this->memberTable where 1 $where";
		$result = $this->DBObject->query($sql);
		if($result){
			$auth = $result->fetch_array(MYSQLI_ASSOC) ;
		}
		return $auth;
	}
	
	#회원 정보 가공하기
	public function modifyMemberData($member){
		if($member['m_join_date'] == '0000-00-00'){ 
			$member['m_join_date'] = '-';
		}
		
		$member['m_birthday'] =$member['m_birthday'] ? ( substr($member['m_birthday'],0,2).'월 '.substr($member['m_birthday'],2,2).'일') : '-';
		
		$member['m_giso'] = $member['m_giso'] ? $member['m_giso'].' 기' : '-';
		
		if($member['m_sex'] != ''){
			$member['m_sex'] = $member['m_sex'] == 1 ? '남자' : '여자' ;
			
		}else{
			$member['m_sex'] = '-';
		}
		if(empty($member['m_tel'])){ $member['m_tel'] = '-'; }
		else{
			if(strlen($member['m_tel'])== 11 ){
				$member['m_tel'] = substr($member['m_tel'],0,3).'-'. substr($member['m_tel'],3,4).'-'. substr($member['m_tel'],7,4);
			}else if(strlen($member['m_tel'])== 10 ){
				$member['m_tel'] = substr($member['m_tel'],0,3).'-'. substr($member['m_tel'],3,3).'-'. substr($member['m_tel'],6,4);
			}
		}
		
		$modifyMember = $member;
		return $modifyMember;
	}
	
	#해당 회원의 코멘트 리스트 가져오기
	public function getMemberCommentList($midx){
		global $member_group;
	
		$sql = "select * from $this->memberCommentTable where member_idx =$midx";
		$result = $this->DBObject->query($sql);
		if($result){
			while($rows = $result->fetch_array(MYSQLI_ASSOC)){
				$list[] = $this->modifyMemberData($rows);
			}
		}else{
			echo 'getMemberCommentList 실패';
			exit;
		}
		return $list;
	}
	
	#회원 코멘트 추가하기
	public function setMemberCommentData($type, $midx, $memo, $mc_write_id, $mc_write_name){
		global $member_group;
	
		$current_date = date('Y-m-d H:i:s');
		$sql = "insert into $this->memberCommentTable set member_idx ='$midx',mc_type ='$type', mc_memo ='$memo', mc_write_id='$mc_write_id',mc_write_name ='$mc_write_name', mc_insert_date='$current_date'";
		$result = $this->DBObject->query($sql);
		if($result){
			$result = $this->DBObject->getInsertId();
		}else{
			echo 'setMemberCommentData 실패';
			exit;
		}
		return $result;
	}
	
	#회원 코멘트 업데이트
	public function updateMemberCommentData($idx, $midx, $memo){
		global $member_group;
	
		$current_date = date('Y-m-d H:i:s');
		$sql = "update $this->memberCommentTable set mc_memo ='$memo',mc_modify_date='$current_date' where member_idx = $midx and idx = $idx";
		$result = $this->DBObject->query($sql);
		if($result){
		}else{
			echo 'updateMemberCommentData 실패';
			exit;
		}
		return $result;
	}
	
	#회원 코멘트 삭제
	public function deleteMemberCommentData($idx){
		global $member_group;
	
		$current_date = date('Y-m-d H:i:s');
		$sql = "delete from $this->memberCommentTable  where idx = $idx";
		$result = $this->DBObject->query($sql);
		if($result){
		}else{
			echo 'deleteMemberCommentData 실패';
			exit;
		}
		return $result;
	}
	
	
	
	#팀 리스트 가져오기
	public function getTeamList($page = 1,$where = '',$is_paging = 0 ){
		
		if($is_paging){
			$sql = "select * from $this->teamTable a where 1 {$where} order by t_insert_date desc";
			$result = $this->DBObject->query($sql);
			$total = $result->num_rows;#총개수
		
			// 페이지 설정
			$page_set = 10; // 한페이지 줄수
			$block_set = 5; // 한페이지 블럭수
		
			$total_page = ceil ($total / $page_set); // 총페이지수(올림함수)
			$total_block = ceil ($total_page / $block_set); // 총블럭수(올림함수)
		
			if (!$page) $page = 1; // 현재페이지(넘어온값)
			$block = ceil ($page / $block_set); // 현재블럭(올림함수)
			$limit_idx = ($page - 1) * $page_set; // limit시작위치
		
			$limit = " LIMIT $limit_idx, $page_set";
		
			$result = $this->DBObject->query($sql.$limit);
		
			if($result){
				$i = 0;
				while($rows = $result->fetch_array(MYSQLI_ASSOC)){
					$list[] = $this->modifyMemberData($rows);
					$list[$i]['no'] = $i+1;
					$i = $i+1;
				}
			}
			$paging = new Paging($page,$total,$page_set,$block_set );
			return array($list,$paging);
		}else{
			$sql = "select *,(select count(idx) from $this->teamMemberTable b where a.idx = b.team_idx ) as team_member_count from $this->teamTable a where 1 {$where} order by t_name";
			$result = $this->DBObject->query($sql);
			
			if($result){
				$total = $result->num_rows;#총개수
				$i = 0;
				while($rows = $result->fetch_array(MYSQLI_ASSOC)){
					$list[] = $this->modifyMemberData($rows);
					$list[$i]['no'] = $i+1;
					$i = $i+1;
				}
			}
			return array($list, $total);
		}
	}
	
	#팀 상세 정보 가져오기
	public function getTeamDetailData($idx){
		
		$sql = "SELECT * FROM $this->teamTable where idx = $idx";
		$result = $this->DBObject->query($sql);
		if($result){
			$team = $result->fetch_array(MYSQLI_ASSOC) ;
		}
		$sql ="SELECT a.*,b.m_group_name,b.m_name, b.idx as midx FROM  sync_team_member a left join sync_member b on a.tm_member_idx = b.idx where a.team_idx = '$idx'";
		
		$result = $this->DBObject->query($sql);
		if($result){
			while($row = $result->fetch_array(MYSQLI_ASSOC)){
				$teamMembers[] = $row;
			}
			
		}
		return array($team,$teamMembers);
	}
	
	#팀 정보 추가하기
	public function setTeamData($t_type, $t_name, $t_memo){
	
		$current_date = date('Y-m-d H:i:s');
		$sql = "insert into $this->teamTable set t_type ='$t_type',t_name ='$t_name', t_memo ='$t_memo', t_insert_date = '$current_date' ";
		$result = $this->DBObject->query($sql);
		if($result){
			$result = $this->DBObject->getInsertId();
		}else{
			echo 'setTeamData 오류';
			exit;
		}
		return $result;
	}
	
	#팀 삭제
	public function deleteTeamData($t_idx){
	
		$sql = "delete from $this->teamTable where idx = '$t_idx'";
		$result = $this->DBObject->query($sql);
		if($result){
		}else{
			echo 'deleteTeamData 오류';
			exit;
		}
		return $result;
	}
	
	#팀원 추가하기
	public function setTeamMemberData($team_idx, $tm_member_idx, $tm_member_name, $tm_position){
		
		$current_date = date('Y-m-d H:i:s');
		$sql = "insert into $this->teamMemberTable  set team_idx =$team_idx,tm_position ='$tm_position', tm_member_idx =$tm_member_idx,tm_member_name = '$tm_member_name' , tm_insert_date = '$current_date' ";
		
		$result = $this->DBObject->query($sql);
		if($result){
			#$result = $this->DBObject->getInsertId();
		}else{
			echo 'setTeamMemberData 오류';
			exit;
		}
		return $result;
	}

	#팀원 정보 업데이트
	public function updateTeamMemberPosition($position, $idx){
	
		$sql = "update $this->teamMemberTable set tm_position ='$position' where idx = $idx";
		$result = $this->DBObject->query($sql);
		if($result){
		}else{
			echo 'updateTeamMemberPosition 실패';
			exit;
		}
		return $result;
	}
	
	#팀원 삭제
	public function deleteTeamMember($idx){
		
		$sql = "delete from $this->teamMemberTable  where idx = $idx";
		$result = $this->DBObject->query($sql);
		if($result){
		}else{
			echo 'deleteTeamMember 실패';
			exit;
		}
		return $result;
	}
	
	#그룹 리스트 가져오기
	public function getGroupData($where = ''){
		$sql = "select * from $this->groupTable where $where";
		$result = $this->DBObject->query($sql);
		if($result){
			while($row = $result->fetch_array(MYSQLI_ASSOC)){
				$groups[] = $row;
			}
		}
		return $groups;
	}
	
	#그룹 맴버 리스트 가져오기
	public function getGroupMemberData($where = ''){
		$sql = "select * from $this->groupMemberTable where 1 $where";
		$result = $this->DBObject->query($sql);
		if($result){
			while($row = $result->fetch_array(MYSQLI_ASSOC)){
				$groupMembers[] = $row;
			}
		}
		return $groupMembers;
	}
	
	#그룹 리스트+맴버 전체 리스트 가져오기
	public function getAllGroupData($where = ''){
		$sql = "select  *,a.idx as gid from sync_group a
				left join sync_group_member b on a.idx = b.group_idx
				left join sync_member c on b.gm_member_idx = c.idx  where 1 $where";
		$result = $this->DBObject->query($sql);
		if($result){
			while($row = $result->fetch_array(MYSQLI_ASSOC)){
				$groupMembers[] = $row;
			}
		}
		return $groupMembers;
	}
	
	
	#그룹 정보 추가하기[공동체, 리더이름, 리더idx ] 
	public function insertGroupData($g_type, $g_name, $leader_idx){ 
		$current_date = date('Y-m-d H:i:s');
		$sql = "insert into $this->groupTable set g_type ='$g_type',g_name ='$g_name', g_leader_id ='$leader_idx', g_insert_date = '$current_date' ";
		$result = $this->DBObject->query($sql);
		if($result){
			$result = $this->DBObject->getInsertId();
		}else{
			exit;
		}
		return $result;
	}
	
	#그룹 정보 수정하기[공동체, 리더이름, 리더idx ]
	public function updateGroupData($where, $idx){
		$sql = "update $this->groupTable set $where where idx = $idx ";
		$result = $this->DBObject->query($sql);
		if($result){
		}else{
			exit;
		}
		return $result;
	}
	
	#그룹 팀원 추가하기
	public function insertGroupMemberData($group_idx, $gm_idx, $gm_name){
		$current_date = date('Y-m-d H:i:s');
		$sql = "insert into $this->groupMemberTable set group_idx ='$group_idx',gm_member_idx ='$gm_idx', gm_member_name ='$gm_name', gm_insert_date = '$current_date' ";
		$result = $this->DBObject->query($sql);
		if($result){
			$result = $this->DBObject->getInsertId();
		}else{
			exit;
		}
		return $result;
	}
	
	#그룹 삭제
	public function deleteGroupData($idx){
		$sql = "delete from $this->groupTable where idx = $idx";
		$result = $this->DBObject->query($sql);
		if($result){
		}else{
			exit;
		}
		return $result;
	}
	
	#그룹 맴버 전체 삭제
	public function deleteGroupMemberData($gidx){
		$sql = "delete from $this->groupMemberTable where group_idx = $gidx";
		$result = $this->DBObject->query($sql);
		if($result){
		}else{
			exit;
		}
		return $result;
	}
	
	#한개의 그룹  가져오기
	public function getGroup($where){
		
		$sql = "select * from $this->groupTable where 1 $where";
		$result = $this->DBObject->query($sql);
		if($result){
			$auth = $result->fetch_array(MYSQLI_ASSOC) ;
		}
		return $auth;
	}
}
?>