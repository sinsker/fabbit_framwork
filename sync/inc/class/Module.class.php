<?
	Class Module implements Singleton{
		
		private static $instance;
		private $dbClass;
		
		private $pageTable = 'sync_page'; 
		private $pageContentTable = 'sync_page_content';
		private $pageContentValueTable = 'sync_page_content_value';
		private $pageModuleTable = 'sync_page_module';
		private $pageDataTable = 'sync_data';
		
		public $category = array(
				'title' => array(
					'default' => '기본 타이틀',
					'image' => '이미지 타이틀',
					'animate' => '애니메이션 타이틀',
				),
				'slide' => array(
					'image' => '이미지 슬라이드',
					'text' => '텍스트 슬라이드',
					'mix' => '혼합 슬라이드',
				),
				'context' => array(
					'mix' => '제목 + 내용',
					'single' => '내용',
					'animate' => '애니메이션 내용'
				)
		);
		
		private function Module($db = ''){
			$this->dbClass = $db;
		}
		
		public static function getInstance(){
			if(Module::$instance === null){
				Module::$instance  = new Module(DB_Class::getInstance());
			}
			return Module::$instance;
		}
		
		//모듈 초기 생성
		public function initPage($key, $name, $subname, $contrent, $p_security, $p_security_code){
			$currentDate = TIME_FULL;
			$sql = "insert into $this->pageTable set p_key = '$key', p_name = '$name', p_subname = '$subname', p_security = '$p_security', p_security_code= '$p_security_code', wid='{$_SESSION['auth_id']}', mdate= '$currentDate', wdate= '$currentDate'";
			$result = $this->dbClass->query($sql);
			if($result){
				return $this->dbClass->getInsertId();
			}else{
				return 0;
			}
		}
		
		//모듈 페이지 리스트를 가져온다.
		public function modulePageList($whereArray = '', $subOption = ''){
			$sql = 'select * from '.$this->pageTable.($whereArray ? 'where '.createWhereText($whereArray) : '').' '.$subOption;
			$result = $this->dbClass->query($sql);
			if($result){
				while($row = $result->fetch_array(MYSQLI_ASSOC)){
					$moduleList[] = $row;
				}
			}
			return $moduleList;
		}
		
		//모듈 페이지 개수
		public function pageCnt($whereArray = ''){
			$sql = 'select count(*) as cnt from '.$this->pageTable.($whereArray ? 'where '.createWhereText($whereArray) : '');
			$result = $this->dbClass->query($sql);
			$row = $result->fetch_array(MYSQLI_ASSOC);
				
			return $row['cnt'];
		}
		
		//모듈 페이지를 가져온다.
		public function getModuleContent($key){
			$sql = 'select * from '.$this->pageTable.' where p_key = \''.$key.'\'';
			$result = $this->dbClass->query($sql);
			if($result){
				$module = $result->fetch_array(MYSQLI_ASSOC);
			}
			return $module;
		}
		
		//보안코드 입력
		public function getPrivateModuleContent($idx, $code){
			$sql = 'select * from '.$this->pageTable.' where idx = \''.$idx.'\' and p_security_code= \''.$code.'\'';
			$result = $this->dbClass->query($sql);
			if($result){
				$module = $result->fetch_array(MYSQLI_ASSOC);
			}
			return $module;
		}
		
		//페이지 공통 속성 변경
		public function updatePage($key, $setData){
				
			$setData['mdate']	= TIME_FULL;
			$setData['wid']		= $_SESSION['auth_id'];
				
			$sql = "update  $this->pageTable set ".createWhereText($setData, ',')." where p_key = '$key'";
				
			$result = $this->dbClass->query($sql);
			
			return $result;
		}
		
		//전문가용 페이지 빌드
		public function professionalBuildPage($key, $setData){
			
			$setData['mdate'] =  TIME_FULL;
			$setData['wid']=$_SESSION['auth_id'];
			
			$sql = "update  $this->pageTable set ".createWhereText($setData, ',')." where p_key = '$key'";
			
			$result = $this->dbClass->query($sql);
			
			if($result){
				return true;
			}else{
				return false;
			}
		}
		
		//기본형 페이지 내용 추가
		public function basicPageCreate($key, $moduleKey){
			$position = 0;
			$sql = "select count(*) cnt from $this->pageContentTable where page_key = '$key'";
			$result = $this->dbClass->query($sql);
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$position =  $row['cnt'];
			
			$sql = "insert into $this->pageContentTable set page_key = '$key', module_key = '$moduleKey', position = $position";
			$result = $this->dbClass->query($sql);
			if($result){
				return true;
			}else{
				return false;
			}
		}
		
		//기본형 모듈 가져오기
		public function getModuleBasicContent($key){
			$sql = 'select * from '.$this->pageContentTable.' where page_key = \''.$key.'\' order by position asc' ;
			$result = $this->dbClass->query($sql);
			$mContent = array();
			while($row = $result->fetch_array(MYSQLI_ASSOC)){
				$mContent[] = $row;
			}
			return $mContent;
		}
		
		//파일 작성
		public function fileWrite($path, $text){
			$pg =  fopen($path,"w") or die('존재하지 않는 파일입니다.');
			fwrite($pg,  $text);
			fclose($pg);
		}
		
		//기본 모듈 추가
		public function basicModuleAdd(){
			
		}
		
		#모듈 데이터 리스트
		public function getDataList($where = '',$page = 1,$page_set = 10){
			$sql = "select * from $this->pageDataTable where 1 {$where}";
			$result = $this->dbClass->query($sql);
			$total = $result->num_rows;#총개수
		
			// 페이지 설정
			$block_set = 5; // 한페이지 블럭수
			if (!$page) $page = 1; // 현재페이지(넘어온값)
		
			$paging = new Paging($page,$total,$page_set,$block_set );
			$limit_idx = $paging->limit; // limit시작위치
			$limit = " LIMIT $limit_idx, $page_set";
		
			$result = $this->dbClass->query($sql.$limit);
			$no = $paging->startNo;
			if($result){
				$i = 0;
				while($rows = $result->fetch_array(MYSQLI_ASSOC)){
					$rows['no'] = $no--;
						
					$list[] = $rows;
				}
			}
		
			return array($list,$paging);
		}
	}
?>