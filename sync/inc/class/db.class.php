<?
Class DB_Class implements Singleton {
	
	private static $instance = null;
	
	private $result;
	private $query;
	private $conn;
	
	private $dbName;
	private $dbUrl;
	private $dbId;
	private $dbPassword;
	
	private $mysqli; //mysqli 객체
	
	private function __construct($db_config){
		if(empty($db_config)){echo '커넥션 정보가 없습니다.'; exit;}
		
		$_mysqli = new mysqli($db_config['dbhost'],$db_config['id'],$db_config['password'],$db_config['dbname']);
		if ($_mysqli->connect_error) {
			die('Error : ('. $_mysqli->connect_errno .') '. $_mysqli->connect_error );
		}
		$this->mysqli = $_mysqli;
	}
	
	public static function getInstance(){
		if(DB_Class::$instance === null){
			global $db_config;
			DB_Class::$instance  = new DB_Class($db_config);
		}
		return DB_Class::$instance;
	}
	
	
	public function query($sql){
		$db = $this->mysqli;
		$this->result = $db->query($sql);
		if(!$this->result ){ echo $db->error; }
		
		return $this->result;
	}
	
	public function queryFetch($sql){
	}
	
	public function getInsertId(){
		return $this->mysqli->insert_id;
	}
	
	#SELECT 쿼리 테이블로 보여주는 함수
	public function viewTable($query, $table_name = ''){
	
		if(!empty($table_name)){
			$result = $this->mysqli->query("SHOW FULL COLUMNS FROM ".$table_name);
			$schema = "<table>";
			while($row = $result->fetch_array()){
				//print_r($row);
				$schema .= "<tr><td>".$row["Field"]."</td><td>".$row["Comment"]."</td></tr>";
			}
			$schema .= "</table>";
		}
	
		//테이블 데이터
		$result = $this->query($query);
		$tableData = "<table>";
		while($row = $result->fetch_array()){
			$tableData .= '<tr>';
	
			if (empty($arr_key)) {
				$arr_key = array_keys($row);
				foreach ($arr_key as $item)	{
					$tableData .= '<th>'.$item.'</th>';
				}
				$tableData .= '</tr>';
				$tableData .= '<tr>';
			}
	
			foreach($row as $key => $item){
	
				$tableData .= "<td>".$row[$key]."</td>";
			}
			$tableData .= "</tr>";
		}
		$tableData .= "</table>";
	
		return $schema.$tableData;
	}
	
	
}

?>