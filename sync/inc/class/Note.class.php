<?
Class Note {
	
	private $memberInfo = array();
	private $DBObject; #DB 클래스
	
	
	public function __construct($dbObject = ''){
		#DB 주입
		if(empty($this->DBObject)){ $this->DBObject = $dbObject;}
	}
	
	#전체 노트 개수
	public function getNotesCount($where = ''){
		$sql = "select count(*) as cnt from sync_note where 1 {$where}";
		$result = $this->DBObject->query($sql);
		$rows = $result->fetch_array();
		return $rows['cnt'];
	}
	
	#노트 리스트 가져오기
	public function getNoteList($page = 1,$where = ''){
		$sql = "select * from sync_note where 1 {$where} order by n_insert_date desc";
		$result = $this->DBObject->query($sql);
		$total = $result->num_rows;#총개수
		// 페이지 설정
		$page_set = 5; // 한페이지 줄수
		$block_set = 5; // 한페이지 블럭수
		
		$total_page = ceil ($total / $page_set); // 총페이지수(올림함수)
		$total_block = ceil ($total_page / $block_set); // 총블럭수(올림함수)
		
		if (!$page) $page = 1; // 현재페이지(넘어온값)
		$block = ceil ($page / $block_set); // 현재블럭(올림함수)
		$limit_idx = ($page - 1) * $page_set; // limit시작위치
		
		$limit = " LIMIT $limit_idx, $page_set";
		
		$result = $this->DBObject->query($sql.$limit);
		
		if($result){
			$i = 0;
			while($rows = $result->fetch_array()){
				$list[] = $this->modifyNoteData($rows);
				$list[$i]['no'] = $i+1;
				$i = $i+1;
			}
		}
		$paging = new Paging($page,$total,$page_set,$block_set );
		return array($list,$paging);
	}
	
	#노트 추가하기
	public function setNoteData($type, $title, $memo, $writeId, $writeName){
		global $member_group;
		
		$current_date = date('Y-m-d H:i:s');
		$sql = "insert into sync_note set n_type = '$type', n_title = '$title', n_memo = '$memo', n_write_id = '$writeId', n_write_name = '$writeName', n_insert_date = '$current_date'";
		$result = $this->DBObject->query($sql);
		if($result){
			$result = $this->DBObject->getInsertId();
		}else{
			echo 'setNoteData 실패';
			exit;
		}
		return $result;
	}
	
	
	#노트 업데이트
	public function updateNoteData($type, $title, $memo, $writeId, $writeName, $idx){
		global $member_group;
	
		$groupName = $member_group[$group];
		$sql = "update sync_note set n_type = '$type', n_title = '$title', n_memo = '$memo', n_write_id = '$writeId', n_write_name = '$writeName' where idx = $idx";
		$result = $this->DBObject->query($sql);
		if($result){
		}else{
			echo 'updateNoteData 실패';
			exit;
		}
		return $result;
	}
	
	#노트 삭제
	public function deleteNoteData($idx){
		global $member_group;
		if(!$idx) return false;
		$sql = "delete from sync_note where idx = $idx";
		$result = $this->DBObject->query($sql);
		if($result){
		}else{
			echo 'updateNoteData 실패';
			exit;
		}
		return $result;
	}
	
	
	#노트 정보 가져오기
	public function getNoteData($where){
		$sql = "select * from sync_note where 1 $where";
		$result = $this->DBObject->query($sql);
		if($result){
			$auth = $result->fetch_array() ;
		}
		return $auth;
	}
	 
	#회원 정보 가공하기
	public function modifyNoteData($note){
		
		$note['n_memo_min'] = mb_substr($note['n_memo'],0,12,'UTF-8');
		
		$insertDate=substr($note['n_insert_date'],0,10);
		if($insertDate == date('Y-m-d')){
			$note['n_date'] = substr($note['n_insert_date'],11,5);
		}else{
			$note['n_date'] = $insertDate;
		}
		
		$modifyNote = $note;
		
		return $modifyNote;
	}
	
	
	public function getUesr(){
		return $this->memberInfo;
	}
	
	
}
?>