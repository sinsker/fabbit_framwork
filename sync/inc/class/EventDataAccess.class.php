<?
	#이벤트 데이터 처리 Class
	Abstract Class EventDataAccess implements DataAccess {
		
		protected $evt_id;
		
		protected $DBObject; #DB 클래스
		protected $dataTable = 'sync_data'; #데이터 저장 변수
		protected $msg; #메시지 객체
		
		public function __construct($dbObject = '', $evt_id){
			#DB 주입
			if(empty($this->DBObject)){ $this->DBObject = $dbObject;}
			$this->evt_id = $evt_id;
			$this->msg = new Massge();
			
		}
		
		abstract function eventInsertData($data);	#데이터 저장 쿼리문 만드는 함수
		abstract function isDataDuplication($data);	#데이터 체크
		abstract function eventMassgeSetting();		#메세지 수정
		
		function getMassage(){
			return $this->msg;
		}
		
		function setData($data){
			
			 $this->eventInsertDataSql($data);
		}
		
		function getData(){
			
		}
	}
?>