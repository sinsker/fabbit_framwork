<?
Class Calendar {
	
	private $DBObject; #DB 클래스
	
	private $calendarTable = 'sync_calendar';
	
	public function __construct($dbObject = ''){
		#DB 주입
		if(empty($this->DBObject)){ $this->DBObject = $dbObject;}
	}
	
	
	#캘린더 정보 입력
	public function addSchedule($set){
		$sql = "insert into {$this->calendarTable} set ".createWhereText( $set, ',');
		$result = $this->DBObject->query($sql);
		return $result;
	}
	
	#캘린더 전체 일정 가져 오기
	public function getSchedule(){

		$sql = "select * from  {$this->calendarTable} ";
		$result = $this->DBObject->query($sql);
		
		if($result){
			while($row = $result->fetch_array(MYSQLI_ASSOC)){
				$rows[] = $row;
			}
		}
		return $rows;
	}
	
	
	#캘린더  일정 가져 오기
	public function getDateSchedule($where = ''){
	
		$sql = "select * from  {$this->calendarTable} where wdate = '$where'";
		$result = $this->DBObject->query($sql);
	
		if($result){
			while($row = $result->fetch_array(MYSQLI_ASSOC)){
				$rows[] = $row;
			}
		}
		return $rows;
	}
	
	public function delS($idx){
		$sql = "delete from {$this->calendarTable} where idx = $idx";
		$result = $this->DBObject->query($sql);
		return $result;
	}
}
?>