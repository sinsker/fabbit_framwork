<?
include_once $_SERVER['DOCUMENT_ROOT'].'/inc/interface/all.interface.php';

Class FileManager implements Singleton {

	private static $instance = null;
	
	private $db;
	private $type;  // file, img
	private $uploadCondition = array();
	
	private $fileTable = 'sync_file';
	
	private function FileManager($_db){
		$this->db = $_db;
	}
	
	public static function getInstance(){
		if(FileManager::$instance === null){
			FileManager::$instance  = new FileManager(DB_Class::getInstance());
		}
		return FileManager::$instance;
	}
	
	//기본 경로 설정
	public function getBasePath($type){
		switch($type){
			case 'file':
				$basePath = $_SERVER['DOCUMENT_ROOT'].'/upload/file';
				break;
			case 'img':
				$basePath = $_SERVER['DOCUMENT_ROOT'].'/upload/img';
				break;
			default:
				break;
		}
		return $basePath;
	}
	
	public function uploadFolerinit($path){
		if(!is_dir($path)){
			@chmod($path, 0777);
		}
	}
	
	//업로드 조건
	function setCondition($type){
		switch($type){
			case 'img': // 이미지 업로드
				$this->uploadCondition = array('jpg','png','jpeg','gif');
				break;
				
			case 'file'; //파일 업로드
				$this->uploadCondition = array('jpg','png','jpeg','gif','zip','pdf','hwp','xls','xlsx','ppt','pptx','txt','docx');
				break;
				
			default :
				$this->uploadCondition = array('txt');
				break;
				
		}
	}
	
	//파일 경로
	function fuileExtension($fileName){
		$f_info = pathinfo($fileName);
		return $f_ext = strtolower($f_info["extension"]);
	}
	
	//파일 업로드
	function fileUpload($file, $path){
		if(!$file) return false;
	
		if($file['size'] == 0){
			alert("사이즈가 0인 파일은 업로드가 불가능 합니다.");
			exit;
		}
		
		if($file['size'] > 1024 * 1024 * 10){
			alert("사이즈가10MB을 넘는 파일은 업로드가 불가능 합니다.");
			exit;
		}
		
		$f_info = pathinfo($file['name']);
		$f_ext = strtolower($f_info["extension"]);
		
		if(!in_array($f_ext, $this->uploadCondition) ){
			alert( $f_ext." 해당 확장자는 업로드 불가능합니다.");
			exit();
		} 
	
	
		$f_a_name = md5(str_replace($f_ext, "", $file['name']).date('YmdHis')).".{$f_ext}";
		$upload_file = $path.'//'.$f_a_name;
		if(is_uploaded_file($file['tmp_name'])) {
			if(!move_uploaded_file($file['tmp_name'], $upload_file)) {
				alert("업로드된 파일을 옮기는 중 오류가 발생하였습니다.");
				exit();
			}
		}
	
		#클라이언트 path
		return array('name' => $f_a_name, 'realname'=> $file['name'], 'extension'=> $f_ext, 'mine'=>$file['type'],  'size' => $file['size']);
	}
	
	
	//다중 업로드 파일 배열 변환
	function reArrayFiles($file_post) {
	
		$file_ary = array();
		$file_count = count($file_post['name']);
		$file_keys = array_keys($file_post);
	
		for ($i=0; $i<$file_count; $i++) {
			foreach ($file_keys as $key) {
				$file_ary[$i][$key] = $file_post[$key][$i];
			}
		}
	
		return $file_ary;
	}
	
	
	//파일 다운로드
	function fileDownload($file, $filename = ''){
		// 파일 Path를 지정합니다.
		// id값등을 이용해 Database에서 찾아오거나 GET이나 POST등으로 가져와 주세요.
		echo $file;
		$file_size = filesize($file);
		if(!$filename){
			$filename = get_file_name($file);
		}
		if (is_file($file)) // 파일이 존재하면
		{
			// 파일 전송용 HTTP 헤더를 설정합니다.
			if(strstr($HTTP_USER_AGENT, "MSIE 5.5"))
			{
				header("Content-Type: doesn/matter");
				header("Content-Disposition: filename=\"".$filename."\"");
				header("Content-Transfer-Encoding: binary");
				Header("Content-Length: ".$file_size);
				header("Pragma: no-cache");
				header("Expires: 0");
			}
			else
			{
				Header("Content-type: application/octet-stream");
				Header("Content-Disposition: attachment; filename=\"".$filename."\"");
				Header("Content-Transfer-Encoding: binary");
				Header("Content-Length: ".$file_size);
				Header("Content-Description: PHP3 Generated Data");
				header("Pragma: no-cache");
				header("Expires: 0");
			}
			//파일을 열어서, 전송합니다.
			$fp = fopen($file, "rb");
			if (!fpassthru($fp))
				fclose($fp);
		}
	}
	
	//파일 삭제
	function fileDelete($file){
		if (is_file($file)){
			unlink($file);
		}
	}
	
	//파일 데이터 저장
	function setFileData($f_location, $f_type, $f_path, $f_url, $f_name, $f_realname, $f_size, $f_mine){
	$agent = ACCESS_AGENT;
	$datetime = TIME_FULL;
	$wip = ACCESS_IP;
	$sql = "INSERT INTO `sync_file` (
				`f_location`,`f_type`,`f_path`,`f_url`,`f_name`,`f_realname`,`f_size`,`f_mine`,`f_agent`,`f_ip`,`f_wdate`
			)
			VALUES
			(
					'$f_location',
					'$f_type',
					'$f_path',
					'$f_url',
					'$f_name',
					'$f_realname',
					'$f_size',
					'$f_mine',
					'$agent',
					'$wip',
					'$datetime'
			)";
		$result = $this->db->query($sql);
	}
	
	//어레이 저장
	function setArrayFileData($arrayData){
		return $this->setFileData($arrayData['f_location'], $arrayData['f_type'], $arrayData['f_path'], $arrayData['f_name'], $arrayData['f_realname'], $arrayData['f_size'], $arrayData['f_mine']);
	}
	
/* 	//
	function fileDatas(){
		if($result){
			$i = 0;
			while($rows = $result->fetch_array(MYSQLI_ASSOC)){
				$list[] = $rows;
				$list[$i]['no'] = $i+1;
				$i = $i+1;
			}
		}
		
	}
 */	
	#파일 리스트 가져오기
	public function getFileList($page = 1,$where = '',$page_set = 10){
		$sql = " select * from $this->fileTable where 1 {$where}";
		$result = $this->db->query($sql);
		$total = $result->num_rows;#총개수
	
		// 페이지 설정
		$block_set = 5; // 한페이지 블럭수
	
		$total_page = ceil ($total / $page_set); // 총페이지수(올림함수)
		$total_block = ceil ($total_page / $block_set); // 총블럭수(올림함수)
	
		if (!$page) $page = 1; // 현재페이지(넘어온값)
		$block = ceil ($page / $block_set); // 현재블럭(올림함수)
		$limit_idx = ($page - 1) * $page_set; // limit시작위치
	
		$limit = " LIMIT $limit_idx, $page_set";
	
		$result = $this->db->query($sql.$limit);
	
		if($result){
			$i = 0;
			while($rows = $result->fetch_array(MYSQLI_ASSOC)){
				$list[] = $rows;
				$list[$i]['no'] = $i+1;
				$i = $i+1;
			}
		}
		$paging = new Paging($page,$total,$page_set,$block_set );
		return array($list, $paging);
	}
	
	#파일 데이터 가져오기
	public function getFileData($where){
		$sql = "select * from $this->fileTable where 1 {$where}";
		$result = $this->db->query($sql);
		$row = $result->fetch_array(MYSQLI_ASSOC);
		return $row;
	}
	
	#파일 데이터 삭제하기
	public function deleteFileData($where){
		if(!$where) return;
		$sql = "delete from $this->fileTable where 1 {$where}";
		$result = $this->db->query($sql);
		return $result;
	}
	
	
}



?>