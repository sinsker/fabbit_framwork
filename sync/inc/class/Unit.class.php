<?
class Unit implements widget {

	private static $subModule = array();  
	
	//생성자	
	public function Unit(){}
	
	/* Unit::load(); */
	public static function load($wtype, $wvar){
		$widget_cssjs = '';
		
		$wskin 	= WIDGET_PATH.'/'.$wtype.'/'.'main.php';
		$wcss 	= WIDGET_PATH.'/'.$wtype.'/'.'main.css';
		$wjs 	= WIDGET_PATH.'/'.$wtype.'/'.'main.js';
		#고유 아이디 생성
		$wvar['id'] = $wvar['id'] ? $wvar['id'] : 'widget-'.uniqid($wtype);
		
		if(is_file($_SERVER['DOCUMENT_ROOT'].'/'.$wcss))	$widget_cssjs .= '<link href="'.$wcss.'" rel="stylesheet">'."\n";
		if(is_file($_SERVER['DOCUMENT_ROOT'].'/'.$wjs))		$widget_cssjs .= '<script src="'.$wjs.'"></script>'."\n";
		if(is_file($_SERVER['DOCUMENT_ROOT'].'/'.$wskin))	include $_SERVER['DOCUMENT_ROOT'].'/'.$wskin;
		if($widget_cssjs != ''){
			array_push(Unit::$subModule, $widget_cssjs);
		}
	}

	public function update(){}
	
	
	public static function outPutFilterSubModule(){
		if(!empty(Unit::$subModule)){
			Unit::$subModule = array_unique(Unit::$subModule, SORT_STRING );
			$outputSubModule = implode("", Unit::$subModule);
			echo $outputSubModule;
		}
	}
	
}

?>