<?
class WedgetCacheManager{

	private $dir;
	private $key;
	private $interval;
	private $cache_ext = '.wc';
	

	public static function getInstance($key, $dir, $interval){
		static $instance = null;
		if( null === $instance ){
			$instance = new static($key, $dir, $interval);
		}
		return $instance;
	}
	
	private function __construct($key, $dir, $interval){
		$this->key = $key;
		$this->dir = $dir;
		$this->interval = $interval;
	}

	#캐쉬생성여부
	function isCache(){
		if(!is_dir($this->dir)){
			@mkdir($this->dir, 0777);
		}
		
		if( !file_exists($this->dir.'/'.$this->key.$this->cache_ext)  || ((time() - filemtime($this->dir.'/'.$this->key.$this->cache_ext)) > $this->interval) ){
			return true;
		}
		return false;
	}

	#불러오기
	function load(){
		include_once $this->dir.'/'.$this->key.$this->cache_ext;
		return ${$this->key};
	}

	#저장하기
	function save($datas){
		$cache = '<?php ';
		if(is_array($datas)){
			$cache .= '$'.$this->key.' = array(';
			foreach($datas as $key => $val){
				$cache .= 'array(';
				foreach($val as $k => $v){
					$cache .=  $k.' => "'.$v.'",';
				}
				$cache .= '),';
			}
			$cache .= '); ';
		}else{
			$cache .= '$'.$this->key.' = "'.$datas.'"; ';
		}
		$cache .= ' ?>';
		
		$fp = fopen($this->dir.'/'.$this->key.$this->cache_ext,'w');
		if($fp){
			fwrite($fp,$cache);
			fclose($fp);
			return true;
		} else {
			return false;
		}
	}
}


?>