<?
if (!defined('SIN')) exit;

/*************************************************************************
**
**  일반 함수 모음
**
*************************************************************************/

// 마이크로 타임을 얻어 계산 형식으로 만듦
function get_microtime()
{
    list($usec, $sec) = explode(" ",microtime());
    return ((float)$usec + (float)$sec);
}

// 현재페이지, 총페이지수, 한페이지에 보여줄 행, URL
function get_paging($write_pages, $cur_page, $total_page, $url, $add="")
{
    $str = "";
    if ($cur_page > 1) {
        $str .= "<a href='" . $url . "1{$add}'>처음</a>";
        //$str .= "[<a href='" . $url . ($cur_page-1) . "'>이전</a>]";
    }

    $start_page = ( ( (int)( ($cur_page - 1 ) / $write_pages ) ) * $write_pages ) + 1;
    $end_page = $start_page + $write_pages - 1;

    if ($end_page >= $total_page) $end_page = $total_page;

    if ($start_page > 1) $str .= " &nbsp;<a href='" . $url . ($start_page-1) . "{$add}'>이전</a>";

    if ($total_page > 1) {
        for ($k=$start_page;$k<=$end_page;$k++) {
            if ($cur_page != $k)
                $str .= " &nbsp;<a href='$url$k{$add}'><span>$k</span></a>";
            else
                $str .= " &nbsp;<b>$k</b> ";
        }
    }

    if ($total_page > $end_page) $str .= " &nbsp;<a href='" . $url . ($end_page+1) . "{$add}'>다음</a>";

    if ($cur_page < $total_page) {
        //$str .= "[<a href='$url" . ($cur_page+1) . "'>다음</a>]";
        $str .= " &nbsp;<a href='$url$total_page{$add}'>맨끝</a>";
    }
    $str .= "";

    return $str;
}


// 변수 또는 배열의 이름과 값을 얻어냄. print_r() 함수의 변형
function print_r2($var)
{
    ob_start();
    print_r($var);
    $str = ob_get_contents();
    ob_end_clean();
    $str = preg_replace("/ /", "&nbsp;", $str);
    echo nl2br("<span style='font-family:Tahoma, 굴림; font-size:9pt;'>$str</span>");
}


// 메타태그를 이용한 URL 이동
// header("location:URL") 을 대체
function goto_url($url)
{
    echo "<script type='text/javascript'> location.replace('$url'); </script>";
    exit;
}


// 세션변수 생성
function set_session($session_name, $value)
{
    if (PHP_VERSION < '5.3.0')
        session_register($session_name);
    // PHP 버전별 차이를 없애기 위한 방법
    $$session_name = $_SESSION["$session_name"] = $value;
}


// 세션변수값 얻음
function get_session($session_name)
{
    return $_SESSION[$session_name];
}


// 쿠키변수 생성
function set_cookie($cookie_name, $value, $expire)
{
    setcookie(md5($cookie_name), base64_encode($value), TIME + $expire, '/', COOKIE_DOMAIN);
}


// 쿠키변수값 얻음
function get_cookie($cookie_name)
{
    return base64_decode($_COOKIE[md5($cookie_name)]);
}


//암호화
function encrypt($key, $string) {
	$password = hash('sha256', $password, true);
    
    // 용량 절감과 보안 향상을 위해 평문을 압축한다.
    
    $plaintext = gzcompress($plaintext);
    
    // 초기화 벡터를 생성한다.
    
    $iv_source = defined('MCRYPT_DEV_URANDOM') ? MCRYPT_DEV_URANDOM : MCRYPT_RAND;
    $iv = mcrypt_create_iv(32, $iv_source);
    
    // 암호화한다.
    
    $ciphertext = mcrypt_encrypt('rijndael-256', $password, $plaintext, 'cbc', $iv);
    
    // 위변조 방지를 위한 HMAC 코드를 생성한다. (encrypt-then-MAC)
    
    $hmac = hash_hmac('sha256', $ciphertext, $password, true);
    
    // 암호문, 초기화 벡터, HMAC 코드를 합하여 반환한다.
    
    return base64_encode($ciphertext . $iv . $hmac);
}


//복호화
function decrypt($ciphertext, $password) {
	 $ciphertext = @base64_decode($ciphertext, true);
    if ($ciphertext === false) return false;
    $len = strlen($ciphertext);
    if ($len < 64) return false;
    $iv = substr($ciphertext, $len - 64, 32);
    $hmac = substr($ciphertext, $len - 32, 32);
    $ciphertext = substr($ciphertext, 0, $len - 64);
    
    // 암호화 함수와 같이 비밀번호를 해싱한다.
    
    $password = hash('sha256', $password, true);
    
    // HMAC 코드를 사용하여 위변조 여부를 체크한다.
    
    $hmac_check = hash_hmac('sha256', $ciphertext, $password, true);
    if ($hmac !== $hmac_check) return false;
    
    // 복호화한다.
    
    $plaintext = @mcrypt_decrypt('rijndael-256', $password, $ciphertext, 'cbc', $iv);
    if ($plaintext === false) return false;
    
    // 압축을 해제하여 평문을 얻는다.
    
    $plaintext = @gzuncompress($plaintext);
    if ($plaintext === false) return false;
    
    // 이상이 없는 경우 평문을 반환한다.
	return $result;
}


function base64encode($string) {
	$data = str_replace(array('+','/','='),array('-','_',''),base64_encode($string));
	return $data;
}

function base64decode($string) {
	$data = str_replace(array('-','_'),array('+','/'),$string);
	$mod4 = strlen($data) % 4;
	if ($mod4) {
		$data .= substr('====', $mod4);
	}
	return base64_decode($data);
}



// 경고메세지를 경고창으로
function alert($msg='', $url='')
{

    if (!$msg) $msg = '올바른 방법으로 이용해 주십시오.';

	//header("Content-Type: text/html; charset=$g4[charset]");
	echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=".CHAR_SET."\">";
	echo "<script type='text/javascript'>alert('$msg');";
    if (!$url)
        echo "history.go(-1);";
    echo "</script>";
    if ($url)
        // 4.06.00 : 불여우의 경우 아래의 코드를 제대로 인식하지 못함
        //echo "<meta http-equiv='refresh' content='0;url=$url'>";
        goto_url($url);
    exit;
}


// 경고메세지 출력후 창을 닫음
function alert_close($msg)
{

	echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=".CHAR_SET."\">";
    echo "<script type='text/javascript'> alert('$msg'); window.close(); </script>";
    exit;
}

// 경고메세지 출력
function alert_only($msg)
{

	echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=".CHAR_SET."\">";
	echo "<script type='text/javascript'> alert('$msg'); window.close(); </script>";
}



// url에 http:// 를 붙인다
function set_http($url)
{
    if (!trim($url)) return;

    if (!preg_match("/^(http|https|ftp|telnet|news|mms)\:\/\//i", $url))
        $url = "http://" . $url;

    return $url;
}


// 파일의 용량을 구한다.
//function get_filesize($file)
function get_filesize($size)
{
    //$size = @filesize(addslashes($file));
    if ($size >= 1048576) {
        $size = number_format($size/1048576, 1) . "M";
    } else if ($size >= 1024) {
        $size = number_format($size/1024, 1) . "K";
    } else {
        $size = number_format($size, 0) . "byte";
    }
    return $size;
}

//배열로 셀렉트 박스 만드는 함수
function inArraySelectBox($_array, $_selected_value , $name, $type=1,$defult=0,$subOption=''){
	$html = "<select class='form-control' name='$name' id='$name' {$subOption}>";
	if($defult){
		$html .= "<option value='' class='text-info'> - 선택 - </option>";
	}

	if(empty($_array)){
		$html .= "<option value='' class='text-info'> - 선택 - </option>";
	}else{
			
		foreach($_array as $key => $value){
			$selected ='';
			if( $key == $_selected_value){
				$selected = "selected";
			}
			$html .= "<option class='text-info' value='$key' $selected >";
			if($type == 1){
				$html .= '['.$key.'] '.$value;
			}else{
				$html .= $value;
			}
			$html .= "</option>";
		}
		$html .= "</select>";
	}

	return $html;
}


// 폴더의 용량 ($dir는 / 없이 넘기세요)
function get_dirsize($dir)
{
    $size = 0;
    $d = dir($dir);
    while ($entry = $d->read()) {
        if ($entry != "." && $entry != "..") {
            $size += filesize("$dir/$entry");
        }
    }
    $d->close();
    return $size;
}

function agent_info($u_agent = ''){
		if(empty($u_agent)) $u_agent = $_SERVER['HTTP_USER_AGENT'];
		
		$ary_m = array("iPhone","iPod","IPad","Android","Blackberry","SymbianOS|SCH-M\d+","Opera Mini","Windows CE","Nokia","Sony","Samsung","LGTelecom","SKT","Phone");
		$bname = 'Unknown';
		$platform = 'Unknown';
		$model = '';
		$version= "";
		$isMobile = 'N';
		
		if (preg_match('/Mobile/i',$u_agent)){
			$isMobile = 'Y';
		}
		
		
		if($isMobile == 'N'){ //모바일이 아닐 경우
			if (preg_match('/linux/i', $u_agent)) {
				$platform = 'Linux';
			}
			elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
				$platform = 'Mac';
			}
			elseif (preg_match('/windows|win32/i', $u_agent)) {
				$platform = 'Windows';
			}
			$model = 'PC '.$platform;
			
		}else{ //모바일일 경우
			
			for($i=0; $i<count($ary_m); $i++){
				if(preg_match("/$ary_m[$i]/i", strtolower($u_agent))) {
					$m_platform = $ary_m[$i];
					break;
				}
			}
			
			$model = $m_platform;
		}
		
		if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
		{
			$bname = '인터넷 익스플로러';
			$ub = "MSIE";
		}
		elseif(preg_match('/Firefox/i',$u_agent))
		{
			$bname = '파이어 폭스';
			$ub = "Firefox";
		}
		elseif(preg_match('/Chrome/i',$u_agent))
		{
			$bname = '크롬';
			$ub = "Chrome";
		}
		elseif(preg_match('/Safari/i',$u_agent))
		{
			$bname = '사파리';
			$ub = "Safari";
		}
		elseif(preg_match('/Opera/i',$u_agent))
		{
			$bname = '오페라';
			$ub = "Opera";
		}
		elseif(preg_match('/Netscape/i',$u_agent))
		{
			$bname = '넷스케이프';
			$ub = "Netscape";
		}else if(preg_match('/facebookexternalhit/i',$u_agent))
		{
			$bname = '스크랩';
			$ub = "facebookext";
			$isBot = 'Y';
		}else if(preg_match('/bandscraper/i',$u_agent))
		{
			$banme = '밴드 스크랩';
			$ub ='naverband';
			$isBot = 'Y';
		}
	
		$known = array('Version', $ub, 'other');
		$pattern = '#(?<browser>' . join('|', $known) .')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
	
		$i = count($matches['browser']);
		if ($i != 1) {
			if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
				$version= $matches['version'][0];
			}
			else {
				$version= $matches['version'][1];
			}
		}
		else {
			$version= $matches['version'][0];
		}
	
		if ($version==null || $version=="") {$version="?";}
		
		if(preg_match('/bot/i',$u_agent)) {
			$isBot = 'Y';
		}
	
		return array(
			'userAgent'	=> $u_agent,
			'name'		=> $bname,
			'shortName'	=> $ub,
			'version'	=> $version,
			'platform'	=> $platform,
			'pattern'	=> $pattern,
			'isMobile'	=> $isMobile,
			'model'		=> $model,
			'isBot'		=> $isBot
		);
}

//엑셀 불러오기
function getExcelDatas($_file){
	set_time_limit(0);
	ini_set('memory','256M');
	ini_set("memory_limit",-1);
	if($_file) {
			
		$mf_file = $_file;
		$f_name = $mf_file['name'];

		$f_info = pathinfo($f_name);
		$f_ext = strtolower($f_info["extension"]);
		
		if($f_ext != "xls" ) {
			alert("xls 엑셀파일만 업로드 가능합니다.");
			exit();
		}
			
		$file_path = $_SERVER['DOCUMENT_ROOT']."/tmp/cache/";
		$f_a_name = md5(str_replace($f_ext, "", $f_name).date('YmdHis')).".{$f_ext}";
		$upload_file = $file_path.$f_a_name;
		if(is_uploaded_file($mf_file['tmp_name'])) {
			if(!move_uploaded_file($mf_file['tmp_name'], $upload_file)) {
				alert("업로드된 파일을 옮기는 중 오류가 발생하였습니다.");
				exit();
			}
			@require_once $_SERVER['DOCUMENT_ROOT']."/inc/lib/excel_reader2.php";
			$data = new Spreadsheet_Excel_Reader();
			$data->setOutputEncoding('UTF-8');
			$data->read($file_path.$f_a_name);
			#unlink($upload_file);// 파일삭제
			return $data;
		}
		return false;
	}else{
		return false;
	}
}

//파일 업로드
function fileUpload($_file, $_typeArray, $_path = ''){
	if(!$_file) return false;
	if(!$_path) $_path = '/upload/';
	
	$f_info = pathinfo( $_file['name']);
	$f_ext = strtolower($f_info["extension"]);
	
	$_type = @implode('|' ,$_typeArray);
	
	if(!preg_match("/($_type)/",$f_ext) ){
		alert( "해당 파일은 업로드 불가능합니다.");
		exit();
	}
		
	
	$f_a_name = md5(str_replace($f_ext, "", $_file['name']).date('YmdHis')).".{$f_ext}";
	$upload_file = $_SERVER['DOCUMENT_ROOT'].$_path.$f_a_name;
	if(is_uploaded_file($_file['tmp_name'])) {
		if(!move_uploaded_file($_file['tmp_name'], $upload_file)) {
			alert("업로드된 파일을 옮기는 중 오류가 발생하였습니다.");
			exit();
		}
	}
	
	#클라이언트 path
	return $_path.$f_a_name;
}


#파일이름 가져오기
function get_file_name($full_file_name) {
	$array = explode('/', $full_file_name);
	if (sizeof($array) <= 1)  return $full_file_name;
	return $array[sizeof($array) -1];
}

#상세 보기[디버깅 용도]
function pr($text){
	echo '<pre>';
	print_r($text);
	echo '</pre>';
}

function fileDownload($file){
	// 파일 Path를 지정합니다.
	// id값등을 이용해 Database에서 찾아오거나 GET이나 POST등으로 가져와 주세요.
	$file_size = filesize($file);
	$filename = get_file_name($file);
	if (is_file($file)) // 파일이 존재하면
	{
		// 파일 전송용 HTTP 헤더를 설정합니다.
		if(strstr($HTTP_USER_AGENT, "MSIE 5.5"))
		{
			header("Content-Type: doesn/matter");
			Header("Content-Length: ".$file_size);
			header("Content-Disposition: filename=".$filename);
			header("Content-Transfer-Encoding: binary");
			header("Pragma: no-cache");
			header("Expires: 0");
		}
		else
		{
			Header("Content-type: file/unknown");
			Header("Content-Disposition: attachment; filename=".$filename);
			Header("Content-Transfer-Encoding: binary");
			Header("Content-Length: ".$file_size);
			Header("Content-Description: PHP3 Generated Data");
			header("Pragma: no-cache");
			header("Expires: 0");
		}
		//파일을 열어서, 전송합니다.
		$fp = fopen($file, "rb");
		if (!fpassthru($fp))
			fclose($fp);
	}
}

function execlDownload($context, $filename = ''){
	if(!$filename) $filename = date('mdHis');
	
	header('Content-Disposition: attachment; filename='.$filename.'.xls');
	header('Content-type: application/vnd.ms-excel; charset='.CHAR_SET );
	header('Content-Description: PHP5 Generated Data');
	header('Cache-Control: no-cache');
	header('Pragma: no-cache');
	echo '<html>';
	echo '	<head>';
	echo '		<meta http-equiv="Content-Type" content="application/vnd.ms-excel; charset="'.CHAR_SET.'">';
	echo '		<style>';
	echo '			td {border: 1px solid #000000;}';
	echo '		</style>';
	echo '	</head>';
	echo $context;
}

function createWhereText($values, $operator = 'and'){
	$whereText = array();
	foreach ($values as $key => $value){
		$type = gettype($value);
		if($type = 'string'){$value = "'$value'"; }
		$text[] = $key.'='.$value;
	}
	$whereText = @join($operator.' ',$text);
	
	return $whereText;
}

// 스킨 치환 및 반환
function fetchSkin($datas, $skin) {
	if(is_array($skin)) $skin = implode('', $skin);
	 
	preg_match_all('/{{(.*?)}}/', $skin, $pattern);
	if(!empty($pattern[1])) {
		$infos = array();
		foreach(array_unique($pattern[1]) as $field){
			$infos[] = $datas[$field];
		}

		$skin = str_replace(array_unique($pattern[0]), $infos, $skin);
	}
	return $skin;
}

//반복문 치환
function fetchContent($datas, $skin){
	if(preg_match_all('/\{\{START\}\}(.|\s)*\{\{END\}\}/',$skin, $pattern)){
		if(!empty($pattern[1])) {
			$repetition = '';
			$loop = count($datas);
			for($i = 0; $i < $loop; $i++){
				$repetition .= fetchSkin($datas[$i],$pattern[0]);
			}
			return preg_replace('/\{\{START\}\}(.|\s)*\{\{END\}\}/',$repetition, $skin);
		}
	}
	return ;
}

//위젯 스킨불러오기
function getEventWidgetSkin($widget, $skinName = ''){
	global $DB_CONNECT,$table,$date,$my,$r,$s,$m,$g,$d,$c,$mod,$_HH,$_HD,$_HS,$_HM,$_HP,$_CA;
	if(!$skinName) $skinName = 'default';
	if (!is_file($g['wdgcod']) )
	{
		if(is_file($g['path_widget'].'event/'.$widget.'/'.$skinName.'.php')) $skin = file_get_contents($g['path_widget'].'event/'.$widget.'/'.$skinName.'.php');

		$wcss = $g['path_widget'].'event/'.$widget.'/'.$skinName.'.css';
		$wjsc = $g['path_widget'].'event/'.$widget.'/'.$skinName.'.js';
		if (is_file($wcss)) $g['widget_cssjs'] .= '<link href="'.$g['s'].'/widgets/'.$widget.'/'.$skinName.'.css" rel="stylesheet">'."\n";
		if (is_file($wjsc)) $g['widget_cssjs'] .= '<script src="'.$g['s'].'/widgets/'.$widget.'/'.$skinName.'.js"></script>'."\n";
	}

	return array($skin, $g['widget_cssjs']);
}


//셀렉트 박스 만드는 함수
function selectBox($_array, $_selected_value , $name, $type=0, $defult=1, $subOption=''){
	echo "<select class=' form-control' name='$name' id='$name' {$subOption}>";
	if($defult){
		echo "<option value=''> - 선택 - </option>";
	}
	foreach($_array as $key => $value){
		$selected ='';
		if( $key == $_selected_value){
			$selected = "selected";
		}
		echo "<option value='$key' $selected >";
		if($type == 1){
			echo '['.$key.'] '.$value;
		}else{
			echo $value;
		}
		echo "</option>";
	}
	echo "</select>";
}
?>