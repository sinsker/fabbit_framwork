<? 

//싱글톤 인터페이스
interface Singleton{
	public static function getInstance(); //인스턴스 가져오기
}

//위젯 인터페이스
interface  widget {
	public static function load($type, $var); //모듈 보여주기
	public function update(); //위젯 업데이트
}

//데이터 처리 인터페이스
interface DataAccess {
	public function setData($data); //데이터 저장
	public function getData(); //데이터 불러오기
}

interface Container {
	
	
	
}

?>