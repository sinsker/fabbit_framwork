<?
// 이 상수가 정의되지 않으면 각각의 개별 페이지는 별도로 실행될 수 없음
define('SIN', TRUE);


if (function_exists("date_default_timezone_set"))
    date_default_timezone_set("Asia/Seoul");

// 자주 사용하는 값
// 서버의 시간과 실제 사용하는 시간이 틀린 경우 수정하세요.
// 하루는 86400 초입니다. 1시간은 3600초
// 6시간이 빠른 경우 time() + (3600 * 6);
// 6시간이 느린 경우 time() - (3600 * 6);
define('TIME', time());
define('TIME_YMD',  date("Y-m-d",TIME));
define('TIME_HIS',  date("H:i:s",TIME) );
define('TIME_FULL', date("Y-m-d H:i:s",TIME) );
define('ACCESS_IP', $_SERVER['REMOTE_ADDR'] );
define('ACCESS_AGENT', $_SERVER['HTTP_USER_AGENT'] );

//HTML 경로 설정
/* 중요  */
define('SYNC','/sync');
define('ROOT', SYNC);
define('RES_PATH',		ROOT.'/resources');
define('CONTENT_PATH',	ROOT. '/contents');
define('SKIN_PATH',		CONTENT_PATH.'/skin');
define('MODULE_PATH',	CONTENT_PATH.'/module');
define('WIDGET_PATH',	CONTENT_PATH.'/widget');
define('TEMPLATE_PATH',	CONTENT_PATH.'/template');

define('ADMIN_PATH',	ROOT. '/admin');
define('ACTION_PATH',	ROOT. '/action');

//HTML 파일 이름 설정
define('GNB',	    'gnb.php' );

//HTML 리소스
define('RES_PATH',		ROOT.'/resources');
define('IMG_PATH',		RES_PATH.'/images');
define('JS_PATH',		RES_PATH.'/js');
define('CSS_PATH',		RES_PATH.'/css');


// 기타
// www.sir.co.kr 과 sir.co.kr 도메인은 서로 다른 도메인으로 인식합니다. 쿠키를 공유하려면 .sir.co.kr 과 같이 입력하세요.
// 이곳에 입력이 없다면 www 붙은 도메인과 그렇지 않은 도메인은 쿠키를 공유하지 않으므로 로그인이 풀릴 수 있습니다.
define('COOKIE_DOMAIN',$_SERVER['HTTP_HOST']);


define('CHAR_SET', 'utf-8');
// config.php 가 있는곳의 웹경로. 뒤에 / 를 붙이지 마세요.

define('URL', 'http://'.$_SERVER['HTTP_HOST']);
define('HTTPS_URL','https://'.$_SERVER['HTTP_HOST']);

define('QUERY_STRING', $_SERVER['QUERY_STRING']);

define('G4_HTML_PURIFIER', false);


#관리자 그룹
$admin_group = array(
		"1" => '사역자',
		"3" => '간사님',
		"4" => '목사님',
		"9" => '일반',
);

#싱크 인원 그룹
$member_group = array(
		"1" => '1공동체',
		"2" => '2공동체',
		"6" => '간사',
		"3" => '목사님',
		"4" => '부감님',
		"5" => '장로님',
);

$module_var = array(
	'title' => array('text'=>'텍스트', 'animation'=>'애니메이션 종류', 'bg_color'=> '배경 색상', 'font_color'=> '글자 색상', 'font_size'=>'폰트 크기'),
	'slide' => array('image'=>'이미지', 'text'=>'텍스트')
)


?>