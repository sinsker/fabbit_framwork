<?
$gnb = array(
		'member' => array(
				'index'		=> array('parent'=>	'SYNC 정보','name'	=>	'인원 리스트','order'	=>	'10000','url'	=>	'?f=member'),
				'mview' 	=> array('parent'=>	'SYNC 정보','name'	=>	'신상 정보','order'	=>	'10002','url'	=>	'?f=member'),
				'group' 	=> array('parent'=>	'SYNC 정보','name'	=>	'가정 관리','order'	=>	'10001','url'	=>	'?f=member&v=group'),
				'gedit' 	=> array('parent'=>	'SYNC 정보','name'	=>	'가정 등록','order'	=>	'10001','url'	=>	'?f=member&v=group'),
				'team'  	=> array('parent'=>	'SYNC 정보','name'	=>	'팀 관리','order'	=>	'10002','url'	=>	'?f=member&v=team'),
				'tdetail'	=> array('parent'=>	'SYNC 정보','name'	=>	'팀 상세보기','order'	=>	'10002','url'	=>	'?f=member&v=team'),
		),
		'notes' => array(
				'index' => array( 'name'	=>	'회의록', 'order'	=>	'20000','url'	=>	'?f=notes'),
		),
		'calendar' => array(
				'index' => array( 'name' =>	'행사 및 기념일 안내', 'order'	=>	'30000','url'	=>	'?f=calendar'),
		),
		'chart' => array(
				'index' => array( 'name'	=>	'전체 차트', 'order'=>	'40000','url'	=>	'?f=chart'),
		),
		'page' => array(
				'module'	=> array('parent'=>	'페이지 관리','name'	=>	'페이지 관리',	'order'	=>	'50000','url'	=>	'?f=page&v=module'),
				'index'		=> array('parent'=>	'페이지 관리','name'	=>	'접근 통계',	'order'	=>	'50001','url'	=>	'?f=page'),
				'mdetail' 	=> array('parent'=>	'페이지 관리','name'	=>	'페이지 상세보기',	'order'	=>	'50001','url'	=>	'?f=page&v=module')
		),
		'file' => array(
				'index'		=> array('parent'=>	'파일 매니저','name'	=>	'이미지 관리',	'order'	=>	'50001','url'	=>	'?f=file'),
				'file' 	=> array('parent'=>	'파일 매니저','name'	=>	'파일 관리',		'order'	=>	'50001','url'	=>	'?f=file&v=file')
		),
		'manager' => array(
				'index'	=> array('parent'=>	'최고관리자','name'	=>	'관리자 목록',	'order'	=>	'50001','url'	=>	'?f=manager'),
				'access' 	=> array('parent'=>	'최고관리자','name'	=>	'접근 기록',	'order'	=>	'50001','url'	=>	'?f=manager&v=access')
		),
		'query' => array(
				'index'		=> array('name'	=>	'개발자 모드',	'order'	=>	'50001','url'	=>	'?f=query')
		),
	)















?>