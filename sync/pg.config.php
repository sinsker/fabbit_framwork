<?
# $pg['f'] : 폴더
# $pg['v']  :폴더 > 파일
# $pg['d']  :폴더 > 파일 > 분기점 

$pg['f'] = stripcslashes(strip_tags(trim($_GET['f'])));
$pg['v'] = stripcslashes(strip_tags(trim($_GET['v'])));
$pg['d'] = stripcslashes(strip_tags(trim($_GET['d'])));
 

#================일반 페이지=====================================
if(empty($pg['f'])) $pg['f'] = "main"; 					


#================관리자 페이지==================================
if(defined('IS_ADMIN')){
	if(empty($pg['f'])) $pg['f'] = "main";  //기본 페이지
	if($_SESSION['is_use'] == 'N'){  $pg['f'] = 'auth'; $pg['v']  = 'dismiss'; } // 로그인 후 승인처리 되지 않았을경우
	if(empty($_SESSION['auth_id'])){ $pg['f'] = 'auth'; $pg['v']  = 'index';} //로그인 하지 않았을 경우
}
#===============================================================


#================공통=====================================
# $pg['d']
if(empty($pg['v'])) $pg['v'] = "index"; #일반 페이지일 경우
if($pg['d']){ $pg['v'] = $pg['v'].'.'.$pg['d']; } 


?>