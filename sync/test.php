<?php

spl_autoload_register('autoload');


function autoload($className) {
	$className = ltrim($className, '\\');
	$fileName = '';
	$namespace = '';
	if($lastNsPos = strpos($className, '\\')) {
		$namespace = substr($className, 0, $lastNsPos);
		$className = substr($className, $lastNsPos + 1);
		$fileName = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
	}
	echo DIRECTORY_SEPARATOR;
	$fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.class.php';
	require $fileName;
}


$a = new Core\A;
#$a = new \Core\B\test;