<?
namespace database;

use core\module\shape\DatabaseInterface;

Class Basic implements DatabaseInterface{
	
	private $result;
	private $mysqli; //mysqli 객체
	
	
	public function __construct($dbConfig = ''){
		if(!empty($dbConfig)){
			$this->connect($dbConfig);
		}
	}
	
	#Override
	public function connect($dbConfig){
		$_mysqli = new \mysqli($dbConfig['host'], $dbConfig['id'], $dbConfig['password'], $dbConfig['name']);
		if ($_mysqli->connect_error) {
			die('Error : ('. $_mysqli->connect_errno .') '. $_mysqli->connect_error );
		}
		$this->mysqli = $_mysqli;
	}
	
	#Override
	public function disconnect(){
		if(!empty($this->mysqli)){
			$this->mysqli->close();
		}
	}
	
	#Override
	public function query($sql, $file = '',$line = ''){
		$db = $this->mysqli;
		$this->result = $db->query($sql);
		if(!$this->result ){ echo $db->error; }
		
		return $this->result;
	}
	
	#Override
	public function fetch($result = ''){
		if($result == ''){
			$result = $this->result;			
		}
		return $result->fetch_array();
		
	}
	
	#Override
	public function insert_id(){
		return $this->mysqli->insert_id;
	}

	
	
}

?>