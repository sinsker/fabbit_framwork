//2016.12.07
//SNS 관련 API 모음
var SNS = (function(SNS, $){
	
	var option = {
		facebook : {
			use : false
		},
		kakaotalk: {
			use : false
		},
		insta: {
			use : false
		},
		naver: {
			use : false
		}
	};
	
	var facebook = function(){
		
		init();
		
		window.fbAsyncInit = function() {
			FB.init({
				appId      : SNS.options.facebook.appId,
				version    : SNS.options.facebook.version,
				xfbml      : true,
				status     : true,
				cookie     : true
			});
			
			FB.getLoginStatus();
		};
		
		function init(){
			
			(function(d, s, id){
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) {return;}
				js = d.createElement(s); js.id = id;
				js.src = "//connect.facebook.net/en_KR/sdk.js";
				fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
			
			//버튼 액션 추가하기
			$('.btn-facebook').click(loginAction);
			$('.btn-facebook-share').click(shareAction);
		}
		
		function loginAction(){
			var loginResultAction = $(this).data('login');
			var callback = $(this).data('callback');
			
			FB.login(function(logresponse){
			   var fbname;  
			   var accessToken = logresponse.authResponse.accessToken; 
			   if (logresponse.status === 'connected') {
				   FB.api('/me', function(response) {
					   
						if(loginResultAction){
							$.post( loginResultAction, { name: response.name, id: response.id })
							.done(function( data ) {
								if(callback !== undefined && typeof eval(callback) == 'function' ){
									eval(callback)(data);
								}
							});
						}
				     });
			   }
				  
			}, {scope: 'public_profile,email'});
		}
		
		
		function shareAction(){
			
			var url = $(this).data('url');
			var share = window.open('http://www.facebook.com/sharer.php?u='+encodeURIComponent(url)+'&t='+encodeURIComponent(url),'facebook_share_dialog','toolbar=0,status=0,width=550,height=436');
			if (share.focus) {
				share.focus();
			}
		}
		
	}
	
	var kakaotalk = function(){
		
	}
	
	var naver = function(){
		
		
		(function(d, s, id){
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) {return;}
			js = d.createElement(s); js.id = id;
			js.src = "//static.nid.naver.com/js/naverLogin_implicit-1.0.2.js";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'naverLogin_implicit'));
		
		
		var naver_id_login = new naver_id_login("YOUR_CLIENT_ID", "YOUR_CALLBACK_URL");
	  	var state = naver_id_login.getUniqState();
	  	naver_id_login.setButton("white", 2,40);
	  	naver_id_login.setDomain("YOUR_SERVICE_URL");
	  	naver_id_login.setState(state);
	  	naver_id_login.setPopup();
	  	naver_id_login.init_naver_id_login();
		
		
		
	}
	
	
	SNS.init = function(options){
		SNS.options = $.extend({}, option, options);
		
		if( SNS.options.facebook.use === true){
			facebook();
		}
		
		if( SNS.options.naver.use === true){
			naver();
		}
	}
	
	return SNS;
	
})(window.SNS = window.SNS || {}, jQuery);