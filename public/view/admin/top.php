<header class="navbar">
        <div class="container-fluid">
            <button class="navbar-toggler mobile-toggler hidden-lg-up" type="button">☰</button>
            <a class="navbar-brand" href="#;"></a>
            <ul class="nav navbar-nav hidden-md-down">
                <li class="nav-item">
                    <a class="nav-link navbar-toggler layout-toggler" href="#;">☰</a>
                </li>
               <!--  <li class="nav-item px-1">
                    <a class="nav-link" href="#;">Dashboard</a>
                </li>
                <li class="nav-item px-1">
                    <a class="nav-link" href="#;">Users</a>
                </li>
                <li class="nav-item px-1">
                    <a class="nav-link" href="#;">Settings</a>
                </li> -->
            </ul>
            <ul class="nav navbar-nav float-xs-right hidden-md-down">
                <li class="nav-item">
                    <a class="nav-link" href="#;"><i class="icon-bell"></i><span class="tag tag-pill tag-danger">5</span></a>
                </li>
               <!--  <li class="nav-item">
                    <a class="nav-link" href="#;"><i class="icon-list"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#;"><i class="icon-location-pin"></i></a>
                </li> -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#;" role="button" aria-haspopup="true" aria-expanded="false">
                        (이미지)
                        <span class="hidden-md-down">접속자이름</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">

                        <div class="dropdown-header text-xs-center">
                            <strong>Account</strong>
                        </div>

                        <!-- <a class="dropdown-item" href="#;"><i class="fa fa-bell-o"></i> Updates<span class="tag tag-info">42</span></a> -->
                        <a class="dropdown-item" href="#;"><i class="fa fa-envelope-o"></i>쪽지<span class="tag tag-success">42</span></a>
                        <a class="dropdown-item" href="#;"><i class="fa fa-comments"></i>댓글<span class="tag tag-warning">42</span></a>

                        <div class="dropdown-header text-xs-center">
                            <strong>설정</strong>
                        </div>
                      <!--   <a class="dropdown-item" href="#;"><i class="fa fa-user"></i> Profile</a>
                        <a class="dropdown-item" href="#;"><i class="fa fa-wrench"></i> Settings</a>
                        <a class="dropdown-item" href="#;"><i class="fa fa-usd"></i> Payments<span class="tag tag-default">42</span></a>
                        <a class="dropdown-item" href="#;"><i class="fa fa-file"></i> Projects<span class="tag tag-primary">42</span></a> -->
                        <div class="divider"></div>
                        <a class="dropdown-item" href="#;"><i class="fa fa-shield"></i>회원정보</a>
                        <a class="dropdown-item" href="#;"><i class="fa fa-lock"></i>로그아웃</a>
                    </div>
                </li>
            </ul>
        </div>
    </header>