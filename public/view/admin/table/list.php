<div class="row">
	<div class="col-sm-12 col-lg-12">
		<div class="card">
			<div class="card-header">
				<i class="fa fa-align-justify"></i>테이블 리스트
				<div class="float-xs-right mb-0">
					<? if($tables){ ?>
		     		<? foreach($tables as $key => $val){?>
					<a href="/admin/database/<?=$val?>" class="btn btn-outline-primary" style="display: inline;"><i class="fa fa-star"></i>&nbsp;<?=$val?></a>
					<? } ?>
					<? } ?>
                </div>
			</div>
			<br>
			
			<div class="card-block">
		     	<table class="table">
		     		<? if($tableColumns){?>
		     		<? foreach($tableColumns as $key => $val){?>
		     		<td >
	     				<?=$val['Field']?>
	     			</td>
		     		<? } ?>
		     		<? } ?>
		     		<? if($tableDetails){?>
		     		<? foreach($tableDetails as $key => $val){?>
		     		<tr>
		     			<? foreach($val as $k => $v){?>
		     			<? if(is_numeric($k)){?>
		     			<td >
		     				<?=$v?>
		     			</td>
		     			<?}?>
		     			<?}?>
		     		</tr>
		     		<? } ?>
		     		<? } ?>
				</table>
			</div>
	     </div>
	</div>
</div>
