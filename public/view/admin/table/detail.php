<div class="row">
	<div class="col-sm-12 col-lg-12">
		<div class="card">
			<div class="card-header">
				<i class="fa fa-align-justify"></i>테이블 리스트
			</div>
			<br>
			
			<div class="card-block">
		     	<table class="table">
		     		<colgroup>
			     		<col width="10%">
			     		<col width="20%">
			     		<col width="10%">
			     		<col width="20%">
		     		</colgroup>
		     		<tr>
		     			<td class="text-center">테이블 제목</td>
		     			<td class="text-center">관리</td>
		     		</tr>
		     		<? if($tables){ ?>
		     		<? foreach($tables as $key => $val){?>
		     		<tr>
		     			<td><?=$val[0]?></td>
		     			<td id=<?=$val[0]?>>
		     				<a class="btn btn-secondary" href="/admin/bbs/set/<?=$val['idx']?>" class="btn btn-default">자세히보기</a>
		     			</td>
		     		</tr>
		     		<? } ?>
		     		<? } ?>
				</table>
			</div>
	     </div>
	</div>
</div>
