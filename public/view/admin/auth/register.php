<style>
	.swiper-container {
     .swiper-container {
        width: 100%;
        height: 100%;
    }
    .swiper-slide {
        text-align: center;
        font-size: 18px;
        background: #fff;
        /* Center slide text vertically */
        display: -webkit-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        -webkit-justify-content: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        -webkit-align-items: center;
        align-items: center;
    }
    .swiper-container-v {
        background: #eee;
    }
</style>
<div class="container d-table">
        <div class="d-100vh-va-middle">
            <div class="row">
                <div class="col-md-8 offset-md-2 ">
                    <div class="card mx-2">
                        <div class="card-block p-2">
                            <h1>회원가입</h1>
                            <form action="/auth/addUser" method="post" >
	                            <p class="text-muted">Create your account</p>
	                            <div class="input-group mb-1">
	                                <span class="input-group-addon"><i class="icon-user"></i>
	                                </span>
	                                <input type="text" name="id" class="form-control" placeholder="아이디">
	                            </div>
	                            <div class="input-group mb-1">
	                                <span class="input-group-addon"><i class="icon-phone"></i></span>
	                                <input type="text" name="tel" class="form-control" placeholder="전화번호">
	                            </div>
	
	                            <div class="input-group mb-1">
	                                <span class="input-group-addon"><i class="icon-lock"></i>
	                                </span>
	                                <input type="password" name="password" class="form-control" placeholder="비밀번호[8자이상]">
	                            </div>
	
	                            <div class="input-group mb-2">
	                                <span class="input-group-addon"><i class="icon-lock"></i>
	                                </span>
	                                <input type="password" name="password_ok" class="form-control" placeholder="비밀번호 재확인">
	                            </div>
	
	                            <button type="submit" class="btn btn-block btn-primary">등록하기</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
