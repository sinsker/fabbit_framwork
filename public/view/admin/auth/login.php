<div class="container d-table">
        <div class="d-100vh-va-middle">
            <div class="row">
                <div class="col-md-8 offset-md-2 ">
                    <div class="card-group">
                        <div class="card p-2">
                            <div class="card-block">
                                <h1>로그인</h1>
                                <p class="text-muted btn btn-link px-0"><a href="/auth/register">아직 회원이 아니신가요?</a></p>
                                <form id="login_form" name="login_form" action="/auth/loginResult" method="post" >
                                	<input type="hidden" name="type" id="type" value="json">
	                                <div class="input-group mb-1">
	                                    <span class="input-group-addon"><i class="icon-user"></i>
	                                    </span>
	                                    <input type="text" name="id" id="id" class="form-control" placeholder="아이디">
	                                </div>
	                                <div class="input-group mb-2">
	                                    <span class="input-group-addon"><i class="icon-lock"></i>
	                                    </span>
	                                    <input type="password" name="password" id="password" class="form-control" placeholder="비밀번호">
	                                </div>
	                                <div class="row">
	                                    <div class="col-xs-8">
	                                    	<button class="btn btn-block btn-facebook" type="button" data-login="/auth/facebookLoginResult" data-callback="login_callback" >
		                                        <span>페이스북 로그인</span>
		                                    </button>
	                                    </div>
	                                    <div class="col-xs-4">
	                                    	<button class="btn btn-primary px-2 btn-login" type="button" style="width: 100%;">로그인</button>
	                                    </div>
	                                </div>
								</form>
                        	</div>
	                        <div id="naver_id_login"></div>
                        </div>
                        <div class="card card-inverse card-primary py-3 hidden-md-down" style="width:44%">
                            <div class="card-block text-xs-center">
                                <div>
                                    <h2>Fabbit</h2>
                                    <p>저렴한 비용으로 시작부터 종료까지 </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script src="/public/resource/js/sns.js"></script>
<script>
	SNS.init({
		facebook : {
			appId : '679998585435148',
			version : 'v2.4',
			use : true 
		}
	});
	
	function login_callback(data) {
		location.href = '/admin'; 
	}
	
	$('.btn-login').click(function(){
		var id		 = $('#id').val();
		var password = $('#password').val();
	
		$.post( "/auth/loginResult", $("#login_form").serialize(),function(data){
			if(data.msg)	alert(data.msg);
			if(data.result) login_callback();
		},'json')
	});
	
</script>
