<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <strong>게시글</strong> 작성
        </div>
        <div class="card-block">
            <form action="/admin/board/<?=$tableName?>/update" name="procForm" method="post" enctype="multipart/form-data" class="form-horizontal ">
            	<input type="hidden" name="table" value = <?=$board['table']?> >
                <div class="form-group row">
                    <label class="col-md-3 form-control-label" for="select">Select</label>
                    <div class="col-md-9">
                        <select id="select" name="select" class="form-control" size="1">
                            <option value="">- 선택 -</option>
                            <option value="1">카테고리</option>
                            <option value="2">Option #2</option>
                            <option value="3">Option #3</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3 form-control-label" for="text-input">제목</label>
                    <div class="col-md-9">
                        <input type="text" id="text-input" name="text-input" class="form-control" placeholder="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3 form-control-label" for="textarea-input">내용</label>
                    <div class="col-md-9">
                        <? $widget->get('editor/daum') ?>
                        <!-- <textarea id="textarea-input" name="textarea-input" rows="9" class="form-control" placeholder=""></textarea> -->
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3 form-control-label" for="file-multiple-input">파일</label>
                    <div class="col-md-9">
                        <input type="file" id="file-multiple-input" name="file-multiple-input" multiple="">
                    </div>
                </div>
            </form>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i> 확인</button>
            <button type="reset" class="btn btn-sm btn-danger"><i class="fa fa-ban"></i> 취소</button>
        </div>
    </div>
</div>