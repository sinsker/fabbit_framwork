<div class="row">
	<div class="col-md-12 col-sm-12 col-lg-12">
		<div class="card ">
				<form class="form-horizontal" name = "bbs_set" action="/admin/bbs/update" method="post" >
				<div class="card-header">
					<strong>게시판</strong> 추가하기
				</div>
				 <div class="card-block">
					<div class="col-lg-6">
						  <div class="form-group row">
						    <label for="inputEmail3" class="col-sm-3 control-label">고유키</label>
						    <div class="col-md-9">
						      <? if($board['idx']){ ?>
							      <input type="hidden" class="form-control" name = "bbs_idx" value="<?=$board['idx']?>" >
							      <label for="bbs_idx" class=" control-label"><?=$board['idx']?></label>
						      <? }else{ ?>
						      	  <label for="bbs_idx" class=" control-label">자동 증가값</label>
						      <? }?>
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="inputPassword3" class="col-sm-3 control-label">테이블명(영문)</label>
						    <div class="col-sm-9">
						       <? if($board['table']){ ?>
							      <input type="hidden" class="form-control" name = "bbs_table" value="<?=$board['table']?>" >
							      <label for="bbs_table" class=" control-label"><?=$board['table']?></label>
						      <? }else{ ?>
						      	   <input type="text" class="form-control" name = "bbs_table" placeholder="테이블명" >
						      <? }?>
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="inputPassword3" class="col-sm-3 control-label">제목</label>
						    <div class="col-sm-9">
						      <input type="text" class="form-control" name = "bbs_title" placeholder="제목"  value="<?=$board['title']?>">
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="inputPassword3" class="col-sm-3 control-label">보여줄 게시글수</label>
						    <div class="col-sm-9">
						      <input type="text" class="form-control" name = "bbs_row" placeholder="게시글수"  value="<?=$board['row']?>">
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="inputPassword3" class="col-sm-3 control-label">보여줄 페이징개수</label>
						    <div class="col-sm-9">
						      <input type="text" class="form-control" name = "bbs_block" placeholder="페이징개수" value="<?=$board['block']?>">
						    </div>
						  </div>
			        	<div class="form-group row">
						    <label for="inputPassword3" class="col-sm-3 control-label">카테고리 ( ,로 구분 )</label>
						    <div class="col-sm-9">
						      <input type="text" class="form-control" name = "bbs_cate" placeholder="카테고리"  value="<?=$board['cate']?>">
						    </div>
						  </div>
						  <div class="form-group row">
						   <label for="inputPassword3" class="col-sm-3 control-label"> 코멘트 사용 여부</label>
						    <div class="col-md-9">
							   <div class="checkbox">
							    <label>
							      <input type="checkbox" name = "is_comment" value="ok" <?=$board['is_comment'] ? 'checked': '' ?> >
							    </label>
							  </div>
							</div>
						  </div>
						  <div class="form-group row">
						   <label for="inputPassword3" class="col-sm-3 control-label"> 로그인 여부</label>
						   <div class="col-md-9">
							   <div class="checkbox">
								    <label>
								      <input type="checkbox" name = "is_login" value="ok" <?=$board['is_login'] ? 'checked': '' ?> >
								    </label>
								  </div>
							  </div>
							</div>
			        </div>
				</div>
				<div class="card-footer text-center center">
						<button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i> 저장</button>
						<button type="reset" class="btn btn-sm btn-danger"><i class="fa fa-ban"></i> 뒤로</button>
				</div>
			</form>
			</div>
		</div>
	</div>
</div>
