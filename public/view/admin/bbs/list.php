<div class="row">
	<div class="col-sm-12 col-lg-12">
		<div class="card">
			<div class="card-header">
				<i class="fa fa-align-justify"></i>게시판 리스트
				<div class="float-xs-right mb-0">
					<a href="/admin/bbs/set" class="btn btn-outline-primary" style="display: inline;"><i class="fa fa-star"></i>&nbsp; 추가하기</a>
                </div>
			</div>
			<br>
			
			<div class="card-block">
		     	<table class="table">
		     		<colgroup>
			     		<col width="10%">
			     		<col width="20%">
			     		<col width="10%">
			     		<col width="20%">
		     		</colgroup>
		     		<tr>
		     			<td class="text-center">Key</td>
		     			<td class="text-center">제목</td>
		     			<td class="text-center">생성 날짜</td>
		     			<td class="text-center">관리</td>
		     		</tr>
		     		<? if($boardList){ ?>
		     		<? foreach($boardList as $key => $val){?>
		     		<tr>
		     			<td><?=$val['table']?></td>
		     			<td><?=$val['title']?></td>
		     			<td><?=$val['wdate']?></td>
		     			<td>
		     				
		     				<a class="btn btn-secondary" href="/admin/bbs/set/<?=$val['idx']?>" class="btn btn-default">수정</a>
		     				<a class="btn btn-danger" href="/admin/bbs/del/<?=$val['idx']?>" class="btn btn-default">삭제</a>
		     			</td>
		     		</tr>
		     		<? } ?>
		     		<? } ?>
				</table>
			</div>
	     </div>
	</div>
</div>
