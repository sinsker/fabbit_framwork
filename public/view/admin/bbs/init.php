<div class="container">
      <div class="header">
        <h3 class="text-muted">Fabbit BBS > Init</h3>
      </div>

      <div class="jumbotron">
        <p class="lead">간편한 모듈 방식의 bbs 입니다. 언제든지 편리하게 추가하고 변경하세요.</p>
      </div>

      <div class="row marketing">
		<form class="form-horizontal" name = "bbs_init" action="boardmanager/bbsInit" method="post" >
	        <div class="col-lg-6">
				  <div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label">DB HOST</label>
				    <div class="col-sm-10">
				      <input type="text" class="form-control" name = "bbs_host" placeholder="Host">
				    </div>
				  </div>
				  <div class="form-group">
				    <label for="inputPassword3" class="col-sm-2 control-label">DB NAME</label>
				    <div class="col-sm-10">
				      <input type="text" class="form-control" name = "bbs_dbname" placeholder="Name">
				    </div>
				  </div>
				  <div class="form-group">
				    <label for="inputPassword3" class="col-sm-2 control-label">ID</label>
				    <div class="col-sm-10">
				      <input type="text" class="form-control" name = "bbs_dbid" placeholder="Id">
				    </div>
				  </div>
				  <div class="form-group">
				    <label for="inputPassword3" class="col-sm-2 control-label">PASSWORD</label>
				    <div class="col-sm-10">
				      <input type="text" class="form-control" name = "bbs_dbpassword" placeholder="Password">
				    </div>
				  </div>
	        </div>
	        <div class="col-lg-6">
				  <div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label">PREFIX</label>
				    <div class="col-sm-10">
				      <input type="text" class="form-control" id="" name = "bbs_prefix" placeholder="PREFIX" value ="bbs_" >
				    </div>
				  </div>
				  <div class="form-group">
				    <label for="inputPassword3" class="col-sm-2 control-label">Row</label>
				    <div class="col-sm-10">
				      <input type="text" class="form-control" name = "bbs_row" placeholder="Row" value ="25">
				    </div>
				  </div>
				  <div class="form-group">
				    <label for="inputPassword3" class="col-sm-2 control-label">Block</label>
				    <div class="col-sm-10">
				      <input type="text" class="form-control" name = "bbs_block" placeholder="Block" value ="5">
				    </div>
				  </div>
	      	</div>
	      	<div class="col-lg-12">
		      	<div class="form-group">
				    <div class=" col-sm-10">
				    	<button type="submit" class="btn btn-default">초기값 세팅</button>
				    </div>
				</div>
			</div>
		</form>
    </div>
