
	<? include './public/view/admin/top.php';?>
	<? include './public/view/admin/lab.php';?>
    <!-- Main content -->
    <main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">HOME</li>
            <li class="breadcrumb-item"><a href="#"><?=$_SESSION['id']?></a> </li>
            <li class="breadcrumb-item active">Dashboard</li>
        </ol>
        <div class="container-fluid">
            <div class="animated fadeIn">
            <? $view ? include './public/view/admin/'.$view.'.php'  : ''?>
            </div>
        </div>
        <!-- /.conainer-fluid -->
    </main>

    <footer class="footer">
        <span class="text-left">
            <a href="http://coreui.io">SYNCLOVE</a> © 2016 Fabbit/Sinil
        </span>
        <span class="float-xs-right">
            Powered by <a href="http://coreui.io">CoreUI</a>
        </span>
    </footer>
    <script src="/public/resource/js/chart.min.js"></script>
    <?=pluginImport('bootstrap-admin','app','js')?>
