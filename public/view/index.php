<html>
	<head>
	   
	</head>
	<body>
		<div id ='root'></div>
		<div id ='random'></div>
	</body>
	<script src="https://unpkg.com/react@latest/dist/react.js"></script>
	<script src="https://unpkg.com/react-dom@latest/dist/react-dom.js"></script>
	<script src="https://unpkg.com/babel-standalone@6.15.0/babel.min.js"></script>
	<script type="text/babel">

import React from 'react';
import ReactDOM from 'react-dom';

//App 컴포넌트
class App extends React.Component {
    render(){
        return (
            <Contacts text={this.props.txt}/>
        );
    }
}
 
class Contacts extends React.Component {
    constructor(props) {
        super(props);
		console.log(this.props);
        this.state = {
            contactData: [
                {name: "Abet", phone: "010-0000-0001"},
                {name: "Betty", phone: "010-0000-0002"},
                {name: "Charlie", phone: "010-0000-0003"},
                {name: "David", phone: "010-0000-0004"}
            ]
        };
    }
	
	_insertContact(name,phone){
		let newState = update(this.state,{
			contactData : { $push: [{"name":name, "phone": phone}] }
			});
		this.setState(newState);
	}
	
    render(){
        return(
            <div>
                <h1>Contacts</h1>
                <ul>
                    {this.state.contactData.map((contact, i) => {
                        return (<ContactInfo name={contact.name}
                                            phone={contact.phone}
                                              key={i}/>);
                    })}
                </ul>
				<ContactRemote onInsert={this._insertContact.bind(this)}/>
            </div>
        );
    }
}
 
class ContactInfo extends React.Component {
    render() {
        return(
            <li>{this.props.name} {this.props.phone}</li>
            );
    }
}

class ContactRemote extends React.Component {
	
	constructor(props){
		super(props);
		this.state = {
			name :'',
			phone :''
		}
	}

	handleChange(e){
		var nextState = {};
		nextState[e.target.name] = e.target.value;
		this.setState(nextState);
		console.log(this.state);
	}

	handleInsert(){
		this.props.onInsert(this.state.name,this.state.phone);
		this.setState({
			name : '',
			phone: ''
		});

	}

	render(){
		return(
			<div>
				<input type="text" name="name" value={this.state.name} onChange={this.handleChange.bind(this)}/>
				<input type="text" name="phone" value={this.state.phone} onChange={this.handleChange.bind(this)} />
				<button onClick={this.handleInsert.bind(this)}>입력</button>
			</div>
		);
	}

}


ReactDOM.render(
	<App  txt="우리" />, document.getElementById('root')
);
</script>
	
</html>

