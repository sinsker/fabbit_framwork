<div class="container d-table">
        <div class="d-100vh-va-middle">
            <div class="row">
                <div class="col-xs-12 col-md-6 offset-md-3">
                    <div class="card mx-2">
                        <div class="card-block p-2">
                            <h1>회원가입</h1>
                            <form action="/auth/addUser" method="post" >
	                            <p class="text-muted">Create your account</p>
	                            <div class="input-group mb-1">
	                                <span class="input-group-addon"><i class="icon-user"></i>
	                                </span>
	                                <input type="text" name="id" class="form-control" placeholder="아이디">
	                            </div>
	
	                            <div class="input-group mb-1">
	                                <span class="input-group-addon"><i class="icon-phone"></i></span>
	                                <input type="text" name="tel" class="form-control" placeholder="전화번호">
	                            </div>
	
	                            <div class="input-group mb-1">
	                                <span class="input-group-addon"><i class="icon-lock"></i>
	                                </span>
	                                <input type="password" name="password" class="form-control" placeholder="비밀번호[8자이상]">
	                            </div>
	
	                            <div class="input-group mb-2">
	                                <span class="input-group-addon"><i class="icon-lock"></i>
	                                </span>
	                                <input type="password" name="password_ok" class="form-control" placeholder="비밀번호 재확인">
	                            </div>
	
	                            <button type="submit" class="btn btn-block btn-primary">등록하기</button>
                            </form>
                        </div>
                        <!-- <div class="card-footer p-2">
                            <div class="row">
                                <div class="col-xs-12">
                                    <button class="btn btn-block btn-facebook" type="button">
                                        <span>페이스북 회원가입</span>
                                    </button>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
            <div class="row">
            	<div class="col-md-6 offset-md-3">
	            	<div class="card mx-2">
		            	<div class="main-carousel">
						  <div class="carousel-cell">
						  	<a href="www.naver.com">
						  		<img alt="" 
						  		 data-flickity-lazyload="/public/resource/images/banner/크기변환_K해커톤_웹배너850x314본선.png" >
						  	</a>
						  </div>
						  <div class="carousel-cell">
						  	<a href="www.naver.com">
						  		<img alt=""
						  		data-flickity-lazyload="/public/resource/images/banner/2014_K해커톤_웹배너_620-164.png" >
						  	</a>
						  </div>
						  <div class="carousel-cell">
						  	<a href="www.naver.com">
						  		<img alt="" 
						  		data-flickity-lazyload="/public/resource/images/banner/p19cna9c2o1g341l57ivan1711f.jpg" >
						  	</a>
						  </div>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
    <style>
	.carousel {
	  background: #EEE;
	}
	.carousel-cell {
	  width: 100%;
	  
	  margin-right: 10px;
	  counter-increment: gallery-cell;
	}
	
	/* cell number */
	.carousel-cell:before {
	}
	
	.main-carousel{
		height: 100%;
	}
	
	.flickity-page-dots{
		bottom: 15px;
	}
	
	.carousel-cell img{
		width: 100%;
	} 
	
	</style>
	<script>
	$('.main-carousel').flickity({
		  // options
		  cellAlign: 'center',
		  contain: true,
		  autoPlay: true,
		  prevNextButtons: false,
		  lazyLoad: true
		  
	});
	</script>