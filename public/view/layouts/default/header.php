<!DOCTYPE html>
<html>
	<head>
		<title>Fabbit</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no,target-densitydpi=medium-dpi">
		<meta name="description" content="">
		<meta name="keyword" content="">
		
		<link href="/public/resource/css/normalize.min.css" rel="stylesheet" type="text/css">
		<?=pluginImport('jquery','jquery-ui.min','css')?>
		<?=pluginImport('flickity','flickity.min','css')?>
		<?=pluginImport('bootstrap-admin','font-awesome.min','css')?>
		<?=pluginImport('bootstrap-admin','simple-line-icons','css')?>
		<?=pluginImport('bootstrap-admin','style','css')?>
		<!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js"></script> -->
		<?=pluginImport('jquery','jquery-1.11.3.min','js')?>
		<?=pluginImport('jquery','jquery-ui.min','js')?>
		<?=pluginImport('flickity','flickity.pkgd.min','js')?>
		<?=pluginImport('bootstrap','bootstrap.min','js')?>
	</head>
	<body>
	
