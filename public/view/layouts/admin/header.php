<!DOCTYPE html>
<html>
	<head>
		<title>SyncLove</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		
		<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no,target-densitydpi=medium-dpi">
		<meta name="description" content="">
		<meta name="keyword" content="">
		
		<link href="/public/resource/css/normalize.min.css" rel="stylesheet" type="text/css">
		<?=pluginImport('jquery','jquery-ui.min','css')?>
		<?=pluginImport('bootstrap-admin','font-awesome.min','css')?>
		<?=pluginImport('bootstrap-admin','simple-line-icons','css')?>
		<?=pluginImport('bootstrap-admin','style','css')?>
		
		<?=pluginImport('jquery','jquery-1.11.3.min','js')?>
		<?=pluginImport('jquery','jquery-ui.min','js')?>
		<?=pluginImport('bootstrap','bootstrap.min','js')?>
		<!-- 
		<script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.8.23/browser.min.js"></script>
		<script src="https://fb.me/react-15.0.1.js"></script>
		<script src="https://fb.me/react-dom-15.0.1.js"></script>
		 -->
		 <link rel="shortcut icon" href="./public/resource/images/favicon/favicon.png">
	</head>
	<body class="avbar-fixed fixed-nav sidebar-nav">
	
