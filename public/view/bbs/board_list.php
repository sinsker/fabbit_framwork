<?
$colspan = 5;
?>
<div class = "container"> 
<!-- 게시판 목록 시작 -->
	<table align="center" cellpadding="0" cellspacing="0" class="table" >
		<tr>
			<td>
			    <div class="board_top">
			        <div style="float:left;">
			            <form name="fcategory" method="get" style="margin:0px;">
			                <? if ($is_category) { ?>
			                <select name=sca onchange="location='<?=$category_location?>'+<?=strtolower($g4[charset])=='utf-8' ? "encodeURIComponent(this.value)" : "this.value"?>;">
			                    <option value=''>전체</option>
			                    <?=$category_option?>
			                </select>
			                <? } ?>
			            </form>
			        </div>
			        <div class="pull-right">
			            <i class="icon-list-alt"></i>
			            <span style="color:#888888; font-weight:bold;">Total <?=number_format($paging->rowCount)?></span>
			            <? if ($rss_href) { ?><a class="btn" href='<?=$rss_href?>'>rss</a><?}?>
			            <? if ($admin_href) { ?><a class="btn btn-mini" href="<?=$admin_href?>">관리자</a><?}?>
			        </div>
			    </div>
			
			    <!-- 제목 -->
			    <form name="fboardlist" method="post" >
			        <input type='hidden' name='table' value='<?=$table?>'>
			        <input type='hidden' name='page' value='<?=$page?>'>
							
			        <table class="table table-striped">
				        <colgroup>
				        	<col width="5%">
				        	<col  width="45%">
				        	<col  width="15%">
				        	<col  width="5%">
				        	<col  width="10%">
				        </colgroup>
				        <tr>
				        	<td align="center">#</td>
				        	<td align="center">제목</td>
				        	<td align="center">작성자</td>
				        	<td align="center">Hit</td>
				        	<td align="center">작성날짜</td>
				    	</tr>
						<? foreach ($list as $key => $val){?>
						<tr>
				        	<td align="center"><?=$val['idx']?></td>
				        	<td align="center"><a href="/board/view/<?=$table?>/<?=$val['idx']?>"><?=$val['title']?></a></td>
				        	<td align="center"><?=$val['uname']?></td>
				        	<td align="center"><?=$val['hit']?></td>
				        	<td align="center"><?=$val['mdate']?></td>
				    	</tr>
						<? } ?>
				    	<? if (count($list) == 0) { echo "<tr><td colspan='$colspan' height=100 align=center>게시물이 없습니다.</td></tr>"; } ?>
					</table>
				</form>
			</td>
		</tr>
		<tr>
			<td>
				<div class="center board_page" style="text-align: center;">
				<? $paging->getPageBlockHtml() ?>
				</div>
				<div class="pull-right">
				   <a class="btn btn-primary" href="<?=$w_href?>"><i class="icon-pencil icon-white"></i>글쓰기</a>
				</div>
			</td>
			
		</tr>
	</table>

	<div class="row">
		
		
		<!-- 검색 -->
		<div class="board_search">
		    <form class="form-inline" name="fsearch" method="get">
		        <input type="hidden" name="bo_table" value="<?=$bo_table?>">
		        <input type="hidden" name="sca"      value="<?=$sca?>">
		        <select name="sfl" class="span2 form-control">
		            <option value="wr_subject">제목</option>
		            <option value="wr_content">내용</option>
		            <option value="wr_subject||wr_content">제목+내용</option>
		            <option value="mb_id,1">회원아이디</option>
		            <option value="wr_name,1">글쓴이</option>
		        </select>
		        <input type="text" name="stx" class=" form-control" maxlength="15" itemname="검색어" required value='<?=stripslashes($stx)?>'>
		        <button class="btn btn-default form-control"><i class="icon-search"></i>검색</button>
		    </form>
		</div>
	</div>

</div> 
<!-- 게시판 목록 끝 -->
