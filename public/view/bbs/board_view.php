<div class = "container"> 
        <legend><h3>게시글 보기</h3></legend>
   
	    <div class="control-group">
			<div class="control-label pull-right" for="name"><strong>작성 날짜 :</strong><?=$write['mdate']?></div>
			<div class="control-label" for="name"><strong>작성자 :</strong><?=$write['uname']?></div>
	    </div>
	    <hr>
		<? if($write['cate']){?>
	    <div class="control-group">
	        <label class="control-label" for="">분 류</label>
            <label class="control-label" for="cate"><?=$write['cate']?></label>
	    </div>
	     <hr>
	    <? } ?>
	    <div class="control-group">
            <div class="control-label" for="cate"><?=$write['title']?></div>
	    </div>
	     <hr>
	    <div class="control-group">
			<div class="bs-callout bs-callout-warning" id="callout-buttons-color-accessibility" style="border: 1px solid #928a8a; padding: 5px;border-radius: 5px; min-height: 200px;">
				<?=$write['content']?>
 			 </div>
	    </div>
	     <hr>
	   <div class="control-group text-center">
			<a class="btn btn-default" id="btn_list" href="//board/get/<?=$table?>">목록</a>
		</div>
</div>

