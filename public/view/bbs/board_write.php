
<?=$editor->view('css')?>
<div class = "container"> 
<form class="form-horizontal" name="fwrite" id="fwrite" method="post"  action="/board/proc"  onsubmit ="return fsubmit();' enctype="multipart/form-data" style=" margin:0 auto">
    <input type=hidden name="table" value="<?=$table?>">
    <fieldset>
        <legend><h3><?=$title?> 게시글 작성</h3></legend>
   
	    <div class="control-group">
	        <label class="control-label" for="name">이 름</label>
	        <div class="controls">
	            <input type="text" id="name" class="form-control" maxlength=20 size=15 name=name itemname="이름"  value="<?=$name?>">
	        </div>
	    </div>
	
	    <div class="control-group">
	        <label class="control-label" for="">분 류</label>
	        <div class="controls">
	            <select name=ca_name class="form-control"  itemname="분류">
	            	<option value="">선택하세요<?=$category_option?>
	            </select>
	        </div>
	    </div>
	
	    <div class="control-group">
	        <label class="control-label" for="wr_subject">제 목</label>
	        <div class="controls">
	            <input type="text" style="width:100%;" name="title" id="title" class="form-control" itemname="제목"  value="<?=$subject?>">
	        </div>
	    </div>
	    
		<div class="control-group">
	    </div>
	    
	    <div class="control-group">
	        <label class="control-label" for="wr_subject">내 용</label>
	
	        <div class="controls">
	        	<?=$editor->get('editor/daum')?>
	        </div>
	    </div>
	    
	   <div class="control-group">
	   		<label class="control-label" ></label>
			<div class="controls">
				<submit class="btn btn-primary" id="btn_submit"  ><i class="icon-pencil icon-white"></i>글쓰기</submit>
				<a class="btn" id="btn_list" href="/board/get/<?=$table?>">목록</a>
			</div>
		</div>

    </fieldset>
</form>
</div>
<script>

	$("#btn_submit").on('click',function(){
		
		if($("#name").val() == ''){
			alert('이름을 입력해 주세요.');
			return false;
		}
			
		if($("#title").val() == ''){ 
			alert('제목을 입력해 주세요.')
			return false;
		}

		saveContent();

	});
</script> 
<?=$editor->view('js')?>

