<!DOCTYPE html>
<html>
    <head>
		<title>Fabbit</title>
		<link href="/app/public/resource/css/bootstrap.min.css" rel="stylesheet">
		<link href="/app/public/resource/css/jquery-ui.min.css" rel="stylesheet">
		<link href="/app/public/resource/css/common.css" rel="stylesheet">
		<link href='https://fonts.googleapis.com/css?family=Dosis' rel='stylesheet' type='text/css'>
		<script src="/app/public/resource/js/jquery-1.11.3.min.js"></script>
	    <script src="/app/public/resource/js/jquery-ui.min.js" ></script>
	    <script src="/app/public/resource/js/bootstrap.min.js" ></script>
    </head>
