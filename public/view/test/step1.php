<div class="main-carousel">
  <div class="carousel-cell">{{step1}}</div>
  <div class="carousel-cell">{{step2}}</div>
  <div class="carousel-cell">{{step3}}</div>
</div>
<style>
* { box-sizing: border-box; }
body { font-family: sans-serif; }
.carousel {
  background: #EEE;
}
.carousel-cell {
  width: 100%;
  height: 200px;
  margin-right: 10px;
  background: #8C8;
  counter-increment: gallery-cell;
}

/* cell number */
.carousel-cell:before {
  display: block;
  text-align: center;
  line-height: 200px;
  font-size: 80px;
  color: white;
}

.flickity-page-dots{
	bottom: 15px;
}

</style>
<script>
	$('.main-carousel').flickity({
		  // options
		  cellAlign: 'center',
		  contain: true,
		  autoPlay: true
	});
</script>