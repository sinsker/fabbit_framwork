<?php

namespace lib\opensourceAPI\juso;

use lib\opensourceAPI\API;

/**
 * @author SIEE
 *
 */
class JusoAPI implements API {
	
	private $access_key;
	
	#key setting construct
	public function __construct($access_key = ''){
		$this->access_key = $access_key;
	}
	
	#@Override
	public function request($event, $options = array()){
		if(!method_exists($this, $event)) return;
		return call_user_func_array(array($this,$event),$options);
	}
	
	#Juso API
	public function search($keyword, $currentPage = '1', $countPerPage = '15', $resultType = 'json'){
		
		if(empty($this->access_key)) return false;
		if(empty($keyword)) return false;
		$keyword = urlencode($keyword);
		// OPEN API 호출 URL 정보 설정
		$url = "http://www.juso.go.kr/addrlink/addrLinkApi.do?currentPage=".$currentPage."&countPerPage=".$countPerPage."&keyword=".$keyword."&confmKey=".$this->access_key."&resultType=".$resultType;
		$is_post = false;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, $is_post);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		if($status_code == 200) {
			return $response;
		}else{
			return false;
		}
	}
}

