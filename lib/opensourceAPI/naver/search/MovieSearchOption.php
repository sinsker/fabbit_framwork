<?php
namespace lib\opensourceAPI\naver\search;

class MovieSearchOption implements SearchOption{
	
	const DISPLAY_MIN = 10;
	const DISPLAY_MAX = 100;
	const START_MIN = 1;
	const START_MAX = 1000;
	
	private $options = array(
		'query'   => '',
		'display' => 35,
		'start'	  => 1,
		'genre'	  => '',
		'country' => '',
		'yearfrom'=> '',
		'yearto'  => ''
	);
	
	private $api	= 'https://openapi.naver.com/v1/search/movie.json';
	
	private $COUNTRY_TYPE = array(
		'KR'=> '한국',
		'JP'=> '일본',
		'US'=> '미국',
		'HK'=> '홍콩',
		'GB'=> '영국',
		'FR'=> '프랑스',
		'ETC'=> '기타',
	); 
	
	private $GENRE_TYPE = array(
		1=>'드라마',2=>'판타지',
		3=>'서부',4=>'공포',
		5=>'로맨스', 6=>'모험',
		7=>'스릴러', 8=>'느와르',
		9=>'컬트', 10=>'다큐멘터리',
		11=>'코미디', 12=>'가족',
		13=>'미스터리', 14=>'전쟁',
		15=>'애니메이션', 16=>'범죄',
		17=>'뮤지컬', 18=>'SF',
		19=>'액션', 20=>'무협',
		21=>'에로', 22=>'서스펜스',
		23=>'서사', 24=>'블랙코미디',
		25=>'실험', 26=>'영화카툰',
		27=>'영화음악', 28=>'영화패러디포스터'
	);
	
	public function __construct($options = ''){
		$this->init($options);
	}
	
	public function init($options){
		if(!is_array($options)) return;
		$this->options = array_merge($this->options, $options);
	}
	public function get($type = ''){
		return $this->options[$type] ? $this->options[$type] : '';
	}
	public function makeRequestUrl($type = 'json'){
		
		foreach($this->options as $key => $val){
			if(empty($val)) continue;
			$query .= ($query ? '&' : '').$key.'='.urlencode($val);
		}
		$encText = $query;
		
		$url = $this->api."?".$encText; // json 결과
		
		return $url;
	}
	
}