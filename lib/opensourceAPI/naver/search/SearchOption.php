<?php
namespace lib\opensourceAPI\naver\search;

interface SearchOption{
	
	public function init($options);
	public function get($type = '');
	public function makeRequestUrl($type = 'json');
}