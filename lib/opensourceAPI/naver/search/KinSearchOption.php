<?php
namespace lib\opensourceAPI\naver\search;

class KinSearchOption implements SearchOption{
	
	const DISPLAY_MIN = 10;
	const DISPLAY_MAX = 100;
	const START_MIN = 1;
	const START_MAX = 1000;
	
	private $api	= 'https://openapi.naver.com/v1/search/kin.json';
	
	private $SORT_TYPE = ['sim','date'];
	
	private $options = array(
		'query'   => '',
		'display' => 35,
		'start'	  => 1,
		'sort'	  => 'sim',
	);
	
	public function __construct($options = ''){
		$this->init($options);
	}
	
	public function init($options){
		if(!is_array($options)) return;
		$this->options = array_merge($this->options, $options);
	}
	public function get($type = ''){
		return $this->options[$type] ? $this->options[$type] : '';
	}
	public function makeRequestUrl($type = 'json'){
		
		foreach($this->options as $key => $val){
			if(empty($val)) continue;
			$query .= ($query ? '&' : '').$key.'='.urlencode($val);
		}
		$encText = $query;
		
		$url = $this->api."?".$encText; // json 결과
		
		return $url;
	}
	
}