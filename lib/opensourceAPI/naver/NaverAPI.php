<?php

namespace lib\opensourceAPI\naver;

use lib\opensourceAPI\API;
use lib\opensourceAPI\naver\search\searchOption;

class NaverAPI implements API {
	
	private $client_id = '';
	private $client_secret = '';
	
	
	#key setting construct
	public function __construct($client_id, $client_secret){
		$this->client_id = $client_id;
		$this->client_secret = $client_secret;
	}
	
	
	#@Override
	public function request($event, $options = array()){
		if(!method_exists($this, $event)) return;
		return call_user_func_array(array($this,$event),$options);
	}
	
	
	#Search API
	public function search(searchOption $searchOption){
		
		$client_id		= $this->client_id;
		$client_secret	= $this->client_secret;
		
		$url = $searchOption->makeRequestUrl();
		$is_post = false;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, $is_post);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$headers = array();
		$headers[] = "X-Naver-Client-Id: ".$client_id;
		$headers[] = "X-Naver-Client-Secret: ".$client_secret;
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$response = curl_exec($ch);
		$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close ($ch);
		if($status_code == 200) {
			return json_decode($response);
		}else{
			return false;
		}
		
	}
	
	
}