<?php 

namespace lib;

  class Paging {
  	
          public $PAGE_ROW;     //한페이지에 보여질 row 개수
          public $PAGE_BLOCK;   //한페이지에 보여질 페이지수
         
          public $page;
          public $rowCount;
         
          public $tableName;
          public $startPage;
          public $endPage;
         
          public $totalBlock;
          public $startBlock;
          public $endBlock;
          public $limit;
          public $startNo; 
          
          private $table;
		  private $db;
		  
          /*  생성자 */
        /*   public function Paging($page, $rowCount,  $page_row, $page_block){
               $this->initPageing($page, $rowCount,  $page_row, $page_block);
          } */
         
          /** getter setter **/
          public function getPage(){
               return $this -> page;              
          }
          public function setPage($page){
               $this -> page = $page;
          }
         
          public function initPageing($page, $rowCount = 0, $page_row, $page_block){
               $this -> PAGE_ROW   = $page_row;
               $this -> PAGE_BLOCK = $page_block;
                   
               if($page == null or $page == '')
                    $page = 1;
               $this -> page = $page;
               $this -> rowCount = $rowCount;
                   
               // 한 페이지에 보여질 row //
               $endPage   = ($page * $this -> PAGE_ROW);
               $startPage = ($endPage - $this -> PAGE_ROW) +1;
                   
               $this->endPage = $endPage >= $rowCount ? $rowCount : $endPage ;
               $this->startPage = $startPage;
               // 한 페이지에 보여질 row end//
                   
               $totalBlock = ceil( $this->rowCount / $this->PAGE_ROW );
               $startBlock = floor(($this->page - 1 ) / $this->PAGE_BLOCK) * $this->PAGE_BLOCK + 1;
               $endBlock = ($startBlock + $this->PAGE_BLOCK - 1);
               $endBlock = $totalBlock <= $endBlock ? $totalBlock : $endBlock;
               
               $this -> limit = ($page - 1) * $page_row;
               $this -> totalBlock = $totalBlock;
               $this -> startBlock = $startBlock;
               $this -> endBlock = $endBlock;
               $this -> startNo =  $rowCount - ($page - 1) * $page_row;
          }
         
          public function getRowCount(){
               return $this-> rowCount;
          }
          
          #페이징 HTML
          public function getPageBlockHtml($url = ''){
          	$start = $this->startBlock;
          	$end   = $this->endBlock;
          	$html="";
          	 
          	$html = "<ul class='pagination'>";
          	if($start - 1 != 0){
          		$prev = $start - 1;
          		$html  .= "<li><a href='/app/board/get/kanaph/$prev'>&laquo;</a></li>";
          	}
          	for($i=$start; $i <= $end; $i++){
          		if(!($i == $this->page))
          			$html .="<li><a href='/app/board/get/kanaph/$i'>$i</a></li>";
          		else
          			$html .="<li class='active'><a href='#'>$i</a></li>";
          	}
          	if($end < $this->totalBlock){
          		$next = $end+1;
          		$html  .= "<li><a href='/app/board/get/kanaph/$next'>&raquo;</a></li>";
          	}
          	$html .= "</ul>";
          	
          	echo $html;
          }
         
     }
?>