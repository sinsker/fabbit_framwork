<?php if (!defined('FABBIT')) exit('잘못된 접근입니다.'); 


define('TIME', time());
define('TIME_YMD',  date("Y-m-d",TIME));
define('TIME_HIS',  date("H:i:s",TIME) );
define('TIME_FULL', date("Y-m-d H:i:s",TIME) );
define('IP', $_SERVER['REMOTE_ADDR'] );
define('AGENT', $_SERVER['HTTP_USER_AGENT'] );

define('URL', 'http://'.$_SERVER['HTTP_HOST']);
define('HTTPS_URL','https://'.$_SERVER['HTTP_HOST']);

define('FX', 'fb_');

/* RESOURCE NAMESPACE*/
define('NAMESPACE_DB', 'database\\');
define('NAMESPACE_LIB', 'lib\\');
define('NAMESPACE_PACKAGE', 'package\\');



