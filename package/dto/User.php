<?php
namespace package\dto;

use core\module\Dto;

class User extends Dto {
	
	protected $id;
	protected $password;
	protected $name;
	protected $tel;
	protected $auth;
	
	public function __construct($id = '', $password = '', $name = '', $tel = '', $auth = ''){
		$this->setDto($id, $password , $name, $tel, $auth);
	}
	
	public function setDto($id = '', $password = '', $name = '', $tel = '', $auth = ''){
		$this->id = $id;
		$this->password = $password;
		$this->name = $name;
		$this->tel = $tel;
		$this->auth = $auth;
		
	}
	
	public function getId(){
		return $this->id;
	}
		
	public function getPassword(){
		return $this->password;
	}
		
	public function getName(){
		return $this->name;
	}
		
	public function getTel(){
		return $this->tel;
	}
		
	public function getAuth(){
		return $this->auth;
	}
	
	
}