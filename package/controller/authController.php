<?php
namespace package\controller;

use core\module\View;

//관리자 인증
class AuthController extends \core\module\Controller{
	
	const FACEBOOK_GUEST	= 1;	//페이스북
	const KAKAOTALK_GUEST	= 2;	//카카오톡
	const NAVER_GUEST		= 3;	//네이버
	
	public function index(){
		self::login();
	}
	
	#로그인
	public function login(){
		View::load('admin/auth/login');
	}
	
	#회원가입
	public function register(){
		View::load('admin/auth/register');
	}
	
	#회원가입 완료
	public function addUser(){
		
		$datas = $this->manager->get('param')->post;
		
		if(!$datas['id']) 		alert('아이디를 입력해주시기 바랍니다.');
		if(!$datas['tel']) 		alert('전화번호를 입력해주시기 바랍니다.');
		if(!$datas['password']) alert('패스워드를 입력해주시기 바랍니다.');
		
		$isUser = $this->model('user')->selectUser(array('id'=>$datas['id']));
		
		if($isUser) alert('이미 가입되어 있는 아이디입니다. 다시 확인해주시기 바랍니다.');
		
		$result = $this->model('user')->insertUser($datas);
		
		if($result) View::load('admin/auth/register');
		else View::load('admin/auth/login');
		
	}
	
	
	#로그인 완료
	public function loginResult(){
		$datas = $this->manager->get('param')->post;
	
		if(!$datas['id']) 		alert('아이디를 입력해주시기 바랍니다.');
		if(!$datas['password']) alert('패스워드를 입력해주시기 바랍니다.');
	
		$user = $this->model('user')->selectUser(array('id'=>$datas['id'], 'password'=>$datas['password']));
		if($user){
			$this->model('user')->lastDateUpdate($datas['id']);
			$_SESSION['user'] = $user['id'];
			$result = true;
		}else{
			$result = false;
			$msg = '로그인 정보가 올바르지 않습니다.';
		}
		
		View::jsonData(array('result'=>$result, 'user'=>$_SESSION['user'], 'msg'=>$msg));
	}
	
	#페이스북 로그인 완료 
	public function facebookLoginResult(){
		$datas = $this->manager->get('param')->post;
		
		if(!$datas['id']) 	alert('정상적인 접근이 아닙니다. 다시 시도해주시기 바랍니다.');
		
		$datas['guest'] = self::FACEBOOK_GUEST; //페이스북
		$user = $this->model('user')->selectUser(array('id'=>$datas['id']));
		if(!$user){
			$result = $this->model('user')->insertUser($datas);
			$user['id'] = $datas['id'];
		}
		
		if($user){
			$this->model('user')->lastDateUpdate($datas['id']);
			$_SESSION['user'] = $user['id'];
			$result = true;
		}else{
			$result = false;
			$msg = '로그인 정보가 올바르지 않습니다.';
		}
		
		View::jsonData(array('result'=>$result, 'user'=>$_SESSION['user'], 'msg'=>$msg));
	}
	
}