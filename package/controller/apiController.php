<?php
namespace package\controller;

use core\module\Controller;
use core\module\View;
use lib\opensourceAPI\naver\NaverAPI;
use lib\opensourceAPI\juso\JusoAPI;
use lib\opensourceAPI\naver\search\LocalSearchOption;
use lib\opensourceAPI\naver\search\MovieSearchOption;
use lib\opensourceAPI\naver\search\BlogSearchOption;
use lib\opensourceAPI\naver\search\KinSearchOption;

class ApiController extends Controller{
	
	#주소호출
	public function juso($keyword, $p = 1, $t = 15){
		$jusoAPI = new JusoAPI("U01TX0FVVEgyMDE2MTIxNDIxMTk1NTE3MzY4");
		$keyword = iconv("EUC-KR", "UTF-8", $keyword);
		$datas = $jusoAPI->request('search',['keyword' => $keyword,'currentPage'=>$p,'countPerPage'=>$t]);
		
		echo($datas);
	}
	
	#검색
	public function search($type, $keyword, $p = 1, $t = 15){
		
		$naverAPI = new NaverAPI("Tllm09xZBSwzSjM1IXII","K5v3EGAuAa");
		
		$keyword = iconv("EUC-KR", "UTF-8", $keyword);
		
		switch ($type){
			case 'local':
				$datas = $naverAPI->request('search',[new LocalSearchOption(['query'=> $keyword])]); #지역
				break;
			case 'movie':
				$datas = $naverAPI->request('search',[new MovieSearchOption(['query'=> $keyword])]); #영화
				break;
			case 'blog':
				$datas = $naverAPI->request('search',[new BlogSearchOption(['query'=> $keyword])]);  #블로그
				break;
			case 'kin':
				$datas = $naverAPI->request('search',[new KinSearchOption(['query'=> $keyword])]);   #지식인
				break;
		}
		
		pr($datas->items);
		
	}
	
	
}