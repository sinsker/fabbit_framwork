<?php
namespace package\controller;

use core\module\Controller;
use core\module\Manager;
use core\module\View;

//게시판 관리 컨트롤러
class BoardManagerController extends Controller{
	
	//초기페이지
	public function index(){
		$this->bbsConfigCheck();
		$this->bbsList();
	}
	
	//초기파일 체크
	public function bbsConfigCheck(){
		if(!empty(Manager::call()->get('config')->get('bbs'))){
			return false;
		}
		if($this->manager->get('param')->getFunction() == 'bbsInit'){
			return false;
		}
		View::load('bbs/bbs_init');
		exit;
	}
	
	
	//bbs 초기화 
	public function bbsInit(){
		$result = $this->model('bbs')->init($this->manager->get('param')->post);
		if($result){
			View::load('bbs/bbs_init_result');
		}else{
			$data['msg'] = '잘못된 정보입니다.';
			View::load('bbs/bbs_init',$data);
		}
	}
	
	//bbs 게시판 리스트
	public function bbsList(){
		$this->bbsConfigCheck();
		
		$data['boardList'] = $this->model('bbs')->getBoardList();
		View::load('bbs/bbs_list',$data);
	}
	
	//bbs 게시판 생성/수정 view
	public function edit($idx = ''){
		$this->bbsConfigCheck();
		
		if($idx) {
			$data = $this->model('bbs')->getBoard('idx',$idx);
		}
		
		View::load('bbs/bbs_set',$data);
	}
	
	//bbs 게시판 생성/수정
	public function set(){
		$this->bbsConfigCheck();
		
		if(!$this->param->post['bbs_table']) alert('테이블을 입력하세요.');
		if(!$this->param->post['bbs_title']) alert('제목을 입력하세요.');
		 
		$this->model('bbs')->create($this->param->post);
		
		$this->bbsList();
	}
	
	//bbs 게시판 삭제
	public function delete($idx){
		$this->bbsConfigCheck();
		
		if(!$idx) alert('게시판의 정보가 정확하지 않습니다.');
		$this->model('bbs')->delete($idx);
		
		View::redirect('/app/boardmanager/');
	}
	
		
}