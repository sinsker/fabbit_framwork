<?php
namespace package\controller;

use core\module\Controller;
use core\module\View;
use core\module\Widget;

//게시판 컨트롤러
class BoardController extends Controller{
	
	
	//초기페이지
	public function index($tableName){
		$this->get($tableName);
	}
	
	//리스트 보기
	public function get($table, $page = 1){
		$config = $this->model('bbs')->getBoard('table',$table);
		if(empty($config)){
			alert('해당 게시판은 존재하지 않습니다. 다시 확인해주세요.');
		}
		$board = $this->model('board')->wlist($table, $config, $this->param->get, $page);
		
		$data = array( 
			'table'	 => $table,
			'w_href' =>	'/app/board/write/'.$table,
			'list'	 => $board[0],
			'paging' => $board[1],
		);
		
		View::load('bbs/board_list', $data);
	}
	
	//게시물 보기
	public function view($table, $idx){
		$write = $this->model('board')->getWrite($table, 'idx', $idx);
		if(empty($write)){ alert('해당 게시글이 존재하지 않습니다.');}
		
		$data['write'] = $write;
		$data['table'] = $table;
		View::load('bbs/board_view', $data);
	}
	
	
	//게시물 작성/수정 페이지
	public function write($table, $idx = ''){
		$config = $this->model('bbs')->getBoard('table',$table);
		if(empty($config)){
			alert('해당 게시판은 존재하지 않습니다. 다시 확인해주세요.');
		}
		
		if(!empty($idx)){
			$write = $this->model('board')->getWrite($table, 'idx', $idx);
		}
		
		$data = array(
			'title' => $config['title'],
			'editor'=> Widget::load('editor/daum',['content' => $content,'form'=> 'fwrite']),
			'table' => $table,
			'write'	=> $write
		);
		
		View::load('bbs/board_write', $data);
	}
	
	//게시판 작성/수정
	public function proc(){
		
		$datas = $this->param->post;
		
		if(empty($datas['name']))	alert('이름을 입력해주세요.');
		if(empty($datas['title']))	alert('제목을 입력해주세요.');
		if(empty($datas['content']))alert('내용을 입력해주세요.');
		
		if(empty($datas['idx'])){
			$result = $this->model('board')->insert($datas['table'], $datas);
		}else{
			$write = $this->model('board')->getWrite($datas['table'], $datas['idx']);
			if(empty($write)){ alert('해당 게시글이 존재하지 않습니다.');}
			
			$result = $this->model('board')->update($datas['table'], $datas, $datas['idx']);
		}
		
		if(empty($result)) alert('게시판 작성 실패하였습니다.');
		
		View::redirect('/app/board/view/'.$datas['table'].'/'.$result);
	}
	
	
	//게시물 삭제
	public function del(){
	
	}
	
	
	
		
}