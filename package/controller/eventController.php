<?php
namespace package\controller;

use core\module\Controller;
use core\module\View;

class EventController extends Controller{
	

	public function index(){
		$this->intro();
	}
	
	#기본 페이지
	public function intro(){
		View::load('event/search/intro');
	}
	
}