<?php
namespace package\controller;

use core\module\Controller;
use core\module\View;

//관리자
class AdminController extends Controller{
	
	private $bbsModel;
	
	public function index(){
		self::main();
	}
	
	#Main View
	public function main(){
		if(empty($_SESSION['user'])) View::redirect('/auth/login');
		View::load('admin/index');
	}
	
	#BBS
	public function bbs($action = 'list',$idx = ''){
		if($this->bbsConfigCheck() === false) return;
		
		switch ($action){
			
			case 'list': 
			default:
				$datas['boardList'] = $this->model('bbs')->getBoardList();
				$datas['view'] = 'bbs/list';
				break;
				
			case 'set':
				$board = $this->model('bbs')->getBoard('idx',$idx);
				$datas = ['board'=> $board, 'view' => 'bbs/set'];
				break;
				
			case 'update':
				$this->model('bbs')->create($this->manager->get('param')->post);
				$datas = ['view' => 'bbs/list'];
				View::redirect('/admin/bbs/list');
				break;
			case 'del':
				$this->model('bbs')->delete($idx);
				$datas = ['view' => 'bbs/list'];
				View::redirect('/admin/bbs/list');
				break;
		}
		
		View::load('admin/index',$datas);
	}
	
	#게시판 > ex) admin/board/notice/list/3
	public function board($tableName, $action, $actionData = 1){
		if($this->bbsConfigCheck() === false) return;
		
		switch ($action){
			case 'list':
			default:
				list($list,$pageing) = $this->model('board')->wlist($tableName, ['row'=>'15','block'=>'5'], $where,$actionData);
				$boardConfig = $this->model('board')->getBoardConfig($tableName);
				$datas['boardList'] = $list;
				$datas['boardConfig'] = $boardConfig;
				$datas['paging'] = $pageing;
				
				$datas['view'] = 'bbs/board_list';
				break;
				
			case 'v':
				$board = $this->model('board')->getWrite($tableName,'idx',$actionData);
				$datas['board'] = $board;
				$datas['view'] = 'bbs/board_view';
				break;
				
			case 'w':
				$board = $this->model('board')->getWrite($tableName,'idx',$actionData);
				$datas['view'] = 'bbs/board_write';
				$datas['widget'] = \core\module\Widget::call()->load('editor/daum', array('form' => 'procForm', 'width' => 1050, 'height' => 524, 'content' => $R['que_text']));
				break;
			case 'update':
				pr($this->manager->get('param')->post);
				break;
				
			case 'del':
				$datas['boardList'] = $list;
				$datas['paging'] = $pageing;
				$datas['view'] = 'bbs/board_list';
				break;
		}
	
		View::load('admin/index', $datas);
	}
	
	//BBS Config Check
	public function bbsConfigCheck(){
		return true;
	}
	
	
	#Database
	public function database($table = ''){
		
		$tableList = $this->model('table')->getTableList($ky);
		
		if($table){
			$tableColumns = $this->model('table')->getTableColumns($table);
			$tableDetails = $this->model('table')->getTableDetailList($table, $ky);
		}
		$datas = ['view' => 'table/list','tables'=> $tableList,'tableDetails'=> $tableDetails,'tableColumns'=>$tableColumns];
		View::load('admin/index',$datas);
	}
	
	
}