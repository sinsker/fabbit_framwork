<?php
namespace package\model;

use core\module\Model;

class BbsModel extends Model{

	private $configPath;
	 
	function __construct(){
		parent::__construct();
		
		$this->databaseComposition('Basic');
		$this->configPath = BASE.'/config/bbs'.SUFFIX;
	}
	
	//게시판 리스트 가져오기
	public function getBoardList(){
		require $this->configPath;
		$bbsList = array();
		
		$sql = "select * from {$bbs["bbs_prefix"]}config";
		$result = $this->database->query($sql);
		
		if($result){
			while($row = $this->database->fetch($result)){
				$bbsList[] = $row;
			}
		}
		return $bbsList;
	}
	
	//게시판 정보 가져오기
	public function getBoard($key , $val){
		require $this->configPath;
		
		$sql = "select * from {$bbs["bbs_prefix"]}config where `$key` = '$val'";
		$result = $this->database->query($sql);
		if($result){
			return $this->database->fetch($result);
		}
		
		return false;
	}
	
	
	
	//게시판 속성 테이블 생성
	private function createConfigTable(){
		require $this->configPath;
		
		if(!$this->database->fetch($this->database->query("SHOW TABLES LIKE '{$bbs["bbs_prefix"]}config'"))){
			$sql = "CREATE TABLE `{$bbs["bbs_prefix"]}config` (
			`idx` int(6) NOT NULL AUTO_INCREMENT,
			`table` varchar(35) NOT NULL,
			`title` varchar(100)  NOT NULL,
			`cate` varchar(100)  NOT NULL,
			`row` varchar(6)  NOT NULL,
			`block` varchar(6)  NOT NULL,
			`is_login` int(1)  NOT NULL,
			`is_comment` int(1)  NOT NULL,
			`board_cnt` int(6)  NOT NULL,
			`comment_cnt` int(6)  NOT NULL,
			`access_cnt` int(6)  NOT NULL,
			`mdate` datetime DEFAULT NULL,
			`wdate` datetime DEFAULT NULL,
			PRIMARY KEY (`idx`),
			KEY `table` (`table`)
			) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";
			$this->database->query($sql);
		}
	}
	
	//게시판 테이블 생성
	private function createBoardTable($set){
		require $this->configPath;
		
		if(!$this->database->fetch($this->database->query("SHOW TABLES LIKE '{$bbs["bbs_prefix"]}{$set['bbs_table']}'"))){
			$sql = "CREATE TABLE `{$bbs["bbs_prefix"]}{$set['bbs_table']}` (
			`idx` int(6) NOT NULL AUTO_INCREMENT,
			`pidx` int(6) NULL ,
			`bdept` int(6) NOT NULL ,
			`border` int(6) NOT NULL ,
			`cate` varchar(50) NOT NULL,
			`title` varchar(100)  NOT NULL,
			`content` text  NOT NULL,
			`uname` varchar(50)  NOT NULL,
			`uid` varchar(50)  NOT NULL,
			`good` int(6)  NOT NULL,
			`headline` int(1)  NOT NULL,
			`hit` int(6)  NOT NULL,
			`agent` varchar(100)  NOT NULL,
			`mdate` datetime DEFAULT NULL,
			`wdate` datetime DEFAULT NULL,
			PRIMARY KEY (`idx`),
			KEY `dept` (`bdept`,`border`)
			) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";
			
			return $this->database->query($sql);
		}
		return false;
	}
	
	//게시판 생성
	public function create($set){
		require $this->configPath;
		
		$this->createConfigTable();
		
		$values = array(
			'`table`' 	 => $set['bbs_table'],
			'title'		 => $set['bbs_title'],
			'cate' 		 => $set['bbs_cate'],
			'row'		 => $set['bbs_row'] ?  $set['bbs_row'] : $bbs["bbs_row"],
			'block' 	 => $set['bbs_block'] ? $set['bbs_block'] : $bbs["bbs_block"],
			'is_login'	 => $set['is_login'] ? 1 : 0,
			'is_comment' => $set['is_comment'] ? 1 : 0,
			'mdate'		 => date('Y-m-d H:i:s')
		) ;
		 
		if($set['bbs_idx']){
			$sql = "update {$bbs["bbs_prefix"]}config set ".createWhereText($values, ',')." where idx = ".$set['bbs_idx'];
			return $this->database->query($sql);
		}else{
			$board = $this->getBoard('table', $set['bbs_table']);
			if(empty($board)){
				$values['wdate'] = date('Y-m-d H:i:s');
				$sql = "insert into {$bbs["bbs_prefix"]}config set ".createWhereText($values, ',');
				$this->database->query($sql);
				$this->createBoardTable($set);
			}
			return true;
		}
		return false;
	}
	
	//게시판 삭제
	public function delete($idx){
		require $this->configPath;
		
		$board = $this->getBoard('idx', $idx);
		if($this->database->fetch($this->database->query("SHOW TABLES LIKE '{$bbs["bbs_prefix"]}{$board['bbs_table']}'"))){
			$this->database->query("drop table {$bbs["bbs_prefix"]}{$board["table"]}");
		}
		
		$sql = "delete from {$bbs["bbs_prefix"]}config  where idx = ".$idx;
		return $this->database->query($sql);
	}
	
}