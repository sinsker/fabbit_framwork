<?php
namespace package\model;

use core\module\Model;

class TableModel extends Model {

	 
	function __construct(){
		parent::__construct();
		$this->databaseComposition('Basic');
	}
	
	//테이블 리스트 가져오기
	public function getTableList($keyword = ''){
		$boardList = array();
		
		$sql = "show tables ";
		if($keyword) $sql .= " like '%".$keyword."%'";
		$result = $this->database->query($sql);
		if($result){
			while($row = $this->database->fetch($result)){
				$boardList[] = $row[0];
			}
		}
		return $boardList;
	}
	
	
	//테이블 컬럼 가져오기
	public function getTableColumns($table){
		$columnList = array();
		
		$sql = "show full columns from ".$table;
		$result = $this->database->query($sql);
		if($result){
			while($row = $this->database->fetch($result)){
				$columnList[] = $row;
			}
		}
		return $columnList;
	}
	
	//테이블 가져오기
	public function getTableDetailList($table, $keyword = ''){
		$tableDetailList = array();
	
		$sql = "select * from ".$table;
		$result = $this->database->query($sql);
		if($result){
			while($row = $this->database->fetch($result)){
				$tableDetailList[] = $row;
			}
		}
	
		return $tableDetailList;
	}
	
	
}