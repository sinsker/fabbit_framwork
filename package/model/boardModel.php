<?php
namespace package\model;

use core\module\Model;

class BoardModel extends Model{

	private $configPath;
	 
	function __construct(){
		parent::__construct();
		
		$this->databaseComposition('Basic');
		$this->configPath = BASE.'/config/bbs'.SUFFIX;
	}
	

	//게시판 글 가져오기
	public function getWrite($tableName, $key, $val){
		require $this->configPath;
		
		$sql = "select * from {$bbs["bbs_prefix"]}{$tableName} where `$key` = '$val'";
		$result = $this->database->query($sql);
		if($result) $row = $this->database->fetch($result);
		
		return $row;
	}
	
	//게시판 옵션 가져오기
	public function getBoardConfig($tableName){
		require $this->configPath;
	
		$sql = "select * from {$bbs["bbs_prefix"]}config where `table` = '$tableName'";
		$result = $this->database->query($sql);
		if($result) $row = $this->database->fetch($result);
		else $row = false;
	
		return $row;
	}
	
	//게시판 글 작성
	public function insert($tableName, $param){
		require $this->configPath;
	
		$set = array(
				'title' 	=> 		$param['title'],
				'content' 	=> 		$param['content'],
				'cate' 		=> 		$param['cate'],
				'uname' 	=> 		$param['name'],
				'uid' 		=> 		$param['id'],
				'mdate' 	=> 		TIME_FULL,
				'wdate' 	=> 		TIME_FULL,
		);
		
		$sql = "insert into {$bbs["bbs_prefix"]}{$tableName} set ".createWhereText($set, ',');
		$result = $this->database->query($sql);
		
		return $result ? $this->database->getInsertId() : false;
	}
	
	//게시판 글 업데이트
	public function update($tableName, $idx, $set){
		require $this->configPath;
		
		$set = array(
				'title' 	=> 		$param['title'],
				'content' 	=> 		$param['content'],
				'cate' 		=> 		$param['cate'],
				'uname' 	=> 		$param['name'],
				'uid' 		=> 		$param['id'],
				'mdate' 	=> 		TIME_FULL,
		);
	
		$sql = "update {$bbs["bbs_prefix"]}{$tableName} set ".createWhereText($set, ',')." where idx = ".$idx;
		$result = $this->database->query($sql);
	
		return $result ? $idx : false;
	}
	
	//게시판 리스트
	public function wlist($tableName, $config, $where = '', $page = 1){
		require $this->configPath;
		$boardList = array();
		
		$sql = "select count(*) as count from {$bbs["bbs_prefix"]}{$tableName} ".(!empty($where) ? createWhereText($where, 'and') : '');
		$total = $this->database->fetch( $this->database->query($sql));
		
		$order = ' order by idx desc ';
		
		$paging = new \lib\Paging();
		
		$paging->initPageing($page, $total['count'], $config['row'], $config['block']);
		$limit = " LIMIT {$paging->limit}, {$config['row']}";
		
		$sql = "select * from {$bbs["bbs_prefix"]}{$tableName} ".(!empty($where) ? createWhereText($where, 'and') : '').$order.$limit;
		$result = $this->database->query($sql);
		
		if($result){
			
			while($row = $this->database->fetch($result)){
				$paging->startNo--;
				$boardList[] = $row;
			}
		}
		
		return array($boardList, $paging);
	}
	
	//게시판 글 삭제
	public function delete($tableName, $idx){
		require $this->configPath;
		$sql = "delete from {$bbs["bbs_prefix"]}{$tableName} where idx = $idx";
		return 	$this->database->query($sql);
	}
	
}