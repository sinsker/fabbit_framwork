<?php
namespace package\model;

use core\module\Model;


class UserModel extends Model{
	 
	function __construct(){
		parent::__construct();
		$this->databaseComposition('Basic');
		$this->table = $this->manager->get('config')->get('table');
		$this->createUserTable();
	}
	

	//유저 등록하기
	public function insertUser($datas){
		
		$set = array(
			'id' 		=> $datas['id'],
			'tel' 		=> $datas['tel'],
			'name' 		=> $datas['name'],
			'guest'		=> $datas['guest'] ? $datas['guest'] : 0,
			'password'	=> $datas['password'],
			'address'	=> $datas['address'],
			'mod_date'	=> TIME_FULL,
		);
		
		if($datas['idx']){
			$sql = "UPDATE ".$this->table['member']." SET ".createWhereText($set,' , ')." WHERE idx = ".$datas['idx'];
		}else{
			$set['reg_date'] = TIME_FULL;
			$sql = "insert into ".$this->table['member']." SET ".createWhereText($set,' , ');
		}
		$result = $this->database->query($sql);
		
		return $result ? true : false;
	}
	
	//유저 정보 가져오기
	public function selectUser($datas){
	
		if(empty($datas)) return false;
		
		$sql = "SELECT * FROM ".$this->table['member']." WHERE ".createWhereText($datas,' AND '). " LIMIT 1";
		$result = $this->database->query($sql);
	
		return $this->database->fetch($result);
	}
	
	//로그인유저 마지막 접속일자 업데이트
	public function lastDateUpdate($id){
	
		if(empty($id)) return false;
		
		$sql = "UPDATE ".$this->table['member']." SET last_date = now() WHERE id = '".$id."'";
		$result = $this->database->query($sql);
		return $result ? true : false;
	}
	
	
	//유저 테이블 생성
	private function createUserTable(){
		
		if(!$this->database->fetch($this->database->query("SHOW TABLES LIKE '{$this->table['member']}'"))){
			$sql = "CREATE TABLE `{$this->table['member']}` (
			`idx` int(6) NOT NULL AUTO_INCREMENT,
			`id` varchar(35) NOT NULL,
			`tel` varchar(20)  NOT NULL,
			`email` varchar(100)  NOT NULL,
			`name` varchar(100),
			`password` varchar(100)  NOT NULL,
			`address` varchar(100)  ,
			`guest` tinyint(4) DEFAULT 0,
			`last_date` datetime DEFAULT NULL,
			`mod_date` datetime DEFAULT NULL,
			`reg_date` datetime DEFAULT NULL,
			PRIMARY KEY (`idx`),
			KEY `id` (`id`)
			) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";
			$this->database->query($sql);
		}
	}
	
	
	public function delete($idx){
		require $this->configPath;
		
		$board = $this->getBoard('idx', $idx);
		if($this->database->fetch($this->database->query("SHOW TABLES LIKE '{$bbs["bbs_prefix"]}{$board['bbs_table']}'"))){
			$this->database->query("drop table {$bbs["bbs_prefix"]}{$board["table"]}");
		}
		
		$sql = "delete from {$bbs["bbs_prefix"]}config  where idx = ".$idx;
		return $this->database->query($sql);
	}
	
}